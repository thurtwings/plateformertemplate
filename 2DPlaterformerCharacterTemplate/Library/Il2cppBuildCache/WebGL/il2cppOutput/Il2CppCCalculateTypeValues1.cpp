﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <stdint.h>
#include <limits>



// System.Comparison`1<System.Int32>
struct Comparison_1_t3430355DCDD0AF453788265DC88E9288D19C4E9C;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7;
// System.Collections.Generic.List`1<System.Single>
struct List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA;
// System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>
struct Task_1_t65FD5EE287B61746F015BBC8E90A97D38D258FB3;
// UnityEngine.Vector3[][]
struct Vector3U5BU5DU5BU5D_tAD34024FBDE3F1F720ECB62B24B914A748C60BCB;
// UnityEngine.Color[]
struct ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834;
// AllIn1SpriteShader.DemoCircleExpositor[]
struct DemoCircleExpositorU5BU5D_tF00D5F6A0A10F7E6455A6B0E26C6172DCCD054A6;
// TMPro.TMP_MeshInfo[]
struct TMP_MeshInfoU5BU5D_t6C0A65D18C54B6FA681B2EB0676B83116FD03119;
// UnityEngine.Transform[]
struct TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D;
// TMPro.Examples.VertexJitter/VertexAnim[]
struct VertexAnimU5BU5D_t069E9FCD87DE8FF65BE288FDB2834711D933AAFF;
// System.Action
struct Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6;
// AllIn1SpriteShader.All1ShaderDemoController
struct All1ShaderDemoController_t66C22AC496E9A0D1FF0AB9CDB81DCE2DB60F485B;
// UnityEngine.AnimationCurve
struct AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03;
// UnityEngine.Audio.AudioMixer
struct AudioMixer_t6F1DE6A2F2CF367B61A125CA0F247E66168AE2B5;
// UnityEngine.AudioSource
struct AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B;
// UnityEngine.CanvasGroup
struct CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F;
// System.Globalization.CultureInfo
struct CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98;
// AllIn1SpriteShader.Demo2AutoScroll
struct Demo2AutoScroll_tF05254B25ADADD0CC28CF7DFC84D7709CA5A7BFB;
// AllIn1SpriteShader.DemoCamera
struct DemoCamera_tB715A63ACE2506E72DFEAB96E6BBCA8FC6BD30CB;
// UnityEngine.Font
struct Font_tB53D3F362CB1A0B92307B362826F212AE2D2A6A9;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24;
// System.Runtime.CompilerServices.IAsyncStateMachine
struct IAsyncStateMachine_tAE063F84A60E1058FCA4E3EA9F555D3462641F7D;
// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C;
// UnityEngine.UI.LayoutElement
struct LayoutElement_tE514951184806899FE23EC4FA6112A5F2038CECF;
// UnityEngine.Material
struct Material_t8927C00353A72755313F046D0CE85178AE8218EE;
// UnityEngine.UI.Outline
struct Outline_t37C754965BCC82FDD6C6878357A1439376C61CC2;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// UnityEngine.Renderer
struct Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C;
// UnityEngine.Rigidbody
struct Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5;
// UnityEngine.UI.ScrollRect
struct ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA;
// System.Threading.SendOrPostCallback
struct SendOrPostCallback_t6B7334CE017AF595535507519400AC02D688DC3C;
// DG.Tweening.Sequence
struct Sequence_tE01FFFCA34A537CE2FF32EDAF451CDEC55A1399E;
// UnityEngine.UI.Slider
struct Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF;
// System.String
struct String_t;
// TMPro.TMP_Text
struct TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262;
// TMPro.TMP_TextInfo
struct TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547;
// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1;
// TMPro.Examples.TextMeshProFloatingText
struct TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200;
// UnityEngine.Transform
struct Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1;
// DG.Tweening.Tween
struct Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941;
// TMPro.Examples.VertexColorCycler
struct VertexColorCycler_t37E80E87D8EAD0D7757CDA45BD5D3AA82FD28374;
// TMPro.Examples.VertexJitter
struct VertexJitter_t4BF9FCCB84221E69D9E3727E32E49710F3B4342A;
// TMPro.Examples.VertexShakeA
struct VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A;
// TMPro.Examples.VertexShakeB
struct VertexShakeB_t348FF1466CEF3BFED37C140B7AEFA2EA90D703C9;
// TMPro.Examples.VertexZoom
struct VertexZoom_t02E8994D3A9EB41AEEF1544EF8C93009F6153163;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// System.Threading.WaitCallback
struct WaitCallback_t82C85517E973DCC6390AFB0BC3C2276F3328A319;
// TMPro.Examples.WarpTextExample
struct WarpTextExample_t9CE7000EFB1B8696E32CC86E87E3D0EDEA3D5A96;
// TMPro.Examples.VertexZoom/<>c__DisplayClass10_0
struct U3CU3Ec__DisplayClass10_0_t1AC05F5499B93AC94E9727B8B0F08D8037AD52A6;



IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_t00DDB26693B7BCCD40544DBD546A67E6CCF39740 
{
public:

public:
};


// System.Object


// UnityEngine.CustomYieldInstruction
struct CustomYieldInstruction_t4ED1543FBAA3143362854EB1867B42E5D190A5C7  : public RuntimeObject
{
public:

public:
};


// DG.Tweening.DOTweenCYInstruction
struct DOTweenCYInstruction_t2B136E8CAA516D794B5E725B7694091A39B8E048  : public RuntimeObject
{
public:

public:
};


// DG.Tweening.DOTweenModuleAudio
struct DOTweenModuleAudio_t543BC1904E317A3213F2CD22BEF3065469D62F05  : public RuntimeObject
{
public:

public:
};


// DG.Tweening.DOTweenModulePhysics
struct DOTweenModulePhysics_t414E562FF12685BE1B2F497B49D50F38EC15A144  : public RuntimeObject
{
public:

public:
};


// DG.Tweening.DOTweenModulePhysics2D
struct DOTweenModulePhysics2D_t3CEB85E8EF37E0F4C0F2C88B476D10A32C1E3B6A  : public RuntimeObject
{
public:

public:
};


// DG.Tweening.DOTweenModuleSprite
struct DOTweenModuleSprite_t47F757863D84DF933AF0E3950C69EFD6AA845BA7  : public RuntimeObject
{
public:

public:
};


// DG.Tweening.DOTweenModuleUI
struct DOTweenModuleUI_tB7CCDB438BF13FDE9F6D1821A4209264D15E838C  : public RuntimeObject
{
public:

public:
};


// DG.Tweening.DOTweenModuleUnityVersion
struct DOTweenModuleUnityVersion_tD1A53828BF15F4A43BA32F551C5399900537B543  : public RuntimeObject
{
public:

public:
};


// DG.Tweening.DOTweenModuleUtils
struct DOTweenModuleUtils_tE7556A40C5BD3526261318C1BAB11C05E3C21D22  : public RuntimeObject
{
public:

public:
};

struct DOTweenModuleUtils_tE7556A40C5BD3526261318C1BAB11C05E3C21D22_StaticFields
{
public:
	// System.Boolean DG.Tweening.DOTweenModuleUtils::_initialized
	bool ____initialized_0;

public:
	inline static int32_t get_offset_of__initialized_0() { return static_cast<int32_t>(offsetof(DOTweenModuleUtils_tE7556A40C5BD3526261318C1BAB11C05E3C21D22_StaticFields, ____initialized_0)); }
	inline bool get__initialized_0() const { return ____initialized_0; }
	inline bool* get_address_of__initialized_0() { return &____initialized_0; }
	inline void set__initialized_0(bool value)
	{
		____initialized_0 = value;
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass0_0
struct U3CU3Ec__DisplayClass0_0_tEEA8EFF30417832F4B36CD90E1600F791CC95E65  : public RuntimeObject
{
public:
	// UnityEngine.AudioSource DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass0_0::target
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_tEEA8EFF30417832F4B36CD90E1600F791CC95E65, ___target_0)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_target_0() const { return ___target_0; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass1_0
struct U3CU3Ec__DisplayClass1_0_t2600072C3FD2599A6D31A9ED29A03A22589413C2  : public RuntimeObject
{
public:
	// UnityEngine.AudioSource DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass1_0::target
	AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t2600072C3FD2599A6D31A9ED29A03A22589413C2, ___target_0)); }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * get_target_0() const { return ___target_0; }
	inline AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(AudioSource_tC4BF65AF8CDCAA63724BB3CA59A7A29249269E6B * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass2_0
struct U3CU3Ec__DisplayClass2_0_t0184927D2FE4A42D7384EF3136D50F0672489998  : public RuntimeObject
{
public:
	// UnityEngine.Audio.AudioMixer DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass2_0::target
	AudioMixer_t6F1DE6A2F2CF367B61A125CA0F247E66168AE2B5 * ___target_0;
	// System.String DG.Tweening.DOTweenModuleAudio/<>c__DisplayClass2_0::floatName
	String_t* ___floatName_1;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t0184927D2FE4A42D7384EF3136D50F0672489998, ___target_0)); }
	inline AudioMixer_t6F1DE6A2F2CF367B61A125CA0F247E66168AE2B5 * get_target_0() const { return ___target_0; }
	inline AudioMixer_t6F1DE6A2F2CF367B61A125CA0F247E66168AE2B5 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(AudioMixer_t6F1DE6A2F2CF367B61A125CA0F247E66168AE2B5 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}

	inline static int32_t get_offset_of_floatName_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t0184927D2FE4A42D7384EF3136D50F0672489998, ___floatName_1)); }
	inline String_t* get_floatName_1() const { return ___floatName_1; }
	inline String_t** get_address_of_floatName_1() { return &___floatName_1; }
	inline void set_floatName_1(String_t* value)
	{
		___floatName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___floatName_1), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass0_0
struct U3CU3Ec__DisplayClass0_0_tA1261915464FFE107DD26211EAE30DFB0FDCDE1B  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass0_0::target
	Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_tA1261915464FFE107DD26211EAE30DFB0FDCDE1B, ___target_0)); }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * get_target_0() const { return ___target_0; }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass10_0
struct U3CU3Ec__DisplayClass10_0_tBFDE0479D69A274D55401EF74EE7B80E41D0B9D2  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass10_0::trans
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___trans_0;
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass10_0::target
	Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * ___target_1;

public:
	inline static int32_t get_offset_of_trans_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_tBFDE0479D69A274D55401EF74EE7B80E41D0B9D2, ___trans_0)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_trans_0() const { return ___trans_0; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_trans_0() { return &___trans_0; }
	inline void set_trans_0(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___trans_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trans_0), (void*)value);
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_tBFDE0479D69A274D55401EF74EE7B80E41D0B9D2, ___target_1)); }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * get_target_1() const { return ___target_1; }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_1), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass1_0
struct U3CU3Ec__DisplayClass1_0_t36A04C5BE03417FE15562E97048AE1D99D7BC9DE  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass1_0::target
	Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_t36A04C5BE03417FE15562E97048AE1D99D7BC9DE, ___target_0)); }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * get_target_0() const { return ___target_0; }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass2_0
struct U3CU3Ec__DisplayClass2_0_t8C8461D8370EE781AD60B2927D6F292BA3BC7071  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass2_0::target
	Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t8C8461D8370EE781AD60B2927D6F292BA3BC7071, ___target_0)); }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * get_target_0() const { return ___target_0; }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass3_0
struct U3CU3Ec__DisplayClass3_0_tDE28A654B70E84028A213E50757FB308DF69837D  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass3_0::target
	Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_tDE28A654B70E84028A213E50757FB308DF69837D, ___target_0)); }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * get_target_0() const { return ___target_0; }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass4_0
struct U3CU3Ec__DisplayClass4_0_tE445E3DD7A59CA1401052CB535F005F72C72289C  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass4_0::target
	Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_tE445E3DD7A59CA1401052CB535F005F72C72289C, ___target_0)); }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * get_target_0() const { return ___target_0; }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass5_0
struct U3CU3Ec__DisplayClass5_0_tE26E9B7B440EFE64270AE1ADE6D0038D2139292C  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass5_0::target
	Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_tE26E9B7B440EFE64270AE1ADE6D0038D2139292C, ___target_0)); }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * get_target_0() const { return ___target_0; }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass7_0
struct U3CU3Ec__DisplayClass7_0_t1DBB166C5F76DFD674B98F06853F228D9592F23E  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass7_0::target
	Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t1DBB166C5F76DFD674B98F06853F228D9592F23E, ___target_0)); }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * get_target_0() const { return ___target_0; }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass8_0
struct U3CU3Ec__DisplayClass8_0_t0A77D41DBE9E18F3D0436EC0B00E69D63CE7FC06  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass8_0::trans
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___trans_0;
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass8_0::target
	Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * ___target_1;

public:
	inline static int32_t get_offset_of_trans_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t0A77D41DBE9E18F3D0436EC0B00E69D63CE7FC06, ___trans_0)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_trans_0() const { return ___trans_0; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_trans_0() { return &___trans_0; }
	inline void set_trans_0(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___trans_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trans_0), (void*)value);
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_t0A77D41DBE9E18F3D0436EC0B00E69D63CE7FC06, ___target_1)); }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * get_target_1() const { return ___target_1; }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_1), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass9_0
struct U3CU3Ec__DisplayClass9_0_t7543D38ED6B04C3E6A9F10E9E9B5B2706F106D80  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass9_0::target
	Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_t7543D38ED6B04C3E6A9F10E9E9B5B2706F106D80, ___target_0)); }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * get_target_0() const { return ___target_0; }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass0_0
struct U3CU3Ec__DisplayClass0_0_t59C91F47993C660785686C5B2B9602BD270D0ABC  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass0_0::target
	Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t59C91F47993C660785686C5B2B9602BD270D0ABC, ___target_0)); }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * get_target_0() const { return ___target_0; }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass1_0
struct U3CU3Ec__DisplayClass1_0_tCA5818119C2BD40AFB4FC0E62ECCB340D6A2EF3B  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass1_0::target
	Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_tCA5818119C2BD40AFB4FC0E62ECCB340D6A2EF3B, ___target_0)); }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * get_target_0() const { return ___target_0; }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass2_0
struct U3CU3Ec__DisplayClass2_0_tC52A012574AEBCF260380BE36A20C3D3C511C78C  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass2_0::target
	Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tC52A012574AEBCF260380BE36A20C3D3C511C78C, ___target_0)); }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * get_target_0() const { return ___target_0; }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass3_0
struct U3CU3Ec__DisplayClass3_0_tF43D4712D7D347D917CCC52F81AA37B4140B6221  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass3_0::target
	Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_tF43D4712D7D347D917CCC52F81AA37B4140B6221, ___target_0)); }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * get_target_0() const { return ___target_0; }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass5_0
struct U3CU3Ec__DisplayClass5_0_t22658A4155648993B6A4478E63486F5D32647B83  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass5_0::target
	Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t22658A4155648993B6A4478E63486F5D32647B83, ___target_0)); }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * get_target_0() const { return ___target_0; }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass6_0
struct U3CU3Ec__DisplayClass6_0_tDB1B8BB04DE5BDCA0C021E08F9A56BFD5CBCC1D3  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass6_0::trans
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___trans_0;
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass6_0::target
	Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * ___target_1;

public:
	inline static int32_t get_offset_of_trans_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_tDB1B8BB04DE5BDCA0C021E08F9A56BFD5CBCC1D3, ___trans_0)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_trans_0() const { return ___trans_0; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_trans_0() { return &___trans_0; }
	inline void set_trans_0(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___trans_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trans_0), (void*)value);
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_tDB1B8BB04DE5BDCA0C021E08F9A56BFD5CBCC1D3, ___target_1)); }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * get_target_1() const { return ___target_1; }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_1), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass7_0
struct U3CU3Ec__DisplayClass7_0_tE2CDA7BAA411BB740C07230F68F100B86EA9B8C1  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass7_0::target
	Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_tE2CDA7BAA411BB740C07230F68F100B86EA9B8C1, ___target_0)); }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * get_target_0() const { return ___target_0; }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass8_0
struct U3CU3Ec__DisplayClass8_0_tAA4162D037CD083C95351D51A8B2B6431BC60395  : public RuntimeObject
{
public:
	// UnityEngine.Transform DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass8_0::trans
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___trans_0;
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass8_0::target
	Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * ___target_1;

public:
	inline static int32_t get_offset_of_trans_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tAA4162D037CD083C95351D51A8B2B6431BC60395, ___trans_0)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_trans_0() const { return ___trans_0; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_trans_0() { return &___trans_0; }
	inline void set_trans_0(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___trans_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___trans_0), (void*)value);
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tAA4162D037CD083C95351D51A8B2B6431BC60395, ___target_1)); }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * get_target_1() const { return ___target_1; }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_1), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass0_0
struct U3CU3Ec__DisplayClass0_0_t9BD7D4304FC2CB67490EC66334C5E4AFF9BFBFE9  : public RuntimeObject
{
public:
	// UnityEngine.SpriteRenderer DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass0_0::target
	SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t9BD7D4304FC2CB67490EC66334C5E4AFF9BFBFE9, ___target_0)); }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * get_target_0() const { return ___target_0; }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass1_0
struct U3CU3Ec__DisplayClass1_0_tD6125020F1D1463CCD52E5E31EB2FB2C07BBDC16  : public RuntimeObject
{
public:
	// UnityEngine.SpriteRenderer DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass1_0::target
	SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_tD6125020F1D1463CCD52E5E31EB2FB2C07BBDC16, ___target_0)); }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * get_target_0() const { return ___target_0; }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass0_0
struct U3CU3Ec__DisplayClass0_0_tC2EF009253B71108FB5E721B030B250028AB8DF0  : public RuntimeObject
{
public:
	// UnityEngine.CanvasGroup DG.Tweening.DOTweenModuleUI/<>c__DisplayClass0_0::target
	CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_tC2EF009253B71108FB5E721B030B250028AB8DF0, ___target_0)); }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * get_target_0() const { return ___target_0; }
	inline CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(CanvasGroup_t6912220105AB4A288A2FD882D163D7218EAA577F * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass10_0
struct U3CU3Ec__DisplayClass10_0_t46712DD26915FEF867B065291449A39E1C4389F4  : public RuntimeObject
{
public:
	// UnityEngine.UI.Outline DG.Tweening.DOTweenModuleUI/<>c__DisplayClass10_0::target
	Outline_t37C754965BCC82FDD6C6878357A1439376C61CC2 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t46712DD26915FEF867B065291449A39E1C4389F4, ___target_0)); }
	inline Outline_t37C754965BCC82FDD6C6878357A1439376C61CC2 * get_target_0() const { return ___target_0; }
	inline Outline_t37C754965BCC82FDD6C6878357A1439376C61CC2 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Outline_t37C754965BCC82FDD6C6878357A1439376C61CC2 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass11_0
struct U3CU3Ec__DisplayClass11_0_t7C74139E9D815DD9E5A90F2BD0DBEC3972BA4AA2  : public RuntimeObject
{
public:
	// UnityEngine.UI.Outline DG.Tweening.DOTweenModuleUI/<>c__DisplayClass11_0::target
	Outline_t37C754965BCC82FDD6C6878357A1439376C61CC2 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass11_0_t7C74139E9D815DD9E5A90F2BD0DBEC3972BA4AA2, ___target_0)); }
	inline Outline_t37C754965BCC82FDD6C6878357A1439376C61CC2 * get_target_0() const { return ___target_0; }
	inline Outline_t37C754965BCC82FDD6C6878357A1439376C61CC2 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Outline_t37C754965BCC82FDD6C6878357A1439376C61CC2 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass12_0
struct U3CU3Ec__DisplayClass12_0_t99ECE448D576B255AEA15DD30EA5E7EEEC239AD4  : public RuntimeObject
{
public:
	// UnityEngine.UI.Outline DG.Tweening.DOTweenModuleUI/<>c__DisplayClass12_0::target
	Outline_t37C754965BCC82FDD6C6878357A1439376C61CC2 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_0_t99ECE448D576B255AEA15DD30EA5E7EEEC239AD4, ___target_0)); }
	inline Outline_t37C754965BCC82FDD6C6878357A1439376C61CC2 * get_target_0() const { return ___target_0; }
	inline Outline_t37C754965BCC82FDD6C6878357A1439376C61CC2 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Outline_t37C754965BCC82FDD6C6878357A1439376C61CC2 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass13_0
struct U3CU3Ec__DisplayClass13_0_t56F545FEED596E262C041215923B9AB718A8012E  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass13_0::target
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t56F545FEED596E262C041215923B9AB718A8012E, ___target_0)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_target_0() const { return ___target_0; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass14_0
struct U3CU3Ec__DisplayClass14_0_tBC68B1D1F46519170BBF39DF32EA928C523894BC  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass14_0::target
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass14_0_tBC68B1D1F46519170BBF39DF32EA928C523894BC, ___target_0)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_target_0() const { return ___target_0; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass15_0
struct U3CU3Ec__DisplayClass15_0_tCBE7359CC5A37A4EB57FE6645D8747CD72E64ABE  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass15_0::target
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass15_0_tCBE7359CC5A37A4EB57FE6645D8747CD72E64ABE, ___target_0)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_target_0() const { return ___target_0; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass16_0
struct U3CU3Ec__DisplayClass16_0_tC3F94EE8FF590D986468D58FC6A98B1A853CAD39  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass16_0::target
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass16_0_tC3F94EE8FF590D986468D58FC6A98B1A853CAD39, ___target_0)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_target_0() const { return ___target_0; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass17_0
struct U3CU3Ec__DisplayClass17_0_tD6C024EA073B66BEEABC86C52D4DF5D5E6D44834  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass17_0::target
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_tD6C024EA073B66BEEABC86C52D4DF5D5E6D44834, ___target_0)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_target_0() const { return ___target_0; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass18_0
struct U3CU3Ec__DisplayClass18_0_tECE7F927669ED93C1B63B2A15741DAD4FECD3A1D  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass18_0::target
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_tECE7F927669ED93C1B63B2A15741DAD4FECD3A1D, ___target_0)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_target_0() const { return ___target_0; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass19_0
struct U3CU3Ec__DisplayClass19_0_tC1C79936B0CC3327C84F5464CF5EDD741B9982CF  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass19_0::target
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass19_0_tC1C79936B0CC3327C84F5464CF5EDD741B9982CF, ___target_0)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_target_0() const { return ___target_0; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass1_0
struct U3CU3Ec__DisplayClass1_0_tA034AE7DB61BCB47CD7FD835EFDBC68C45C67B1F  : public RuntimeObject
{
public:
	// UnityEngine.UI.Graphic DG.Tweening.DOTweenModuleUI/<>c__DisplayClass1_0::target
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_tA034AE7DB61BCB47CD7FD835EFDBC68C45C67B1F, ___target_0)); }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * get_target_0() const { return ___target_0; }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass20_0
struct U3CU3Ec__DisplayClass20_0_t97519398B17EF2EBB028A2A87122AE1B4EEEE2DE  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass20_0::target
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass20_0_t97519398B17EF2EBB028A2A87122AE1B4EEEE2DE, ___target_0)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_target_0() const { return ___target_0; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass21_0
struct U3CU3Ec__DisplayClass21_0_tA436E6685FA064BC921B5AA0C6B6834A1DB25D1B  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass21_0::target
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tA436E6685FA064BC921B5AA0C6B6834A1DB25D1B, ___target_0)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_target_0() const { return ___target_0; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass22_0
struct U3CU3Ec__DisplayClass22_0_tC659DEBA764F66319C5A2E51C8269ACC8D8EA831  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass22_0::target
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_tC659DEBA764F66319C5A2E51C8269ACC8D8EA831, ___target_0)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_target_0() const { return ___target_0; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass23_0
struct U3CU3Ec__DisplayClass23_0_tB215983097317F0F13C9C5E1EC951AD353EAFB4A  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass23_0::target
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass23_0_tB215983097317F0F13C9C5E1EC951AD353EAFB4A, ___target_0)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_target_0() const { return ___target_0; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass24_0
struct U3CU3Ec__DisplayClass24_0_tB4266531A17A8F2DBB4245371D4AFB4F59B86CBE  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass24_0::target
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass24_0_tB4266531A17A8F2DBB4245371D4AFB4F59B86CBE, ___target_0)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_target_0() const { return ___target_0; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass25_0
struct U3CU3Ec__DisplayClass25_0_tFD7B77C1798CBDB2998FA0FE51958080E5768BF3  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass25_0::target
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass25_0_tFD7B77C1798CBDB2998FA0FE51958080E5768BF3, ___target_0)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_target_0() const { return ___target_0; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass26_0
struct U3CU3Ec__DisplayClass26_0_t621BC92C98A92034824603E7FAE682A9EFCE0BF5  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass26_0::target
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass26_0_t621BC92C98A92034824603E7FAE682A9EFCE0BF5, ___target_0)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_target_0() const { return ___target_0; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass27_0
struct U3CU3Ec__DisplayClass27_0_tFECD12D643617983E5F7A6F87A1C7D1A09CDCA72  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass27_0::target
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass27_0_tFECD12D643617983E5F7A6F87A1C7D1A09CDCA72, ___target_0)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_target_0() const { return ___target_0; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass28_0
struct U3CU3Ec__DisplayClass28_0_tC2CE28B4736EA23E814F29C741156E76BE478D62  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass28_0::target
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass28_0_tC2CE28B4736EA23E814F29C741156E76BE478D62, ___target_0)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_target_0() const { return ___target_0; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass2_0
struct U3CU3Ec__DisplayClass2_0_t7824BF68052DA9F6B5BABC08C2CF630706983BFF  : public RuntimeObject
{
public:
	// UnityEngine.UI.Graphic DG.Tweening.DOTweenModuleUI/<>c__DisplayClass2_0::target
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t7824BF68052DA9F6B5BABC08C2CF630706983BFF, ___target_0)); }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * get_target_0() const { return ___target_0; }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass30_0
struct U3CU3Ec__DisplayClass30_0_t603E038217702DD25F01B276B450C75EF72AAC45  : public RuntimeObject
{
public:
	// UnityEngine.UI.ScrollRect DG.Tweening.DOTweenModuleUI/<>c__DisplayClass30_0::target
	ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass30_0_t603E038217702DD25F01B276B450C75EF72AAC45, ___target_0)); }
	inline ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * get_target_0() const { return ___target_0; }
	inline ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass31_0
struct U3CU3Ec__DisplayClass31_0_tB69CA5B0D5BA880AEBEA6538A9F652AC34DC72EB  : public RuntimeObject
{
public:
	// UnityEngine.UI.ScrollRect DG.Tweening.DOTweenModuleUI/<>c__DisplayClass31_0::target
	ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass31_0_tB69CA5B0D5BA880AEBEA6538A9F652AC34DC72EB, ___target_0)); }
	inline ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * get_target_0() const { return ___target_0; }
	inline ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass32_0
struct U3CU3Ec__DisplayClass32_0_tF3A2EE6CF0838B5BACDAC1F0055812FA553CF3EB  : public RuntimeObject
{
public:
	// UnityEngine.UI.ScrollRect DG.Tweening.DOTweenModuleUI/<>c__DisplayClass32_0::target
	ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass32_0_tF3A2EE6CF0838B5BACDAC1F0055812FA553CF3EB, ___target_0)); }
	inline ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * get_target_0() const { return ___target_0; }
	inline ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(ScrollRect_tB16156010F89FFDAAB2127CA878608FD91B9FBEA * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass33_0
struct U3CU3Ec__DisplayClass33_0_tA698C001E2B4ED0498F08BC403BB3537DEF81388  : public RuntimeObject
{
public:
	// UnityEngine.UI.Slider DG.Tweening.DOTweenModuleUI/<>c__DisplayClass33_0::target
	Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_0_tA698C001E2B4ED0498F08BC403BB3537DEF81388, ___target_0)); }
	inline Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * get_target_0() const { return ___target_0; }
	inline Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Slider_tBF39A11CC24CBD3F8BD728982ACAEAE43989B51A * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass34_0
struct U3CU3Ec__DisplayClass34_0_tF34CFB3D582FB76EBBD4988DB05E0FF88A300B65  : public RuntimeObject
{
public:
	// UnityEngine.UI.Text DG.Tweening.DOTweenModuleUI/<>c__DisplayClass34_0::target
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass34_0_tF34CFB3D582FB76EBBD4988DB05E0FF88A300B65, ___target_0)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_target_0() const { return ___target_0; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass35_0
struct U3CU3Ec__DisplayClass35_0_t0508164E64C6FCB8163048A2BDF6AC1DA322A6D3  : public RuntimeObject
{
public:
	// System.Int32 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass35_0::v
	int32_t ___v_0;
	// UnityEngine.UI.Text DG.Tweening.DOTweenModuleUI/<>c__DisplayClass35_0::target
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___target_1;
	// System.Boolean DG.Tweening.DOTweenModuleUI/<>c__DisplayClass35_0::addThousandsSeparator
	bool ___addThousandsSeparator_2;
	// System.Globalization.CultureInfo DG.Tweening.DOTweenModuleUI/<>c__DisplayClass35_0::cInfo
	CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * ___cInfo_3;

public:
	inline static int32_t get_offset_of_v_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t0508164E64C6FCB8163048A2BDF6AC1DA322A6D3, ___v_0)); }
	inline int32_t get_v_0() const { return ___v_0; }
	inline int32_t* get_address_of_v_0() { return &___v_0; }
	inline void set_v_0(int32_t value)
	{
		___v_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t0508164E64C6FCB8163048A2BDF6AC1DA322A6D3, ___target_1)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_target_1() const { return ___target_1; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_1), (void*)value);
	}

	inline static int32_t get_offset_of_addThousandsSeparator_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t0508164E64C6FCB8163048A2BDF6AC1DA322A6D3, ___addThousandsSeparator_2)); }
	inline bool get_addThousandsSeparator_2() const { return ___addThousandsSeparator_2; }
	inline bool* get_address_of_addThousandsSeparator_2() { return &___addThousandsSeparator_2; }
	inline void set_addThousandsSeparator_2(bool value)
	{
		___addThousandsSeparator_2 = value;
	}

	inline static int32_t get_offset_of_cInfo_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t0508164E64C6FCB8163048A2BDF6AC1DA322A6D3, ___cInfo_3)); }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * get_cInfo_3() const { return ___cInfo_3; }
	inline CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 ** get_address_of_cInfo_3() { return &___cInfo_3; }
	inline void set_cInfo_3(CultureInfo_t1B787142231DB79ABDCE0659823F908A040E9A98 * value)
	{
		___cInfo_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___cInfo_3), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass36_0
struct U3CU3Ec__DisplayClass36_0_t1822B5FD884B7F114AE8D127456AE144480D2AB7  : public RuntimeObject
{
public:
	// UnityEngine.UI.Text DG.Tweening.DOTweenModuleUI/<>c__DisplayClass36_0::target
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass36_0_t1822B5FD884B7F114AE8D127456AE144480D2AB7, ___target_0)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_target_0() const { return ___target_0; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass37_0
struct U3CU3Ec__DisplayClass37_0_t4E8C18778C78A6917596D8C43A8A1193A27BD719  : public RuntimeObject
{
public:
	// UnityEngine.UI.Text DG.Tweening.DOTweenModuleUI/<>c__DisplayClass37_0::target
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass37_0_t4E8C18778C78A6917596D8C43A8A1193A27BD719, ___target_0)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_target_0() const { return ___target_0; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass3_0
struct U3CU3Ec__DisplayClass3_0_t5AB498C9410AC66EFDAF2A14D9B43572DB5F3187  : public RuntimeObject
{
public:
	// UnityEngine.UI.Image DG.Tweening.DOTweenModuleUI/<>c__DisplayClass3_0::target
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t5AB498C9410AC66EFDAF2A14D9B43572DB5F3187, ___target_0)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_target_0() const { return ___target_0; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass41_0
struct U3CU3Ec__DisplayClass41_0_t7357FED270559E937C6096D1CB738C0EF5AA4DC4  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass41_0::target
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass41_0_t7357FED270559E937C6096D1CB738C0EF5AA4DC4, ___target_0)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_target_0() const { return ___target_0; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass4_0
struct U3CU3Ec__DisplayClass4_0_tB484E31E27A9D01E3BB25AF4D20552E7B314EE21  : public RuntimeObject
{
public:
	// UnityEngine.UI.Image DG.Tweening.DOTweenModuleUI/<>c__DisplayClass4_0::target
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_tB484E31E27A9D01E3BB25AF4D20552E7B314EE21, ___target_0)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_target_0() const { return ___target_0; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass5_0
struct U3CU3Ec__DisplayClass5_0_tD091EE72EF971B43BB9CB23ABF0762A15FC219EF  : public RuntimeObject
{
public:
	// UnityEngine.UI.Image DG.Tweening.DOTweenModuleUI/<>c__DisplayClass5_0::target
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_tD091EE72EF971B43BB9CB23ABF0762A15FC219EF, ___target_0)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_target_0() const { return ___target_0; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass7_0
struct U3CU3Ec__DisplayClass7_0_tC4DB2DC8B7DC8BB09DB48A7536CEC71847744DFB  : public RuntimeObject
{
public:
	// UnityEngine.UI.LayoutElement DG.Tweening.DOTweenModuleUI/<>c__DisplayClass7_0::target
	LayoutElement_tE514951184806899FE23EC4FA6112A5F2038CECF * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_tC4DB2DC8B7DC8BB09DB48A7536CEC71847744DFB, ___target_0)); }
	inline LayoutElement_tE514951184806899FE23EC4FA6112A5F2038CECF * get_target_0() const { return ___target_0; }
	inline LayoutElement_tE514951184806899FE23EC4FA6112A5F2038CECF ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(LayoutElement_tE514951184806899FE23EC4FA6112A5F2038CECF * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass8_0
struct U3CU3Ec__DisplayClass8_0_tC948330164C5D90D6BCA06D460E316067C20A284  : public RuntimeObject
{
public:
	// UnityEngine.UI.LayoutElement DG.Tweening.DOTweenModuleUI/<>c__DisplayClass8_0::target
	LayoutElement_tE514951184806899FE23EC4FA6112A5F2038CECF * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tC948330164C5D90D6BCA06D460E316067C20A284, ___target_0)); }
	inline LayoutElement_tE514951184806899FE23EC4FA6112A5F2038CECF * get_target_0() const { return ___target_0; }
	inline LayoutElement_tE514951184806899FE23EC4FA6112A5F2038CECF ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(LayoutElement_tE514951184806899FE23EC4FA6112A5F2038CECF * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass9_0
struct U3CU3Ec__DisplayClass9_0_t45A80CA8BC8D26360B96ECBA9C16C98DDB8FBFF9  : public RuntimeObject
{
public:
	// UnityEngine.UI.LayoutElement DG.Tweening.DOTweenModuleUI/<>c__DisplayClass9_0::target
	LayoutElement_tE514951184806899FE23EC4FA6112A5F2038CECF * ___target_0;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_t45A80CA8BC8D26360B96ECBA9C16C98DDB8FBFF9, ___target_0)); }
	inline LayoutElement_tE514951184806899FE23EC4FA6112A5F2038CECF * get_target_0() const { return ___target_0; }
	inline LayoutElement_tE514951184806899FE23EC4FA6112A5F2038CECF ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(LayoutElement_tE514951184806899FE23EC4FA6112A5F2038CECF * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/Utils
struct Utils_t7418F53E30422B8377F6B148B50CBA4D73EB91C6  : public RuntimeObject
{
public:

public:
};


// DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass8_0
struct U3CU3Ec__DisplayClass8_0_tA4E985E6B2BDBFEBB6D60F8C6849A49889FB4408  : public RuntimeObject
{
public:
	// UnityEngine.Material DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass8_0::target
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___target_0;
	// System.Int32 DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass8_0::propertyID
	int32_t ___propertyID_1;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tA4E985E6B2BDBFEBB6D60F8C6849A49889FB4408, ___target_0)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_target_0() const { return ___target_0; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}

	inline static int32_t get_offset_of_propertyID_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tA4E985E6B2BDBFEBB6D60F8C6849A49889FB4408, ___propertyID_1)); }
	inline int32_t get_propertyID_1() const { return ___propertyID_1; }
	inline int32_t* get_address_of_propertyID_1() { return &___propertyID_1; }
	inline void set_propertyID_1(int32_t value)
	{
		___propertyID_1 = value;
	}
};


// DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass9_0
struct U3CU3Ec__DisplayClass9_0_t0D399C765492884FD834195CAA9979C9DD9F0E77  : public RuntimeObject
{
public:
	// UnityEngine.Material DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass9_0::target
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___target_0;
	// System.Int32 DG.Tweening.DOTweenModuleUnityVersion/<>c__DisplayClass9_0::propertyID
	int32_t ___propertyID_1;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_t0D399C765492884FD834195CAA9979C9DD9F0E77, ___target_0)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_target_0() const { return ___target_0; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}

	inline static int32_t get_offset_of_propertyID_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass9_0_t0D399C765492884FD834195CAA9979C9DD9F0E77, ___propertyID_1)); }
	inline int32_t get_propertyID_1() const { return ___propertyID_1; }
	inline int32_t* get_address_of_propertyID_1() { return &___propertyID_1; }
	inline void set_propertyID_1(int32_t value)
	{
		___propertyID_1 = value;
	}
};


// DG.Tweening.DOTweenModuleUtils/Physics
struct Physics_tAC6486071FC9891A5D19DC417B8F421FDA56C36F  : public RuntimeObject
{
public:

public:
};


// AllIn1SpriteShader.Demo2AutoScroll/<ScrollElements>d__4
struct U3CScrollElementsU3Ed__4_tCD9059A48792FEA66DFED0E0F2274DD9F5C097EC  : public RuntimeObject
{
public:
	// System.Int32 AllIn1SpriteShader.Demo2AutoScroll/<ScrollElements>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object AllIn1SpriteShader.Demo2AutoScroll/<ScrollElements>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// AllIn1SpriteShader.Demo2AutoScroll AllIn1SpriteShader.Demo2AutoScroll/<ScrollElements>d__4::<>4__this
	Demo2AutoScroll_tF05254B25ADADD0CC28CF7DFC84D7709CA5A7BFB * ___U3CU3E4__this_2;
	// System.Int32 AllIn1SpriteShader.Demo2AutoScroll/<ScrollElements>d__4::<i>5__2
	int32_t ___U3CiU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CScrollElementsU3Ed__4_tCD9059A48792FEA66DFED0E0F2274DD9F5C097EC, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CScrollElementsU3Ed__4_tCD9059A48792FEA66DFED0E0F2274DD9F5C097EC, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CScrollElementsU3Ed__4_tCD9059A48792FEA66DFED0E0F2274DD9F5C097EC, ___U3CU3E4__this_2)); }
	inline Demo2AutoScroll_tF05254B25ADADD0CC28CF7DFC84D7709CA5A7BFB * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Demo2AutoScroll_tF05254B25ADADD0CC28CF7DFC84D7709CA5A7BFB ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Demo2AutoScroll_tF05254B25ADADD0CC28CF7DFC84D7709CA5A7BFB * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CScrollElementsU3Ed__4_tCD9059A48792FEA66DFED0E0F2274DD9F5C097EC, ___U3CiU3E5__2_3)); }
	inline int32_t get_U3CiU3E5__2_3() const { return ___U3CiU3E5__2_3; }
	inline int32_t* get_address_of_U3CiU3E5__2_3() { return &___U3CiU3E5__2_3; }
	inline void set_U3CiU3E5__2_3(int32_t value)
	{
		___U3CiU3E5__2_3 = value;
	}
};


// AllIn1SpriteShader.DemoCamera/<SetCamAfterStart>d__8
struct U3CSetCamAfterStartU3Ed__8_t6A9998AF9B7B20E9B90CEB89BBC0E37EE1255338  : public RuntimeObject
{
public:
	// System.Int32 AllIn1SpriteShader.DemoCamera/<SetCamAfterStart>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object AllIn1SpriteShader.DemoCamera/<SetCamAfterStart>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// AllIn1SpriteShader.DemoCamera AllIn1SpriteShader.DemoCamera/<SetCamAfterStart>d__8::<>4__this
	DemoCamera_tB715A63ACE2506E72DFEAB96E6BBCA8FC6BD30CB * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSetCamAfterStartU3Ed__8_t6A9998AF9B7B20E9B90CEB89BBC0E37EE1255338, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSetCamAfterStartU3Ed__8_t6A9998AF9B7B20E9B90CEB89BBC0E37EE1255338, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSetCamAfterStartU3Ed__8_t6A9998AF9B7B20E9B90CEB89BBC0E37EE1255338, ___U3CU3E4__this_2)); }
	inline DemoCamera_tB715A63ACE2506E72DFEAB96E6BBCA8FC6BD30CB * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline DemoCamera_tB715A63ACE2506E72DFEAB96E6BBCA8FC6BD30CB ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(DemoCamera_tB715A63ACE2506E72DFEAB96E6BBCA8FC6BD30CB * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}
};


// TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3
struct U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TMPro.Examples.VertexColorCycler TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::<>4__this
	VertexColorCycler_t37E80E87D8EAD0D7757CDA45BD5D3AA82FD28374 * ___U3CU3E4__this_2;
	// TMPro.TMP_TextInfo TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::<textInfo>5__2
	TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * ___U3CtextInfoU3E5__2_3;
	// System.Int32 TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::<currentCharacter>5__3
	int32_t ___U3CcurrentCharacterU3E5__3_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF, ___U3CU3E4__this_2)); }
	inline VertexColorCycler_t37E80E87D8EAD0D7757CDA45BD5D3AA82FD28374 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline VertexColorCycler_t37E80E87D8EAD0D7757CDA45BD5D3AA82FD28374 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(VertexColorCycler_t37E80E87D8EAD0D7757CDA45BD5D3AA82FD28374 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtextInfoU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF, ___U3CtextInfoU3E5__2_3)); }
	inline TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * get_U3CtextInfoU3E5__2_3() const { return ___U3CtextInfoU3E5__2_3; }
	inline TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 ** get_address_of_U3CtextInfoU3E5__2_3() { return &___U3CtextInfoU3E5__2_3; }
	inline void set_U3CtextInfoU3E5__2_3(TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * value)
	{
		___U3CtextInfoU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtextInfoU3E5__2_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcurrentCharacterU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF, ___U3CcurrentCharacterU3E5__3_4)); }
	inline int32_t get_U3CcurrentCharacterU3E5__3_4() const { return ___U3CcurrentCharacterU3E5__3_4; }
	inline int32_t* get_address_of_U3CcurrentCharacterU3E5__3_4() { return &___U3CcurrentCharacterU3E5__3_4; }
	inline void set_U3CcurrentCharacterU3E5__3_4(int32_t value)
	{
		___U3CcurrentCharacterU3E5__3_4 = value;
	}
};


// TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11
struct U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TMPro.Examples.VertexJitter TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::<>4__this
	VertexJitter_t4BF9FCCB84221E69D9E3727E32E49710F3B4342A * ___U3CU3E4__this_2;
	// TMPro.TMP_TextInfo TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::<textInfo>5__2
	TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * ___U3CtextInfoU3E5__2_3;
	// System.Int32 TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::<loopCount>5__3
	int32_t ___U3CloopCountU3E5__3_4;
	// TMPro.Examples.VertexJitter/VertexAnim[] TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::<vertexAnim>5__4
	VertexAnimU5BU5D_t069E9FCD87DE8FF65BE288FDB2834711D933AAFF* ___U3CvertexAnimU3E5__4_5;
	// TMPro.TMP_MeshInfo[] TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::<cachedMeshInfo>5__5
	TMP_MeshInfoU5BU5D_t6C0A65D18C54B6FA681B2EB0676B83116FD03119* ___U3CcachedMeshInfoU3E5__5_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741, ___U3CU3E4__this_2)); }
	inline VertexJitter_t4BF9FCCB84221E69D9E3727E32E49710F3B4342A * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline VertexJitter_t4BF9FCCB84221E69D9E3727E32E49710F3B4342A ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(VertexJitter_t4BF9FCCB84221E69D9E3727E32E49710F3B4342A * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtextInfoU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741, ___U3CtextInfoU3E5__2_3)); }
	inline TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * get_U3CtextInfoU3E5__2_3() const { return ___U3CtextInfoU3E5__2_3; }
	inline TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 ** get_address_of_U3CtextInfoU3E5__2_3() { return &___U3CtextInfoU3E5__2_3; }
	inline void set_U3CtextInfoU3E5__2_3(TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * value)
	{
		___U3CtextInfoU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtextInfoU3E5__2_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CloopCountU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741, ___U3CloopCountU3E5__3_4)); }
	inline int32_t get_U3CloopCountU3E5__3_4() const { return ___U3CloopCountU3E5__3_4; }
	inline int32_t* get_address_of_U3CloopCountU3E5__3_4() { return &___U3CloopCountU3E5__3_4; }
	inline void set_U3CloopCountU3E5__3_4(int32_t value)
	{
		___U3CloopCountU3E5__3_4 = value;
	}

	inline static int32_t get_offset_of_U3CvertexAnimU3E5__4_5() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741, ___U3CvertexAnimU3E5__4_5)); }
	inline VertexAnimU5BU5D_t069E9FCD87DE8FF65BE288FDB2834711D933AAFF* get_U3CvertexAnimU3E5__4_5() const { return ___U3CvertexAnimU3E5__4_5; }
	inline VertexAnimU5BU5D_t069E9FCD87DE8FF65BE288FDB2834711D933AAFF** get_address_of_U3CvertexAnimU3E5__4_5() { return &___U3CvertexAnimU3E5__4_5; }
	inline void set_U3CvertexAnimU3E5__4_5(VertexAnimU5BU5D_t069E9FCD87DE8FF65BE288FDB2834711D933AAFF* value)
	{
		___U3CvertexAnimU3E5__4_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CvertexAnimU3E5__4_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcachedMeshInfoU3E5__5_6() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741, ___U3CcachedMeshInfoU3E5__5_6)); }
	inline TMP_MeshInfoU5BU5D_t6C0A65D18C54B6FA681B2EB0676B83116FD03119* get_U3CcachedMeshInfoU3E5__5_6() const { return ___U3CcachedMeshInfoU3E5__5_6; }
	inline TMP_MeshInfoU5BU5D_t6C0A65D18C54B6FA681B2EB0676B83116FD03119** get_address_of_U3CcachedMeshInfoU3E5__5_6() { return &___U3CcachedMeshInfoU3E5__5_6; }
	inline void set_U3CcachedMeshInfoU3E5__5_6(TMP_MeshInfoU5BU5D_t6C0A65D18C54B6FA681B2EB0676B83116FD03119* value)
	{
		___U3CcachedMeshInfoU3E5__5_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CcachedMeshInfoU3E5__5_6), (void*)value);
	}
};


// TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11
struct U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TMPro.Examples.VertexShakeA TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::<>4__this
	VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A * ___U3CU3E4__this_2;
	// TMPro.TMP_TextInfo TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::<textInfo>5__2
	TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * ___U3CtextInfoU3E5__2_3;
	// UnityEngine.Vector3[][] TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::<copyOfVertices>5__3
	Vector3U5BU5DU5BU5D_tAD34024FBDE3F1F720ECB62B24B914A748C60BCB* ___U3CcopyOfVerticesU3E5__3_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155, ___U3CU3E4__this_2)); }
	inline VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtextInfoU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155, ___U3CtextInfoU3E5__2_3)); }
	inline TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * get_U3CtextInfoU3E5__2_3() const { return ___U3CtextInfoU3E5__2_3; }
	inline TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 ** get_address_of_U3CtextInfoU3E5__2_3() { return &___U3CtextInfoU3E5__2_3; }
	inline void set_U3CtextInfoU3E5__2_3(TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * value)
	{
		___U3CtextInfoU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtextInfoU3E5__2_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcopyOfVerticesU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155, ___U3CcopyOfVerticesU3E5__3_4)); }
	inline Vector3U5BU5DU5BU5D_tAD34024FBDE3F1F720ECB62B24B914A748C60BCB* get_U3CcopyOfVerticesU3E5__3_4() const { return ___U3CcopyOfVerticesU3E5__3_4; }
	inline Vector3U5BU5DU5BU5D_tAD34024FBDE3F1F720ECB62B24B914A748C60BCB** get_address_of_U3CcopyOfVerticesU3E5__3_4() { return &___U3CcopyOfVerticesU3E5__3_4; }
	inline void set_U3CcopyOfVerticesU3E5__3_4(Vector3U5BU5DU5BU5D_tAD34024FBDE3F1F720ECB62B24B914A748C60BCB* value)
	{
		___U3CcopyOfVerticesU3E5__3_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CcopyOfVerticesU3E5__3_4), (void*)value);
	}
};


// TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10
struct U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TMPro.Examples.VertexShakeB TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::<>4__this
	VertexShakeB_t348FF1466CEF3BFED37C140B7AEFA2EA90D703C9 * ___U3CU3E4__this_2;
	// TMPro.TMP_TextInfo TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::<textInfo>5__2
	TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * ___U3CtextInfoU3E5__2_3;
	// UnityEngine.Vector3[][] TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::<copyOfVertices>5__3
	Vector3U5BU5DU5BU5D_tAD34024FBDE3F1F720ECB62B24B914A748C60BCB* ___U3CcopyOfVerticesU3E5__3_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6, ___U3CU3E4__this_2)); }
	inline VertexShakeB_t348FF1466CEF3BFED37C140B7AEFA2EA90D703C9 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline VertexShakeB_t348FF1466CEF3BFED37C140B7AEFA2EA90D703C9 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(VertexShakeB_t348FF1466CEF3BFED37C140B7AEFA2EA90D703C9 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtextInfoU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6, ___U3CtextInfoU3E5__2_3)); }
	inline TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * get_U3CtextInfoU3E5__2_3() const { return ___U3CtextInfoU3E5__2_3; }
	inline TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 ** get_address_of_U3CtextInfoU3E5__2_3() { return &___U3CtextInfoU3E5__2_3; }
	inline void set_U3CtextInfoU3E5__2_3(TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * value)
	{
		___U3CtextInfoU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtextInfoU3E5__2_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcopyOfVerticesU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6, ___U3CcopyOfVerticesU3E5__3_4)); }
	inline Vector3U5BU5DU5BU5D_tAD34024FBDE3F1F720ECB62B24B914A748C60BCB* get_U3CcopyOfVerticesU3E5__3_4() const { return ___U3CcopyOfVerticesU3E5__3_4; }
	inline Vector3U5BU5DU5BU5D_tAD34024FBDE3F1F720ECB62B24B914A748C60BCB** get_address_of_U3CcopyOfVerticesU3E5__3_4() { return &___U3CcopyOfVerticesU3E5__3_4; }
	inline void set_U3CcopyOfVerticesU3E5__3_4(Vector3U5BU5DU5BU5D_tAD34024FBDE3F1F720ECB62B24B914A748C60BCB* value)
	{
		___U3CcopyOfVerticesU3E5__3_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CcopyOfVerticesU3E5__3_4), (void*)value);
	}
};


// TMPro.Examples.VertexZoom/<>c__DisplayClass10_0
struct U3CU3Ec__DisplayClass10_0_t1AC05F5499B93AC94E9727B8B0F08D8037AD52A6  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Single> TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::modifiedCharScale
	List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * ___modifiedCharScale_0;
	// System.Comparison`1<System.Int32> TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::<>9__0
	Comparison_1_t3430355DCDD0AF453788265DC88E9288D19C4E9C * ___U3CU3E9__0_1;

public:
	inline static int32_t get_offset_of_modifiedCharScale_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t1AC05F5499B93AC94E9727B8B0F08D8037AD52A6, ___modifiedCharScale_0)); }
	inline List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * get_modifiedCharScale_0() const { return ___modifiedCharScale_0; }
	inline List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA ** get_address_of_modifiedCharScale_0() { return &___modifiedCharScale_0; }
	inline void set_modifiedCharScale_0(List_1_t6726F9309570A0BDC5D42E10777F3E2931C487AA * value)
	{
		___modifiedCharScale_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___modifiedCharScale_0), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass10_0_t1AC05F5499B93AC94E9727B8B0F08D8037AD52A6, ___U3CU3E9__0_1)); }
	inline Comparison_1_t3430355DCDD0AF453788265DC88E9288D19C4E9C * get_U3CU3E9__0_1() const { return ___U3CU3E9__0_1; }
	inline Comparison_1_t3430355DCDD0AF453788265DC88E9288D19C4E9C ** get_address_of_U3CU3E9__0_1() { return &___U3CU3E9__0_1; }
	inline void set_U3CU3E9__0_1(Comparison_1_t3430355DCDD0AF453788265DC88E9288D19C4E9C * value)
	{
		___U3CU3E9__0_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E9__0_1), (void*)value);
	}
};


// TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10
struct U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TMPro.Examples.VertexZoom TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::<>4__this
	VertexZoom_t02E8994D3A9EB41AEEF1544EF8C93009F6153163 * ___U3CU3E4__this_2;
	// TMPro.Examples.VertexZoom/<>c__DisplayClass10_0 TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::<>8__1
	U3CU3Ec__DisplayClass10_0_t1AC05F5499B93AC94E9727B8B0F08D8037AD52A6 * ___U3CU3E8__1_3;
	// TMPro.TMP_TextInfo TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::<textInfo>5__2
	TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * ___U3CtextInfoU3E5__2_4;
	// TMPro.TMP_MeshInfo[] TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::<cachedMeshInfoVertexData>5__3
	TMP_MeshInfoU5BU5D_t6C0A65D18C54B6FA681B2EB0676B83116FD03119* ___U3CcachedMeshInfoVertexDataU3E5__3_5;
	// System.Collections.Generic.List`1<System.Int32> TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::<scaleSortingOrder>5__4
	List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * ___U3CscaleSortingOrderU3E5__4_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40, ___U3CU3E4__this_2)); }
	inline VertexZoom_t02E8994D3A9EB41AEEF1544EF8C93009F6153163 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline VertexZoom_t02E8994D3A9EB41AEEF1544EF8C93009F6153163 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(VertexZoom_t02E8994D3A9EB41AEEF1544EF8C93009F6153163 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E8__1_3() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40, ___U3CU3E8__1_3)); }
	inline U3CU3Ec__DisplayClass10_0_t1AC05F5499B93AC94E9727B8B0F08D8037AD52A6 * get_U3CU3E8__1_3() const { return ___U3CU3E8__1_3; }
	inline U3CU3Ec__DisplayClass10_0_t1AC05F5499B93AC94E9727B8B0F08D8037AD52A6 ** get_address_of_U3CU3E8__1_3() { return &___U3CU3E8__1_3; }
	inline void set_U3CU3E8__1_3(U3CU3Ec__DisplayClass10_0_t1AC05F5499B93AC94E9727B8B0F08D8037AD52A6 * value)
	{
		___U3CU3E8__1_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E8__1_3), (void*)value);
	}

	inline static int32_t get_offset_of_U3CtextInfoU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40, ___U3CtextInfoU3E5__2_4)); }
	inline TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * get_U3CtextInfoU3E5__2_4() const { return ___U3CtextInfoU3E5__2_4; }
	inline TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 ** get_address_of_U3CtextInfoU3E5__2_4() { return &___U3CtextInfoU3E5__2_4; }
	inline void set_U3CtextInfoU3E5__2_4(TMP_TextInfo_t33ACB74FB814F588497640C86976E5DB6DD7B547 * value)
	{
		___U3CtextInfoU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CtextInfoU3E5__2_4), (void*)value);
	}

	inline static int32_t get_offset_of_U3CcachedMeshInfoVertexDataU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40, ___U3CcachedMeshInfoVertexDataU3E5__3_5)); }
	inline TMP_MeshInfoU5BU5D_t6C0A65D18C54B6FA681B2EB0676B83116FD03119* get_U3CcachedMeshInfoVertexDataU3E5__3_5() const { return ___U3CcachedMeshInfoVertexDataU3E5__3_5; }
	inline TMP_MeshInfoU5BU5D_t6C0A65D18C54B6FA681B2EB0676B83116FD03119** get_address_of_U3CcachedMeshInfoVertexDataU3E5__3_5() { return &___U3CcachedMeshInfoVertexDataU3E5__3_5; }
	inline void set_U3CcachedMeshInfoVertexDataU3E5__3_5(TMP_MeshInfoU5BU5D_t6C0A65D18C54B6FA681B2EB0676B83116FD03119* value)
	{
		___U3CcachedMeshInfoVertexDataU3E5__3_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CcachedMeshInfoVertexDataU3E5__3_5), (void*)value);
	}

	inline static int32_t get_offset_of_U3CscaleSortingOrderU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40, ___U3CscaleSortingOrderU3E5__4_6)); }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * get_U3CscaleSortingOrderU3E5__4_6() const { return ___U3CscaleSortingOrderU3E5__4_6; }
	inline List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 ** get_address_of_U3CscaleSortingOrderU3E5__4_6() { return &___U3CscaleSortingOrderU3E5__4_6; }
	inline void set_U3CscaleSortingOrderU3E5__4_6(List_1_t260B41F956D673396C33A4CF94E8D6C4389EACB7 * value)
	{
		___U3CscaleSortingOrderU3E5__4_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CscaleSortingOrderU3E5__4_6), (void*)value);
	}
};


// TMPro.Examples.WarpTextExample/<WarpText>d__8
struct U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.WarpTextExample/<WarpText>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// TMPro.Examples.WarpTextExample TMPro.Examples.WarpTextExample/<WarpText>d__8::<>4__this
	WarpTextExample_t9CE7000EFB1B8696E32CC86E87E3D0EDEA3D5A96 * ___U3CU3E4__this_2;
	// System.Single TMPro.Examples.WarpTextExample/<WarpText>d__8::<old_CurveScale>5__2
	float ___U3Cold_CurveScaleU3E5__2_3;
	// UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample/<WarpText>d__8::<old_curve>5__3
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___U3Cold_curveU3E5__3_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76, ___U3CU3E4__this_2)); }
	inline WarpTextExample_t9CE7000EFB1B8696E32CC86E87E3D0EDEA3D5A96 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline WarpTextExample_t9CE7000EFB1B8696E32CC86E87E3D0EDEA3D5A96 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(WarpTextExample_t9CE7000EFB1B8696E32CC86E87E3D0EDEA3D5A96 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3Cold_CurveScaleU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76, ___U3Cold_CurveScaleU3E5__2_3)); }
	inline float get_U3Cold_CurveScaleU3E5__2_3() const { return ___U3Cold_CurveScaleU3E5__2_3; }
	inline float* get_address_of_U3Cold_CurveScaleU3E5__2_3() { return &___U3Cold_CurveScaleU3E5__2_3; }
	inline void set_U3Cold_CurveScaleU3E5__2_3(float value)
	{
		___U3Cold_CurveScaleU3E5__2_3 = value;
	}

	inline static int32_t get_offset_of_U3Cold_curveU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76, ___U3Cold_curveU3E5__3_4)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_U3Cold_curveU3E5__3_4() const { return ___U3Cold_curveU3E5__3_4; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_U3Cold_curveU3E5__3_4() { return &___U3Cold_curveU3E5__3_4; }
	inline void set_U3Cold_curveU3E5__3_4(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___U3Cold_curveU3E5__3_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3Cold_curveU3E5__3_4), (void*)value);
	}
};


// System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 
{
public:
	// System.Runtime.CompilerServices.IAsyncStateMachine System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_stateMachine
	RuntimeObject* ___m_stateMachine_0;
	// System.Action System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_defaultContextAction
	Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * ___m_defaultContextAction_1;

public:
	inline static int32_t get_offset_of_m_stateMachine_0() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34, ___m_stateMachine_0)); }
	inline RuntimeObject* get_m_stateMachine_0() const { return ___m_stateMachine_0; }
	inline RuntimeObject** get_address_of_m_stateMachine_0() { return &___m_stateMachine_0; }
	inline void set_m_stateMachine_0(RuntimeObject* value)
	{
		___m_stateMachine_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_stateMachine_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_defaultContextAction_1() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34, ___m_defaultContextAction_1)); }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * get_m_defaultContextAction_1() const { return ___m_defaultContextAction_1; }
	inline Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 ** get_address_of_m_defaultContextAction_1() { return &___m_defaultContextAction_1; }
	inline void set_m_defaultContextAction_1(Action_tAF41423D285AE0862865348CF6CE51CD085ABBA6 * value)
	{
		___m_defaultContextAction_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_defaultContextAction_1), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34_marshaled_pinvoke
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34_marshaled_com
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};

// UnityEngine.Color
struct Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.Quaternion
struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___identityQuaternion_4 = value;
	}
};


// UnityEngine.Vector2
struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___zeroVector_2)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___oneVector_3)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___upVector_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___downVector_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___leftVector_6)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___rightVector_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___negativeInfinityVector_9 = value;
	}
};


// UnityEngine.Vector3
struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___zeroVector_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___oneVector_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___upVector_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___downVector_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___leftVector_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___rightVector_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___forwardVector_11)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___backVector_12)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___negativeInfinityVector_14 = value;
	}
};


// DG.Tweening.DOTweenCYInstruction/WaitForCompletion
struct WaitForCompletion_t1E6C917DF8283C359606916D4568BF90BADF2A31  : public CustomYieldInstruction_t4ED1543FBAA3143362854EB1867B42E5D190A5C7
{
public:
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction/WaitForCompletion::t
	Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t_0;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(WaitForCompletion_t1E6C917DF8283C359606916D4568BF90BADF2A31, ___t_0)); }
	inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * get_t_0() const { return ___t_0; }
	inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 ** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_0), (void*)value);
	}
};


// DG.Tweening.DOTweenCYInstruction/WaitForElapsedLoops
struct WaitForElapsedLoops_t814A277753ACAF2E527FBEDAB7B34438DD308A3B  : public CustomYieldInstruction_t4ED1543FBAA3143362854EB1867B42E5D190A5C7
{
public:
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction/WaitForElapsedLoops::t
	Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t_0;
	// System.Int32 DG.Tweening.DOTweenCYInstruction/WaitForElapsedLoops::elapsedLoops
	int32_t ___elapsedLoops_1;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(WaitForElapsedLoops_t814A277753ACAF2E527FBEDAB7B34438DD308A3B, ___t_0)); }
	inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * get_t_0() const { return ___t_0; }
	inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 ** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_0), (void*)value);
	}

	inline static int32_t get_offset_of_elapsedLoops_1() { return static_cast<int32_t>(offsetof(WaitForElapsedLoops_t814A277753ACAF2E527FBEDAB7B34438DD308A3B, ___elapsedLoops_1)); }
	inline int32_t get_elapsedLoops_1() const { return ___elapsedLoops_1; }
	inline int32_t* get_address_of_elapsedLoops_1() { return &___elapsedLoops_1; }
	inline void set_elapsedLoops_1(int32_t value)
	{
		___elapsedLoops_1 = value;
	}
};


// DG.Tweening.DOTweenCYInstruction/WaitForKill
struct WaitForKill_t83BF7021ECF7425C005B480D7A4DFD34B395E257  : public CustomYieldInstruction_t4ED1543FBAA3143362854EB1867B42E5D190A5C7
{
public:
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction/WaitForKill::t
	Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t_0;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(WaitForKill_t83BF7021ECF7425C005B480D7A4DFD34B395E257, ___t_0)); }
	inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * get_t_0() const { return ___t_0; }
	inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 ** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_0), (void*)value);
	}
};


// DG.Tweening.DOTweenCYInstruction/WaitForPosition
struct WaitForPosition_t1FD3726569822C1BE6C5922A6D6FBA319FF441EA  : public CustomYieldInstruction_t4ED1543FBAA3143362854EB1867B42E5D190A5C7
{
public:
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction/WaitForPosition::t
	Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t_0;
	// System.Single DG.Tweening.DOTweenCYInstruction/WaitForPosition::position
	float ___position_1;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(WaitForPosition_t1FD3726569822C1BE6C5922A6D6FBA319FF441EA, ___t_0)); }
	inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * get_t_0() const { return ___t_0; }
	inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 ** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_0), (void*)value);
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(WaitForPosition_t1FD3726569822C1BE6C5922A6D6FBA319FF441EA, ___position_1)); }
	inline float get_position_1() const { return ___position_1; }
	inline float* get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(float value)
	{
		___position_1 = value;
	}
};


// DG.Tweening.DOTweenCYInstruction/WaitForRewind
struct WaitForRewind_t4B81EC806C18944320A115540DD5662347EF9881  : public CustomYieldInstruction_t4ED1543FBAA3143362854EB1867B42E5D190A5C7
{
public:
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction/WaitForRewind::t
	Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t_0;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(WaitForRewind_t4B81EC806C18944320A115540DD5662347EF9881, ___t_0)); }
	inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * get_t_0() const { return ___t_0; }
	inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 ** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_0), (void*)value);
	}
};


// DG.Tweening.DOTweenCYInstruction/WaitForStart
struct WaitForStart_tA1CFF8828622F5558D854DD2EA5677F8F76D2CEE  : public CustomYieldInstruction_t4ED1543FBAA3143362854EB1867B42E5D190A5C7
{
public:
	// DG.Tweening.Tween DG.Tweening.DOTweenCYInstruction/WaitForStart::t
	Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t_0;

public:
	inline static int32_t get_offset_of_t_0() { return static_cast<int32_t>(offsetof(WaitForStart_tA1CFF8828622F5558D854DD2EA5677F8F76D2CEE, ___t_0)); }
	inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * get_t_0() const { return ___t_0; }
	inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 ** get_address_of_t_0() { return &___t_0; }
	inline void set_t_0(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * value)
	{
		___t_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_0), (void*)value);
	}
};


// TMPro.Examples.VertexJitter/VertexAnim
struct VertexAnim_t3C1CE8C591FCD7E249D77127BED0B743EAD556F4 
{
public:
	// System.Single TMPro.Examples.VertexJitter/VertexAnim::angleRange
	float ___angleRange_0;
	// System.Single TMPro.Examples.VertexJitter/VertexAnim::angle
	float ___angle_1;
	// System.Single TMPro.Examples.VertexJitter/VertexAnim::speed
	float ___speed_2;

public:
	inline static int32_t get_offset_of_angleRange_0() { return static_cast<int32_t>(offsetof(VertexAnim_t3C1CE8C591FCD7E249D77127BED0B743EAD556F4, ___angleRange_0)); }
	inline float get_angleRange_0() const { return ___angleRange_0; }
	inline float* get_address_of_angleRange_0() { return &___angleRange_0; }
	inline void set_angleRange_0(float value)
	{
		___angleRange_0 = value;
	}

	inline static int32_t get_offset_of_angle_1() { return static_cast<int32_t>(offsetof(VertexAnim_t3C1CE8C591FCD7E249D77127BED0B743EAD556F4, ___angle_1)); }
	inline float get_angle_1() const { return ___angle_1; }
	inline float* get_address_of_angle_1() { return &___angle_1; }
	inline void set_angle_1(float value)
	{
		___angle_1 = value;
	}

	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(VertexAnim_t3C1CE8C591FCD7E249D77127BED0B743EAD556F4, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}
};


// System.Runtime.CompilerServices.YieldAwaitable/YieldAwaiter
struct YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE 
{
public:
	union
	{
		struct
		{
		};
		uint8_t YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE__padding[1];
	};

public:
};

struct YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE_StaticFields
{
public:
	// System.Threading.WaitCallback System.Runtime.CompilerServices.YieldAwaitable/YieldAwaiter::s_waitCallbackRunAction
	WaitCallback_t82C85517E973DCC6390AFB0BC3C2276F3328A319 * ___s_waitCallbackRunAction_0;
	// System.Threading.SendOrPostCallback System.Runtime.CompilerServices.YieldAwaitable/YieldAwaiter::s_sendOrPostCallbackRunAction
	SendOrPostCallback_t6B7334CE017AF595535507519400AC02D688DC3C * ___s_sendOrPostCallbackRunAction_1;

public:
	inline static int32_t get_offset_of_s_waitCallbackRunAction_0() { return static_cast<int32_t>(offsetof(YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE_StaticFields, ___s_waitCallbackRunAction_0)); }
	inline WaitCallback_t82C85517E973DCC6390AFB0BC3C2276F3328A319 * get_s_waitCallbackRunAction_0() const { return ___s_waitCallbackRunAction_0; }
	inline WaitCallback_t82C85517E973DCC6390AFB0BC3C2276F3328A319 ** get_address_of_s_waitCallbackRunAction_0() { return &___s_waitCallbackRunAction_0; }
	inline void set_s_waitCallbackRunAction_0(WaitCallback_t82C85517E973DCC6390AFB0BC3C2276F3328A319 * value)
	{
		___s_waitCallbackRunAction_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_waitCallbackRunAction_0), (void*)value);
	}

	inline static int32_t get_offset_of_s_sendOrPostCallbackRunAction_1() { return static_cast<int32_t>(offsetof(YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE_StaticFields, ___s_sendOrPostCallbackRunAction_1)); }
	inline SendOrPostCallback_t6B7334CE017AF595535507519400AC02D688DC3C * get_s_sendOrPostCallbackRunAction_1() const { return ___s_sendOrPostCallbackRunAction_1; }
	inline SendOrPostCallback_t6B7334CE017AF595535507519400AC02D688DC3C ** get_address_of_s_sendOrPostCallbackRunAction_1() { return &___s_sendOrPostCallbackRunAction_1; }
	inline void set_s_sendOrPostCallbackRunAction_1(SendOrPostCallback_t6B7334CE017AF595535507519400AC02D688DC3C * value)
	{
		___s_sendOrPostCallbackRunAction_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_sendOrPostCallbackRunAction_1), (void*)value);
	}
};


// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Threading.Tasks.VoidTaskResult>
struct AsyncTaskMethodBuilder_1_t3E10C35B53D8718724E2BF748600FB762F4719AD 
{
public:
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_coreState
	AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34  ___m_coreState_1;
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_task
	Task_1_t65FD5EE287B61746F015BBC8E90A97D38D258FB3 * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t3E10C35B53D8718724E2BF748600FB762F4719AD, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t2C85055E04767C52B9F66144476FCBF500DBFA34  value)
	{
		___m_coreState_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_coreState_1))->___m_stateMachine_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_coreState_1))->___m_defaultContextAction_1), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t3E10C35B53D8718724E2BF748600FB762F4719AD, ___m_task_2)); }
	inline Task_1_t65FD5EE287B61746F015BBC8E90A97D38D258FB3 * get_m_task_2() const { return ___m_task_2; }
	inline Task_1_t65FD5EE287B61746F015BBC8E90A97D38D258FB3 ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_1_t65FD5EE287B61746F015BBC8E90A97D38D258FB3 * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_task_2), (void*)value);
	}
};

struct AsyncTaskMethodBuilder_1_t3E10C35B53D8718724E2BF748600FB762F4719AD_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::s_defaultResultTask
	Task_1_t65FD5EE287B61746F015BBC8E90A97D38D258FB3 * ___s_defaultResultTask_0;

public:
	inline static int32_t get_offset_of_s_defaultResultTask_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t3E10C35B53D8718724E2BF748600FB762F4719AD_StaticFields, ___s_defaultResultTask_0)); }
	inline Task_1_t65FD5EE287B61746F015BBC8E90A97D38D258FB3 * get_s_defaultResultTask_0() const { return ___s_defaultResultTask_0; }
	inline Task_1_t65FD5EE287B61746F015BBC8E90A97D38D258FB3 ** get_address_of_s_defaultResultTask_0() { return &___s_defaultResultTask_0; }
	inline void set_s_defaultResultTask_0(Task_1_t65FD5EE287B61746F015BBC8E90A97D38D258FB3 * value)
	{
		___s_defaultResultTask_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_defaultResultTask_0), (void*)value);
	}
};


// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0
struct U3CU3Ec__DisplayClass6_0_tBD455AA807525EA305559C96A573EAAA5D3B91BC  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0::target
	Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * ___target_0;
	// System.Single DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0::startPosY
	float ___startPosY_1;
	// System.Boolean DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0::offsetYSet
	bool ___offsetYSet_2;
	// System.Single DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0::offsetY
	float ___offsetY_3;
	// DG.Tweening.Sequence DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0::s
	Sequence_tE01FFFCA34A537CE2FF32EDAF451CDEC55A1399E * ___s_4;
	// UnityEngine.Vector3 DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0::endValue
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___endValue_5;
	// DG.Tweening.Tween DG.Tweening.DOTweenModulePhysics/<>c__DisplayClass6_0::yTween
	Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___yTween_6;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_tBD455AA807525EA305559C96A573EAAA5D3B91BC, ___target_0)); }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * get_target_0() const { return ___target_0; }
	inline Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody_t101F2E2F9F16E765A77429B2DE4527D2047A887A * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}

	inline static int32_t get_offset_of_startPosY_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_tBD455AA807525EA305559C96A573EAAA5D3B91BC, ___startPosY_1)); }
	inline float get_startPosY_1() const { return ___startPosY_1; }
	inline float* get_address_of_startPosY_1() { return &___startPosY_1; }
	inline void set_startPosY_1(float value)
	{
		___startPosY_1 = value;
	}

	inline static int32_t get_offset_of_offsetYSet_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_tBD455AA807525EA305559C96A573EAAA5D3B91BC, ___offsetYSet_2)); }
	inline bool get_offsetYSet_2() const { return ___offsetYSet_2; }
	inline bool* get_address_of_offsetYSet_2() { return &___offsetYSet_2; }
	inline void set_offsetYSet_2(bool value)
	{
		___offsetYSet_2 = value;
	}

	inline static int32_t get_offset_of_offsetY_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_tBD455AA807525EA305559C96A573EAAA5D3B91BC, ___offsetY_3)); }
	inline float get_offsetY_3() const { return ___offsetY_3; }
	inline float* get_address_of_offsetY_3() { return &___offsetY_3; }
	inline void set_offsetY_3(float value)
	{
		___offsetY_3 = value;
	}

	inline static int32_t get_offset_of_s_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_tBD455AA807525EA305559C96A573EAAA5D3B91BC, ___s_4)); }
	inline Sequence_tE01FFFCA34A537CE2FF32EDAF451CDEC55A1399E * get_s_4() const { return ___s_4; }
	inline Sequence_tE01FFFCA34A537CE2FF32EDAF451CDEC55A1399E ** get_address_of_s_4() { return &___s_4; }
	inline void set_s_4(Sequence_tE01FFFCA34A537CE2FF32EDAF451CDEC55A1399E * value)
	{
		___s_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_4), (void*)value);
	}

	inline static int32_t get_offset_of_endValue_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_tBD455AA807525EA305559C96A573EAAA5D3B91BC, ___endValue_5)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_endValue_5() const { return ___endValue_5; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_endValue_5() { return &___endValue_5; }
	inline void set_endValue_5(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___endValue_5 = value;
	}

	inline static int32_t get_offset_of_yTween_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_tBD455AA807525EA305559C96A573EAAA5D3B91BC, ___yTween_6)); }
	inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * get_yTween_6() const { return ___yTween_6; }
	inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 ** get_address_of_yTween_6() { return &___yTween_6; }
	inline void set_yTween_6(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * value)
	{
		___yTween_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___yTween_6), (void*)value);
	}
};


// DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0
struct U3CU3Ec__DisplayClass4_0_t5D8B5391F9E829555C0E3D5C13C690EE1FC0882B  : public RuntimeObject
{
public:
	// UnityEngine.Rigidbody2D DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::target
	Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * ___target_0;
	// System.Single DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::startPosY
	float ___startPosY_1;
	// System.Boolean DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::offsetYSet
	bool ___offsetYSet_2;
	// System.Single DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::offsetY
	float ___offsetY_3;
	// DG.Tweening.Sequence DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::s
	Sequence_tE01FFFCA34A537CE2FF32EDAF451CDEC55A1399E * ___s_4;
	// UnityEngine.Vector2 DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::endValue
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___endValue_5;
	// DG.Tweening.Tween DG.Tweening.DOTweenModulePhysics2D/<>c__DisplayClass4_0::yTween
	Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___yTween_6;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t5D8B5391F9E829555C0E3D5C13C690EE1FC0882B, ___target_0)); }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * get_target_0() const { return ___target_0; }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}

	inline static int32_t get_offset_of_startPosY_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t5D8B5391F9E829555C0E3D5C13C690EE1FC0882B, ___startPosY_1)); }
	inline float get_startPosY_1() const { return ___startPosY_1; }
	inline float* get_address_of_startPosY_1() { return &___startPosY_1; }
	inline void set_startPosY_1(float value)
	{
		___startPosY_1 = value;
	}

	inline static int32_t get_offset_of_offsetYSet_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t5D8B5391F9E829555C0E3D5C13C690EE1FC0882B, ___offsetYSet_2)); }
	inline bool get_offsetYSet_2() const { return ___offsetYSet_2; }
	inline bool* get_address_of_offsetYSet_2() { return &___offsetYSet_2; }
	inline void set_offsetYSet_2(bool value)
	{
		___offsetYSet_2 = value;
	}

	inline static int32_t get_offset_of_offsetY_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t5D8B5391F9E829555C0E3D5C13C690EE1FC0882B, ___offsetY_3)); }
	inline float get_offsetY_3() const { return ___offsetY_3; }
	inline float* get_address_of_offsetY_3() { return &___offsetY_3; }
	inline void set_offsetY_3(float value)
	{
		___offsetY_3 = value;
	}

	inline static int32_t get_offset_of_s_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t5D8B5391F9E829555C0E3D5C13C690EE1FC0882B, ___s_4)); }
	inline Sequence_tE01FFFCA34A537CE2FF32EDAF451CDEC55A1399E * get_s_4() const { return ___s_4; }
	inline Sequence_tE01FFFCA34A537CE2FF32EDAF451CDEC55A1399E ** get_address_of_s_4() { return &___s_4; }
	inline void set_s_4(Sequence_tE01FFFCA34A537CE2FF32EDAF451CDEC55A1399E * value)
	{
		___s_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_4), (void*)value);
	}

	inline static int32_t get_offset_of_endValue_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t5D8B5391F9E829555C0E3D5C13C690EE1FC0882B, ___endValue_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_endValue_5() const { return ___endValue_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_endValue_5() { return &___endValue_5; }
	inline void set_endValue_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___endValue_5 = value;
	}

	inline static int32_t get_offset_of_yTween_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t5D8B5391F9E829555C0E3D5C13C690EE1FC0882B, ___yTween_6)); }
	inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * get_yTween_6() const { return ___yTween_6; }
	inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 ** get_address_of_yTween_6() { return &___yTween_6; }
	inline void set_yTween_6(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * value)
	{
		___yTween_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___yTween_6), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass3_0
struct U3CU3Ec__DisplayClass3_0_t09C0D33A30486B259B9B120ED13EB7DC5BE67D21  : public RuntimeObject
{
public:
	// UnityEngine.Color DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass3_0::to
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___to_0;
	// UnityEngine.SpriteRenderer DG.Tweening.DOTweenModuleSprite/<>c__DisplayClass3_0::target
	SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t09C0D33A30486B259B9B120ED13EB7DC5BE67D21, ___to_0)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_to_0() const { return ___to_0; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t09C0D33A30486B259B9B120ED13EB7DC5BE67D21, ___target_1)); }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * get_target_1() const { return ___target_1; }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_1), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0
struct U3CU3Ec__DisplayClass29_0_tC211D657D2FF1145B447BE8AFC7BFE9BF7B559FC  : public RuntimeObject
{
public:
	// UnityEngine.RectTransform DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::target
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___target_0;
	// System.Single DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::startPosY
	float ___startPosY_1;
	// System.Boolean DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::offsetYSet
	bool ___offsetYSet_2;
	// System.Single DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::offsetY
	float ___offsetY_3;
	// DG.Tweening.Sequence DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::s
	Sequence_tE01FFFCA34A537CE2FF32EDAF451CDEC55A1399E * ___s_4;
	// UnityEngine.Vector2 DG.Tweening.DOTweenModuleUI/<>c__DisplayClass29_0::endValue
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___endValue_5;

public:
	inline static int32_t get_offset_of_target_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_tC211D657D2FF1145B447BE8AFC7BFE9BF7B559FC, ___target_0)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_target_0() const { return ___target_0; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_target_0() { return &___target_0; }
	inline void set_target_0(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___target_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_0), (void*)value);
	}

	inline static int32_t get_offset_of_startPosY_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_tC211D657D2FF1145B447BE8AFC7BFE9BF7B559FC, ___startPosY_1)); }
	inline float get_startPosY_1() const { return ___startPosY_1; }
	inline float* get_address_of_startPosY_1() { return &___startPosY_1; }
	inline void set_startPosY_1(float value)
	{
		___startPosY_1 = value;
	}

	inline static int32_t get_offset_of_offsetYSet_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_tC211D657D2FF1145B447BE8AFC7BFE9BF7B559FC, ___offsetYSet_2)); }
	inline bool get_offsetYSet_2() const { return ___offsetYSet_2; }
	inline bool* get_address_of_offsetYSet_2() { return &___offsetYSet_2; }
	inline void set_offsetYSet_2(bool value)
	{
		___offsetYSet_2 = value;
	}

	inline static int32_t get_offset_of_offsetY_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_tC211D657D2FF1145B447BE8AFC7BFE9BF7B559FC, ___offsetY_3)); }
	inline float get_offsetY_3() const { return ___offsetY_3; }
	inline float* get_address_of_offsetY_3() { return &___offsetY_3; }
	inline void set_offsetY_3(float value)
	{
		___offsetY_3 = value;
	}

	inline static int32_t get_offset_of_s_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_tC211D657D2FF1145B447BE8AFC7BFE9BF7B559FC, ___s_4)); }
	inline Sequence_tE01FFFCA34A537CE2FF32EDAF451CDEC55A1399E * get_s_4() const { return ___s_4; }
	inline Sequence_tE01FFFCA34A537CE2FF32EDAF451CDEC55A1399E ** get_address_of_s_4() { return &___s_4; }
	inline void set_s_4(Sequence_tE01FFFCA34A537CE2FF32EDAF451CDEC55A1399E * value)
	{
		___s_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_4), (void*)value);
	}

	inline static int32_t get_offset_of_endValue_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_tC211D657D2FF1145B447BE8AFC7BFE9BF7B559FC, ___endValue_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_endValue_5() const { return ___endValue_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_endValue_5() { return &___endValue_5; }
	inline void set_endValue_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___endValue_5 = value;
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass38_0
struct U3CU3Ec__DisplayClass38_0_tC895FF3B6BF5E3B981757EEEEF7B92DF8921CEF3  : public RuntimeObject
{
public:
	// UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass38_0::to
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___to_0;
	// UnityEngine.UI.Graphic DG.Tweening.DOTweenModuleUI/<>c__DisplayClass38_0::target
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_tC895FF3B6BF5E3B981757EEEEF7B92DF8921CEF3, ___to_0)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_to_0() const { return ___to_0; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_tC895FF3B6BF5E3B981757EEEEF7B92DF8921CEF3, ___target_1)); }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * get_target_1() const { return ___target_1; }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_1), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass39_0
struct U3CU3Ec__DisplayClass39_0_t3B390AF92BA6587233EB45EA510C0B964CEA5E85  : public RuntimeObject
{
public:
	// UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass39_0::to
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___to_0;
	// UnityEngine.UI.Image DG.Tweening.DOTweenModuleUI/<>c__DisplayClass39_0::target
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass39_0_t3B390AF92BA6587233EB45EA510C0B964CEA5E85, ___to_0)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_to_0() const { return ___to_0; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass39_0_t3B390AF92BA6587233EB45EA510C0B964CEA5E85, ___target_1)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_target_1() const { return ___target_1; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_1), (void*)value);
	}
};


// DG.Tweening.DOTweenModuleUI/<>c__DisplayClass40_0
struct U3CU3Ec__DisplayClass40_0_t9AC2D818C1A29DD006244E0FD140AF47F6063D75  : public RuntimeObject
{
public:
	// UnityEngine.Color DG.Tweening.DOTweenModuleUI/<>c__DisplayClass40_0::to
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___to_0;
	// UnityEngine.UI.Text DG.Tweening.DOTweenModuleUI/<>c__DisplayClass40_0::target
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___target_1;

public:
	inline static int32_t get_offset_of_to_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass40_0_t9AC2D818C1A29DD006244E0FD140AF47F6063D75, ___to_0)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_to_0() const { return ___to_0; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_to_0() { return &___to_0; }
	inline void set_to_0(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___to_0 = value;
	}

	inline static int32_t get_offset_of_target_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass40_0_t9AC2D818C1A29DD006244E0FD140AF47F6063D75, ___target_1)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_target_1() const { return ___target_1; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_target_1() { return &___target_1; }
	inline void set_target_1(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___target_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___target_1), (void*)value);
	}
};


// System.Runtime.CompilerServices.AsyncTaskMethodBuilder
struct AsyncTaskMethodBuilder_t7A010673279CD8726E70047F1D15B3D17C56503B 
{
public:
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Threading.Tasks.VoidTaskResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder::m_builder
	AsyncTaskMethodBuilder_1_t3E10C35B53D8718724E2BF748600FB762F4719AD  ___m_builder_1;

public:
	inline static int32_t get_offset_of_m_builder_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_t7A010673279CD8726E70047F1D15B3D17C56503B, ___m_builder_1)); }
	inline AsyncTaskMethodBuilder_1_t3E10C35B53D8718724E2BF748600FB762F4719AD  get_m_builder_1() const { return ___m_builder_1; }
	inline AsyncTaskMethodBuilder_1_t3E10C35B53D8718724E2BF748600FB762F4719AD * get_address_of_m_builder_1() { return &___m_builder_1; }
	inline void set_m_builder_1(AsyncTaskMethodBuilder_1_t3E10C35B53D8718724E2BF748600FB762F4719AD  value)
	{
		___m_builder_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_builder_1))->___m_coreState_1))->___m_stateMachine_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___m_builder_1))->___m_coreState_1))->___m_defaultContextAction_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_builder_1))->___m_task_2), (void*)NULL);
		#endif
	}
};

struct AsyncTaskMethodBuilder_t7A010673279CD8726E70047F1D15B3D17C56503B_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder::s_cachedCompleted
	Task_1_t65FD5EE287B61746F015BBC8E90A97D38D258FB3 * ___s_cachedCompleted_0;

public:
	inline static int32_t get_offset_of_s_cachedCompleted_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_t7A010673279CD8726E70047F1D15B3D17C56503B_StaticFields, ___s_cachedCompleted_0)); }
	inline Task_1_t65FD5EE287B61746F015BBC8E90A97D38D258FB3 * get_s_cachedCompleted_0() const { return ___s_cachedCompleted_0; }
	inline Task_1_t65FD5EE287B61746F015BBC8E90A97D38D258FB3 ** get_address_of_s_cachedCompleted_0() { return &___s_cachedCompleted_0; }
	inline void set_s_cachedCompleted_0(Task_1_t65FD5EE287B61746F015BBC8E90A97D38D258FB3 * value)
	{
		___s_cachedCompleted_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_cachedCompleted_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncTaskMethodBuilder
struct AsyncTaskMethodBuilder_t7A010673279CD8726E70047F1D15B3D17C56503B_marshaled_pinvoke
{
	AsyncTaskMethodBuilder_1_t3E10C35B53D8718724E2BF748600FB762F4719AD  ___m_builder_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncTaskMethodBuilder
struct AsyncTaskMethodBuilder_t7A010673279CD8726E70047F1D15B3D17C56503B_marshaled_com
{
	AsyncTaskMethodBuilder_1_t3E10C35B53D8718724E2BF748600FB762F4719AD  ___m_builder_1;
};

// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForCompletion>d__10
struct U3CAsyncWaitForCompletionU3Ed__10_t3E0E42DB84F8756703D33D722892C9AA4B47BE05 
{
public:
	// System.Int32 DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForCompletion>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForCompletion>d__10::<>t__builder
	AsyncTaskMethodBuilder_t7A010673279CD8726E70047F1D15B3D17C56503B  ___U3CU3Et__builder_1;
	// DG.Tweening.Tween DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForCompletion>d__10::t
	Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t_2;
	// System.Runtime.CompilerServices.YieldAwaitable/YieldAwaiter DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForCompletion>d__10::<>u__1
	YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE  ___U3CU3Eu__1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAsyncWaitForCompletionU3Ed__10_t3E0E42DB84F8756703D33D722892C9AA4B47BE05, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CAsyncWaitForCompletionU3Ed__10_t3E0E42DB84F8756703D33D722892C9AA4B47BE05, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t7A010673279CD8726E70047F1D15B3D17C56503B  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t7A010673279CD8726E70047F1D15B3D17C56503B * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t7A010673279CD8726E70047F1D15B3D17C56503B  value)
	{
		___U3CU3Et__builder_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___U3CU3Et__builder_1))->___m_builder_1))->___m_coreState_1))->___m_stateMachine_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___U3CU3Et__builder_1))->___m_builder_1))->___m_coreState_1))->___m_defaultContextAction_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___U3CU3Et__builder_1))->___m_builder_1))->___m_task_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_t_2() { return static_cast<int32_t>(offsetof(U3CAsyncWaitForCompletionU3Ed__10_t3E0E42DB84F8756703D33D722892C9AA4B47BE05, ___t_2)); }
	inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * get_t_2() const { return ___t_2; }
	inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 ** get_address_of_t_2() { return &___t_2; }
	inline void set_t_2(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * value)
	{
		___t_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_3() { return static_cast<int32_t>(offsetof(U3CAsyncWaitForCompletionU3Ed__10_t3E0E42DB84F8756703D33D722892C9AA4B47BE05, ___U3CU3Eu__1_3)); }
	inline YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE  get_U3CU3Eu__1_3() const { return ___U3CU3Eu__1_3; }
	inline YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE * get_address_of_U3CU3Eu__1_3() { return &___U3CU3Eu__1_3; }
	inline void set_U3CU3Eu__1_3(YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE  value)
	{
		___U3CU3Eu__1_3 = value;
	}
};


// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForElapsedLoops>d__13
struct U3CAsyncWaitForElapsedLoopsU3Ed__13_t02CD6395191734801D7559C69A870C35E3F5BAA4 
{
public:
	// System.Int32 DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForElapsedLoops>d__13::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForElapsedLoops>d__13::<>t__builder
	AsyncTaskMethodBuilder_t7A010673279CD8726E70047F1D15B3D17C56503B  ___U3CU3Et__builder_1;
	// DG.Tweening.Tween DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForElapsedLoops>d__13::t
	Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t_2;
	// System.Int32 DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForElapsedLoops>d__13::elapsedLoops
	int32_t ___elapsedLoops_3;
	// System.Runtime.CompilerServices.YieldAwaitable/YieldAwaiter DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForElapsedLoops>d__13::<>u__1
	YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE  ___U3CU3Eu__1_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAsyncWaitForElapsedLoopsU3Ed__13_t02CD6395191734801D7559C69A870C35E3F5BAA4, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CAsyncWaitForElapsedLoopsU3Ed__13_t02CD6395191734801D7559C69A870C35E3F5BAA4, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t7A010673279CD8726E70047F1D15B3D17C56503B  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t7A010673279CD8726E70047F1D15B3D17C56503B * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t7A010673279CD8726E70047F1D15B3D17C56503B  value)
	{
		___U3CU3Et__builder_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___U3CU3Et__builder_1))->___m_builder_1))->___m_coreState_1))->___m_stateMachine_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___U3CU3Et__builder_1))->___m_builder_1))->___m_coreState_1))->___m_defaultContextAction_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___U3CU3Et__builder_1))->___m_builder_1))->___m_task_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_t_2() { return static_cast<int32_t>(offsetof(U3CAsyncWaitForElapsedLoopsU3Ed__13_t02CD6395191734801D7559C69A870C35E3F5BAA4, ___t_2)); }
	inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * get_t_2() const { return ___t_2; }
	inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 ** get_address_of_t_2() { return &___t_2; }
	inline void set_t_2(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * value)
	{
		___t_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_2), (void*)value);
	}

	inline static int32_t get_offset_of_elapsedLoops_3() { return static_cast<int32_t>(offsetof(U3CAsyncWaitForElapsedLoopsU3Ed__13_t02CD6395191734801D7559C69A870C35E3F5BAA4, ___elapsedLoops_3)); }
	inline int32_t get_elapsedLoops_3() const { return ___elapsedLoops_3; }
	inline int32_t* get_address_of_elapsedLoops_3() { return &___elapsedLoops_3; }
	inline void set_elapsedLoops_3(int32_t value)
	{
		___elapsedLoops_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_4() { return static_cast<int32_t>(offsetof(U3CAsyncWaitForElapsedLoopsU3Ed__13_t02CD6395191734801D7559C69A870C35E3F5BAA4, ___U3CU3Eu__1_4)); }
	inline YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE  get_U3CU3Eu__1_4() const { return ___U3CU3Eu__1_4; }
	inline YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE * get_address_of_U3CU3Eu__1_4() { return &___U3CU3Eu__1_4; }
	inline void set_U3CU3Eu__1_4(YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE  value)
	{
		___U3CU3Eu__1_4 = value;
	}
};


// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForKill>d__12
struct U3CAsyncWaitForKillU3Ed__12_tE01D14FE8C3DA6A6E7AE41CFBCA93F0006EEC74C 
{
public:
	// System.Int32 DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForKill>d__12::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForKill>d__12::<>t__builder
	AsyncTaskMethodBuilder_t7A010673279CD8726E70047F1D15B3D17C56503B  ___U3CU3Et__builder_1;
	// DG.Tweening.Tween DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForKill>d__12::t
	Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t_2;
	// System.Runtime.CompilerServices.YieldAwaitable/YieldAwaiter DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForKill>d__12::<>u__1
	YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE  ___U3CU3Eu__1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAsyncWaitForKillU3Ed__12_tE01D14FE8C3DA6A6E7AE41CFBCA93F0006EEC74C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CAsyncWaitForKillU3Ed__12_tE01D14FE8C3DA6A6E7AE41CFBCA93F0006EEC74C, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t7A010673279CD8726E70047F1D15B3D17C56503B  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t7A010673279CD8726E70047F1D15B3D17C56503B * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t7A010673279CD8726E70047F1D15B3D17C56503B  value)
	{
		___U3CU3Et__builder_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___U3CU3Et__builder_1))->___m_builder_1))->___m_coreState_1))->___m_stateMachine_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___U3CU3Et__builder_1))->___m_builder_1))->___m_coreState_1))->___m_defaultContextAction_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___U3CU3Et__builder_1))->___m_builder_1))->___m_task_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_t_2() { return static_cast<int32_t>(offsetof(U3CAsyncWaitForKillU3Ed__12_tE01D14FE8C3DA6A6E7AE41CFBCA93F0006EEC74C, ___t_2)); }
	inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * get_t_2() const { return ___t_2; }
	inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 ** get_address_of_t_2() { return &___t_2; }
	inline void set_t_2(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * value)
	{
		___t_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_3() { return static_cast<int32_t>(offsetof(U3CAsyncWaitForKillU3Ed__12_tE01D14FE8C3DA6A6E7AE41CFBCA93F0006EEC74C, ___U3CU3Eu__1_3)); }
	inline YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE  get_U3CU3Eu__1_3() const { return ___U3CU3Eu__1_3; }
	inline YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE * get_address_of_U3CU3Eu__1_3() { return &___U3CU3Eu__1_3; }
	inline void set_U3CU3Eu__1_3(YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE  value)
	{
		___U3CU3Eu__1_3 = value;
	}
};


// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForPosition>d__14
struct U3CAsyncWaitForPositionU3Ed__14_t967D3DD670C2DF6ED3578D83496A2EA55E2399EF 
{
public:
	// System.Int32 DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForPosition>d__14::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForPosition>d__14::<>t__builder
	AsyncTaskMethodBuilder_t7A010673279CD8726E70047F1D15B3D17C56503B  ___U3CU3Et__builder_1;
	// DG.Tweening.Tween DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForPosition>d__14::t
	Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t_2;
	// System.Single DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForPosition>d__14::position
	float ___position_3;
	// System.Runtime.CompilerServices.YieldAwaitable/YieldAwaiter DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForPosition>d__14::<>u__1
	YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE  ___U3CU3Eu__1_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAsyncWaitForPositionU3Ed__14_t967D3DD670C2DF6ED3578D83496A2EA55E2399EF, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CAsyncWaitForPositionU3Ed__14_t967D3DD670C2DF6ED3578D83496A2EA55E2399EF, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t7A010673279CD8726E70047F1D15B3D17C56503B  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t7A010673279CD8726E70047F1D15B3D17C56503B * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t7A010673279CD8726E70047F1D15B3D17C56503B  value)
	{
		___U3CU3Et__builder_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___U3CU3Et__builder_1))->___m_builder_1))->___m_coreState_1))->___m_stateMachine_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___U3CU3Et__builder_1))->___m_builder_1))->___m_coreState_1))->___m_defaultContextAction_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___U3CU3Et__builder_1))->___m_builder_1))->___m_task_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_t_2() { return static_cast<int32_t>(offsetof(U3CAsyncWaitForPositionU3Ed__14_t967D3DD670C2DF6ED3578D83496A2EA55E2399EF, ___t_2)); }
	inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * get_t_2() const { return ___t_2; }
	inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 ** get_address_of_t_2() { return &___t_2; }
	inline void set_t_2(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * value)
	{
		___t_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_2), (void*)value);
	}

	inline static int32_t get_offset_of_position_3() { return static_cast<int32_t>(offsetof(U3CAsyncWaitForPositionU3Ed__14_t967D3DD670C2DF6ED3578D83496A2EA55E2399EF, ___position_3)); }
	inline float get_position_3() const { return ___position_3; }
	inline float* get_address_of_position_3() { return &___position_3; }
	inline void set_position_3(float value)
	{
		___position_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_4() { return static_cast<int32_t>(offsetof(U3CAsyncWaitForPositionU3Ed__14_t967D3DD670C2DF6ED3578D83496A2EA55E2399EF, ___U3CU3Eu__1_4)); }
	inline YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE  get_U3CU3Eu__1_4() const { return ___U3CU3Eu__1_4; }
	inline YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE * get_address_of_U3CU3Eu__1_4() { return &___U3CU3Eu__1_4; }
	inline void set_U3CU3Eu__1_4(YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE  value)
	{
		___U3CU3Eu__1_4 = value;
	}
};


// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForRewind>d__11
struct U3CAsyncWaitForRewindU3Ed__11_tF0EC64BBA722E64EB39CEE1AF064F9C86C99D394 
{
public:
	// System.Int32 DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForRewind>d__11::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForRewind>d__11::<>t__builder
	AsyncTaskMethodBuilder_t7A010673279CD8726E70047F1D15B3D17C56503B  ___U3CU3Et__builder_1;
	// DG.Tweening.Tween DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForRewind>d__11::t
	Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t_2;
	// System.Runtime.CompilerServices.YieldAwaitable/YieldAwaiter DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForRewind>d__11::<>u__1
	YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE  ___U3CU3Eu__1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAsyncWaitForRewindU3Ed__11_tF0EC64BBA722E64EB39CEE1AF064F9C86C99D394, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CAsyncWaitForRewindU3Ed__11_tF0EC64BBA722E64EB39CEE1AF064F9C86C99D394, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t7A010673279CD8726E70047F1D15B3D17C56503B  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t7A010673279CD8726E70047F1D15B3D17C56503B * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t7A010673279CD8726E70047F1D15B3D17C56503B  value)
	{
		___U3CU3Et__builder_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___U3CU3Et__builder_1))->___m_builder_1))->___m_coreState_1))->___m_stateMachine_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___U3CU3Et__builder_1))->___m_builder_1))->___m_coreState_1))->___m_defaultContextAction_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___U3CU3Et__builder_1))->___m_builder_1))->___m_task_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_t_2() { return static_cast<int32_t>(offsetof(U3CAsyncWaitForRewindU3Ed__11_tF0EC64BBA722E64EB39CEE1AF064F9C86C99D394, ___t_2)); }
	inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * get_t_2() const { return ___t_2; }
	inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 ** get_address_of_t_2() { return &___t_2; }
	inline void set_t_2(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * value)
	{
		___t_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_3() { return static_cast<int32_t>(offsetof(U3CAsyncWaitForRewindU3Ed__11_tF0EC64BBA722E64EB39CEE1AF064F9C86C99D394, ___U3CU3Eu__1_3)); }
	inline YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE  get_U3CU3Eu__1_3() const { return ___U3CU3Eu__1_3; }
	inline YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE * get_address_of_U3CU3Eu__1_3() { return &___U3CU3Eu__1_3; }
	inline void set_U3CU3Eu__1_3(YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE  value)
	{
		___U3CU3Eu__1_3 = value;
	}
};


// DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForStart>d__15
struct U3CAsyncWaitForStartU3Ed__15_tF788606B86239515A56729BADA889C7FA953D434 
{
public:
	// System.Int32 DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForStart>d__15::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForStart>d__15::<>t__builder
	AsyncTaskMethodBuilder_t7A010673279CD8726E70047F1D15B3D17C56503B  ___U3CU3Et__builder_1;
	// DG.Tweening.Tween DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForStart>d__15::t
	Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * ___t_2;
	// System.Runtime.CompilerServices.YieldAwaitable/YieldAwaiter DG.Tweening.DOTweenModuleUnityVersion/<AsyncWaitForStart>d__15::<>u__1
	YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE  ___U3CU3Eu__1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CAsyncWaitForStartU3Ed__15_tF788606B86239515A56729BADA889C7FA953D434, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CAsyncWaitForStartU3Ed__15_tF788606B86239515A56729BADA889C7FA953D434, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t7A010673279CD8726E70047F1D15B3D17C56503B  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t7A010673279CD8726E70047F1D15B3D17C56503B * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t7A010673279CD8726E70047F1D15B3D17C56503B  value)
	{
		___U3CU3Et__builder_1 = value;
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___U3CU3Et__builder_1))->___m_builder_1))->___m_coreState_1))->___m_stateMachine_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&((&(((&___U3CU3Et__builder_1))->___m_builder_1))->___m_coreState_1))->___m_defaultContextAction_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((&(((&___U3CU3Et__builder_1))->___m_builder_1))->___m_task_2), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_t_2() { return static_cast<int32_t>(offsetof(U3CAsyncWaitForStartU3Ed__15_tF788606B86239515A56729BADA889C7FA953D434, ___t_2)); }
	inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * get_t_2() const { return ___t_2; }
	inline Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 ** get_address_of_t_2() { return &___t_2; }
	inline void set_t_2(Tween_tF17E40B3AD65D4E0C7E78D43A9224C6A1FC4C941 * value)
	{
		___t_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___t_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_3() { return static_cast<int32_t>(offsetof(U3CAsyncWaitForStartU3Ed__15_tF788606B86239515A56729BADA889C7FA953D434, ___U3CU3Eu__1_3)); }
	inline YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE  get_U3CU3Eu__1_3() const { return ___U3CU3Eu__1_3; }
	inline YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE * get_address_of_U3CU3Eu__1_3() { return &___U3CU3Eu__1_3; }
	inline void set_U3CU3Eu__1_3(YieldAwaiter_t32B66444215FB828BBC8DF4984069FBDA1DC12FE  value)
	{
		___U3CU3Eu__1_3 = value;
	}
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// AllIn1SpriteShader.All1CreateUnifiedOutline
struct All1CreateUnifiedOutline_tBA1639F5738F0B8808AD83CAD699BA0CE3843871  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Material AllIn1SpriteShader.All1CreateUnifiedOutline::outlineMaterial
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___outlineMaterial_4;
	// UnityEngine.Transform AllIn1SpriteShader.All1CreateUnifiedOutline::outlineParentTransform
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___outlineParentTransform_5;
	// System.Int32 AllIn1SpriteShader.All1CreateUnifiedOutline::duplicateOrderInLayer
	int32_t ___duplicateOrderInLayer_6;
	// System.String AllIn1SpriteShader.All1CreateUnifiedOutline::duplicateSortingLayer
	String_t* ___duplicateSortingLayer_7;
	// System.Boolean AllIn1SpriteShader.All1CreateUnifiedOutline::createUnifiedOutline
	bool ___createUnifiedOutline_8;

public:
	inline static int32_t get_offset_of_outlineMaterial_4() { return static_cast<int32_t>(offsetof(All1CreateUnifiedOutline_tBA1639F5738F0B8808AD83CAD699BA0CE3843871, ___outlineMaterial_4)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_outlineMaterial_4() const { return ___outlineMaterial_4; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_outlineMaterial_4() { return &___outlineMaterial_4; }
	inline void set_outlineMaterial_4(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___outlineMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___outlineMaterial_4), (void*)value);
	}

	inline static int32_t get_offset_of_outlineParentTransform_5() { return static_cast<int32_t>(offsetof(All1CreateUnifiedOutline_tBA1639F5738F0B8808AD83CAD699BA0CE3843871, ___outlineParentTransform_5)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_outlineParentTransform_5() const { return ___outlineParentTransform_5; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_outlineParentTransform_5() { return &___outlineParentTransform_5; }
	inline void set_outlineParentTransform_5(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___outlineParentTransform_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___outlineParentTransform_5), (void*)value);
	}

	inline static int32_t get_offset_of_duplicateOrderInLayer_6() { return static_cast<int32_t>(offsetof(All1CreateUnifiedOutline_tBA1639F5738F0B8808AD83CAD699BA0CE3843871, ___duplicateOrderInLayer_6)); }
	inline int32_t get_duplicateOrderInLayer_6() const { return ___duplicateOrderInLayer_6; }
	inline int32_t* get_address_of_duplicateOrderInLayer_6() { return &___duplicateOrderInLayer_6; }
	inline void set_duplicateOrderInLayer_6(int32_t value)
	{
		___duplicateOrderInLayer_6 = value;
	}

	inline static int32_t get_offset_of_duplicateSortingLayer_7() { return static_cast<int32_t>(offsetof(All1CreateUnifiedOutline_tBA1639F5738F0B8808AD83CAD699BA0CE3843871, ___duplicateSortingLayer_7)); }
	inline String_t* get_duplicateSortingLayer_7() const { return ___duplicateSortingLayer_7; }
	inline String_t** get_address_of_duplicateSortingLayer_7() { return &___duplicateSortingLayer_7; }
	inline void set_duplicateSortingLayer_7(String_t* value)
	{
		___duplicateSortingLayer_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___duplicateSortingLayer_7), (void*)value);
	}

	inline static int32_t get_offset_of_createUnifiedOutline_8() { return static_cast<int32_t>(offsetof(All1CreateUnifiedOutline_tBA1639F5738F0B8808AD83CAD699BA0CE3843871, ___createUnifiedOutline_8)); }
	inline bool get_createUnifiedOutline_8() const { return ___createUnifiedOutline_8; }
	inline bool* get_address_of_createUnifiedOutline_8() { return &___createUnifiedOutline_8; }
	inline void set_createUnifiedOutline_8(bool value)
	{
		___createUnifiedOutline_8 = value;
	}
};


// AllIn1SpriteShader.All1DemoChangeScene
struct All1DemoChangeScene_t745D2B3D1324B9234417476AEFD290126D58822F  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// AllIn1SpriteShader.All1DemoClaw
struct All1DemoClaw_t5E199580EC962A3A1495325AB0B044AC39944E79  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Transform AllIn1SpriteShader.All1DemoClaw::claw1
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___claw1_4;
	// UnityEngine.Transform AllIn1SpriteShader.All1DemoClaw::claw2
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___claw2_5;
	// System.Single AllIn1SpriteShader.All1DemoClaw::startAngle
	float ___startAngle_6;
	// UnityEngine.Vector2 AllIn1SpriteShader.All1DemoClaw::speedRange
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___speedRange_7;
	// UnityEngine.Vector2 AllIn1SpriteShader.All1DemoClaw::maxRange
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___maxRange_8;
	// UnityEngine.Vector3 AllIn1SpriteShader.All1DemoClaw::eulerClaw1
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___eulerClaw1_9;
	// UnityEngine.Vector3 AllIn1SpriteShader.All1DemoClaw::eulerClaw2
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___eulerClaw2_10;
	// System.Single AllIn1SpriteShader.All1DemoClaw::rotationAmount
	float ___rotationAmount_11;
	// System.Single AllIn1SpriteShader.All1DemoClaw::rand
	float ___rand_12;
	// System.Single AllIn1SpriteShader.All1DemoClaw::range
	float ___range_13;
	// System.Single AllIn1SpriteShader.All1DemoClaw::speed
	float ___speed_14;

public:
	inline static int32_t get_offset_of_claw1_4() { return static_cast<int32_t>(offsetof(All1DemoClaw_t5E199580EC962A3A1495325AB0B044AC39944E79, ___claw1_4)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_claw1_4() const { return ___claw1_4; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_claw1_4() { return &___claw1_4; }
	inline void set_claw1_4(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___claw1_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___claw1_4), (void*)value);
	}

	inline static int32_t get_offset_of_claw2_5() { return static_cast<int32_t>(offsetof(All1DemoClaw_t5E199580EC962A3A1495325AB0B044AC39944E79, ___claw2_5)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_claw2_5() const { return ___claw2_5; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_claw2_5() { return &___claw2_5; }
	inline void set_claw2_5(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___claw2_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___claw2_5), (void*)value);
	}

	inline static int32_t get_offset_of_startAngle_6() { return static_cast<int32_t>(offsetof(All1DemoClaw_t5E199580EC962A3A1495325AB0B044AC39944E79, ___startAngle_6)); }
	inline float get_startAngle_6() const { return ___startAngle_6; }
	inline float* get_address_of_startAngle_6() { return &___startAngle_6; }
	inline void set_startAngle_6(float value)
	{
		___startAngle_6 = value;
	}

	inline static int32_t get_offset_of_speedRange_7() { return static_cast<int32_t>(offsetof(All1DemoClaw_t5E199580EC962A3A1495325AB0B044AC39944E79, ___speedRange_7)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_speedRange_7() const { return ___speedRange_7; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_speedRange_7() { return &___speedRange_7; }
	inline void set_speedRange_7(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___speedRange_7 = value;
	}

	inline static int32_t get_offset_of_maxRange_8() { return static_cast<int32_t>(offsetof(All1DemoClaw_t5E199580EC962A3A1495325AB0B044AC39944E79, ___maxRange_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_maxRange_8() const { return ___maxRange_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_maxRange_8() { return &___maxRange_8; }
	inline void set_maxRange_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___maxRange_8 = value;
	}

	inline static int32_t get_offset_of_eulerClaw1_9() { return static_cast<int32_t>(offsetof(All1DemoClaw_t5E199580EC962A3A1495325AB0B044AC39944E79, ___eulerClaw1_9)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_eulerClaw1_9() const { return ___eulerClaw1_9; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_eulerClaw1_9() { return &___eulerClaw1_9; }
	inline void set_eulerClaw1_9(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___eulerClaw1_9 = value;
	}

	inline static int32_t get_offset_of_eulerClaw2_10() { return static_cast<int32_t>(offsetof(All1DemoClaw_t5E199580EC962A3A1495325AB0B044AC39944E79, ___eulerClaw2_10)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_eulerClaw2_10() const { return ___eulerClaw2_10; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_eulerClaw2_10() { return &___eulerClaw2_10; }
	inline void set_eulerClaw2_10(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___eulerClaw2_10 = value;
	}

	inline static int32_t get_offset_of_rotationAmount_11() { return static_cast<int32_t>(offsetof(All1DemoClaw_t5E199580EC962A3A1495325AB0B044AC39944E79, ___rotationAmount_11)); }
	inline float get_rotationAmount_11() const { return ___rotationAmount_11; }
	inline float* get_address_of_rotationAmount_11() { return &___rotationAmount_11; }
	inline void set_rotationAmount_11(float value)
	{
		___rotationAmount_11 = value;
	}

	inline static int32_t get_offset_of_rand_12() { return static_cast<int32_t>(offsetof(All1DemoClaw_t5E199580EC962A3A1495325AB0B044AC39944E79, ___rand_12)); }
	inline float get_rand_12() const { return ___rand_12; }
	inline float* get_address_of_rand_12() { return &___rand_12; }
	inline void set_rand_12(float value)
	{
		___rand_12 = value;
	}

	inline static int32_t get_offset_of_range_13() { return static_cast<int32_t>(offsetof(All1DemoClaw_t5E199580EC962A3A1495325AB0B044AC39944E79, ___range_13)); }
	inline float get_range_13() const { return ___range_13; }
	inline float* get_address_of_range_13() { return &___range_13; }
	inline void set_range_13(float value)
	{
		___range_13 = value;
	}

	inline static int32_t get_offset_of_speed_14() { return static_cast<int32_t>(offsetof(All1DemoClaw_t5E199580EC962A3A1495325AB0B044AC39944E79, ___speed_14)); }
	inline float get_speed_14() const { return ___speed_14; }
	inline float* get_address_of_speed_14() { return &___speed_14; }
	inline void set_speed_14(float value)
	{
		___speed_14 = value;
	}
};


// AllIn1SpriteShader.All1DemoRandomZ
struct All1DemoRandomZ_tC82E43E979B4238AF1D7827924CA4CE745120AAB  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Vector2 AllIn1SpriteShader.All1DemoRandomZ::randomZRange
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___randomZRange_4;

public:
	inline static int32_t get_offset_of_randomZRange_4() { return static_cast<int32_t>(offsetof(All1DemoRandomZ_tC82E43E979B4238AF1D7827924CA4CE745120AAB, ___randomZRange_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_randomZRange_4() const { return ___randomZRange_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_randomZRange_4() { return &___randomZRange_4; }
	inline void set_randomZRange_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___randomZRange_4 = value;
	}
};


// AllIn1SpriteShader.All1DemoUpAndDown
struct All1DemoUpAndDown_t4ED240FF408287F1E3E22BBFC8CCB97947CBFE52  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Vector2 AllIn1SpriteShader.All1DemoUpAndDown::reachRange
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___reachRange_4;
	// UnityEngine.Vector2 AllIn1SpriteShader.All1DemoUpAndDown::speedRange
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___speedRange_5;
	// UnityEngine.Vector3 AllIn1SpriteShader.All1DemoUpAndDown::newPosition
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___newPosition_6;
	// UnityEngine.Vector3 AllIn1SpriteShader.All1DemoUpAndDown::startPos
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___startPos_7;
	// System.Single AllIn1SpriteShader.All1DemoUpAndDown::range
	float ___range_8;
	// System.Single AllIn1SpriteShader.All1DemoUpAndDown::positionOffset
	float ___positionOffset_9;
	// System.Single AllIn1SpriteShader.All1DemoUpAndDown::rand
	float ___rand_10;
	// System.Single AllIn1SpriteShader.All1DemoUpAndDown::speed
	float ___speed_11;

public:
	inline static int32_t get_offset_of_reachRange_4() { return static_cast<int32_t>(offsetof(All1DemoUpAndDown_t4ED240FF408287F1E3E22BBFC8CCB97947CBFE52, ___reachRange_4)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_reachRange_4() const { return ___reachRange_4; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_reachRange_4() { return &___reachRange_4; }
	inline void set_reachRange_4(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___reachRange_4 = value;
	}

	inline static int32_t get_offset_of_speedRange_5() { return static_cast<int32_t>(offsetof(All1DemoUpAndDown_t4ED240FF408287F1E3E22BBFC8CCB97947CBFE52, ___speedRange_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_speedRange_5() const { return ___speedRange_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_speedRange_5() { return &___speedRange_5; }
	inline void set_speedRange_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___speedRange_5 = value;
	}

	inline static int32_t get_offset_of_newPosition_6() { return static_cast<int32_t>(offsetof(All1DemoUpAndDown_t4ED240FF408287F1E3E22BBFC8CCB97947CBFE52, ___newPosition_6)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_newPosition_6() const { return ___newPosition_6; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_newPosition_6() { return &___newPosition_6; }
	inline void set_newPosition_6(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___newPosition_6 = value;
	}

	inline static int32_t get_offset_of_startPos_7() { return static_cast<int32_t>(offsetof(All1DemoUpAndDown_t4ED240FF408287F1E3E22BBFC8CCB97947CBFE52, ___startPos_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_startPos_7() const { return ___startPos_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_startPos_7() { return &___startPos_7; }
	inline void set_startPos_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___startPos_7 = value;
	}

	inline static int32_t get_offset_of_range_8() { return static_cast<int32_t>(offsetof(All1DemoUpAndDown_t4ED240FF408287F1E3E22BBFC8CCB97947CBFE52, ___range_8)); }
	inline float get_range_8() const { return ___range_8; }
	inline float* get_address_of_range_8() { return &___range_8; }
	inline void set_range_8(float value)
	{
		___range_8 = value;
	}

	inline static int32_t get_offset_of_positionOffset_9() { return static_cast<int32_t>(offsetof(All1DemoUpAndDown_t4ED240FF408287F1E3E22BBFC8CCB97947CBFE52, ___positionOffset_9)); }
	inline float get_positionOffset_9() const { return ___positionOffset_9; }
	inline float* get_address_of_positionOffset_9() { return &___positionOffset_9; }
	inline void set_positionOffset_9(float value)
	{
		___positionOffset_9 = value;
	}

	inline static int32_t get_offset_of_rand_10() { return static_cast<int32_t>(offsetof(All1DemoUpAndDown_t4ED240FF408287F1E3E22BBFC8CCB97947CBFE52, ___rand_10)); }
	inline float get_rand_10() const { return ___rand_10; }
	inline float* get_address_of_rand_10() { return &___rand_10; }
	inline void set_rand_10(float value)
	{
		___rand_10 = value;
	}

	inline static int32_t get_offset_of_speed_11() { return static_cast<int32_t>(offsetof(All1DemoUpAndDown_t4ED240FF408287F1E3E22BBFC8CCB97947CBFE52, ___speed_11)); }
	inline float get_speed_11() const { return ___speed_11; }
	inline float* get_address_of_speed_11() { return &___speed_11; }
	inline void set_speed_11(float value)
	{
		___speed_11 = value;
	}
};


// AllIn1SpriteShader.All1DemoUrpCamMove
struct All1DemoUrpCamMove_tB49185CA1616225AF2684AFD24826201D48E6134  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single AllIn1SpriteShader.All1DemoUrpCamMove::speed
	float ___speed_4;
	// UnityEngine.Vector2 AllIn1SpriteShader.All1DemoUrpCamMove::input
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___input_5;
	// UnityEngine.Rigidbody2D AllIn1SpriteShader.All1DemoUrpCamMove::rb
	Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * ___rb_6;

public:
	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(All1DemoUrpCamMove_tB49185CA1616225AF2684AFD24826201D48E6134, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_input_5() { return static_cast<int32_t>(offsetof(All1DemoUrpCamMove_tB49185CA1616225AF2684AFD24826201D48E6134, ___input_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_input_5() const { return ___input_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_input_5() { return &___input_5; }
	inline void set_input_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___input_5 = value;
	}

	inline static int32_t get_offset_of_rb_6() { return static_cast<int32_t>(offsetof(All1DemoUrpCamMove_tB49185CA1616225AF2684AFD24826201D48E6134, ___rb_6)); }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * get_rb_6() const { return ___rb_6; }
	inline Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 ** get_address_of_rb_6() { return &___rb_6; }
	inline void set_rb_6(Rigidbody2D_tD23204FEE9CB4A36737043B97FD409DE05D5DCE5 * value)
	{
		___rb_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___rb_6), (void*)value);
	}
};


// AllIn1SpriteShader.All1ShaderDemoController
struct All1ShaderDemoController_t66C22AC496E9A0D1FF0AB9CDB81DCE2DB60F485B  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// AllIn1SpriteShader.DemoCircleExpositor[] AllIn1SpriteShader.All1ShaderDemoController::expositors
	DemoCircleExpositorU5BU5D_tF00D5F6A0A10F7E6455A6B0E26C6172DCCD054A6* ___expositors_4;
	// UnityEngine.UI.Text AllIn1SpriteShader.All1ShaderDemoController::expositorsTitle
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___expositorsTitle_5;
	// UnityEngine.UI.Text AllIn1SpriteShader.All1ShaderDemoController::expositorsTitleOutline
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___expositorsTitleOutline_6;
	// System.Single AllIn1SpriteShader.All1ShaderDemoController::expositorDistance
	float ___expositorDistance_7;
	// System.Int32 AllIn1SpriteShader.All1ShaderDemoController::currExpositor
	int32_t ___currExpositor_8;
	// UnityEngine.GameObject AllIn1SpriteShader.All1ShaderDemoController::background
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___background_9;
	// UnityEngine.Material AllIn1SpriteShader.All1ShaderDemoController::backgroundMat
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___backgroundMat_10;
	// System.Single AllIn1SpriteShader.All1ShaderDemoController::colorLerpSpeed
	float ___colorLerpSpeed_11;
	// UnityEngine.Color[] AllIn1SpriteShader.All1ShaderDemoController::targetColors
	ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* ___targetColors_12;
	// UnityEngine.Color[] AllIn1SpriteShader.All1ShaderDemoController::currentColors
	ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* ___currentColors_13;

public:
	inline static int32_t get_offset_of_expositors_4() { return static_cast<int32_t>(offsetof(All1ShaderDemoController_t66C22AC496E9A0D1FF0AB9CDB81DCE2DB60F485B, ___expositors_4)); }
	inline DemoCircleExpositorU5BU5D_tF00D5F6A0A10F7E6455A6B0E26C6172DCCD054A6* get_expositors_4() const { return ___expositors_4; }
	inline DemoCircleExpositorU5BU5D_tF00D5F6A0A10F7E6455A6B0E26C6172DCCD054A6** get_address_of_expositors_4() { return &___expositors_4; }
	inline void set_expositors_4(DemoCircleExpositorU5BU5D_tF00D5F6A0A10F7E6455A6B0E26C6172DCCD054A6* value)
	{
		___expositors_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___expositors_4), (void*)value);
	}

	inline static int32_t get_offset_of_expositorsTitle_5() { return static_cast<int32_t>(offsetof(All1ShaderDemoController_t66C22AC496E9A0D1FF0AB9CDB81DCE2DB60F485B, ___expositorsTitle_5)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_expositorsTitle_5() const { return ___expositorsTitle_5; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_expositorsTitle_5() { return &___expositorsTitle_5; }
	inline void set_expositorsTitle_5(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___expositorsTitle_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___expositorsTitle_5), (void*)value);
	}

	inline static int32_t get_offset_of_expositorsTitleOutline_6() { return static_cast<int32_t>(offsetof(All1ShaderDemoController_t66C22AC496E9A0D1FF0AB9CDB81DCE2DB60F485B, ___expositorsTitleOutline_6)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_expositorsTitleOutline_6() const { return ___expositorsTitleOutline_6; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_expositorsTitleOutline_6() { return &___expositorsTitleOutline_6; }
	inline void set_expositorsTitleOutline_6(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___expositorsTitleOutline_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___expositorsTitleOutline_6), (void*)value);
	}

	inline static int32_t get_offset_of_expositorDistance_7() { return static_cast<int32_t>(offsetof(All1ShaderDemoController_t66C22AC496E9A0D1FF0AB9CDB81DCE2DB60F485B, ___expositorDistance_7)); }
	inline float get_expositorDistance_7() const { return ___expositorDistance_7; }
	inline float* get_address_of_expositorDistance_7() { return &___expositorDistance_7; }
	inline void set_expositorDistance_7(float value)
	{
		___expositorDistance_7 = value;
	}

	inline static int32_t get_offset_of_currExpositor_8() { return static_cast<int32_t>(offsetof(All1ShaderDemoController_t66C22AC496E9A0D1FF0AB9CDB81DCE2DB60F485B, ___currExpositor_8)); }
	inline int32_t get_currExpositor_8() const { return ___currExpositor_8; }
	inline int32_t* get_address_of_currExpositor_8() { return &___currExpositor_8; }
	inline void set_currExpositor_8(int32_t value)
	{
		___currExpositor_8 = value;
	}

	inline static int32_t get_offset_of_background_9() { return static_cast<int32_t>(offsetof(All1ShaderDemoController_t66C22AC496E9A0D1FF0AB9CDB81DCE2DB60F485B, ___background_9)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_background_9() const { return ___background_9; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_background_9() { return &___background_9; }
	inline void set_background_9(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___background_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___background_9), (void*)value);
	}

	inline static int32_t get_offset_of_backgroundMat_10() { return static_cast<int32_t>(offsetof(All1ShaderDemoController_t66C22AC496E9A0D1FF0AB9CDB81DCE2DB60F485B, ___backgroundMat_10)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_backgroundMat_10() const { return ___backgroundMat_10; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_backgroundMat_10() { return &___backgroundMat_10; }
	inline void set_backgroundMat_10(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___backgroundMat_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___backgroundMat_10), (void*)value);
	}

	inline static int32_t get_offset_of_colorLerpSpeed_11() { return static_cast<int32_t>(offsetof(All1ShaderDemoController_t66C22AC496E9A0D1FF0AB9CDB81DCE2DB60F485B, ___colorLerpSpeed_11)); }
	inline float get_colorLerpSpeed_11() const { return ___colorLerpSpeed_11; }
	inline float* get_address_of_colorLerpSpeed_11() { return &___colorLerpSpeed_11; }
	inline void set_colorLerpSpeed_11(float value)
	{
		___colorLerpSpeed_11 = value;
	}

	inline static int32_t get_offset_of_targetColors_12() { return static_cast<int32_t>(offsetof(All1ShaderDemoController_t66C22AC496E9A0D1FF0AB9CDB81DCE2DB60F485B, ___targetColors_12)); }
	inline ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* get_targetColors_12() const { return ___targetColors_12; }
	inline ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834** get_address_of_targetColors_12() { return &___targetColors_12; }
	inline void set_targetColors_12(ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* value)
	{
		___targetColors_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___targetColors_12), (void*)value);
	}

	inline static int32_t get_offset_of_currentColors_13() { return static_cast<int32_t>(offsetof(All1ShaderDemoController_t66C22AC496E9A0D1FF0AB9CDB81DCE2DB60F485B, ___currentColors_13)); }
	inline ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* get_currentColors_13() const { return ___currentColors_13; }
	inline ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834** get_address_of_currentColors_13() { return &___currentColors_13; }
	inline void set_currentColors_13(ColorU5BU5D_t358DD89F511301E663AD9157305B94A2DEFF8834* value)
	{
		___currentColors_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___currentColors_13), (void*)value);
	}
};


// AllIn1SpriteShader.All1TextureOffsetOverTime
struct All1TextureOffsetOverTime_t8F1A316C57F3674EE7F61A7C07F07C9E891AD9FF  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String AllIn1SpriteShader.All1TextureOffsetOverTime::texturePropertyName
	String_t* ___texturePropertyName_4;
	// UnityEngine.Vector2 AllIn1SpriteShader.All1TextureOffsetOverTime::offsetSpeed
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___offsetSpeed_5;
	// UnityEngine.Material AllIn1SpriteShader.All1TextureOffsetOverTime::mat
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___mat_6;
	// System.Int32 AllIn1SpriteShader.All1TextureOffsetOverTime::textureShaderId
	int32_t ___textureShaderId_7;
	// UnityEngine.Vector2 AllIn1SpriteShader.All1TextureOffsetOverTime::currOffset
	Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  ___currOffset_8;

public:
	inline static int32_t get_offset_of_texturePropertyName_4() { return static_cast<int32_t>(offsetof(All1TextureOffsetOverTime_t8F1A316C57F3674EE7F61A7C07F07C9E891AD9FF, ___texturePropertyName_4)); }
	inline String_t* get_texturePropertyName_4() const { return ___texturePropertyName_4; }
	inline String_t** get_address_of_texturePropertyName_4() { return &___texturePropertyName_4; }
	inline void set_texturePropertyName_4(String_t* value)
	{
		___texturePropertyName_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___texturePropertyName_4), (void*)value);
	}

	inline static int32_t get_offset_of_offsetSpeed_5() { return static_cast<int32_t>(offsetof(All1TextureOffsetOverTime_t8F1A316C57F3674EE7F61A7C07F07C9E891AD9FF, ___offsetSpeed_5)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_offsetSpeed_5() const { return ___offsetSpeed_5; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_offsetSpeed_5() { return &___offsetSpeed_5; }
	inline void set_offsetSpeed_5(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___offsetSpeed_5 = value;
	}

	inline static int32_t get_offset_of_mat_6() { return static_cast<int32_t>(offsetof(All1TextureOffsetOverTime_t8F1A316C57F3674EE7F61A7C07F07C9E891AD9FF, ___mat_6)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_mat_6() const { return ___mat_6; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_mat_6() { return &___mat_6; }
	inline void set_mat_6(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___mat_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mat_6), (void*)value);
	}

	inline static int32_t get_offset_of_textureShaderId_7() { return static_cast<int32_t>(offsetof(All1TextureOffsetOverTime_t8F1A316C57F3674EE7F61A7C07F07C9E891AD9FF, ___textureShaderId_7)); }
	inline int32_t get_textureShaderId_7() const { return ___textureShaderId_7; }
	inline int32_t* get_address_of_textureShaderId_7() { return &___textureShaderId_7; }
	inline void set_textureShaderId_7(int32_t value)
	{
		___textureShaderId_7 = value;
	}

	inline static int32_t get_offset_of_currOffset_8() { return static_cast<int32_t>(offsetof(All1TextureOffsetOverTime_t8F1A316C57F3674EE7F61A7C07F07C9E891AD9FF, ___currOffset_8)); }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  get_currOffset_8() const { return ___currOffset_8; }
	inline Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9 * get_address_of_currOffset_8() { return &___currOffset_8; }
	inline void set_currOffset_8(Vector2_tBB32F2736AEC229A7BFBCE18197EC0F6AC7EC2D9  value)
	{
		___currOffset_8 = value;
	}
};


// AllIn1SpriteShader.AllIn1ScrollProperty
struct AllIn1ScrollProperty_tD8A7A1DABEA614260D493EAEE03ED89B6F17450B  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.String AllIn1SpriteShader.AllIn1ScrollProperty::numericPropertyName
	String_t* ___numericPropertyName_4;
	// System.Single AllIn1SpriteShader.AllIn1ScrollProperty::scrollSpeed
	float ___scrollSpeed_5;
	// System.Boolean AllIn1SpriteShader.AllIn1ScrollProperty::applyModulo
	bool ___applyModulo_6;
	// System.Single AllIn1SpriteShader.AllIn1ScrollProperty::modulo
	float ___modulo_7;
	// UnityEngine.Material AllIn1SpriteShader.AllIn1ScrollProperty::mat
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___mat_8;
	// System.Int32 AllIn1SpriteShader.AllIn1ScrollProperty::propertyShaderID
	int32_t ___propertyShaderID_9;
	// System.Single AllIn1SpriteShader.AllIn1ScrollProperty::currValue
	float ___currValue_10;

public:
	inline static int32_t get_offset_of_numericPropertyName_4() { return static_cast<int32_t>(offsetof(AllIn1ScrollProperty_tD8A7A1DABEA614260D493EAEE03ED89B6F17450B, ___numericPropertyName_4)); }
	inline String_t* get_numericPropertyName_4() const { return ___numericPropertyName_4; }
	inline String_t** get_address_of_numericPropertyName_4() { return &___numericPropertyName_4; }
	inline void set_numericPropertyName_4(String_t* value)
	{
		___numericPropertyName_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___numericPropertyName_4), (void*)value);
	}

	inline static int32_t get_offset_of_scrollSpeed_5() { return static_cast<int32_t>(offsetof(AllIn1ScrollProperty_tD8A7A1DABEA614260D493EAEE03ED89B6F17450B, ___scrollSpeed_5)); }
	inline float get_scrollSpeed_5() const { return ___scrollSpeed_5; }
	inline float* get_address_of_scrollSpeed_5() { return &___scrollSpeed_5; }
	inline void set_scrollSpeed_5(float value)
	{
		___scrollSpeed_5 = value;
	}

	inline static int32_t get_offset_of_applyModulo_6() { return static_cast<int32_t>(offsetof(AllIn1ScrollProperty_tD8A7A1DABEA614260D493EAEE03ED89B6F17450B, ___applyModulo_6)); }
	inline bool get_applyModulo_6() const { return ___applyModulo_6; }
	inline bool* get_address_of_applyModulo_6() { return &___applyModulo_6; }
	inline void set_applyModulo_6(bool value)
	{
		___applyModulo_6 = value;
	}

	inline static int32_t get_offset_of_modulo_7() { return static_cast<int32_t>(offsetof(AllIn1ScrollProperty_tD8A7A1DABEA614260D493EAEE03ED89B6F17450B, ___modulo_7)); }
	inline float get_modulo_7() const { return ___modulo_7; }
	inline float* get_address_of_modulo_7() { return &___modulo_7; }
	inline void set_modulo_7(float value)
	{
		___modulo_7 = value;
	}

	inline static int32_t get_offset_of_mat_8() { return static_cast<int32_t>(offsetof(AllIn1ScrollProperty_tD8A7A1DABEA614260D493EAEE03ED89B6F17450B, ___mat_8)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_mat_8() const { return ___mat_8; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_mat_8() { return &___mat_8; }
	inline void set_mat_8(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___mat_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mat_8), (void*)value);
	}

	inline static int32_t get_offset_of_propertyShaderID_9() { return static_cast<int32_t>(offsetof(AllIn1ScrollProperty_tD8A7A1DABEA614260D493EAEE03ED89B6F17450B, ___propertyShaderID_9)); }
	inline int32_t get_propertyShaderID_9() const { return ___propertyShaderID_9; }
	inline int32_t* get_address_of_propertyShaderID_9() { return &___propertyShaderID_9; }
	inline void set_propertyShaderID_9(int32_t value)
	{
		___propertyShaderID_9 = value;
	}

	inline static int32_t get_offset_of_currValue_10() { return static_cast<int32_t>(offsetof(AllIn1ScrollProperty_tD8A7A1DABEA614260D493EAEE03ED89B6F17450B, ___currValue_10)); }
	inline float get_currValue_10() const { return ___currValue_10; }
	inline float* get_address_of_currValue_10() { return &___currValue_10; }
	inline void set_currValue_10(float value)
	{
		___currValue_10 = value;
	}
};


// AllIn1SpriteShader.Demo2AutoScroll
struct Demo2AutoScroll_tF05254B25ADADD0CC28CF7DFC84D7709CA5A7BFB  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Transform[] AllIn1SpriteShader.Demo2AutoScroll::children
	TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* ___children_4;
	// System.Single AllIn1SpriteShader.Demo2AutoScroll::totalTime
	float ___totalTime_5;
	// UnityEngine.GameObject AllIn1SpriteShader.Demo2AutoScroll::sceneDescription
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___sceneDescription_6;

public:
	inline static int32_t get_offset_of_children_4() { return static_cast<int32_t>(offsetof(Demo2AutoScroll_tF05254B25ADADD0CC28CF7DFC84D7709CA5A7BFB, ___children_4)); }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* get_children_4() const { return ___children_4; }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D** get_address_of_children_4() { return &___children_4; }
	inline void set_children_4(TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* value)
	{
		___children_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___children_4), (void*)value);
	}

	inline static int32_t get_offset_of_totalTime_5() { return static_cast<int32_t>(offsetof(Demo2AutoScroll_tF05254B25ADADD0CC28CF7DFC84D7709CA5A7BFB, ___totalTime_5)); }
	inline float get_totalTime_5() const { return ___totalTime_5; }
	inline float* get_address_of_totalTime_5() { return &___totalTime_5; }
	inline void set_totalTime_5(float value)
	{
		___totalTime_5 = value;
	}

	inline static int32_t get_offset_of_sceneDescription_6() { return static_cast<int32_t>(offsetof(Demo2AutoScroll_tF05254B25ADADD0CC28CF7DFC84D7709CA5A7BFB, ___sceneDescription_6)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_sceneDescription_6() const { return ___sceneDescription_6; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_sceneDescription_6() { return &___sceneDescription_6; }
	inline void set_sceneDescription_6(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___sceneDescription_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___sceneDescription_6), (void*)value);
	}
};


// AllIn1SpriteShader.DemoCamera
struct DemoCamera_tB715A63ACE2506E72DFEAB96E6BBCA8FC6BD30CB  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Transform AllIn1SpriteShader.DemoCamera::targetedItem
	Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * ___targetedItem_4;
	// AllIn1SpriteShader.All1ShaderDemoController AllIn1SpriteShader.DemoCamera::demoController
	All1ShaderDemoController_t66C22AC496E9A0D1FF0AB9CDB81DCE2DB60F485B * ___demoController_5;
	// System.Single AllIn1SpriteShader.DemoCamera::speed
	float ___speed_6;
	// UnityEngine.Vector3 AllIn1SpriteShader.DemoCamera::offset
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___offset_7;
	// UnityEngine.Vector3 AllIn1SpriteShader.DemoCamera::target
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___target_8;
	// System.Boolean AllIn1SpriteShader.DemoCamera::canUpdate
	bool ___canUpdate_9;

public:
	inline static int32_t get_offset_of_targetedItem_4() { return static_cast<int32_t>(offsetof(DemoCamera_tB715A63ACE2506E72DFEAB96E6BBCA8FC6BD30CB, ___targetedItem_4)); }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * get_targetedItem_4() const { return ___targetedItem_4; }
	inline Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 ** get_address_of_targetedItem_4() { return &___targetedItem_4; }
	inline void set_targetedItem_4(Transform_tA8193BB29D4D2C7EC04918F3ED1816345186C3F1 * value)
	{
		___targetedItem_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___targetedItem_4), (void*)value);
	}

	inline static int32_t get_offset_of_demoController_5() { return static_cast<int32_t>(offsetof(DemoCamera_tB715A63ACE2506E72DFEAB96E6BBCA8FC6BD30CB, ___demoController_5)); }
	inline All1ShaderDemoController_t66C22AC496E9A0D1FF0AB9CDB81DCE2DB60F485B * get_demoController_5() const { return ___demoController_5; }
	inline All1ShaderDemoController_t66C22AC496E9A0D1FF0AB9CDB81DCE2DB60F485B ** get_address_of_demoController_5() { return &___demoController_5; }
	inline void set_demoController_5(All1ShaderDemoController_t66C22AC496E9A0D1FF0AB9CDB81DCE2DB60F485B * value)
	{
		___demoController_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___demoController_5), (void*)value);
	}

	inline static int32_t get_offset_of_speed_6() { return static_cast<int32_t>(offsetof(DemoCamera_tB715A63ACE2506E72DFEAB96E6BBCA8FC6BD30CB, ___speed_6)); }
	inline float get_speed_6() const { return ___speed_6; }
	inline float* get_address_of_speed_6() { return &___speed_6; }
	inline void set_speed_6(float value)
	{
		___speed_6 = value;
	}

	inline static int32_t get_offset_of_offset_7() { return static_cast<int32_t>(offsetof(DemoCamera_tB715A63ACE2506E72DFEAB96E6BBCA8FC6BD30CB, ___offset_7)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_offset_7() const { return ___offset_7; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_offset_7() { return &___offset_7; }
	inline void set_offset_7(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___offset_7 = value;
	}

	inline static int32_t get_offset_of_target_8() { return static_cast<int32_t>(offsetof(DemoCamera_tB715A63ACE2506E72DFEAB96E6BBCA8FC6BD30CB, ___target_8)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_target_8() const { return ___target_8; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_target_8() { return &___target_8; }
	inline void set_target_8(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___target_8 = value;
	}

	inline static int32_t get_offset_of_canUpdate_9() { return static_cast<int32_t>(offsetof(DemoCamera_tB715A63ACE2506E72DFEAB96E6BBCA8FC6BD30CB, ___canUpdate_9)); }
	inline bool get_canUpdate_9() const { return ___canUpdate_9; }
	inline bool* get_address_of_canUpdate_9() { return &___canUpdate_9; }
	inline void set_canUpdate_9(bool value)
	{
		___canUpdate_9 = value;
	}
};


// AllIn1SpriteShader.DemoCircleExpositor
struct DemoCircleExpositor_tDDF61D2EAD6F6D02C955A131906530C1A8F79B4F  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single AllIn1SpriteShader.DemoCircleExpositor::radius
	float ___radius_4;
	// System.Single AllIn1SpriteShader.DemoCircleExpositor::rotateSpeed
	float ___rotateSpeed_5;
	// System.Single AllIn1SpriteShader.DemoCircleExpositor::zOffset
	float ___zOffset_6;
	// UnityEngine.Transform[] AllIn1SpriteShader.DemoCircleExpositor::items
	TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* ___items_7;
	// System.Int32 AllIn1SpriteShader.DemoCircleExpositor::count
	int32_t ___count_8;
	// System.Int32 AllIn1SpriteShader.DemoCircleExpositor::currentTarget
	int32_t ___currentTarget_9;
	// System.Single AllIn1SpriteShader.DemoCircleExpositor::offsetRotation
	float ___offsetRotation_10;
	// System.Single AllIn1SpriteShader.DemoCircleExpositor::iniY
	float ___iniY_11;
	// UnityEngine.Quaternion AllIn1SpriteShader.DemoCircleExpositor::dummyRotation
	Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  ___dummyRotation_12;

public:
	inline static int32_t get_offset_of_radius_4() { return static_cast<int32_t>(offsetof(DemoCircleExpositor_tDDF61D2EAD6F6D02C955A131906530C1A8F79B4F, ___radius_4)); }
	inline float get_radius_4() const { return ___radius_4; }
	inline float* get_address_of_radius_4() { return &___radius_4; }
	inline void set_radius_4(float value)
	{
		___radius_4 = value;
	}

	inline static int32_t get_offset_of_rotateSpeed_5() { return static_cast<int32_t>(offsetof(DemoCircleExpositor_tDDF61D2EAD6F6D02C955A131906530C1A8F79B4F, ___rotateSpeed_5)); }
	inline float get_rotateSpeed_5() const { return ___rotateSpeed_5; }
	inline float* get_address_of_rotateSpeed_5() { return &___rotateSpeed_5; }
	inline void set_rotateSpeed_5(float value)
	{
		___rotateSpeed_5 = value;
	}

	inline static int32_t get_offset_of_zOffset_6() { return static_cast<int32_t>(offsetof(DemoCircleExpositor_tDDF61D2EAD6F6D02C955A131906530C1A8F79B4F, ___zOffset_6)); }
	inline float get_zOffset_6() const { return ___zOffset_6; }
	inline float* get_address_of_zOffset_6() { return &___zOffset_6; }
	inline void set_zOffset_6(float value)
	{
		___zOffset_6 = value;
	}

	inline static int32_t get_offset_of_items_7() { return static_cast<int32_t>(offsetof(DemoCircleExpositor_tDDF61D2EAD6F6D02C955A131906530C1A8F79B4F, ___items_7)); }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* get_items_7() const { return ___items_7; }
	inline TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D** get_address_of_items_7() { return &___items_7; }
	inline void set_items_7(TransformU5BU5D_t7821C0520CC567C0A069329C01AE9C058C7E3F1D* value)
	{
		___items_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___items_7), (void*)value);
	}

	inline static int32_t get_offset_of_count_8() { return static_cast<int32_t>(offsetof(DemoCircleExpositor_tDDF61D2EAD6F6D02C955A131906530C1A8F79B4F, ___count_8)); }
	inline int32_t get_count_8() const { return ___count_8; }
	inline int32_t* get_address_of_count_8() { return &___count_8; }
	inline void set_count_8(int32_t value)
	{
		___count_8 = value;
	}

	inline static int32_t get_offset_of_currentTarget_9() { return static_cast<int32_t>(offsetof(DemoCircleExpositor_tDDF61D2EAD6F6D02C955A131906530C1A8F79B4F, ___currentTarget_9)); }
	inline int32_t get_currentTarget_9() const { return ___currentTarget_9; }
	inline int32_t* get_address_of_currentTarget_9() { return &___currentTarget_9; }
	inline void set_currentTarget_9(int32_t value)
	{
		___currentTarget_9 = value;
	}

	inline static int32_t get_offset_of_offsetRotation_10() { return static_cast<int32_t>(offsetof(DemoCircleExpositor_tDDF61D2EAD6F6D02C955A131906530C1A8F79B4F, ___offsetRotation_10)); }
	inline float get_offsetRotation_10() const { return ___offsetRotation_10; }
	inline float* get_address_of_offsetRotation_10() { return &___offsetRotation_10; }
	inline void set_offsetRotation_10(float value)
	{
		___offsetRotation_10 = value;
	}

	inline static int32_t get_offset_of_iniY_11() { return static_cast<int32_t>(offsetof(DemoCircleExpositor_tDDF61D2EAD6F6D02C955A131906530C1A8F79B4F, ___iniY_11)); }
	inline float get_iniY_11() const { return ___iniY_11; }
	inline float* get_address_of_iniY_11() { return &___iniY_11; }
	inline void set_iniY_11(float value)
	{
		___iniY_11 = value;
	}

	inline static int32_t get_offset_of_dummyRotation_12() { return static_cast<int32_t>(offsetof(DemoCircleExpositor_tDDF61D2EAD6F6D02C955A131906530C1A8F79B4F, ___dummyRotation_12)); }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  get_dummyRotation_12() const { return ___dummyRotation_12; }
	inline Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4 * get_address_of_dummyRotation_12() { return &___dummyRotation_12; }
	inline void set_dummyRotation_12(Quaternion_t6D28618CF65156D4A0AD747370DDFD0C514A31B4  value)
	{
		___dummyRotation_12 = value;
	}
};


// AllIn1SpriteShader.DemoItem
struct DemoItem_tA9345BB55EBA626D1FABEC62B65FA14670C9B950  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};

struct DemoItem_tA9345BB55EBA626D1FABEC62B65FA14670C9B950_StaticFields
{
public:
	// UnityEngine.Vector3 AllIn1SpriteShader.DemoItem::lookAtZ
	Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  ___lookAtZ_4;

public:
	inline static int32_t get_offset_of_lookAtZ_4() { return static_cast<int32_t>(offsetof(DemoItem_tA9345BB55EBA626D1FABEC62B65FA14670C9B950_StaticFields, ___lookAtZ_4)); }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  get_lookAtZ_4() const { return ___lookAtZ_4; }
	inline Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E * get_address_of_lookAtZ_4() { return &___lookAtZ_4; }
	inline void set_lookAtZ_4(Vector3_t65B972D6A585A0A5B63153CF1177A90D3C90D65E  value)
	{
		___lookAtZ_4 = value;
	}
};


// AllIn1SpriteShader.DemoRandomColorSwap
struct DemoRandomColorSwap_t4D6DE09EC4B8935A28D8C1132C3FE5E56EC6D3C6  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Material AllIn1SpriteShader.DemoRandomColorSwap::mat
	Material_t8927C00353A72755313F046D0CE85178AE8218EE * ___mat_4;

public:
	inline static int32_t get_offset_of_mat_4() { return static_cast<int32_t>(offsetof(DemoRandomColorSwap_t4D6DE09EC4B8935A28D8C1132C3FE5E56EC6D3C6, ___mat_4)); }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE * get_mat_4() const { return ___mat_4; }
	inline Material_t8927C00353A72755313F046D0CE85178AE8218EE ** get_address_of_mat_4() { return &___mat_4; }
	inline void set_mat_4(Material_t8927C00353A72755313F046D0CE85178AE8218EE * value)
	{
		___mat_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___mat_4), (void*)value);
	}
};


// AllIn1SpriteShader.DemoRepositionExpositor
struct DemoRepositionExpositor_tC8D94A1C9F9F6B7BFAA03E6D611D56D0E589C355  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single AllIn1SpriteShader.DemoRepositionExpositor::paddingX
	float ___paddingX_4;

public:
	inline static int32_t get_offset_of_paddingX_4() { return static_cast<int32_t>(offsetof(DemoRepositionExpositor_tC8D94A1C9F9F6B7BFAA03E6D611D56D0E589C355, ___paddingX_4)); }
	inline float get_paddingX_4() const { return ___paddingX_4; }
	inline float* get_address_of_paddingX_4() { return &___paddingX_4; }
	inline void set_paddingX_4(float value)
	{
		___paddingX_4 = value;
	}
};


// AllIn1SpriteShader.RandomSeed
struct RandomSeed_t308F7B3257B04EAD3CA31458EED83DA0D1687323  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// AllIn1SpriteShader.SetAtlasUvs
struct SetAtlasUvs_t6726EDF14C84A61B515F2D7461B45EE43F7B1966  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Boolean AllIn1SpriteShader.SetAtlasUvs::updateEveryFrame
	bool ___updateEveryFrame_4;
	// UnityEngine.Renderer AllIn1SpriteShader.SetAtlasUvs::render
	Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * ___render_5;
	// UnityEngine.SpriteRenderer AllIn1SpriteShader.SetAtlasUvs::spriteRender
	SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * ___spriteRender_6;
	// UnityEngine.UI.Image AllIn1SpriteShader.SetAtlasUvs::uiImage
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___uiImage_7;
	// System.Boolean AllIn1SpriteShader.SetAtlasUvs::isUI
	bool ___isUI_8;

public:
	inline static int32_t get_offset_of_updateEveryFrame_4() { return static_cast<int32_t>(offsetof(SetAtlasUvs_t6726EDF14C84A61B515F2D7461B45EE43F7B1966, ___updateEveryFrame_4)); }
	inline bool get_updateEveryFrame_4() const { return ___updateEveryFrame_4; }
	inline bool* get_address_of_updateEveryFrame_4() { return &___updateEveryFrame_4; }
	inline void set_updateEveryFrame_4(bool value)
	{
		___updateEveryFrame_4 = value;
	}

	inline static int32_t get_offset_of_render_5() { return static_cast<int32_t>(offsetof(SetAtlasUvs_t6726EDF14C84A61B515F2D7461B45EE43F7B1966, ___render_5)); }
	inline Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * get_render_5() const { return ___render_5; }
	inline Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C ** get_address_of_render_5() { return &___render_5; }
	inline void set_render_5(Renderer_t58147AB5B00224FE1460FD47542DC0DA7EC9378C * value)
	{
		___render_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___render_5), (void*)value);
	}

	inline static int32_t get_offset_of_spriteRender_6() { return static_cast<int32_t>(offsetof(SetAtlasUvs_t6726EDF14C84A61B515F2D7461B45EE43F7B1966, ___spriteRender_6)); }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * get_spriteRender_6() const { return ___spriteRender_6; }
	inline SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF ** get_address_of_spriteRender_6() { return &___spriteRender_6; }
	inline void set_spriteRender_6(SpriteRenderer_t3F35AD5498243C170B46F5FFDB582AAEF78615EF * value)
	{
		___spriteRender_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spriteRender_6), (void*)value);
	}

	inline static int32_t get_offset_of_uiImage_7() { return static_cast<int32_t>(offsetof(SetAtlasUvs_t6726EDF14C84A61B515F2D7461B45EE43F7B1966, ___uiImage_7)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_uiImage_7() const { return ___uiImage_7; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_uiImage_7() { return &___uiImage_7; }
	inline void set_uiImage_7(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___uiImage_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___uiImage_7), (void*)value);
	}

	inline static int32_t get_offset_of_isUI_8() { return static_cast<int32_t>(offsetof(SetAtlasUvs_t6726EDF14C84A61B515F2D7461B45EE43F7B1966, ___isUI_8)); }
	inline bool get_isUI_8() const { return ___isUI_8; }
	inline bool* get_address_of_isUI_8() { return &___isUI_8; }
	inline void set_isUI_8(bool value)
	{
		___isUI_8 = value;
	}
};


// AllIn1SpriteShader.SetGlobalTime
struct SetGlobalTime_tE062F66EDD7E40277C2793978E483F54B0928A08  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 AllIn1SpriteShader.SetGlobalTime::globalTime
	int32_t ___globalTime_4;

public:
	inline static int32_t get_offset_of_globalTime_4() { return static_cast<int32_t>(offsetof(SetGlobalTime_tE062F66EDD7E40277C2793978E483F54B0928A08, ___globalTime_4)); }
	inline int32_t get_globalTime_4() const { return ___globalTime_4; }
	inline int32_t* get_address_of_globalTime_4() { return &___globalTime_4; }
	inline void set_globalTime_4(int32_t value)
	{
		___globalTime_4 = value;
	}
};


// TMPro.Examples.TextMeshSpawner
struct TextMeshSpawner_t44E5536D2A0C022FA8037C71741171A999A86364  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Int32 TMPro.Examples.TextMeshSpawner::SpawnType
	int32_t ___SpawnType_4;
	// System.Int32 TMPro.Examples.TextMeshSpawner::NumberOfNPC
	int32_t ___NumberOfNPC_5;
	// UnityEngine.Font TMPro.Examples.TextMeshSpawner::TheFont
	Font_tB53D3F362CB1A0B92307B362826F212AE2D2A6A9 * ___TheFont_6;
	// TMPro.Examples.TextMeshProFloatingText TMPro.Examples.TextMeshSpawner::floatingText_Script
	TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200 * ___floatingText_Script_7;

public:
	inline static int32_t get_offset_of_SpawnType_4() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t44E5536D2A0C022FA8037C71741171A999A86364, ___SpawnType_4)); }
	inline int32_t get_SpawnType_4() const { return ___SpawnType_4; }
	inline int32_t* get_address_of_SpawnType_4() { return &___SpawnType_4; }
	inline void set_SpawnType_4(int32_t value)
	{
		___SpawnType_4 = value;
	}

	inline static int32_t get_offset_of_NumberOfNPC_5() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t44E5536D2A0C022FA8037C71741171A999A86364, ___NumberOfNPC_5)); }
	inline int32_t get_NumberOfNPC_5() const { return ___NumberOfNPC_5; }
	inline int32_t* get_address_of_NumberOfNPC_5() { return &___NumberOfNPC_5; }
	inline void set_NumberOfNPC_5(int32_t value)
	{
		___NumberOfNPC_5 = value;
	}

	inline static int32_t get_offset_of_TheFont_6() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t44E5536D2A0C022FA8037C71741171A999A86364, ___TheFont_6)); }
	inline Font_tB53D3F362CB1A0B92307B362826F212AE2D2A6A9 * get_TheFont_6() const { return ___TheFont_6; }
	inline Font_tB53D3F362CB1A0B92307B362826F212AE2D2A6A9 ** get_address_of_TheFont_6() { return &___TheFont_6; }
	inline void set_TheFont_6(Font_tB53D3F362CB1A0B92307B362826F212AE2D2A6A9 * value)
	{
		___TheFont_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TheFont_6), (void*)value);
	}

	inline static int32_t get_offset_of_floatingText_Script_7() { return static_cast<int32_t>(offsetof(TextMeshSpawner_t44E5536D2A0C022FA8037C71741171A999A86364, ___floatingText_Script_7)); }
	inline TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200 * get_floatingText_Script_7() const { return ___floatingText_Script_7; }
	inline TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200 ** get_address_of_floatingText_Script_7() { return &___floatingText_Script_7; }
	inline void set_floatingText_Script_7(TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200 * value)
	{
		___floatingText_Script_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___floatingText_Script_7), (void*)value);
	}
};


// TMPro.Examples.VertexColorCycler
struct VertexColorCycler_t37E80E87D8EAD0D7757CDA45BD5D3AA82FD28374  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// TMPro.TMP_Text TMPro.Examples.VertexColorCycler::m_TextComponent
	TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * ___m_TextComponent_4;

public:
	inline static int32_t get_offset_of_m_TextComponent_4() { return static_cast<int32_t>(offsetof(VertexColorCycler_t37E80E87D8EAD0D7757CDA45BD5D3AA82FD28374, ___m_TextComponent_4)); }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * get_m_TextComponent_4() const { return ___m_TextComponent_4; }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 ** get_address_of_m_TextComponent_4() { return &___m_TextComponent_4; }
	inline void set_m_TextComponent_4(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * value)
	{
		___m_TextComponent_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextComponent_4), (void*)value);
	}
};


// TMPro.Examples.VertexJitter
struct VertexJitter_t4BF9FCCB84221E69D9E3727E32E49710F3B4342A  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single TMPro.Examples.VertexJitter::AngleMultiplier
	float ___AngleMultiplier_4;
	// System.Single TMPro.Examples.VertexJitter::SpeedMultiplier
	float ___SpeedMultiplier_5;
	// System.Single TMPro.Examples.VertexJitter::CurveScale
	float ___CurveScale_6;
	// TMPro.TMP_Text TMPro.Examples.VertexJitter::m_TextComponent
	TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * ___m_TextComponent_7;
	// System.Boolean TMPro.Examples.VertexJitter::hasTextChanged
	bool ___hasTextChanged_8;

public:
	inline static int32_t get_offset_of_AngleMultiplier_4() { return static_cast<int32_t>(offsetof(VertexJitter_t4BF9FCCB84221E69D9E3727E32E49710F3B4342A, ___AngleMultiplier_4)); }
	inline float get_AngleMultiplier_4() const { return ___AngleMultiplier_4; }
	inline float* get_address_of_AngleMultiplier_4() { return &___AngleMultiplier_4; }
	inline void set_AngleMultiplier_4(float value)
	{
		___AngleMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_5() { return static_cast<int32_t>(offsetof(VertexJitter_t4BF9FCCB84221E69D9E3727E32E49710F3B4342A, ___SpeedMultiplier_5)); }
	inline float get_SpeedMultiplier_5() const { return ___SpeedMultiplier_5; }
	inline float* get_address_of_SpeedMultiplier_5() { return &___SpeedMultiplier_5; }
	inline void set_SpeedMultiplier_5(float value)
	{
		___SpeedMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_CurveScale_6() { return static_cast<int32_t>(offsetof(VertexJitter_t4BF9FCCB84221E69D9E3727E32E49710F3B4342A, ___CurveScale_6)); }
	inline float get_CurveScale_6() const { return ___CurveScale_6; }
	inline float* get_address_of_CurveScale_6() { return &___CurveScale_6; }
	inline void set_CurveScale_6(float value)
	{
		___CurveScale_6 = value;
	}

	inline static int32_t get_offset_of_m_TextComponent_7() { return static_cast<int32_t>(offsetof(VertexJitter_t4BF9FCCB84221E69D9E3727E32E49710F3B4342A, ___m_TextComponent_7)); }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * get_m_TextComponent_7() const { return ___m_TextComponent_7; }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 ** get_address_of_m_TextComponent_7() { return &___m_TextComponent_7; }
	inline void set_m_TextComponent_7(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * value)
	{
		___m_TextComponent_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextComponent_7), (void*)value);
	}

	inline static int32_t get_offset_of_hasTextChanged_8() { return static_cast<int32_t>(offsetof(VertexJitter_t4BF9FCCB84221E69D9E3727E32E49710F3B4342A, ___hasTextChanged_8)); }
	inline bool get_hasTextChanged_8() const { return ___hasTextChanged_8; }
	inline bool* get_address_of_hasTextChanged_8() { return &___hasTextChanged_8; }
	inline void set_hasTextChanged_8(bool value)
	{
		___hasTextChanged_8 = value;
	}
};


// TMPro.Examples.VertexShakeA
struct VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single TMPro.Examples.VertexShakeA::AngleMultiplier
	float ___AngleMultiplier_4;
	// System.Single TMPro.Examples.VertexShakeA::SpeedMultiplier
	float ___SpeedMultiplier_5;
	// System.Single TMPro.Examples.VertexShakeA::ScaleMultiplier
	float ___ScaleMultiplier_6;
	// System.Single TMPro.Examples.VertexShakeA::RotationMultiplier
	float ___RotationMultiplier_7;
	// TMPro.TMP_Text TMPro.Examples.VertexShakeA::m_TextComponent
	TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * ___m_TextComponent_8;
	// System.Boolean TMPro.Examples.VertexShakeA::hasTextChanged
	bool ___hasTextChanged_9;

public:
	inline static int32_t get_offset_of_AngleMultiplier_4() { return static_cast<int32_t>(offsetof(VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A, ___AngleMultiplier_4)); }
	inline float get_AngleMultiplier_4() const { return ___AngleMultiplier_4; }
	inline float* get_address_of_AngleMultiplier_4() { return &___AngleMultiplier_4; }
	inline void set_AngleMultiplier_4(float value)
	{
		___AngleMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_5() { return static_cast<int32_t>(offsetof(VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A, ___SpeedMultiplier_5)); }
	inline float get_SpeedMultiplier_5() const { return ___SpeedMultiplier_5; }
	inline float* get_address_of_SpeedMultiplier_5() { return &___SpeedMultiplier_5; }
	inline void set_SpeedMultiplier_5(float value)
	{
		___SpeedMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_ScaleMultiplier_6() { return static_cast<int32_t>(offsetof(VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A, ___ScaleMultiplier_6)); }
	inline float get_ScaleMultiplier_6() const { return ___ScaleMultiplier_6; }
	inline float* get_address_of_ScaleMultiplier_6() { return &___ScaleMultiplier_6; }
	inline void set_ScaleMultiplier_6(float value)
	{
		___ScaleMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_RotationMultiplier_7() { return static_cast<int32_t>(offsetof(VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A, ___RotationMultiplier_7)); }
	inline float get_RotationMultiplier_7() const { return ___RotationMultiplier_7; }
	inline float* get_address_of_RotationMultiplier_7() { return &___RotationMultiplier_7; }
	inline void set_RotationMultiplier_7(float value)
	{
		___RotationMultiplier_7 = value;
	}

	inline static int32_t get_offset_of_m_TextComponent_8() { return static_cast<int32_t>(offsetof(VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A, ___m_TextComponent_8)); }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * get_m_TextComponent_8() const { return ___m_TextComponent_8; }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 ** get_address_of_m_TextComponent_8() { return &___m_TextComponent_8; }
	inline void set_m_TextComponent_8(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * value)
	{
		___m_TextComponent_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextComponent_8), (void*)value);
	}

	inline static int32_t get_offset_of_hasTextChanged_9() { return static_cast<int32_t>(offsetof(VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A, ___hasTextChanged_9)); }
	inline bool get_hasTextChanged_9() const { return ___hasTextChanged_9; }
	inline bool* get_address_of_hasTextChanged_9() { return &___hasTextChanged_9; }
	inline void set_hasTextChanged_9(bool value)
	{
		___hasTextChanged_9 = value;
	}
};


// TMPro.Examples.VertexShakeB
struct VertexShakeB_t348FF1466CEF3BFED37C140B7AEFA2EA90D703C9  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single TMPro.Examples.VertexShakeB::AngleMultiplier
	float ___AngleMultiplier_4;
	// System.Single TMPro.Examples.VertexShakeB::SpeedMultiplier
	float ___SpeedMultiplier_5;
	// System.Single TMPro.Examples.VertexShakeB::CurveScale
	float ___CurveScale_6;
	// TMPro.TMP_Text TMPro.Examples.VertexShakeB::m_TextComponent
	TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * ___m_TextComponent_7;
	// System.Boolean TMPro.Examples.VertexShakeB::hasTextChanged
	bool ___hasTextChanged_8;

public:
	inline static int32_t get_offset_of_AngleMultiplier_4() { return static_cast<int32_t>(offsetof(VertexShakeB_t348FF1466CEF3BFED37C140B7AEFA2EA90D703C9, ___AngleMultiplier_4)); }
	inline float get_AngleMultiplier_4() const { return ___AngleMultiplier_4; }
	inline float* get_address_of_AngleMultiplier_4() { return &___AngleMultiplier_4; }
	inline void set_AngleMultiplier_4(float value)
	{
		___AngleMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_5() { return static_cast<int32_t>(offsetof(VertexShakeB_t348FF1466CEF3BFED37C140B7AEFA2EA90D703C9, ___SpeedMultiplier_5)); }
	inline float get_SpeedMultiplier_5() const { return ___SpeedMultiplier_5; }
	inline float* get_address_of_SpeedMultiplier_5() { return &___SpeedMultiplier_5; }
	inline void set_SpeedMultiplier_5(float value)
	{
		___SpeedMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_CurveScale_6() { return static_cast<int32_t>(offsetof(VertexShakeB_t348FF1466CEF3BFED37C140B7AEFA2EA90D703C9, ___CurveScale_6)); }
	inline float get_CurveScale_6() const { return ___CurveScale_6; }
	inline float* get_address_of_CurveScale_6() { return &___CurveScale_6; }
	inline void set_CurveScale_6(float value)
	{
		___CurveScale_6 = value;
	}

	inline static int32_t get_offset_of_m_TextComponent_7() { return static_cast<int32_t>(offsetof(VertexShakeB_t348FF1466CEF3BFED37C140B7AEFA2EA90D703C9, ___m_TextComponent_7)); }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * get_m_TextComponent_7() const { return ___m_TextComponent_7; }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 ** get_address_of_m_TextComponent_7() { return &___m_TextComponent_7; }
	inline void set_m_TextComponent_7(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * value)
	{
		___m_TextComponent_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextComponent_7), (void*)value);
	}

	inline static int32_t get_offset_of_hasTextChanged_8() { return static_cast<int32_t>(offsetof(VertexShakeB_t348FF1466CEF3BFED37C140B7AEFA2EA90D703C9, ___hasTextChanged_8)); }
	inline bool get_hasTextChanged_8() const { return ___hasTextChanged_8; }
	inline bool* get_address_of_hasTextChanged_8() { return &___hasTextChanged_8; }
	inline void set_hasTextChanged_8(bool value)
	{
		___hasTextChanged_8 = value;
	}
};


// TMPro.Examples.VertexZoom
struct VertexZoom_t02E8994D3A9EB41AEEF1544EF8C93009F6153163  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single TMPro.Examples.VertexZoom::AngleMultiplier
	float ___AngleMultiplier_4;
	// System.Single TMPro.Examples.VertexZoom::SpeedMultiplier
	float ___SpeedMultiplier_5;
	// System.Single TMPro.Examples.VertexZoom::CurveScale
	float ___CurveScale_6;
	// TMPro.TMP_Text TMPro.Examples.VertexZoom::m_TextComponent
	TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * ___m_TextComponent_7;
	// System.Boolean TMPro.Examples.VertexZoom::hasTextChanged
	bool ___hasTextChanged_8;

public:
	inline static int32_t get_offset_of_AngleMultiplier_4() { return static_cast<int32_t>(offsetof(VertexZoom_t02E8994D3A9EB41AEEF1544EF8C93009F6153163, ___AngleMultiplier_4)); }
	inline float get_AngleMultiplier_4() const { return ___AngleMultiplier_4; }
	inline float* get_address_of_AngleMultiplier_4() { return &___AngleMultiplier_4; }
	inline void set_AngleMultiplier_4(float value)
	{
		___AngleMultiplier_4 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_5() { return static_cast<int32_t>(offsetof(VertexZoom_t02E8994D3A9EB41AEEF1544EF8C93009F6153163, ___SpeedMultiplier_5)); }
	inline float get_SpeedMultiplier_5() const { return ___SpeedMultiplier_5; }
	inline float* get_address_of_SpeedMultiplier_5() { return &___SpeedMultiplier_5; }
	inline void set_SpeedMultiplier_5(float value)
	{
		___SpeedMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_CurveScale_6() { return static_cast<int32_t>(offsetof(VertexZoom_t02E8994D3A9EB41AEEF1544EF8C93009F6153163, ___CurveScale_6)); }
	inline float get_CurveScale_6() const { return ___CurveScale_6; }
	inline float* get_address_of_CurveScale_6() { return &___CurveScale_6; }
	inline void set_CurveScale_6(float value)
	{
		___CurveScale_6 = value;
	}

	inline static int32_t get_offset_of_m_TextComponent_7() { return static_cast<int32_t>(offsetof(VertexZoom_t02E8994D3A9EB41AEEF1544EF8C93009F6153163, ___m_TextComponent_7)); }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * get_m_TextComponent_7() const { return ___m_TextComponent_7; }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 ** get_address_of_m_TextComponent_7() { return &___m_TextComponent_7; }
	inline void set_m_TextComponent_7(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * value)
	{
		___m_TextComponent_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextComponent_7), (void*)value);
	}

	inline static int32_t get_offset_of_hasTextChanged_8() { return static_cast<int32_t>(offsetof(VertexZoom_t02E8994D3A9EB41AEEF1544EF8C93009F6153163, ___hasTextChanged_8)); }
	inline bool get_hasTextChanged_8() const { return ___hasTextChanged_8; }
	inline bool* get_address_of_hasTextChanged_8() { return &___hasTextChanged_8; }
	inline void set_hasTextChanged_8(bool value)
	{
		___hasTextChanged_8 = value;
	}
};


// TMPro.Examples.WarpTextExample
struct WarpTextExample_t9CE7000EFB1B8696E32CC86E87E3D0EDEA3D5A96  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// TMPro.TMP_Text TMPro.Examples.WarpTextExample::m_TextComponent
	TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * ___m_TextComponent_4;
	// UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::VertexCurve
	AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * ___VertexCurve_5;
	// System.Single TMPro.Examples.WarpTextExample::AngleMultiplier
	float ___AngleMultiplier_6;
	// System.Single TMPro.Examples.WarpTextExample::SpeedMultiplier
	float ___SpeedMultiplier_7;
	// System.Single TMPro.Examples.WarpTextExample::CurveScale
	float ___CurveScale_8;

public:
	inline static int32_t get_offset_of_m_TextComponent_4() { return static_cast<int32_t>(offsetof(WarpTextExample_t9CE7000EFB1B8696E32CC86E87E3D0EDEA3D5A96, ___m_TextComponent_4)); }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * get_m_TextComponent_4() const { return ___m_TextComponent_4; }
	inline TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 ** get_address_of_m_TextComponent_4() { return &___m_TextComponent_4; }
	inline void set_m_TextComponent_4(TMP_Text_t86179C97C713E1A6B3751B48DC7A16C874A7B262 * value)
	{
		___m_TextComponent_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TextComponent_4), (void*)value);
	}

	inline static int32_t get_offset_of_VertexCurve_5() { return static_cast<int32_t>(offsetof(WarpTextExample_t9CE7000EFB1B8696E32CC86E87E3D0EDEA3D5A96, ___VertexCurve_5)); }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * get_VertexCurve_5() const { return ___VertexCurve_5; }
	inline AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 ** get_address_of_VertexCurve_5() { return &___VertexCurve_5; }
	inline void set_VertexCurve_5(AnimationCurve_t2D452A14820CEDB83BFF2C911682A4E59001AD03 * value)
	{
		___VertexCurve_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___VertexCurve_5), (void*)value);
	}

	inline static int32_t get_offset_of_AngleMultiplier_6() { return static_cast<int32_t>(offsetof(WarpTextExample_t9CE7000EFB1B8696E32CC86E87E3D0EDEA3D5A96, ___AngleMultiplier_6)); }
	inline float get_AngleMultiplier_6() const { return ___AngleMultiplier_6; }
	inline float* get_address_of_AngleMultiplier_6() { return &___AngleMultiplier_6; }
	inline void set_AngleMultiplier_6(float value)
	{
		___AngleMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_SpeedMultiplier_7() { return static_cast<int32_t>(offsetof(WarpTextExample_t9CE7000EFB1B8696E32CC86E87E3D0EDEA3D5A96, ___SpeedMultiplier_7)); }
	inline float get_SpeedMultiplier_7() const { return ___SpeedMultiplier_7; }
	inline float* get_address_of_SpeedMultiplier_7() { return &___SpeedMultiplier_7; }
	inline void set_SpeedMultiplier_7(float value)
	{
		___SpeedMultiplier_7 = value;
	}

	inline static int32_t get_offset_of_CurveScale_8() { return static_cast<int32_t>(offsetof(WarpTextExample_t9CE7000EFB1B8696E32CC86E87E3D0EDEA3D5A96, ___CurveScale_8)); }
	inline float get_CurveScale_8() const { return ___CurveScale_8; }
	inline float* get_address_of_CurveScale_8() { return &___CurveScale_8; }
	inline void set_CurveScale_8(float value)
	{
		___CurveScale_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4545;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4545 = { sizeof (TextMeshSpawner_t44E5536D2A0C022FA8037C71741171A999A86364), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4546;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4546 = { sizeof (U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4547;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4547 = { sizeof (VertexColorCycler_t37E80E87D8EAD0D7757CDA45BD5D3AA82FD28374), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4548;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4548 = { sizeof (VertexAnim_t3C1CE8C591FCD7E249D77127BED0B743EAD556F4)+ sizeof (RuntimeObject), sizeof(VertexAnim_t3C1CE8C591FCD7E249D77127BED0B743EAD556F4 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4549;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4549 = { sizeof (U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4550;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4550 = { sizeof (VertexJitter_t4BF9FCCB84221E69D9E3727E32E49710F3B4342A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4551;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4551 = { sizeof (U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4552;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4552 = { sizeof (VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4553;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4553 = { sizeof (U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4554;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4554 = { sizeof (VertexShakeB_t348FF1466CEF3BFED37C140B7AEFA2EA90D703C9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4555;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4555 = { sizeof (U3CU3Ec__DisplayClass10_0_t1AC05F5499B93AC94E9727B8B0F08D8037AD52A6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4556;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4556 = { sizeof (U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4557;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4557 = { sizeof (VertexZoom_t02E8994D3A9EB41AEEF1544EF8C93009F6153163), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4558;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4558 = { sizeof (U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4559;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4559 = { sizeof (WarpTextExample_t9CE7000EFB1B8696E32CC86E87E3D0EDEA3D5A96), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4560;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4560 = { sizeof (All1ShaderDemoController_t66C22AC496E9A0D1FF0AB9CDB81DCE2DB60F485B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4561;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4561 = { sizeof (All1TextureOffsetOverTime_t8F1A316C57F3674EE7F61A7C07F07C9E891AD9FF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4562;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4562 = { sizeof (AllIn1ScrollProperty_tD8A7A1DABEA614260D493EAEE03ED89B6F17450B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4563;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4563 = { sizeof (U3CScrollElementsU3Ed__4_tCD9059A48792FEA66DFED0E0F2274DD9F5C097EC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4564;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4564 = { sizeof (Demo2AutoScroll_tF05254B25ADADD0CC28CF7DFC84D7709CA5A7BFB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4565;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4565 = { sizeof (U3CSetCamAfterStartU3Ed__8_t6A9998AF9B7B20E9B90CEB89BBC0E37EE1255338), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4566;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4566 = { sizeof (DemoCamera_tB715A63ACE2506E72DFEAB96E6BBCA8FC6BD30CB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4567;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4567 = { sizeof (DemoCircleExpositor_tDDF61D2EAD6F6D02C955A131906530C1A8F79B4F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4568;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4568 = { sizeof (DemoItem_tA9345BB55EBA626D1FABEC62B65FA14670C9B950), -1, sizeof(DemoItem_tA9345BB55EBA626D1FABEC62B65FA14670C9B950_StaticFields), 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4569;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4569 = { sizeof (DemoRandomColorSwap_t4D6DE09EC4B8935A28D8C1132C3FE5E56EC6D3C6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4570;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4570 = { sizeof (DemoRepositionExpositor_tC8D94A1C9F9F6B7BFAA03E6D611D56D0E589C355), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4571;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4571 = { sizeof (All1CreateUnifiedOutline_tBA1639F5738F0B8808AD83CAD699BA0CE3843871), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4572;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4572 = { sizeof (RandomSeed_t308F7B3257B04EAD3CA31458EED83DA0D1687323), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4573;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4573 = { sizeof (SetAtlasUvs_t6726EDF14C84A61B515F2D7461B45EE43F7B1966), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4574;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4574 = { sizeof (SetGlobalTime_tE062F66EDD7E40277C2793978E483F54B0928A08), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4575;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4575 = { sizeof (All1DemoChangeScene_t745D2B3D1324B9234417476AEFD290126D58822F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4576;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4576 = { sizeof (All1DemoClaw_t5E199580EC962A3A1495325AB0B044AC39944E79), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4577;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4577 = { sizeof (All1DemoRandomZ_tC82E43E979B4238AF1D7827924CA4CE745120AAB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4578;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4578 = { sizeof (All1DemoUpAndDown_t4ED240FF408287F1E3E22BBFC8CCB97947CBFE52), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4579;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4579 = { sizeof (All1DemoUrpCamMove_tB49185CA1616225AF2684AFD24826201D48E6134), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4580;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4580 = { sizeof (U3CModuleU3E_t00DDB26693B7BCCD40544DBD546A67E6CCF39740), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4581;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4581 = { sizeof (U3CU3Ec__DisplayClass0_0_tEEA8EFF30417832F4B36CD90E1600F791CC95E65), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4582;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4582 = { sizeof (U3CU3Ec__DisplayClass1_0_t2600072C3FD2599A6D31A9ED29A03A22589413C2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4583;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4583 = { sizeof (U3CU3Ec__DisplayClass2_0_t0184927D2FE4A42D7384EF3136D50F0672489998), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4584;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4584 = { sizeof (DOTweenModuleAudio_t543BC1904E317A3213F2CD22BEF3065469D62F05), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4585;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4585 = { sizeof (U3CU3Ec__DisplayClass0_0_tA1261915464FFE107DD26211EAE30DFB0FDCDE1B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4586;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4586 = { sizeof (U3CU3Ec__DisplayClass1_0_t36A04C5BE03417FE15562E97048AE1D99D7BC9DE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4587;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4587 = { sizeof (U3CU3Ec__DisplayClass2_0_t8C8461D8370EE781AD60B2927D6F292BA3BC7071), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4588;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4588 = { sizeof (U3CU3Ec__DisplayClass3_0_tDE28A654B70E84028A213E50757FB308DF69837D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4589;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4589 = { sizeof (U3CU3Ec__DisplayClass4_0_tE445E3DD7A59CA1401052CB535F005F72C72289C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4590;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4590 = { sizeof (U3CU3Ec__DisplayClass5_0_tE26E9B7B440EFE64270AE1ADE6D0038D2139292C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4591;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4591 = { sizeof (U3CU3Ec__DisplayClass6_0_tBD455AA807525EA305559C96A573EAAA5D3B91BC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4592;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4592 = { sizeof (U3CU3Ec__DisplayClass7_0_t1DBB166C5F76DFD674B98F06853F228D9592F23E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4593;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4593 = { sizeof (U3CU3Ec__DisplayClass8_0_t0A77D41DBE9E18F3D0436EC0B00E69D63CE7FC06), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4594;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4594 = { sizeof (U3CU3Ec__DisplayClass9_0_t7543D38ED6B04C3E6A9F10E9E9B5B2706F106D80), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4595;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4595 = { sizeof (U3CU3Ec__DisplayClass10_0_tBFDE0479D69A274D55401EF74EE7B80E41D0B9D2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4596;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4596 = { sizeof (DOTweenModulePhysics_t414E562FF12685BE1B2F497B49D50F38EC15A144), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4597;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4597 = { sizeof (U3CU3Ec__DisplayClass0_0_t59C91F47993C660785686C5B2B9602BD270D0ABC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4598;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4598 = { sizeof (U3CU3Ec__DisplayClass1_0_tCA5818119C2BD40AFB4FC0E62ECCB340D6A2EF3B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4599;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4599 = { sizeof (U3CU3Ec__DisplayClass2_0_tC52A012574AEBCF260380BE36A20C3D3C511C78C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4600;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4600 = { sizeof (U3CU3Ec__DisplayClass3_0_tF43D4712D7D347D917CCC52F81AA37B4140B6221), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4601;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4601 = { sizeof (U3CU3Ec__DisplayClass4_0_t5D8B5391F9E829555C0E3D5C13C690EE1FC0882B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4602;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4602 = { sizeof (U3CU3Ec__DisplayClass5_0_t22658A4155648993B6A4478E63486F5D32647B83), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4603;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4603 = { sizeof (U3CU3Ec__DisplayClass6_0_tDB1B8BB04DE5BDCA0C021E08F9A56BFD5CBCC1D3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4604;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4604 = { sizeof (U3CU3Ec__DisplayClass7_0_tE2CDA7BAA411BB740C07230F68F100B86EA9B8C1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4605;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4605 = { sizeof (U3CU3Ec__DisplayClass8_0_tAA4162D037CD083C95351D51A8B2B6431BC60395), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4606;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4606 = { sizeof (DOTweenModulePhysics2D_t3CEB85E8EF37E0F4C0F2C88B476D10A32C1E3B6A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4607;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4607 = { sizeof (U3CU3Ec__DisplayClass0_0_t9BD7D4304FC2CB67490EC66334C5E4AFF9BFBFE9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4608;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4608 = { sizeof (U3CU3Ec__DisplayClass1_0_tD6125020F1D1463CCD52E5E31EB2FB2C07BBDC16), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4609;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4609 = { sizeof (U3CU3Ec__DisplayClass3_0_t09C0D33A30486B259B9B120ED13EB7DC5BE67D21), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4610;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4610 = { sizeof (DOTweenModuleSprite_t47F757863D84DF933AF0E3950C69EFD6AA845BA7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4611;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4611 = { sizeof (Utils_t7418F53E30422B8377F6B148B50CBA4D73EB91C6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4612;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4612 = { sizeof (U3CU3Ec__DisplayClass0_0_tC2EF009253B71108FB5E721B030B250028AB8DF0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4613;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4613 = { sizeof (U3CU3Ec__DisplayClass1_0_tA034AE7DB61BCB47CD7FD835EFDBC68C45C67B1F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4614;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4614 = { sizeof (U3CU3Ec__DisplayClass2_0_t7824BF68052DA9F6B5BABC08C2CF630706983BFF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4615;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4615 = { sizeof (U3CU3Ec__DisplayClass3_0_t5AB498C9410AC66EFDAF2A14D9B43572DB5F3187), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4616;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4616 = { sizeof (U3CU3Ec__DisplayClass4_0_tB484E31E27A9D01E3BB25AF4D20552E7B314EE21), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4617;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4617 = { sizeof (U3CU3Ec__DisplayClass5_0_tD091EE72EF971B43BB9CB23ABF0762A15FC219EF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4618;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4618 = { sizeof (U3CU3Ec__DisplayClass7_0_tC4DB2DC8B7DC8BB09DB48A7536CEC71847744DFB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4619;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4619 = { sizeof (U3CU3Ec__DisplayClass8_0_tC948330164C5D90D6BCA06D460E316067C20A284), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4620;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4620 = { sizeof (U3CU3Ec__DisplayClass9_0_t45A80CA8BC8D26360B96ECBA9C16C98DDB8FBFF9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4621;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4621 = { sizeof (U3CU3Ec__DisplayClass10_0_t46712DD26915FEF867B065291449A39E1C4389F4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4622;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4622 = { sizeof (U3CU3Ec__DisplayClass11_0_t7C74139E9D815DD9E5A90F2BD0DBEC3972BA4AA2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4623;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4623 = { sizeof (U3CU3Ec__DisplayClass12_0_t99ECE448D576B255AEA15DD30EA5E7EEEC239AD4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4624;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4624 = { sizeof (U3CU3Ec__DisplayClass13_0_t56F545FEED596E262C041215923B9AB718A8012E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4625;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4625 = { sizeof (U3CU3Ec__DisplayClass14_0_tBC68B1D1F46519170BBF39DF32EA928C523894BC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4626;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4626 = { sizeof (U3CU3Ec__DisplayClass15_0_tCBE7359CC5A37A4EB57FE6645D8747CD72E64ABE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4627;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4627 = { sizeof (U3CU3Ec__DisplayClass16_0_tC3F94EE8FF590D986468D58FC6A98B1A853CAD39), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4628;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4628 = { sizeof (U3CU3Ec__DisplayClass17_0_tD6C024EA073B66BEEABC86C52D4DF5D5E6D44834), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4629;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4629 = { sizeof (U3CU3Ec__DisplayClass18_0_tECE7F927669ED93C1B63B2A15741DAD4FECD3A1D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4630;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4630 = { sizeof (U3CU3Ec__DisplayClass19_0_tC1C79936B0CC3327C84F5464CF5EDD741B9982CF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4631;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4631 = { sizeof (U3CU3Ec__DisplayClass20_0_t97519398B17EF2EBB028A2A87122AE1B4EEEE2DE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4632;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4632 = { sizeof (U3CU3Ec__DisplayClass21_0_tA436E6685FA064BC921B5AA0C6B6834A1DB25D1B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4633;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4633 = { sizeof (U3CU3Ec__DisplayClass22_0_tC659DEBA764F66319C5A2E51C8269ACC8D8EA831), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4634;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4634 = { sizeof (U3CU3Ec__DisplayClass23_0_tB215983097317F0F13C9C5E1EC951AD353EAFB4A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4635;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4635 = { sizeof (U3CU3Ec__DisplayClass24_0_tB4266531A17A8F2DBB4245371D4AFB4F59B86CBE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4636;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4636 = { sizeof (U3CU3Ec__DisplayClass25_0_tFD7B77C1798CBDB2998FA0FE51958080E5768BF3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4637;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4637 = { sizeof (U3CU3Ec__DisplayClass26_0_t621BC92C98A92034824603E7FAE682A9EFCE0BF5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4638;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4638 = { sizeof (U3CU3Ec__DisplayClass27_0_tFECD12D643617983E5F7A6F87A1C7D1A09CDCA72), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4639;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4639 = { sizeof (U3CU3Ec__DisplayClass28_0_tC2CE28B4736EA23E814F29C741156E76BE478D62), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4640;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4640 = { sizeof (U3CU3Ec__DisplayClass29_0_tC211D657D2FF1145B447BE8AFC7BFE9BF7B559FC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4641;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4641 = { sizeof (U3CU3Ec__DisplayClass30_0_t603E038217702DD25F01B276B450C75EF72AAC45), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4642;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4642 = { sizeof (U3CU3Ec__DisplayClass31_0_tB69CA5B0D5BA880AEBEA6538A9F652AC34DC72EB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4643;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4643 = { sizeof (U3CU3Ec__DisplayClass32_0_tF3A2EE6CF0838B5BACDAC1F0055812FA553CF3EB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4644;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4644 = { sizeof (U3CU3Ec__DisplayClass33_0_tA698C001E2B4ED0498F08BC403BB3537DEF81388), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4645;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4645 = { sizeof (U3CU3Ec__DisplayClass34_0_tF34CFB3D582FB76EBBD4988DB05E0FF88A300B65), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4646;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4646 = { sizeof (U3CU3Ec__DisplayClass35_0_t0508164E64C6FCB8163048A2BDF6AC1DA322A6D3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4647;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4647 = { sizeof (U3CU3Ec__DisplayClass36_0_t1822B5FD884B7F114AE8D127456AE144480D2AB7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4648;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4648 = { sizeof (U3CU3Ec__DisplayClass37_0_t4E8C18778C78A6917596D8C43A8A1193A27BD719), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4649;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4649 = { sizeof (U3CU3Ec__DisplayClass38_0_tC895FF3B6BF5E3B981757EEEEF7B92DF8921CEF3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4650;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4650 = { sizeof (U3CU3Ec__DisplayClass39_0_t3B390AF92BA6587233EB45EA510C0B964CEA5E85), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4651;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4651 = { sizeof (U3CU3Ec__DisplayClass40_0_t9AC2D818C1A29DD006244E0FD140AF47F6063D75), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4652;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4652 = { sizeof (U3CU3Ec__DisplayClass41_0_t7357FED270559E937C6096D1CB738C0EF5AA4DC4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4653;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4653 = { sizeof (DOTweenModuleUI_tB7CCDB438BF13FDE9F6D1821A4209264D15E838C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4654;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4654 = { sizeof (U3CU3Ec__DisplayClass8_0_tA4E985E6B2BDBFEBB6D60F8C6849A49889FB4408), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4655;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4655 = { sizeof (U3CU3Ec__DisplayClass9_0_t0D399C765492884FD834195CAA9979C9DD9F0E77), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4656;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4656 = { sizeof (U3CAsyncWaitForCompletionU3Ed__10_t3E0E42DB84F8756703D33D722892C9AA4B47BE05)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4657;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4657 = { sizeof (U3CAsyncWaitForRewindU3Ed__11_tF0EC64BBA722E64EB39CEE1AF064F9C86C99D394)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4658;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4658 = { sizeof (U3CAsyncWaitForKillU3Ed__12_tE01D14FE8C3DA6A6E7AE41CFBCA93F0006EEC74C)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4659;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4659 = { sizeof (U3CAsyncWaitForElapsedLoopsU3Ed__13_t02CD6395191734801D7559C69A870C35E3F5BAA4)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4660;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4660 = { sizeof (U3CAsyncWaitForPositionU3Ed__14_t967D3DD670C2DF6ED3578D83496A2EA55E2399EF)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4661;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4661 = { sizeof (U3CAsyncWaitForStartU3Ed__15_tF788606B86239515A56729BADA889C7FA953D434)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4662;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4662 = { sizeof (DOTweenModuleUnityVersion_tD1A53828BF15F4A43BA32F551C5399900537B543), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4663;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4663 = { sizeof (WaitForCompletion_t1E6C917DF8283C359606916D4568BF90BADF2A31), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4664;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4664 = { sizeof (WaitForRewind_t4B81EC806C18944320A115540DD5662347EF9881), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4665;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4665 = { sizeof (WaitForKill_t83BF7021ECF7425C005B480D7A4DFD34B395E257), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4666;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4666 = { sizeof (WaitForElapsedLoops_t814A277753ACAF2E527FBEDAB7B34438DD308A3B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4667;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4667 = { sizeof (WaitForPosition_t1FD3726569822C1BE6C5922A6D6FBA319FF441EA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4668;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4668 = { sizeof (WaitForStart_tA1CFF8828622F5558D854DD2EA5677F8F76D2CEE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4669;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4669 = { sizeof (DOTweenCYInstruction_t2B136E8CAA516D794B5E725B7694091A39B8E048), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4670;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4670 = { sizeof (Physics_tAC6486071FC9891A5D19DC417B8F421FDA56C36F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize4671;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize4671 = { sizeof (DOTweenModuleUtils_tE7556A40C5BD3526261318C1BAB11C05E3C21D22), -1, sizeof(DOTweenModuleUtils_tE7556A40C5BD3526261318C1BAB11C05E3C21D22_StaticFields), 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
