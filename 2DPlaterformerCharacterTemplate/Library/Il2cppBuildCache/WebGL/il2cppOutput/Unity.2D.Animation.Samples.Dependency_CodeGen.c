﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Unity.U2D.Animation.Sample.Dependency.AnimationSampleDependency::Update()
extern void AnimationSampleDependency_Update_m0471AEEFB5CBA47B226980DB69CDE619C1F22F5F (void);
// 0x00000002 System.Void Unity.U2D.Animation.Sample.Dependency.AnimationSampleDependency::.ctor()
extern void AnimationSampleDependency__ctor_m57B83B798984B6DFB3E7C7785C6D8A9128EFB6A6 (void);
static Il2CppMethodPointer s_methodPointers[2] = 
{
	AnimationSampleDependency_Update_m0471AEEFB5CBA47B226980DB69CDE619C1F22F5F,
	AnimationSampleDependency__ctor_m57B83B798984B6DFB3E7C7785C6D8A9128EFB6A6,
};
static const int32_t s_InvokerIndices[2] = 
{
	3890,
	3890,
};
extern const CustomAttributesCacheGenerator g_Unity_2D_Animation_Samples_Dependency_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Unity_2D_Animation_Samples_Dependency_CodeGenModule;
const Il2CppCodeGenModule g_Unity_2D_Animation_Samples_Dependency_CodeGenModule = 
{
	"Unity.2D.Animation.Samples.Dependency.dll",
	2,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_Unity_2D_Animation_Samples_Dependency_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
