﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void Unity.U2D.Animation.Sample.LoadSwapDLC::LoadAssetBundle()
extern void LoadSwapDLC_LoadAssetBundle_m301C6948D8818670E724C70BD102A3AEA87B3241 (void);
// 0x00000002 System.Void Unity.U2D.Animation.Sample.LoadSwapDLC::.ctor()
extern void LoadSwapDLC__ctor_mBBB28374E13DE271CB6D7409EFCA4451F58F6E88 (void);
// 0x00000003 System.Void Unity.U2D.Animation.Sample.SwapFullSkin::Start()
extern void SwapFullSkin_Start_mD1816F2BEEE956E48C51D7F3A84980476472473C (void);
// 0x00000004 System.Void Unity.U2D.Animation.Sample.SwapFullSkin::OnDropDownValueChanged(System.Int32)
extern void SwapFullSkin_OnDropDownValueChanged_m7083F235642A81B6D2F6B81B43B8C0DED47F2A45 (void);
// 0x00000005 System.Void Unity.U2D.Animation.Sample.SwapFullSkin::UpdateSelectionChoice()
extern void SwapFullSkin_UpdateSelectionChoice_mE20B29107CD6953FEAFCF2707C150CD55AAA3F78 (void);
// 0x00000006 System.Void Unity.U2D.Animation.Sample.SwapFullSkin::.ctor()
extern void SwapFullSkin__ctor_mA12BC6BA6C3A18C09534BA7C181D7B0D2132B46D (void);
// 0x00000007 System.Void Unity.U2D.Animation.Sample.SwapPart::Start()
extern void SwapPart_Start_m50A8093A53940A60C36B6C2402E6527BB69C93AC (void);
// 0x00000008 System.Void Unity.U2D.Animation.Sample.SwapPart::.ctor()
extern void SwapPart__ctor_mA3952C56B7F13FB47C5E3750921B5E81AF43F796 (void);
// 0x00000009 System.Void Unity.U2D.Animation.Sample.SwapPart/<>c__DisplayClass2_0::.ctor()
extern void U3CU3Ec__DisplayClass2_0__ctor_m68376F15EF2246A0E99F1E9513F11E037F84CE1C (void);
// 0x0000000A System.Void Unity.U2D.Animation.Sample.SwapPart/<>c__DisplayClass2_0::<Start>b__0(System.Int32)
extern void U3CU3Ec__DisplayClass2_0_U3CStartU3Eb__0_m9083B95FAEDB7D46812287FDB7003029DD4BA255 (void);
static Il2CppMethodPointer s_methodPointers[10] = 
{
	LoadSwapDLC_LoadAssetBundle_m301C6948D8818670E724C70BD102A3AEA87B3241,
	LoadSwapDLC__ctor_mBBB28374E13DE271CB6D7409EFCA4451F58F6E88,
	SwapFullSkin_Start_mD1816F2BEEE956E48C51D7F3A84980476472473C,
	SwapFullSkin_OnDropDownValueChanged_m7083F235642A81B6D2F6B81B43B8C0DED47F2A45,
	SwapFullSkin_UpdateSelectionChoice_mE20B29107CD6953FEAFCF2707C150CD55AAA3F78,
	SwapFullSkin__ctor_mA12BC6BA6C3A18C09534BA7C181D7B0D2132B46D,
	SwapPart_Start_m50A8093A53940A60C36B6C2402E6527BB69C93AC,
	SwapPart__ctor_mA3952C56B7F13FB47C5E3750921B5E81AF43F796,
	U3CU3Ec__DisplayClass2_0__ctor_m68376F15EF2246A0E99F1E9513F11E037F84CE1C,
	U3CU3Ec__DisplayClass2_0_U3CStartU3Eb__0_m9083B95FAEDB7D46812287FDB7003029DD4BA255,
};
static const int32_t s_InvokerIndices[10] = 
{
	3890,
	3890,
	3890,
	3123,
	3890,
	3890,
	3890,
	3890,
	3890,
	3123,
};
extern const CustomAttributesCacheGenerator g_Unity_2D_Animation_Samples_Runtime_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_Unity_2D_Animation_Samples_Runtime_CodeGenModule;
const Il2CppCodeGenModule g_Unity_2D_Animation_Samples_Runtime_CodeGenModule = 
{
	"Unity.2D.Animation.Samples.Runtime.dll",
	10,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_Unity_2D_Animation_Samples_Runtime_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
