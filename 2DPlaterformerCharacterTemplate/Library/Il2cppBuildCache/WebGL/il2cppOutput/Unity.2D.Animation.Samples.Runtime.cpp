﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>


template <typename R>
struct InterfaceFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};

// System.Collections.Generic.Dictionary`2<UnityEngine.Experimental.U2D.Animation.SpriteLibrary/StringAndHash,System.Collections.Generic.Dictionary`2<UnityEngine.Experimental.U2D.Animation.SpriteLibrary/StringAndHash,UnityEngine.Sprite>>
struct Dictionary_2_t9457E0FD7EA3D50BEA3997ED3A3A8D56FA7AB2B3;
// System.Func`2<System.String,System.Int32>
struct Func_2_t22FDC42FE24524FB9989A84A6707691EFD7A5AAC;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t52B1AC8D9E5E1ED28DF6C46A37C9A1B00B394F9D;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset>
struct IEnumerable_1_t75E9B4B978C7F821F4367F78BFDC0F217B2C47C9;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_tBD60400523D840591A17E4CBBACC79397F68FAA2;
// System.Collections.Generic.List`1<UnityEngine.CanvasGroup>
struct List_1_t34AA4AF4E7352129CA58045901530E41445AC16D;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5;
// System.Collections.Generic.List`1<UnityEngine.Experimental.U2D.Animation.SpriteLibCategory>
struct List_1_t2668D3FAFDFE0475A96A3CEEF099939FBBE9DFCF;
// System.Collections.Generic.List`1<UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset>
struct List_1_tD6FC97E7B9C0BAE2041ACEC751C58EFD62D2F9B1;
// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem>
struct List_1_t4CFF6A6E1A912AE4990A34B2AA4A1FE2C9FB0033;
// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>
struct List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4;
// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween>
struct TweenRunner_1_t428873023FD8831B6DCE3CBD53ADD7D37AC8222D;
// UnityEngine.Events.UnityAction`1<System.Int32>
struct UnityAction_1_t5CF46572372725E6225588C466A7AF5C8597AA79;
// UnityEngine.Events.UnityEvent`1<System.Int32>
struct UnityEvent_1_tB235B5DAD099AC425DC059D10DEB8B97A35E2BBF;
// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Delegate[]
struct DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8;
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
// UnityEngine.Object[]
struct ObjectU5BU5D_t1256A8B00BB71C7F582BF08257BE4F826FF64873;
// UnityEngine.UI.Selectable[]
struct SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535;
// UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset[]
struct SpriteLibraryAssetU5BU5D_t8208BFA0067B2DFDA16254E72287E09338255B27;
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
// Unity.U2D.Animation.Sample.SwapFullSkin[]
struct SwapFullSkinU5BU5D_t00CE6E2419CAEB9600A030401C7A056244987CF8;
// Unity.U2D.Animation.Sample.SwapOptionData[]
struct SwapOptionDataU5BU5D_t6D1276EE178F77CE5243BF5799FFAE1BB5F177F2;
// UnityEngine.UI.Dropdown/OptionData[]
struct OptionDataU5BU5D_t76E953160486FF629DE132F60738702D374E11A5;
// UnityEngine.UI.AnimationTriggers
struct AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11;
// UnityEngine.AssetBundle
struct AssetBundle_t4D34D7FDF0F230DC641DC1FCFA2C0E7E9E628FA4;
// UnityEngine.AssetBundleManifest
struct AssetBundleManifest_tC4C999A85A1FC2B60215566D0462EE645674160A;
// System.AsyncCallback
struct AsyncCallback_tA7921BEF974919C46FF8F9D9867C567B200BB0EA;
// System.DelegateData
struct DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288;
// UnityEngine.UI.Dropdown
struct Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96;
// UnityEngine.GameObject
struct GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319;
// UnityEngine.UI.Graphic
struct Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24;
// System.IAsyncResult
struct IAsyncResult_tC9F97BF36FCF122D29D3101D80642278297BF370;
// UnityEngine.UI.Image
struct Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9;
// Unity.U2D.Animation.Sample.LoadSwapDLC
struct LoadSwapDLC_t2B7B0C8BE6033FEA11E55BBA53380698F97AFE49;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A;
// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC;
// UnityEngine.RectTransform
struct RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072;
// UnityEngine.UI.Selectable
struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD;
// UnityEngine.Sprite
struct Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9;
// UnityEngine.Experimental.U2D.Animation.SpriteLibrary
struct SpriteLibrary_tC62AFE771C7CF1FA407ECE48A4C627139F415C3A;
// UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset
struct SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF;
// UnityEngine.Experimental.U2D.Animation.SpriteResolver
struct SpriteResolver_t5B3D64444E443E530CC3C9CA5336DED4721F7956;
// System.String
struct String_t;
// Unity.U2D.Animation.Sample.SwapFullSkin
struct SwapFullSkin_t115938BCBBCAED7A2735A385C54CC8C3CE3B82A8;
// Unity.U2D.Animation.Sample.SwapPart
struct SwapPart_t3D3F08F6133F7F815ABC581DC01B75B8FAB631BF;
// UnityEngine.UI.Text
struct Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;
// UnityEngine.UI.Dropdown/DropdownEvent
struct DropdownEvent_tEB2C75C3DBC789936B31D9A979FD62E047846CFB;
// UnityEngine.UI.Dropdown/OptionData
struct OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857;
// UnityEngine.UI.Dropdown/OptionDataList
struct OptionDataList_t524EBDB7A2B178269FD5B4740108D0EC6404B4B6;
// Unity.U2D.Animation.Sample.SwapPart/<>c__DisplayClass2_0
struct U3CU3Ec__DisplayClass2_0_tE1B0DB639BE84CA15D2CBD9694D84FDEBB76188F;

IL2CPP_EXTERN_C RuntimeClass* Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerable_1_tBD60400523D840591A17E4CBBACC79397F68FAA2_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_1_t0DE5AA701B682A891412350E63D3E441F98F205C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Path_tF1D95B78D57C1C1211BA6633FF2AC22FD6C48921_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* U3CU3Ec__DisplayClass2_0_tE1B0DB639BE84CA15D2CBD9694D84FDEBB76188F_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* UnityAction_1_t5CF46572372725E6225588C466A7AF5C8597AA79_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral143AE77221E6B246FE30F96946F8BA08E6E45D4B;
IL2CPP_EXTERN_C String_t* _stringLiteral540C7DC960FC2D69DA59953A19B3C046E4F5F427;
IL2CPP_EXTERN_C String_t* _stringLiteralB217B9D874A0DC94192798D2FDAA1F2A10B2022D;
IL2CPP_EXTERN_C String_t* _stringLiteralF85CF216DC124F456BB630B25B86A7E27701E678;
IL2CPP_EXTERN_C const RuntimeMethod* AssetBundle_LoadAsset_TisAssetBundleManifest_tC4C999A85A1FC2B60215566D0462EE645674160A_m1ACB39E544E25B9EFC8E9DC0E2C8FDCE3740D767_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_Count_TisString_t_m73DFA608B1C4B59DF24B8910F113F39B62FF139D_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Enumerable_ToList_TisSpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF_m184B25FE5D05152697CE9C1FA0A68C977FE9C803_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_m3A23DE48D4BE459A85DB75B470E3BCFA63662334_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_Add_mC2611000DA97145D1DFC867ECA604FBB3E8628EC_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_ToArray_m0F5F017BABE20CE363FDBE9E9DA9CF4574C2BF12_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1__ctor_m32A29DCC109DE9CA0E9445719CB6CEEC99AEDA05_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* List_1_get_Item_mF979E11037D572E72D866EC260EF0434EF296E67_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* SwapFullSkin_OnDropDownValueChanged_m7083F235642A81B6D2F6B81B43B8C0DED47F2A45_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* U3CU3Ec__DisplayClass2_0_U3CStartU3Eb__0_m9083B95FAEDB7D46812287FDB7003029DD4BA255_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityAction_1__ctor_m595DA2ECB187B2DF951D640BFEBCE02F3F800A3F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* UnityEvent_1_AddListener_mFCFAC8ACA3F75283268DC2629ADEB5504E8FC0C2_RuntimeMethod_var;
struct Delegate_t_marshaled_com;
struct Delegate_t_marshaled_pinvoke;

struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE;
struct ObjectU5BU5D_t1256A8B00BB71C7F582BF08257BE4F826FF64873;
struct SpriteLibraryAssetU5BU5D_t8208BFA0067B2DFDA16254E72287E09338255B27;
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A;
struct SwapFullSkinU5BU5D_t00CE6E2419CAEB9600A030401C7A056244987CF8;
struct SwapOptionDataU5BU5D_t6D1276EE178F77CE5243BF5799FFAE1BB5F177F2;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tEC29A58B70AD672393B5FF8C118D3EDDBF1A3888 
{
public:

public:
};


// System.Object


// System.Collections.Generic.List`1<System.Object>
struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____items_1)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__items_1() const { return ____items_1; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5_StaticFields, ____emptyArray_5)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get__emptyArray_5() const { return ____emptyArray_5; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset>
struct List_1_tD6FC97E7B9C0BAE2041ACEC751C58EFD62D2F9B1  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	SpriteLibraryAssetU5BU5D_t8208BFA0067B2DFDA16254E72287E09338255B27* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tD6FC97E7B9C0BAE2041ACEC751C58EFD62D2F9B1, ____items_1)); }
	inline SpriteLibraryAssetU5BU5D_t8208BFA0067B2DFDA16254E72287E09338255B27* get__items_1() const { return ____items_1; }
	inline SpriteLibraryAssetU5BU5D_t8208BFA0067B2DFDA16254E72287E09338255B27** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(SpriteLibraryAssetU5BU5D_t8208BFA0067B2DFDA16254E72287E09338255B27* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tD6FC97E7B9C0BAE2041ACEC751C58EFD62D2F9B1, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tD6FC97E7B9C0BAE2041ACEC751C58EFD62D2F9B1, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tD6FC97E7B9C0BAE2041ACEC751C58EFD62D2F9B1, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tD6FC97E7B9C0BAE2041ACEC751C58EFD62D2F9B1_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	SpriteLibraryAssetU5BU5D_t8208BFA0067B2DFDA16254E72287E09338255B27* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tD6FC97E7B9C0BAE2041ACEC751C58EFD62D2F9B1_StaticFields, ____emptyArray_5)); }
	inline SpriteLibraryAssetU5BU5D_t8208BFA0067B2DFDA16254E72287E09338255B27* get__emptyArray_5() const { return ____emptyArray_5; }
	inline SpriteLibraryAssetU5BU5D_t8208BFA0067B2DFDA16254E72287E09338255B27** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(SpriteLibraryAssetU5BU5D_t8208BFA0067B2DFDA16254E72287E09338255B27* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};


// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>
struct List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	OptionDataU5BU5D_t76E953160486FF629DE132F60738702D374E11A5* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4, ____items_1)); }
	inline OptionDataU5BU5D_t76E953160486FF629DE132F60738702D374E11A5* get__items_1() const { return ____items_1; }
	inline OptionDataU5BU5D_t76E953160486FF629DE132F60738702D374E11A5** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(OptionDataU5BU5D_t76E953160486FF629DE132F60738702D374E11A5* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____items_1), (void*)value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_4), (void*)value);
	}
};

struct List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	OptionDataU5BU5D_t76E953160486FF629DE132F60738702D374E11A5* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4_StaticFields, ____emptyArray_5)); }
	inline OptionDataU5BU5D_t76E953160486FF629DE132F60738702D374E11A5* get__emptyArray_5() const { return ____emptyArray_5; }
	inline OptionDataU5BU5D_t76E953160486FF629DE132F60738702D374E11A5** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(OptionDataU5BU5D_t76E953160486FF629DE132F60738702D374E11A5* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____emptyArray_5), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// UnityEngine.Events.UnityEventBase
struct UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * ___m_PersistentCalls_1;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_2;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_Calls_0)); }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_tB7C66AA0C00F9C102C8BDC17A144E569AC7527A9 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Calls_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t9A1D83DA2BA3118C103FA87D93CE92557A956FDC * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PersistentCalls_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_2() { return static_cast<int32_t>(offsetof(UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB, ___m_CallsDirty_2)); }
	inline bool get_m_CallsDirty_2() const { return ___m_CallsDirty_2; }
	inline bool* get_address_of_m_CallsDirty_2() { return &___m_CallsDirty_2; }
	inline void set_m_CallsDirty_2(bool value)
	{
		___m_CallsDirty_2 = value;
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// UnityEngine.UI.Dropdown/OptionData
struct OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857  : public RuntimeObject
{
public:
	// System.String UnityEngine.UI.Dropdown/OptionData::m_Text
	String_t* ___m_Text_0;
	// UnityEngine.Sprite UnityEngine.UI.Dropdown/OptionData::m_Image
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_Image_1;

public:
	inline static int32_t get_offset_of_m_Text_0() { return static_cast<int32_t>(offsetof(OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857, ___m_Text_0)); }
	inline String_t* get_m_Text_0() const { return ___m_Text_0; }
	inline String_t** get_address_of_m_Text_0() { return &___m_Text_0; }
	inline void set_m_Text_0(String_t* value)
	{
		___m_Text_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Text_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Image_1() { return static_cast<int32_t>(offsetof(OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857, ___m_Image_1)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_Image_1() const { return ___m_Image_1; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_Image_1() { return &___m_Image_1; }
	inline void set_m_Image_1(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_Image_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Image_1), (void*)value);
	}
};


// UnityEngine.Events.UnityEvent`1<System.Int32>
struct UnityEvent_1_tB235B5DAD099AC425DC059D10DEB8B97A35E2BBF  : public UnityEventBase_tBB43047292084BA63C5CBB1A379A8BB88611C6FB
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* ___m_InvokeArray_3;

public:
	inline static int32_t get_offset_of_m_InvokeArray_3() { return static_cast<int32_t>(offsetof(UnityEvent_1_tB235B5DAD099AC425DC059D10DEB8B97A35E2BBF, ___m_InvokeArray_3)); }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* get_m_InvokeArray_3() const { return ___m_InvokeArray_3; }
	inline ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE** get_address_of_m_InvokeArray_3() { return &___m_InvokeArray_3; }
	inline void set_m_InvokeArray_3(ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* value)
	{
		___m_InvokeArray_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_InvokeArray_3), (void*)value);
	}
};


// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// UnityEngine.Color
struct Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// System.Int32
struct Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_tFDE5F8CD43D10453F6A2E0C77FE48C6CC7009046, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E 
{
public:
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_HighlightedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_PressedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_SelectedSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	// UnityEngine.Sprite UnityEngine.UI.SpriteState::m_DisabledSprite
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;

public:
	inline static int32_t get_offset_of_m_HighlightedSprite_0() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_HighlightedSprite_0)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_HighlightedSprite_0() const { return ___m_HighlightedSprite_0; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_HighlightedSprite_0() { return &___m_HighlightedSprite_0; }
	inline void set_m_HighlightedSprite_0(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_HighlightedSprite_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_HighlightedSprite_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_PressedSprite_1() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_PressedSprite_1)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_PressedSprite_1() const { return ___m_PressedSprite_1; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_PressedSprite_1() { return &___m_PressedSprite_1; }
	inline void set_m_PressedSprite_1(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_PressedSprite_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_PressedSprite_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectedSprite_2() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_SelectedSprite_2)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_SelectedSprite_2() const { return ___m_SelectedSprite_2; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_SelectedSprite_2() { return &___m_SelectedSprite_2; }
	inline void set_m_SelectedSprite_2(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_SelectedSprite_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectedSprite_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_DisabledSprite_3() { return static_cast<int32_t>(offsetof(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E, ___m_DisabledSprite_3)); }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * get_m_DisabledSprite_3() const { return ___m_DisabledSprite_3; }
	inline Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 ** get_address_of_m_DisabledSprite_3() { return &___m_DisabledSprite_3; }
	inline void set_m_DisabledSprite_3(Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * value)
	{
		___m_DisabledSprite_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_DisabledSprite_3), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_pinvoke
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};
// Native definition for COM marshalling of UnityEngine.UI.SpriteState
struct SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E_marshaled_com
{
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_HighlightedSprite_0;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_PressedSprite_1;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_SelectedSprite_2;
	Sprite_t5B10B1178EC2E6F53D33FFD77557F31C08A51ED9 * ___m_DisabledSprite_3;
};

// Unity.U2D.Animation.Sample.SwapOptionData
struct SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0 
{
public:
	// UnityEngine.UI.Dropdown Unity.U2D.Animation.Sample.SwapOptionData::dropdown
	Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * ___dropdown_0;
	// UnityEngine.Experimental.U2D.Animation.SpriteResolver Unity.U2D.Animation.Sample.SwapOptionData::spriteResolver
	SpriteResolver_t5B3D64444E443E530CC3C9CA5336DED4721F7956 * ___spriteResolver_1;
	// System.String Unity.U2D.Animation.Sample.SwapOptionData::category
	String_t* ___category_2;

public:
	inline static int32_t get_offset_of_dropdown_0() { return static_cast<int32_t>(offsetof(SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0, ___dropdown_0)); }
	inline Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * get_dropdown_0() const { return ___dropdown_0; }
	inline Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 ** get_address_of_dropdown_0() { return &___dropdown_0; }
	inline void set_dropdown_0(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * value)
	{
		___dropdown_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dropdown_0), (void*)value);
	}

	inline static int32_t get_offset_of_spriteResolver_1() { return static_cast<int32_t>(offsetof(SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0, ___spriteResolver_1)); }
	inline SpriteResolver_t5B3D64444E443E530CC3C9CA5336DED4721F7956 * get_spriteResolver_1() const { return ___spriteResolver_1; }
	inline SpriteResolver_t5B3D64444E443E530CC3C9CA5336DED4721F7956 ** get_address_of_spriteResolver_1() { return &___spriteResolver_1; }
	inline void set_spriteResolver_1(SpriteResolver_t5B3D64444E443E530CC3C9CA5336DED4721F7956 * value)
	{
		___spriteResolver_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spriteResolver_1), (void*)value);
	}

	inline static int32_t get_offset_of_category_2() { return static_cast<int32_t>(offsetof(SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0, ___category_2)); }
	inline String_t* get_category_2() const { return ___category_2; }
	inline String_t** get_address_of_category_2() { return &___category_2; }
	inline void set_category_2(String_t* value)
	{
		___category_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___category_2), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of Unity.U2D.Animation.Sample.SwapOptionData
struct SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0_marshaled_pinvoke
{
	Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * ___dropdown_0;
	SpriteResolver_t5B3D64444E443E530CC3C9CA5336DED4721F7956 * ___spriteResolver_1;
	char* ___category_2;
};
// Native definition for COM marshalling of Unity.U2D.Animation.Sample.SwapOptionData
struct SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0_marshaled_com
{
	Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * ___dropdown_0;
	SpriteResolver_t5B3D64444E443E530CC3C9CA5336DED4721F7956 * ___spriteResolver_1;
	Il2CppChar* ___category_2;
};

// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// UnityEngine.UI.ColorBlock
struct ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 
{
public:
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_NormalColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_NormalColor_0;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_HighlightedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_HighlightedColor_1;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_PressedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_PressedColor_2;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_SelectedColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_SelectedColor_3;
	// UnityEngine.Color UnityEngine.UI.ColorBlock::m_DisabledColor
	Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  ___m_DisabledColor_4;
	// System.Single UnityEngine.UI.ColorBlock::m_ColorMultiplier
	float ___m_ColorMultiplier_5;
	// System.Single UnityEngine.UI.ColorBlock::m_FadeDuration
	float ___m_FadeDuration_6;

public:
	inline static int32_t get_offset_of_m_NormalColor_0() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_NormalColor_0)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_NormalColor_0() const { return ___m_NormalColor_0; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_NormalColor_0() { return &___m_NormalColor_0; }
	inline void set_m_NormalColor_0(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_NormalColor_0 = value;
	}

	inline static int32_t get_offset_of_m_HighlightedColor_1() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_HighlightedColor_1)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_HighlightedColor_1() const { return ___m_HighlightedColor_1; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_HighlightedColor_1() { return &___m_HighlightedColor_1; }
	inline void set_m_HighlightedColor_1(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_HighlightedColor_1 = value;
	}

	inline static int32_t get_offset_of_m_PressedColor_2() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_PressedColor_2)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_PressedColor_2() const { return ___m_PressedColor_2; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_PressedColor_2() { return &___m_PressedColor_2; }
	inline void set_m_PressedColor_2(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_PressedColor_2 = value;
	}

	inline static int32_t get_offset_of_m_SelectedColor_3() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_SelectedColor_3)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_SelectedColor_3() const { return ___m_SelectedColor_3; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_SelectedColor_3() { return &___m_SelectedColor_3; }
	inline void set_m_SelectedColor_3(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_SelectedColor_3 = value;
	}

	inline static int32_t get_offset_of_m_DisabledColor_4() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_DisabledColor_4)); }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  get_m_DisabledColor_4() const { return ___m_DisabledColor_4; }
	inline Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659 * get_address_of_m_DisabledColor_4() { return &___m_DisabledColor_4; }
	inline void set_m_DisabledColor_4(Color_tF40DAF76C04FFECF3FE6024F85A294741C9CC659  value)
	{
		___m_DisabledColor_4 = value;
	}

	inline static int32_t get_offset_of_m_ColorMultiplier_5() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_ColorMultiplier_5)); }
	inline float get_m_ColorMultiplier_5() const { return ___m_ColorMultiplier_5; }
	inline float* get_address_of_m_ColorMultiplier_5() { return &___m_ColorMultiplier_5; }
	inline void set_m_ColorMultiplier_5(float value)
	{
		___m_ColorMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_m_FadeDuration_6() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955, ___m_FadeDuration_6)); }
	inline float get_m_FadeDuration_6() const { return ___m_FadeDuration_6; }
	inline float* get_address_of_m_FadeDuration_6() { return &___m_FadeDuration_6; }
	inline void set_m_FadeDuration_6(float value)
	{
		___m_FadeDuration_6 = value;
	}
};

struct ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955_StaticFields
{
public:
	// UnityEngine.UI.ColorBlock UnityEngine.UI.ColorBlock::defaultColorBlock
	ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  ___defaultColorBlock_7;

public:
	inline static int32_t get_offset_of_defaultColorBlock_7() { return static_cast<int32_t>(offsetof(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955_StaticFields, ___defaultColorBlock_7)); }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  get_defaultColorBlock_7() const { return ___defaultColorBlock_7; }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * get_address_of_defaultColorBlock_7() { return &___defaultColorBlock_7; }
	inline void set_defaultColorBlock_7(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  value)
	{
		___defaultColorBlock_7 = value;
	}
};


// System.Delegate
struct Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_target_2), (void*)value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___method_info_7), (void*)value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___original_method_info_8), (void*)value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * get_data_9() const { return ___data_9; }
	inline DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___data_9), (void*)value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t17DD30660E330C49381DAA99F934BE75CB11F288 * ___data_9;
	int32_t ___method_is_virtual_10;
};

// UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.UI.Dropdown/DropdownEvent
struct DropdownEvent_tEB2C75C3DBC789936B31D9A979FD62E047846CFB  : public UnityEvent_1_tB235B5DAD099AC425DC059D10DEB8B97A35E2BBF
{
public:

public:
};


// UnityEngine.UI.Navigation/Mode
struct Mode_t3113FDF05158BBA1DFC78D7F69E4C1D25135CB0F 
{
public:
	// System.Int32 UnityEngine.UI.Navigation/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t3113FDF05158BBA1DFC78D7F69E4C1D25135CB0F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.UI.Selectable/Transition
struct Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14 
{
public:
	// System.Int32 UnityEngine.UI.Selectable/Transition::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Transition_t1FC449676815A798E758D32E8BE6DC0A2511DF14, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// Unity.U2D.Animation.Sample.SwapPart/<>c__DisplayClass2_0
struct U3CU3Ec__DisplayClass2_0_tE1B0DB639BE84CA15D2CBD9694D84FDEBB76188F  : public RuntimeObject
{
public:
	// Unity.U2D.Animation.Sample.SwapOptionData Unity.U2D.Animation.Sample.SwapPart/<>c__DisplayClass2_0::swapOption
	SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0  ___swapOption_0;

public:
	inline static int32_t get_offset_of_swapOption_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tE1B0DB639BE84CA15D2CBD9694D84FDEBB76188F, ___swapOption_0)); }
	inline SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0  get_swapOption_0() const { return ___swapOption_0; }
	inline SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0 * get_address_of_swapOption_0() { return &___swapOption_0; }
	inline void set_swapOption_0(SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0  value)
	{
		___swapOption_0 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___swapOption_0))->___dropdown_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___swapOption_0))->___spriteResolver_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___swapOption_0))->___category_2), (void*)NULL);
		#endif
	}
};


// UnityEngine.AssetBundle
struct AssetBundle_t4D34D7FDF0F230DC641DC1FCFA2C0E7E9E628FA4  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.AssetBundleManifest
struct AssetBundleManifest_tC4C999A85A1FC2B60215566D0462EE645674160A  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// UnityEngine.Component
struct Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};


// System.MulticastDelegate
struct MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_t677D8FE08A5F99E8EE49150B73966CD6E9BF7DB8* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___delegates_11), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	Delegate_t_marshaled_pinvoke** ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	Delegate_t_marshaled_com** ___delegates_11;
};

// UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A 
{
public:
	// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::m_Mode
	int32_t ___m_Mode_0;
	// System.Boolean UnityEngine.UI.Navigation::m_WrapAround
	bool ___m_WrapAround_1;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnUp
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnDown
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnLeft
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::m_SelectOnRight
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;

public:
	inline static int32_t get_offset_of_m_Mode_0() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_Mode_0)); }
	inline int32_t get_m_Mode_0() const { return ___m_Mode_0; }
	inline int32_t* get_address_of_m_Mode_0() { return &___m_Mode_0; }
	inline void set_m_Mode_0(int32_t value)
	{
		___m_Mode_0 = value;
	}

	inline static int32_t get_offset_of_m_WrapAround_1() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_WrapAround_1)); }
	inline bool get_m_WrapAround_1() const { return ___m_WrapAround_1; }
	inline bool* get_address_of_m_WrapAround_1() { return &___m_WrapAround_1; }
	inline void set_m_WrapAround_1(bool value)
	{
		___m_WrapAround_1 = value;
	}

	inline static int32_t get_offset_of_m_SelectOnUp_2() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnUp_2)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnUp_2() const { return ___m_SelectOnUp_2; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnUp_2() { return &___m_SelectOnUp_2; }
	inline void set_m_SelectOnUp_2(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnUp_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnUp_2), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnDown_3() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnDown_3)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnDown_3() const { return ___m_SelectOnDown_3; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnDown_3() { return &___m_SelectOnDown_3; }
	inline void set_m_SelectOnDown_3(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnDown_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnDown_3), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnLeft_4() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnLeft_4)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnLeft_4() const { return ___m_SelectOnLeft_4; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnLeft_4() { return &___m_SelectOnLeft_4; }
	inline void set_m_SelectOnLeft_4(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnLeft_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnLeft_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_SelectOnRight_5() { return static_cast<int32_t>(offsetof(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A, ___m_SelectOnRight_5)); }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * get_m_SelectOnRight_5() const { return ___m_SelectOnRight_5; }
	inline Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD ** get_address_of_m_SelectOnRight_5() { return &___m_SelectOnRight_5; }
	inline void set_m_SelectOnRight_5(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * value)
	{
		___m_SelectOnRight_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SelectOnRight_5), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A_marshaled_pinvoke
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;
};
// Native definition for COM marshalling of UnityEngine.UI.Navigation
struct Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A_marshaled_com
{
	int32_t ___m_Mode_0;
	int32_t ___m_WrapAround_1;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnUp_2;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnDown_3;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnLeft_4;
	Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD * ___m_SelectOnRight_5;
};

// UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A  : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A
{
public:

public:
};

// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_pinvoke : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A_marshaled_com : public Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_marshaled_com
{
};

// UnityEngine.Events.UnityAction`1<System.Int32>
struct UnityAction_1_t5CF46572372725E6225588C466A7AF5C8597AA79  : public MulticastDelegate_t
{
public:

public:
};


// UnityEngine.Behaviour
struct Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9  : public Component_t62FBC8D2420DA4BE9037AFE430740F6B3EECA684
{
public:

public:
};


// UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset
struct SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF  : public ScriptableObject_t4361E08CEBF052C650D3666C7CEC37EB31DE116A
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Experimental.U2D.Animation.SpriteLibCategory> UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset::m_Labels
	List_1_t2668D3FAFDFE0475A96A3CEEF099939FBBE9DFCF * ___m_Labels_4;

public:
	inline static int32_t get_offset_of_m_Labels_4() { return static_cast<int32_t>(offsetof(SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF, ___m_Labels_4)); }
	inline List_1_t2668D3FAFDFE0475A96A3CEEF099939FBBE9DFCF * get_m_Labels_4() const { return ___m_Labels_4; }
	inline List_1_t2668D3FAFDFE0475A96A3CEEF099939FBBE9DFCF ** get_address_of_m_Labels_4() { return &___m_Labels_4; }
	inline void set_m_Labels_4(List_1_t2668D3FAFDFE0475A96A3CEEF099939FBBE9DFCF * value)
	{
		___m_Labels_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Labels_4), (void*)value);
	}
};

struct SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF_StaticFields
{
public:
	// System.Func`2<System.String,System.Int32> UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset::GetStringHash
	Func_2_t22FDC42FE24524FB9989A84A6707691EFD7A5AAC * ___GetStringHash_5;

public:
	inline static int32_t get_offset_of_GetStringHash_5() { return static_cast<int32_t>(offsetof(SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF_StaticFields, ___GetStringHash_5)); }
	inline Func_2_t22FDC42FE24524FB9989A84A6707691EFD7A5AAC * get_GetStringHash_5() const { return ___GetStringHash_5; }
	inline Func_2_t22FDC42FE24524FB9989A84A6707691EFD7A5AAC ** get_address_of_GetStringHash_5() { return &___GetStringHash_5; }
	inline void set_GetStringHash_5(Func_2_t22FDC42FE24524FB9989A84A6707691EFD7A5AAC * value)
	{
		___GetStringHash_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___GetStringHash_5), (void*)value);
	}
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A  : public Behaviour_t1A3DDDCF73B4627928FBFE02ED52B7251777DBD9
{
public:

public:
};


// Unity.U2D.Animation.Sample.LoadSwapDLC
struct LoadSwapDLC_t2B7B0C8BE6033FEA11E55BBA53380698F97AFE49  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// Unity.U2D.Animation.Sample.SwapFullSkin[] Unity.U2D.Animation.Sample.LoadSwapDLC::swapFullSkin
	SwapFullSkinU5BU5D_t00CE6E2419CAEB9600A030401C7A056244987CF8* ___swapFullSkin_5;

public:
	inline static int32_t get_offset_of_swapFullSkin_5() { return static_cast<int32_t>(offsetof(LoadSwapDLC_t2B7B0C8BE6033FEA11E55BBA53380698F97AFE49, ___swapFullSkin_5)); }
	inline SwapFullSkinU5BU5D_t00CE6E2419CAEB9600A030401C7A056244987CF8* get_swapFullSkin_5() const { return ___swapFullSkin_5; }
	inline SwapFullSkinU5BU5D_t00CE6E2419CAEB9600A030401C7A056244987CF8** get_address_of_swapFullSkin_5() { return &___swapFullSkin_5; }
	inline void set_swapFullSkin_5(SwapFullSkinU5BU5D_t00CE6E2419CAEB9600A030401C7A056244987CF8* value)
	{
		___swapFullSkin_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___swapFullSkin_5), (void*)value);
	}
};


// UnityEngine.Experimental.U2D.Animation.SpriteLibrary
struct SpriteLibrary_tC62AFE771C7CF1FA407ECE48A4C627139F415C3A  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset UnityEngine.Experimental.U2D.Animation.SpriteLibrary::m_SpriteLibraryAsset
	SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF * ___m_SpriteLibraryAsset_4;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Experimental.U2D.Animation.SpriteLibrary/StringAndHash,System.Collections.Generic.Dictionary`2<UnityEngine.Experimental.U2D.Animation.SpriteLibrary/StringAndHash,UnityEngine.Sprite>> UnityEngine.Experimental.U2D.Animation.SpriteLibrary::m_Overrides
	Dictionary_2_t9457E0FD7EA3D50BEA3997ED3A3A8D56FA7AB2B3 * ___m_Overrides_5;

public:
	inline static int32_t get_offset_of_m_SpriteLibraryAsset_4() { return static_cast<int32_t>(offsetof(SpriteLibrary_tC62AFE771C7CF1FA407ECE48A4C627139F415C3A, ___m_SpriteLibraryAsset_4)); }
	inline SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF * get_m_SpriteLibraryAsset_4() const { return ___m_SpriteLibraryAsset_4; }
	inline SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF ** get_address_of_m_SpriteLibraryAsset_4() { return &___m_SpriteLibraryAsset_4; }
	inline void set_m_SpriteLibraryAsset_4(SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF * value)
	{
		___m_SpriteLibraryAsset_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_SpriteLibraryAsset_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_Overrides_5() { return static_cast<int32_t>(offsetof(SpriteLibrary_tC62AFE771C7CF1FA407ECE48A4C627139F415C3A, ___m_Overrides_5)); }
	inline Dictionary_2_t9457E0FD7EA3D50BEA3997ED3A3A8D56FA7AB2B3 * get_m_Overrides_5() const { return ___m_Overrides_5; }
	inline Dictionary_2_t9457E0FD7EA3D50BEA3997ED3A3A8D56FA7AB2B3 ** get_address_of_m_Overrides_5() { return &___m_Overrides_5; }
	inline void set_m_Overrides_5(Dictionary_2_t9457E0FD7EA3D50BEA3997ED3A3A8D56FA7AB2B3 * value)
	{
		___m_Overrides_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Overrides_5), (void*)value);
	}
};


// UnityEngine.Experimental.U2D.Animation.SpriteResolver
struct SpriteResolver_t5B3D64444E443E530CC3C9CA5336DED4721F7956  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// System.Single UnityEngine.Experimental.U2D.Animation.SpriteResolver::m_CategoryHash
	float ___m_CategoryHash_4;
	// System.Single UnityEngine.Experimental.U2D.Animation.SpriteResolver::m_labelHash
	float ___m_labelHash_5;
	// System.Int32 UnityEngine.Experimental.U2D.Animation.SpriteResolver::m_CategoryHashInt
	int32_t ___m_CategoryHashInt_6;
	// System.Int32 UnityEngine.Experimental.U2D.Animation.SpriteResolver::m_LabelHashInt
	int32_t ___m_LabelHashInt_7;
	// System.Int32 UnityEngine.Experimental.U2D.Animation.SpriteResolver::m_PreviousCategoryHash
	int32_t ___m_PreviousCategoryHash_8;
	// System.Int32 UnityEngine.Experimental.U2D.Animation.SpriteResolver::m_PreviouslabelHash
	int32_t ___m_PreviouslabelHash_9;

public:
	inline static int32_t get_offset_of_m_CategoryHash_4() { return static_cast<int32_t>(offsetof(SpriteResolver_t5B3D64444E443E530CC3C9CA5336DED4721F7956, ___m_CategoryHash_4)); }
	inline float get_m_CategoryHash_4() const { return ___m_CategoryHash_4; }
	inline float* get_address_of_m_CategoryHash_4() { return &___m_CategoryHash_4; }
	inline void set_m_CategoryHash_4(float value)
	{
		___m_CategoryHash_4 = value;
	}

	inline static int32_t get_offset_of_m_labelHash_5() { return static_cast<int32_t>(offsetof(SpriteResolver_t5B3D64444E443E530CC3C9CA5336DED4721F7956, ___m_labelHash_5)); }
	inline float get_m_labelHash_5() const { return ___m_labelHash_5; }
	inline float* get_address_of_m_labelHash_5() { return &___m_labelHash_5; }
	inline void set_m_labelHash_5(float value)
	{
		___m_labelHash_5 = value;
	}

	inline static int32_t get_offset_of_m_CategoryHashInt_6() { return static_cast<int32_t>(offsetof(SpriteResolver_t5B3D64444E443E530CC3C9CA5336DED4721F7956, ___m_CategoryHashInt_6)); }
	inline int32_t get_m_CategoryHashInt_6() const { return ___m_CategoryHashInt_6; }
	inline int32_t* get_address_of_m_CategoryHashInt_6() { return &___m_CategoryHashInt_6; }
	inline void set_m_CategoryHashInt_6(int32_t value)
	{
		___m_CategoryHashInt_6 = value;
	}

	inline static int32_t get_offset_of_m_LabelHashInt_7() { return static_cast<int32_t>(offsetof(SpriteResolver_t5B3D64444E443E530CC3C9CA5336DED4721F7956, ___m_LabelHashInt_7)); }
	inline int32_t get_m_LabelHashInt_7() const { return ___m_LabelHashInt_7; }
	inline int32_t* get_address_of_m_LabelHashInt_7() { return &___m_LabelHashInt_7; }
	inline void set_m_LabelHashInt_7(int32_t value)
	{
		___m_LabelHashInt_7 = value;
	}

	inline static int32_t get_offset_of_m_PreviousCategoryHash_8() { return static_cast<int32_t>(offsetof(SpriteResolver_t5B3D64444E443E530CC3C9CA5336DED4721F7956, ___m_PreviousCategoryHash_8)); }
	inline int32_t get_m_PreviousCategoryHash_8() const { return ___m_PreviousCategoryHash_8; }
	inline int32_t* get_address_of_m_PreviousCategoryHash_8() { return &___m_PreviousCategoryHash_8; }
	inline void set_m_PreviousCategoryHash_8(int32_t value)
	{
		___m_PreviousCategoryHash_8 = value;
	}

	inline static int32_t get_offset_of_m_PreviouslabelHash_9() { return static_cast<int32_t>(offsetof(SpriteResolver_t5B3D64444E443E530CC3C9CA5336DED4721F7956, ___m_PreviouslabelHash_9)); }
	inline int32_t get_m_PreviouslabelHash_9() const { return ___m_PreviouslabelHash_9; }
	inline int32_t* get_address_of_m_PreviouslabelHash_9() { return &___m_PreviouslabelHash_9; }
	inline void set_m_PreviouslabelHash_9(int32_t value)
	{
		___m_PreviouslabelHash_9 = value;
	}
};


// Unity.U2D.Animation.Sample.SwapFullSkin
struct SwapFullSkin_t115938BCBBCAED7A2735A385C54CC8C3CE3B82A8  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset[] Unity.U2D.Animation.Sample.SwapFullSkin::spriteLibraries
	SpriteLibraryAssetU5BU5D_t8208BFA0067B2DFDA16254E72287E09338255B27* ___spriteLibraries_4;
	// UnityEngine.Experimental.U2D.Animation.SpriteLibrary Unity.U2D.Animation.Sample.SwapFullSkin::spriteLibraryTarget
	SpriteLibrary_tC62AFE771C7CF1FA407ECE48A4C627139F415C3A * ___spriteLibraryTarget_5;
	// UnityEngine.UI.Dropdown Unity.U2D.Animation.Sample.SwapFullSkin::dropDownSelection
	Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * ___dropDownSelection_6;

public:
	inline static int32_t get_offset_of_spriteLibraries_4() { return static_cast<int32_t>(offsetof(SwapFullSkin_t115938BCBBCAED7A2735A385C54CC8C3CE3B82A8, ___spriteLibraries_4)); }
	inline SpriteLibraryAssetU5BU5D_t8208BFA0067B2DFDA16254E72287E09338255B27* get_spriteLibraries_4() const { return ___spriteLibraries_4; }
	inline SpriteLibraryAssetU5BU5D_t8208BFA0067B2DFDA16254E72287E09338255B27** get_address_of_spriteLibraries_4() { return &___spriteLibraries_4; }
	inline void set_spriteLibraries_4(SpriteLibraryAssetU5BU5D_t8208BFA0067B2DFDA16254E72287E09338255B27* value)
	{
		___spriteLibraries_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spriteLibraries_4), (void*)value);
	}

	inline static int32_t get_offset_of_spriteLibraryTarget_5() { return static_cast<int32_t>(offsetof(SwapFullSkin_t115938BCBBCAED7A2735A385C54CC8C3CE3B82A8, ___spriteLibraryTarget_5)); }
	inline SpriteLibrary_tC62AFE771C7CF1FA407ECE48A4C627139F415C3A * get_spriteLibraryTarget_5() const { return ___spriteLibraryTarget_5; }
	inline SpriteLibrary_tC62AFE771C7CF1FA407ECE48A4C627139F415C3A ** get_address_of_spriteLibraryTarget_5() { return &___spriteLibraryTarget_5; }
	inline void set_spriteLibraryTarget_5(SpriteLibrary_tC62AFE771C7CF1FA407ECE48A4C627139F415C3A * value)
	{
		___spriteLibraryTarget_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spriteLibraryTarget_5), (void*)value);
	}

	inline static int32_t get_offset_of_dropDownSelection_6() { return static_cast<int32_t>(offsetof(SwapFullSkin_t115938BCBBCAED7A2735A385C54CC8C3CE3B82A8, ___dropDownSelection_6)); }
	inline Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * get_dropDownSelection_6() const { return ___dropDownSelection_6; }
	inline Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 ** get_address_of_dropDownSelection_6() { return &___dropDownSelection_6; }
	inline void set_dropDownSelection_6(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * value)
	{
		___dropDownSelection_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___dropDownSelection_6), (void*)value);
	}
};


// Unity.U2D.Animation.Sample.SwapPart
struct SwapPart_t3D3F08F6133F7F815ABC581DC01B75B8FAB631BF  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:
	// UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset Unity.U2D.Animation.Sample.SwapPart::spriteLibraryAsset
	SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF * ___spriteLibraryAsset_4;
	// Unity.U2D.Animation.Sample.SwapOptionData[] Unity.U2D.Animation.Sample.SwapPart::swapOptionData
	SwapOptionDataU5BU5D_t6D1276EE178F77CE5243BF5799FFAE1BB5F177F2* ___swapOptionData_5;

public:
	inline static int32_t get_offset_of_spriteLibraryAsset_4() { return static_cast<int32_t>(offsetof(SwapPart_t3D3F08F6133F7F815ABC581DC01B75B8FAB631BF, ___spriteLibraryAsset_4)); }
	inline SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF * get_spriteLibraryAsset_4() const { return ___spriteLibraryAsset_4; }
	inline SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF ** get_address_of_spriteLibraryAsset_4() { return &___spriteLibraryAsset_4; }
	inline void set_spriteLibraryAsset_4(SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF * value)
	{
		___spriteLibraryAsset_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___spriteLibraryAsset_4), (void*)value);
	}

	inline static int32_t get_offset_of_swapOptionData_5() { return static_cast<int32_t>(offsetof(SwapPart_t3D3F08F6133F7F815ABC581DC01B75B8FAB631BF, ___swapOptionData_5)); }
	inline SwapOptionDataU5BU5D_t6D1276EE178F77CE5243BF5799FFAE1BB5F177F2* get_swapOptionData_5() const { return ___swapOptionData_5; }
	inline SwapOptionDataU5BU5D_t6D1276EE178F77CE5243BF5799FFAE1BB5F177F2** get_address_of_swapOptionData_5() { return &___swapOptionData_5; }
	inline void set_swapOptionData_5(SwapOptionDataU5BU5D_t6D1276EE178F77CE5243BF5799FFAE1BB5F177F2* value)
	{
		___swapOptionData_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___swapOptionData_5), (void*)value);
	}
};


// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E  : public MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A
{
public:

public:
};


// UnityEngine.UI.Selectable
struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD  : public UIBehaviour_tD1C6E2D542222546D68510ECE74036EFBC3C3B0E
{
public:
	// System.Boolean UnityEngine.UI.Selectable::m_EnableCalled
	bool ___m_EnableCalled_6;
	// UnityEngine.UI.Navigation UnityEngine.UI.Selectable::m_Navigation
	Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  ___m_Navigation_7;
	// UnityEngine.UI.Selectable/Transition UnityEngine.UI.Selectable::m_Transition
	int32_t ___m_Transition_8;
	// UnityEngine.UI.ColorBlock UnityEngine.UI.Selectable::m_Colors
	ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  ___m_Colors_9;
	// UnityEngine.UI.SpriteState UnityEngine.UI.Selectable::m_SpriteState
	SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  ___m_SpriteState_10;
	// UnityEngine.UI.AnimationTriggers UnityEngine.UI.Selectable::m_AnimationTriggers
	AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * ___m_AnimationTriggers_11;
	// System.Boolean UnityEngine.UI.Selectable::m_Interactable
	bool ___m_Interactable_12;
	// UnityEngine.UI.Graphic UnityEngine.UI.Selectable::m_TargetGraphic
	Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * ___m_TargetGraphic_13;
	// System.Boolean UnityEngine.UI.Selectable::m_GroupsAllowInteraction
	bool ___m_GroupsAllowInteraction_14;
	// System.Int32 UnityEngine.UI.Selectable::m_CurrentIndex
	int32_t ___m_CurrentIndex_15;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerInside>k__BackingField
	bool ___U3CisPointerInsideU3Ek__BackingField_16;
	// System.Boolean UnityEngine.UI.Selectable::<isPointerDown>k__BackingField
	bool ___U3CisPointerDownU3Ek__BackingField_17;
	// System.Boolean UnityEngine.UI.Selectable::<hasSelection>k__BackingField
	bool ___U3ChasSelectionU3Ek__BackingField_18;
	// System.Collections.Generic.List`1<UnityEngine.CanvasGroup> UnityEngine.UI.Selectable::m_CanvasGroupCache
	List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * ___m_CanvasGroupCache_19;

public:
	inline static int32_t get_offset_of_m_EnableCalled_6() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_EnableCalled_6)); }
	inline bool get_m_EnableCalled_6() const { return ___m_EnableCalled_6; }
	inline bool* get_address_of_m_EnableCalled_6() { return &___m_EnableCalled_6; }
	inline void set_m_EnableCalled_6(bool value)
	{
		___m_EnableCalled_6 = value;
	}

	inline static int32_t get_offset_of_m_Navigation_7() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Navigation_7)); }
	inline Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  get_m_Navigation_7() const { return ___m_Navigation_7; }
	inline Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A * get_address_of_m_Navigation_7() { return &___m_Navigation_7; }
	inline void set_m_Navigation_7(Navigation_t1CF0FFB22C0357CD64714FB7A40A275F899D363A  value)
	{
		___m_Navigation_7 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnUp_2), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnDown_3), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnLeft_4), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_Navigation_7))->___m_SelectOnRight_5), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_Transition_8() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Transition_8)); }
	inline int32_t get_m_Transition_8() const { return ___m_Transition_8; }
	inline int32_t* get_address_of_m_Transition_8() { return &___m_Transition_8; }
	inline void set_m_Transition_8(int32_t value)
	{
		___m_Transition_8 = value;
	}

	inline static int32_t get_offset_of_m_Colors_9() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Colors_9)); }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  get_m_Colors_9() const { return ___m_Colors_9; }
	inline ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955 * get_address_of_m_Colors_9() { return &___m_Colors_9; }
	inline void set_m_Colors_9(ColorBlock_t04DFBB97B4772D2E00FD17ED2E3E6590E6916955  value)
	{
		___m_Colors_9 = value;
	}

	inline static int32_t get_offset_of_m_SpriteState_10() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_SpriteState_10)); }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  get_m_SpriteState_10() const { return ___m_SpriteState_10; }
	inline SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E * get_address_of_m_SpriteState_10() { return &___m_SpriteState_10; }
	inline void set_m_SpriteState_10(SpriteState_t9024961148433175CE2F3D9E8E9239A8B1CAB15E  value)
	{
		___m_SpriteState_10 = value;
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_HighlightedSprite_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_PressedSprite_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_SelectedSprite_2), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&(((&___m_SpriteState_10))->___m_DisabledSprite_3), (void*)NULL);
		#endif
	}

	inline static int32_t get_offset_of_m_AnimationTriggers_11() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_AnimationTriggers_11)); }
	inline AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * get_m_AnimationTriggers_11() const { return ___m_AnimationTriggers_11; }
	inline AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 ** get_address_of_m_AnimationTriggers_11() { return &___m_AnimationTriggers_11; }
	inline void set_m_AnimationTriggers_11(AnimationTriggers_tF38CA7FA631709E096B57D732668D86081F44C11 * value)
	{
		___m_AnimationTriggers_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AnimationTriggers_11), (void*)value);
	}

	inline static int32_t get_offset_of_m_Interactable_12() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_Interactable_12)); }
	inline bool get_m_Interactable_12() const { return ___m_Interactable_12; }
	inline bool* get_address_of_m_Interactable_12() { return &___m_Interactable_12; }
	inline void set_m_Interactable_12(bool value)
	{
		___m_Interactable_12 = value;
	}

	inline static int32_t get_offset_of_m_TargetGraphic_13() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_TargetGraphic_13)); }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * get_m_TargetGraphic_13() const { return ___m_TargetGraphic_13; }
	inline Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 ** get_address_of_m_TargetGraphic_13() { return &___m_TargetGraphic_13; }
	inline void set_m_TargetGraphic_13(Graphic_tF07D777035055CF93BA5F46F77ED5EDFEFF9AE24 * value)
	{
		___m_TargetGraphic_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_TargetGraphic_13), (void*)value);
	}

	inline static int32_t get_offset_of_m_GroupsAllowInteraction_14() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_GroupsAllowInteraction_14)); }
	inline bool get_m_GroupsAllowInteraction_14() const { return ___m_GroupsAllowInteraction_14; }
	inline bool* get_address_of_m_GroupsAllowInteraction_14() { return &___m_GroupsAllowInteraction_14; }
	inline void set_m_GroupsAllowInteraction_14(bool value)
	{
		___m_GroupsAllowInteraction_14 = value;
	}

	inline static int32_t get_offset_of_m_CurrentIndex_15() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_CurrentIndex_15)); }
	inline int32_t get_m_CurrentIndex_15() const { return ___m_CurrentIndex_15; }
	inline int32_t* get_address_of_m_CurrentIndex_15() { return &___m_CurrentIndex_15; }
	inline void set_m_CurrentIndex_15(int32_t value)
	{
		___m_CurrentIndex_15 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerInsideU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3CisPointerInsideU3Ek__BackingField_16)); }
	inline bool get_U3CisPointerInsideU3Ek__BackingField_16() const { return ___U3CisPointerInsideU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CisPointerInsideU3Ek__BackingField_16() { return &___U3CisPointerInsideU3Ek__BackingField_16; }
	inline void set_U3CisPointerInsideU3Ek__BackingField_16(bool value)
	{
		___U3CisPointerInsideU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CisPointerDownU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3CisPointerDownU3Ek__BackingField_17)); }
	inline bool get_U3CisPointerDownU3Ek__BackingField_17() const { return ___U3CisPointerDownU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CisPointerDownU3Ek__BackingField_17() { return &___U3CisPointerDownU3Ek__BackingField_17; }
	inline void set_U3CisPointerDownU3Ek__BackingField_17(bool value)
	{
		___U3CisPointerDownU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3ChasSelectionU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___U3ChasSelectionU3Ek__BackingField_18)); }
	inline bool get_U3ChasSelectionU3Ek__BackingField_18() const { return ___U3ChasSelectionU3Ek__BackingField_18; }
	inline bool* get_address_of_U3ChasSelectionU3Ek__BackingField_18() { return &___U3ChasSelectionU3Ek__BackingField_18; }
	inline void set_U3ChasSelectionU3Ek__BackingField_18(bool value)
	{
		___U3ChasSelectionU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_m_CanvasGroupCache_19() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD, ___m_CanvasGroupCache_19)); }
	inline List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * get_m_CanvasGroupCache_19() const { return ___m_CanvasGroupCache_19; }
	inline List_1_t34AA4AF4E7352129CA58045901530E41445AC16D ** get_address_of_m_CanvasGroupCache_19() { return &___m_CanvasGroupCache_19; }
	inline void set_m_CanvasGroupCache_19(List_1_t34AA4AF4E7352129CA58045901530E41445AC16D * value)
	{
		___m_CanvasGroupCache_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CanvasGroupCache_19), (void*)value);
	}
};

struct Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields
{
public:
	// UnityEngine.UI.Selectable[] UnityEngine.UI.Selectable::s_Selectables
	SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* ___s_Selectables_4;
	// System.Int32 UnityEngine.UI.Selectable::s_SelectableCount
	int32_t ___s_SelectableCount_5;

public:
	inline static int32_t get_offset_of_s_Selectables_4() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields, ___s_Selectables_4)); }
	inline SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* get_s_Selectables_4() const { return ___s_Selectables_4; }
	inline SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535** get_address_of_s_Selectables_4() { return &___s_Selectables_4; }
	inline void set_s_Selectables_4(SelectableU5BU5D_tECF9F5BDBF0652A937D18F10C883EFDAE2E62535* value)
	{
		___s_Selectables_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_Selectables_4), (void*)value);
	}

	inline static int32_t get_offset_of_s_SelectableCount_5() { return static_cast<int32_t>(offsetof(Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD_StaticFields, ___s_SelectableCount_5)); }
	inline int32_t get_s_SelectableCount_5() const { return ___s_SelectableCount_5; }
	inline int32_t* get_address_of_s_SelectableCount_5() { return &___s_SelectableCount_5; }
	inline void set_s_SelectableCount_5(int32_t value)
	{
		___s_SelectableCount_5 = value;
	}
};


// UnityEngine.UI.Dropdown
struct Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96  : public Selectable_t34088A3677CC9D344F81B0D91999D8C5963D7DBD
{
public:
	// UnityEngine.RectTransform UnityEngine.UI.Dropdown::m_Template
	RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * ___m_Template_20;
	// UnityEngine.UI.Text UnityEngine.UI.Dropdown::m_CaptionText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___m_CaptionText_21;
	// UnityEngine.UI.Image UnityEngine.UI.Dropdown::m_CaptionImage
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___m_CaptionImage_22;
	// UnityEngine.UI.Text UnityEngine.UI.Dropdown::m_ItemText
	Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * ___m_ItemText_23;
	// UnityEngine.UI.Image UnityEngine.UI.Dropdown::m_ItemImage
	Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * ___m_ItemImage_24;
	// System.Int32 UnityEngine.UI.Dropdown::m_Value
	int32_t ___m_Value_25;
	// UnityEngine.UI.Dropdown/OptionDataList UnityEngine.UI.Dropdown::m_Options
	OptionDataList_t524EBDB7A2B178269FD5B4740108D0EC6404B4B6 * ___m_Options_26;
	// UnityEngine.UI.Dropdown/DropdownEvent UnityEngine.UI.Dropdown::m_OnValueChanged
	DropdownEvent_tEB2C75C3DBC789936B31D9A979FD62E047846CFB * ___m_OnValueChanged_27;
	// System.Single UnityEngine.UI.Dropdown::m_AlphaFadeSpeed
	float ___m_AlphaFadeSpeed_28;
	// UnityEngine.GameObject UnityEngine.UI.Dropdown::m_Dropdown
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_Dropdown_29;
	// UnityEngine.GameObject UnityEngine.UI.Dropdown::m_Blocker
	GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * ___m_Blocker_30;
	// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/DropdownItem> UnityEngine.UI.Dropdown::m_Items
	List_1_t4CFF6A6E1A912AE4990A34B2AA4A1FE2C9FB0033 * ___m_Items_31;
	// UnityEngine.UI.CoroutineTween.TweenRunner`1<UnityEngine.UI.CoroutineTween.FloatTween> UnityEngine.UI.Dropdown::m_AlphaTweenRunner
	TweenRunner_1_t428873023FD8831B6DCE3CBD53ADD7D37AC8222D * ___m_AlphaTweenRunner_32;
	// System.Boolean UnityEngine.UI.Dropdown::validTemplate
	bool ___validTemplate_33;

public:
	inline static int32_t get_offset_of_m_Template_20() { return static_cast<int32_t>(offsetof(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96, ___m_Template_20)); }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * get_m_Template_20() const { return ___m_Template_20; }
	inline RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 ** get_address_of_m_Template_20() { return &___m_Template_20; }
	inline void set_m_Template_20(RectTransform_t8A6A306FB29A6C8C22010CF9040E319753571072 * value)
	{
		___m_Template_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Template_20), (void*)value);
	}

	inline static int32_t get_offset_of_m_CaptionText_21() { return static_cast<int32_t>(offsetof(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96, ___m_CaptionText_21)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_m_CaptionText_21() const { return ___m_CaptionText_21; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_m_CaptionText_21() { return &___m_CaptionText_21; }
	inline void set_m_CaptionText_21(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___m_CaptionText_21 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CaptionText_21), (void*)value);
	}

	inline static int32_t get_offset_of_m_CaptionImage_22() { return static_cast<int32_t>(offsetof(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96, ___m_CaptionImage_22)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_m_CaptionImage_22() const { return ___m_CaptionImage_22; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_m_CaptionImage_22() { return &___m_CaptionImage_22; }
	inline void set_m_CaptionImage_22(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___m_CaptionImage_22 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_CaptionImage_22), (void*)value);
	}

	inline static int32_t get_offset_of_m_ItemText_23() { return static_cast<int32_t>(offsetof(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96, ___m_ItemText_23)); }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * get_m_ItemText_23() const { return ___m_ItemText_23; }
	inline Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 ** get_address_of_m_ItemText_23() { return &___m_ItemText_23; }
	inline void set_m_ItemText_23(Text_t6A2339DA6C05AE2646FC1A6C8FCC127391BE7FA1 * value)
	{
		___m_ItemText_23 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ItemText_23), (void*)value);
	}

	inline static int32_t get_offset_of_m_ItemImage_24() { return static_cast<int32_t>(offsetof(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96, ___m_ItemImage_24)); }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * get_m_ItemImage_24() const { return ___m_ItemImage_24; }
	inline Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C ** get_address_of_m_ItemImage_24() { return &___m_ItemImage_24; }
	inline void set_m_ItemImage_24(Image_t4021FF27176E44BFEDDCBE43C7FE6B713EC70D3C * value)
	{
		___m_ItemImage_24 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_ItemImage_24), (void*)value);
	}

	inline static int32_t get_offset_of_m_Value_25() { return static_cast<int32_t>(offsetof(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96, ___m_Value_25)); }
	inline int32_t get_m_Value_25() const { return ___m_Value_25; }
	inline int32_t* get_address_of_m_Value_25() { return &___m_Value_25; }
	inline void set_m_Value_25(int32_t value)
	{
		___m_Value_25 = value;
	}

	inline static int32_t get_offset_of_m_Options_26() { return static_cast<int32_t>(offsetof(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96, ___m_Options_26)); }
	inline OptionDataList_t524EBDB7A2B178269FD5B4740108D0EC6404B4B6 * get_m_Options_26() const { return ___m_Options_26; }
	inline OptionDataList_t524EBDB7A2B178269FD5B4740108D0EC6404B4B6 ** get_address_of_m_Options_26() { return &___m_Options_26; }
	inline void set_m_Options_26(OptionDataList_t524EBDB7A2B178269FD5B4740108D0EC6404B4B6 * value)
	{
		___m_Options_26 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Options_26), (void*)value);
	}

	inline static int32_t get_offset_of_m_OnValueChanged_27() { return static_cast<int32_t>(offsetof(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96, ___m_OnValueChanged_27)); }
	inline DropdownEvent_tEB2C75C3DBC789936B31D9A979FD62E047846CFB * get_m_OnValueChanged_27() const { return ___m_OnValueChanged_27; }
	inline DropdownEvent_tEB2C75C3DBC789936B31D9A979FD62E047846CFB ** get_address_of_m_OnValueChanged_27() { return &___m_OnValueChanged_27; }
	inline void set_m_OnValueChanged_27(DropdownEvent_tEB2C75C3DBC789936B31D9A979FD62E047846CFB * value)
	{
		___m_OnValueChanged_27 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_OnValueChanged_27), (void*)value);
	}

	inline static int32_t get_offset_of_m_AlphaFadeSpeed_28() { return static_cast<int32_t>(offsetof(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96, ___m_AlphaFadeSpeed_28)); }
	inline float get_m_AlphaFadeSpeed_28() const { return ___m_AlphaFadeSpeed_28; }
	inline float* get_address_of_m_AlphaFadeSpeed_28() { return &___m_AlphaFadeSpeed_28; }
	inline void set_m_AlphaFadeSpeed_28(float value)
	{
		___m_AlphaFadeSpeed_28 = value;
	}

	inline static int32_t get_offset_of_m_Dropdown_29() { return static_cast<int32_t>(offsetof(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96, ___m_Dropdown_29)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_Dropdown_29() const { return ___m_Dropdown_29; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_Dropdown_29() { return &___m_Dropdown_29; }
	inline void set_m_Dropdown_29(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_Dropdown_29 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Dropdown_29), (void*)value);
	}

	inline static int32_t get_offset_of_m_Blocker_30() { return static_cast<int32_t>(offsetof(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96, ___m_Blocker_30)); }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * get_m_Blocker_30() const { return ___m_Blocker_30; }
	inline GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 ** get_address_of_m_Blocker_30() { return &___m_Blocker_30; }
	inline void set_m_Blocker_30(GameObject_tC000A2E1A7CF1E10FD7BA08863287C072207C319 * value)
	{
		___m_Blocker_30 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Blocker_30), (void*)value);
	}

	inline static int32_t get_offset_of_m_Items_31() { return static_cast<int32_t>(offsetof(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96, ___m_Items_31)); }
	inline List_1_t4CFF6A6E1A912AE4990A34B2AA4A1FE2C9FB0033 * get_m_Items_31() const { return ___m_Items_31; }
	inline List_1_t4CFF6A6E1A912AE4990A34B2AA4A1FE2C9FB0033 ** get_address_of_m_Items_31() { return &___m_Items_31; }
	inline void set_m_Items_31(List_1_t4CFF6A6E1A912AE4990A34B2AA4A1FE2C9FB0033 * value)
	{
		___m_Items_31 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Items_31), (void*)value);
	}

	inline static int32_t get_offset_of_m_AlphaTweenRunner_32() { return static_cast<int32_t>(offsetof(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96, ___m_AlphaTweenRunner_32)); }
	inline TweenRunner_1_t428873023FD8831B6DCE3CBD53ADD7D37AC8222D * get_m_AlphaTweenRunner_32() const { return ___m_AlphaTweenRunner_32; }
	inline TweenRunner_1_t428873023FD8831B6DCE3CBD53ADD7D37AC8222D ** get_address_of_m_AlphaTweenRunner_32() { return &___m_AlphaTweenRunner_32; }
	inline void set_m_AlphaTweenRunner_32(TweenRunner_1_t428873023FD8831B6DCE3CBD53ADD7D37AC8222D * value)
	{
		___m_AlphaTweenRunner_32 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_AlphaTweenRunner_32), (void*)value);
	}

	inline static int32_t get_offset_of_validTemplate_33() { return static_cast<int32_t>(offsetof(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96, ___validTemplate_33)); }
	inline bool get_validTemplate_33() const { return ___validTemplate_33; }
	inline bool* get_address_of_validTemplate_33() { return &___validTemplate_33; }
	inline void set_validTemplate_33(bool value)
	{
		___validTemplate_33 = value;
	}
};

struct Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96_StaticFields
{
public:
	// UnityEngine.UI.Dropdown/OptionData UnityEngine.UI.Dropdown::s_NoOptionData
	OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857 * ___s_NoOptionData_34;

public:
	inline static int32_t get_offset_of_s_NoOptionData_34() { return static_cast<int32_t>(offsetof(Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96_StaticFields, ___s_NoOptionData_34)); }
	inline OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857 * get_s_NoOptionData_34() const { return ___s_NoOptionData_34; }
	inline OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857 ** get_address_of_s_NoOptionData_34() { return &___s_NoOptionData_34; }
	inline void set_s_NoOptionData_34(OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857 * value)
	{
		___s_NoOptionData_34 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_NoOptionData_34), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.String[]
struct StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Object[]
struct ObjectU5BU5D_t1256A8B00BB71C7F582BF08257BE4F826FF64873  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * m_Items[1];

public:
	inline Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline Object_tF2F3778131EFF286AF62B7B013A170F95A91571A ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline Object_tF2F3778131EFF286AF62B7B013A170F95A91571A ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// Unity.U2D.Animation.Sample.SwapFullSkin[]
struct SwapFullSkinU5BU5D_t00CE6E2419CAEB9600A030401C7A056244987CF8  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) SwapFullSkin_t115938BCBBCAED7A2735A385C54CC8C3CE3B82A8 * m_Items[1];

public:
	inline SwapFullSkin_t115938BCBBCAED7A2735A385C54CC8C3CE3B82A8 * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline SwapFullSkin_t115938BCBBCAED7A2735A385C54CC8C3CE3B82A8 ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, SwapFullSkin_t115938BCBBCAED7A2735A385C54CC8C3CE3B82A8 * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline SwapFullSkin_t115938BCBBCAED7A2735A385C54CC8C3CE3B82A8 * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline SwapFullSkin_t115938BCBBCAED7A2735A385C54CC8C3CE3B82A8 ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, SwapFullSkin_t115938BCBBCAED7A2735A385C54CC8C3CE3B82A8 * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset[]
struct SpriteLibraryAssetU5BU5D_t8208BFA0067B2DFDA16254E72287E09338255B27  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF * m_Items[1];

public:
	inline SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};
// Unity.U2D.Animation.Sample.SwapOptionData[]
struct SwapOptionDataU5BU5D_t6D1276EE178F77CE5243BF5799FFAE1BB5F177F2  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0  m_Items[1];

public:
	inline SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0  GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0 * GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0  value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___dropdown_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___spriteResolver_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___category_2), (void*)NULL);
		#endif
	}
	inline SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0  GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0 * GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0  value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___dropdown_0), (void*)NULL);
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___spriteResolver_1), (void*)NULL);
		#endif
		#if IL2CPP_ENABLE_STRICT_WRITE_BARRIERS
		Il2CppCodeGenWriteBarrier((void**)&((m_Items + index)->___category_2), (void*)NULL);
		#endif
	}
};
// System.Object[]
struct ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) RuntimeObject * m_Items[1];

public:
	inline RuntimeObject * GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, RuntimeObject * value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline RuntimeObject * GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline RuntimeObject ** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, RuntimeObject * value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// !!0 UnityEngine.AssetBundle::LoadAsset<System.Object>(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * AssetBundle_LoadAsset_TisRuntimeObject_m5C55BB9F750DD28D2C89E2FED48B01FE3CF18158_gshared (AssetBundle_t4D34D7FDF0F230DC641DC1FCFA2C0E7E9E628FA4 * __this, String_t* ___name0, const RuntimeMethod* method);
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * Enumerable_ToList_TisRuntimeObject_mA4E485F973C6DF746B8DB54CA6F54192D4231CA2_gshared (RuntimeObject* ___source0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::Add(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, RuntimeObject * ___item0, const RuntimeMethod* method);
// !0[] System.Collections.Generic.List`1<System.Object>::ToArray()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* List_1_ToArray_mA737986DE6389E9DD8FA8E3D4E222DE4DA34958D_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void List_1__ctor_mFEB2301A6F28290A828A979BA9CC847B16B3D538_gshared (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___capacity0, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::.ctor(System.Object,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityAction_1__ctor_m595DA2ECB187B2DF951D640BFEBCE02F3F800A3F_gshared (UnityAction_1_t5CF46572372725E6225588C466A7AF5C8597AA79 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::AddListener(UnityEngine.Events.UnityAction`1<!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void UnityEvent_1_AddListener_mFCFAC8ACA3F75283268DC2629ADEB5504E8FC0C2_gshared (UnityEvent_1_tB235B5DAD099AC425DC059D10DEB8B97A35E2BBF * __this, UnityAction_1_t5CF46572372725E6225588C466A7AF5C8597AA79 * ___call0, const RuntimeMethod* method);
// System.Int32 System.Linq.Enumerable::Count<System.Object>(System.Collections.Generic.IEnumerable`1<!!0>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Enumerable_Count_TisRuntimeObject_mF35F8B37C78D02C08BB4F806038CA6EDE548A6B5_gshared (RuntimeObject* ___source0, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method);

// System.String UnityEngine.Application::get_streamingAssetsPath()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Application_get_streamingAssetsPath_mA1FBABB08D7A4590A468C7CD940CD442B58C91E1 (const RuntimeMethod* method);
// System.String System.IO.Path::Combine(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Path_Combine_mC22E47A9BB232F02ED3B6B5F6DD53338D37782EF (String_t* ___path10, String_t* ___path21, const RuntimeMethod* method);
// UnityEngine.AssetBundle UnityEngine.AssetBundle::LoadFromFile(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR AssetBundle_t4D34D7FDF0F230DC641DC1FCFA2C0E7E9E628FA4 * AssetBundle_LoadFromFile_m326379558FA2CA731C294D5F9905EA3EAE3B5E52 (String_t* ___path0, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54 (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___x0, Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * ___y1, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogWarning(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogWarning_m24085D883C9E74D7AB423F0625E13259923960E7 (RuntimeObject * ___message0, const RuntimeMethod* method);
// !!0 UnityEngine.AssetBundle::LoadAsset<UnityEngine.AssetBundleManifest>(System.String)
inline AssetBundleManifest_tC4C999A85A1FC2B60215566D0462EE645674160A * AssetBundle_LoadAsset_TisAssetBundleManifest_tC4C999A85A1FC2B60215566D0462EE645674160A_m1ACB39E544E25B9EFC8E9DC0E2C8FDCE3740D767 (AssetBundle_t4D34D7FDF0F230DC641DC1FCFA2C0E7E9E628FA4 * __this, String_t* ___name0, const RuntimeMethod* method)
{
	return ((  AssetBundleManifest_tC4C999A85A1FC2B60215566D0462EE645674160A * (*) (AssetBundle_t4D34D7FDF0F230DC641DC1FCFA2C0E7E9E628FA4 *, String_t*, const RuntimeMethod*))AssetBundle_LoadAsset_TisRuntimeObject_m5C55BB9F750DD28D2C89E2FED48B01FE3CF18158_gshared)(__this, ___name0, method);
}
// System.String[] UnityEngine.AssetBundleManifest::GetAllAssetBundles()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* AssetBundleManifest_GetAllAssetBundles_m88F1FE72F516B548E430E76EC3E75669BDE822FF (AssetBundleManifest_tC4C999A85A1FC2B60215566D0462EE645674160A * __this, const RuntimeMethod* method);
// UnityEngine.Object[] UnityEngine.AssetBundle::LoadAllAssets()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ObjectU5BU5D_t1256A8B00BB71C7F582BF08257BE4F826FF64873* AssetBundle_LoadAllAssets_mB132E05BAFF4776C59BC80082A2FE68060741603 (AssetBundle_t4D34D7FDF0F230DC641DC1FCFA2C0E7E9E628FA4 * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1<!!0> System.Linq.Enumerable::ToList<UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset>(System.Collections.Generic.IEnumerable`1<!!0>)
inline List_1_tD6FC97E7B9C0BAE2041ACEC751C58EFD62D2F9B1 * Enumerable_ToList_TisSpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF_m184B25FE5D05152697CE9C1FA0A68C977FE9C803 (RuntimeObject* ___source0, const RuntimeMethod* method)
{
	return ((  List_1_tD6FC97E7B9C0BAE2041ACEC751C58EFD62D2F9B1 * (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_ToList_TisRuntimeObject_mA4E485F973C6DF746B8DB54CA6F54192D4231CA2_gshared)(___source0, method);
}
// System.Void System.Collections.Generic.List`1<UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset>::Add(!0)
inline void List_1_Add_m3A23DE48D4BE459A85DB75B470E3BCFA63662334 (List_1_tD6FC97E7B9C0BAE2041ACEC751C58EFD62D2F9B1 * __this, SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tD6FC97E7B9C0BAE2041ACEC751C58EFD62D2F9B1 *, SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF *, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// !0[] System.Collections.Generic.List`1<UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset>::ToArray()
inline SpriteLibraryAssetU5BU5D_t8208BFA0067B2DFDA16254E72287E09338255B27* List_1_ToArray_m0F5F017BABE20CE363FDBE9E9DA9CF4574C2BF12 (List_1_tD6FC97E7B9C0BAE2041ACEC751C58EFD62D2F9B1 * __this, const RuntimeMethod* method)
{
	return ((  SpriteLibraryAssetU5BU5D_t8208BFA0067B2DFDA16254E72287E09338255B27* (*) (List_1_tD6FC97E7B9C0BAE2041ACEC751C58EFD62D2F9B1 *, const RuntimeMethod*))List_1_ToArray_mA737986DE6389E9DD8FA8E3D4E222DE4DA34958D_gshared)(__this, method);
}
// System.Void Unity.U2D.Animation.Sample.SwapFullSkin::UpdateSelectionChoice()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SwapFullSkin_UpdateSelectionChoice_mE20B29107CD6953FEAFCF2707C150CD55AAA3F78 (SwapFullSkin_t115938BCBBCAED7A2735A385C54CC8C3CE3B82A8 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED (MonoBehaviour_t37A501200D970A8257124B0EAE00A0FF3DDC354A * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Experimental.U2D.Animation.SpriteLibrary::set_spriteLibraryAsset(UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpriteLibrary_set_spriteLibraryAsset_m7EF14FF3116EE8AA0A821E55E379E605966AB01B (SpriteLibrary_tC62AFE771C7CF1FA407ECE48A4C627139F415C3A * __this, SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF * ___value0, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Dropdown::ClearOptions()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dropdown_ClearOptions_m7F59A8B054698715921D2B0E37EB1808BE53C23C (Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::.ctor(System.Int32)
inline void List_1__ctor_m32A29DCC109DE9CA0E9445719CB6CEEC99AEDA05 (List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4 * __this, int32_t ___capacity0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4 *, int32_t, const RuntimeMethod*))List_1__ctor_mFEB2301A6F28290A828A979BA9CC847B16B3D538_gshared)(__this, ___capacity0, method);
}
// System.String UnityEngine.Object::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * __this, const RuntimeMethod* method);
// System.Void UnityEngine.UI.Dropdown/OptionData::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void OptionData__ctor_m5AF14BD8BBF6118AC51A7A9A38AE3AB2DE3C2675 (OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857 * __this, String_t* ___text0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::Add(!0)
inline void List_1_Add_mC2611000DA97145D1DFC867ECA604FBB3E8628EC (List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4 * __this, OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857 * ___item0, const RuntimeMethod* method)
{
	((  void (*) (List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4 *, OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857 *, const RuntimeMethod*))List_1_Add_mE5B3CBB3A625606D9BC4337FEAAF1D66BCB6F96E_gshared)(__this, ___item0, method);
}
// System.Void UnityEngine.UI.Dropdown::set_options(System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dropdown_set_options_m64FE183D8C988AC643870012A98F3095E2C2C14B (Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * __this, List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4 * ___value0, const RuntimeMethod* method);
// UnityEngine.UI.Dropdown/DropdownEvent UnityEngine.UI.Dropdown::get_onValueChanged()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR DropdownEvent_tEB2C75C3DBC789936B31D9A979FD62E047846CFB * Dropdown_get_onValueChanged_m05479714AEB528CF5CE93ED09870E98E6EC94CFA_inline (Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Events.UnityAction`1<System.Int32>::.ctor(System.Object,System.IntPtr)
inline void UnityAction_1__ctor_m595DA2ECB187B2DF951D640BFEBCE02F3F800A3F (UnityAction_1_t5CF46572372725E6225588C466A7AF5C8597AA79 * __this, RuntimeObject * ___object0, intptr_t ___method1, const RuntimeMethod* method)
{
	((  void (*) (UnityAction_1_t5CF46572372725E6225588C466A7AF5C8597AA79 *, RuntimeObject *, intptr_t, const RuntimeMethod*))UnityAction_1__ctor_m595DA2ECB187B2DF951D640BFEBCE02F3F800A3F_gshared)(__this, ___object0, ___method1, method);
}
// System.Void UnityEngine.Events.UnityEvent`1<System.Int32>::AddListener(UnityEngine.Events.UnityAction`1<!0>)
inline void UnityEvent_1_AddListener_mFCFAC8ACA3F75283268DC2629ADEB5504E8FC0C2 (UnityEvent_1_tB235B5DAD099AC425DC059D10DEB8B97A35E2BBF * __this, UnityAction_1_t5CF46572372725E6225588C466A7AF5C8597AA79 * ___call0, const RuntimeMethod* method)
{
	((  void (*) (UnityEvent_1_tB235B5DAD099AC425DC059D10DEB8B97A35E2BBF *, UnityAction_1_t5CF46572372725E6225588C466A7AF5C8597AA79 *, const RuntimeMethod*))UnityEvent_1_AddListener_mFCFAC8ACA3F75283268DC2629ADEB5504E8FC0C2_gshared)(__this, ___call0, method);
}
// System.Void Unity.U2D.Animation.Sample.SwapPart/<>c__DisplayClass2_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0__ctor_m68376F15EF2246A0E99F1E9513F11E037F84CE1C (U3CU3Ec__DisplayClass2_0_tE1B0DB639BE84CA15D2CBD9694D84FDEBB76188F * __this, const RuntimeMethod* method);
// System.Collections.Generic.IEnumerable`1<System.String> UnityEngine.Experimental.U2D.Animation.SpriteLibraryAsset::GetCategoryLabelNames(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject* SpriteLibraryAsset_GetCategoryLabelNames_mCBE6FB145B0CFF3C08CCA0720B8114B479B74FCA (SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF * __this, String_t* ___category0, const RuntimeMethod* method);
// System.Int32 System.Linq.Enumerable::Count<System.String>(System.Collections.Generic.IEnumerable`1<!!0>)
inline int32_t Enumerable_Count_TisString_t_m73DFA608B1C4B59DF24B8910F113F39B62FF139D (RuntimeObject* ___source0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (RuntimeObject*, const RuntimeMethod*))Enumerable_Count_TisRuntimeObject_mF35F8B37C78D02C08BB4F806038CA6EDE548A6B5_gshared)(___source0, method);
}
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData> UnityEngine.UI.Dropdown::get_options()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4 * Dropdown_get_options_mF427A2157CDD901C12F1B160C4D1F8207D7111D0 (Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<UnityEngine.UI.Dropdown/OptionData>::get_Item(System.Int32)
inline OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857 * List_1_get_Item_mF979E11037D572E72D866EC260EF0434EF296E67_inline (List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	return ((  OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857 * (*) (List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4 *, int32_t, const RuntimeMethod*))List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline)(__this, ___index0, method);
}
// System.String UnityEngine.UI.Dropdown/OptionData::get_text()
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* OptionData_get_text_m8652FE3866405C4C7C3782659009EF2C7E54D232_inline (OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Experimental.U2D.Animation.SpriteResolver::SetCategoryAndLabel(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpriteResolver_SetCategoryAndLabel_m3A76B2CE2DEB1E9985818996176FC83915C7CF37 (SpriteResolver_t5B3D64444E443E530CC3C9CA5336DED4721F7956 * __this, String_t* ___category0, String_t* ___label1, const RuntimeMethod* method);
// System.Void System.ThrowHelper::ThrowArgumentOutOfRangeException()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929 (const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Unity.U2D.Animation.Sample.LoadSwapDLC::LoadAssetBundle()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LoadSwapDLC_LoadAssetBundle_m301C6948D8818670E724C70BD102A3AEA87B3241 (LoadSwapDLC_t2B7B0C8BE6033FEA11E55BBA53380698F97AFE49 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&AssetBundle_LoadAsset_TisAssetBundleManifest_tC4C999A85A1FC2B60215566D0462EE645674160A_m1ACB39E544E25B9EFC8E9DC0E2C8FDCE3740D767_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_ToList_TisSpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF_m184B25FE5D05152697CE9C1FA0A68C977FE9C803_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_m3A23DE48D4BE459A85DB75B470E3BCFA63662334_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_ToArray_m0F5F017BABE20CE363FDBE9E9DA9CF4574C2BF12_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Path_tF1D95B78D57C1C1211BA6633FF2AC22FD6C48921_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral143AE77221E6B246FE30F96946F8BA08E6E45D4B);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteral540C7DC960FC2D69DA59953A19B3C046E4F5F427);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralB217B9D874A0DC94192798D2FDAA1F2A10B2022D);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&_stringLiteralF85CF216DC124F456BB630B25B86A7E27701E678);
		s_Il2CppMethodInitialized = true;
	}
	String_t* V_0 = NULL;
	AssetBundle_t4D34D7FDF0F230DC641DC1FCFA2C0E7E9E628FA4 * V_1 = NULL;
	AssetBundleManifest_tC4C999A85A1FC2B60215566D0462EE645674160A * V_2 = NULL;
	StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* V_3 = NULL;
	int32_t V_4 = 0;
	String_t* V_5 = NULL;
	ObjectU5BU5D_t1256A8B00BB71C7F582BF08257BE4F826FF64873* V_6 = NULL;
	int32_t V_7 = 0;
	Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * V_8 = NULL;
	SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF * V_9 = NULL;
	SwapFullSkinU5BU5D_t00CE6E2419CAEB9600A030401C7A056244987CF8* V_10 = NULL;
	int32_t V_11 = 0;
	List_1_tD6FC97E7B9C0BAE2041ACEC751C58EFD62D2F9B1 * V_12 = NULL;
	{
		// var assetBundlePath = Path.Combine(Application.streamingAssetsPath, k_AssetBundleName);
		String_t* L_0;
		L_0 = Application_get_streamingAssetsPath_mA1FBABB08D7A4590A468C7CD940CD442B58C91E1(/*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Path_tF1D95B78D57C1C1211BA6633FF2AC22FD6C48921_il2cpp_TypeInfo_var);
		String_t* L_1;
		L_1 = Path_Combine_mC22E47A9BB232F02ED3B6B5F6DD53338D37782EF(L_0, _stringLiteral540C7DC960FC2D69DA59953A19B3C046E4F5F427, /*hidden argument*/NULL);
		V_0 = L_1;
		// var bundle = AssetBundle.LoadFromFile(Path.Combine(assetBundlePath, k_AssetBundleName));
		String_t* L_2 = V_0;
		String_t* L_3;
		L_3 = Path_Combine_mC22E47A9BB232F02ED3B6B5F6DD53338D37782EF(L_2, _stringLiteral540C7DC960FC2D69DA59953A19B3C046E4F5F427, /*hidden argument*/NULL);
		AssetBundle_t4D34D7FDF0F230DC641DC1FCFA2C0E7E9E628FA4 * L_4;
		L_4 = AssetBundle_LoadFromFile_m326379558FA2CA731C294D5F9905EA3EAE3B5E52(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		// if (bundle == null)
		AssetBundle_t4D34D7FDF0F230DC641DC1FCFA2C0E7E9E628FA4 * L_5 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_6;
		L_6 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_5, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0035;
		}
	}
	{
		// Debug.LogWarning("AssetBundle not found");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogWarning_m24085D883C9E74D7AB423F0625E13259923960E7(_stringLiteralB217B9D874A0DC94192798D2FDAA1F2A10B2022D, /*hidden argument*/NULL);
		// return;
		return;
	}

IL_0035:
	{
		// var manifest = bundle.LoadAsset<AssetBundleManifest>("AssetBundleManifest");
		AssetBundle_t4D34D7FDF0F230DC641DC1FCFA2C0E7E9E628FA4 * L_7 = V_1;
		AssetBundleManifest_tC4C999A85A1FC2B60215566D0462EE645674160A * L_8;
		L_8 = AssetBundle_LoadAsset_TisAssetBundleManifest_tC4C999A85A1FC2B60215566D0462EE645674160A_m1ACB39E544E25B9EFC8E9DC0E2C8FDCE3740D767(L_7, _stringLiteral143AE77221E6B246FE30F96946F8BA08E6E45D4B, /*hidden argument*/AssetBundle_LoadAsset_TisAssetBundleManifest_tC4C999A85A1FC2B60215566D0462EE645674160A_m1ACB39E544E25B9EFC8E9DC0E2C8FDCE3740D767_RuntimeMethod_var);
		V_2 = L_8;
		// if (manifest == null)
		AssetBundleManifest_tC4C999A85A1FC2B60215566D0462EE645674160A * L_9 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tF2F3778131EFF286AF62B7B013A170F95A91571A_il2cpp_TypeInfo_var);
		bool L_10;
		L_10 = Object_op_Equality_mEE9EC7EB5C7DC3E95B94AB904E1986FC4D566D54(L_9, (Object_tF2F3778131EFF286AF62B7B013A170F95A91571A *)NULL, /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_0055;
		}
	}
	{
		// Debug.LogWarning("Unable to load manifest");
		IL2CPP_RUNTIME_CLASS_INIT(Debug_tEB68BCBEB8EFD60F8043C67146DC05E7F50F374B_il2cpp_TypeInfo_var);
		Debug_LogWarning_m24085D883C9E74D7AB423F0625E13259923960E7(_stringLiteralF85CF216DC124F456BB630B25B86A7E27701E678, /*hidden argument*/NULL);
		// return;
		return;
	}

IL_0055:
	{
		// foreach (var assetBundleName in manifest.GetAllAssetBundles())
		AssetBundleManifest_tC4C999A85A1FC2B60215566D0462EE645674160A * L_11 = V_2;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_12;
		L_12 = AssetBundleManifest_GetAllAssetBundles_m88F1FE72F516B548E430E76EC3E75669BDE822FF(L_11, /*hidden argument*/NULL);
		V_3 = L_12;
		V_4 = 0;
		goto IL_00f2;
	}

IL_0064:
	{
		// foreach (var assetBundleName in manifest.GetAllAssetBundles())
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_13 = V_3;
		int32_t L_14 = V_4;
		int32_t L_15 = L_14;
		String_t* L_16 = (L_13)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_15));
		V_5 = L_16;
		// var subBundle = AssetBundle.LoadFromFile(Path.Combine(assetBundlePath, assetBundleName));
		String_t* L_17 = V_0;
		String_t* L_18 = V_5;
		IL2CPP_RUNTIME_CLASS_INIT(Path_tF1D95B78D57C1C1211BA6633FF2AC22FD6C48921_il2cpp_TypeInfo_var);
		String_t* L_19;
		L_19 = Path_Combine_mC22E47A9BB232F02ED3B6B5F6DD53338D37782EF(L_17, L_18, /*hidden argument*/NULL);
		AssetBundle_t4D34D7FDF0F230DC641DC1FCFA2C0E7E9E628FA4 * L_20;
		L_20 = AssetBundle_LoadFromFile_m326379558FA2CA731C294D5F9905EA3EAE3B5E52(L_19, /*hidden argument*/NULL);
		// var assets = subBundle.LoadAllAssets();
		ObjectU5BU5D_t1256A8B00BB71C7F582BF08257BE4F826FF64873* L_21;
		L_21 = AssetBundle_LoadAllAssets_mB132E05BAFF4776C59BC80082A2FE68060741603(L_20, /*hidden argument*/NULL);
		// foreach (var asset in assets)
		V_6 = L_21;
		V_7 = 0;
		goto IL_00e4;
	}

IL_0083:
	{
		// foreach (var asset in assets)
		ObjectU5BU5D_t1256A8B00BB71C7F582BF08257BE4F826FF64873* L_22 = V_6;
		int32_t L_23 = V_7;
		int32_t L_24 = L_23;
		Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * L_25 = (L_22)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_24));
		V_8 = L_25;
		// if (asset is SpriteLibraryAsset)
		Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * L_26 = V_8;
		if (!((SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF *)IsInstClass((RuntimeObject*)L_26, SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF_il2cpp_TypeInfo_var)))
		{
			goto IL_00de;
		}
	}
	{
		// var sla = (SpriteLibraryAsset)asset;
		Object_tF2F3778131EFF286AF62B7B013A170F95A91571A * L_27 = V_8;
		V_9 = ((SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF *)CastclassClass((RuntimeObject*)L_27, SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF_il2cpp_TypeInfo_var));
		// foreach (var sfs in swapFullSkin)
		SwapFullSkinU5BU5D_t00CE6E2419CAEB9600A030401C7A056244987CF8* L_28 = __this->get_swapFullSkin_5();
		V_10 = L_28;
		V_11 = 0;
		goto IL_00d6;
	}

IL_00a9:
	{
		// foreach (var sfs in swapFullSkin)
		SwapFullSkinU5BU5D_t00CE6E2419CAEB9600A030401C7A056244987CF8* L_29 = V_10;
		int32_t L_30 = V_11;
		int32_t L_31 = L_30;
		SwapFullSkin_t115938BCBBCAED7A2735A385C54CC8C3CE3B82A8 * L_32 = (L_29)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_31));
		// var list = sfs.spriteLibraries.ToList();
		SwapFullSkin_t115938BCBBCAED7A2735A385C54CC8C3CE3B82A8 * L_33 = L_32;
		SpriteLibraryAssetU5BU5D_t8208BFA0067B2DFDA16254E72287E09338255B27* L_34 = L_33->get_spriteLibraries_4();
		List_1_tD6FC97E7B9C0BAE2041ACEC751C58EFD62D2F9B1 * L_35;
		L_35 = Enumerable_ToList_TisSpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF_m184B25FE5D05152697CE9C1FA0A68C977FE9C803((RuntimeObject*)(RuntimeObject*)L_34, /*hidden argument*/Enumerable_ToList_TisSpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF_m184B25FE5D05152697CE9C1FA0A68C977FE9C803_RuntimeMethod_var);
		V_12 = L_35;
		// list.Add(sla);
		List_1_tD6FC97E7B9C0BAE2041ACEC751C58EFD62D2F9B1 * L_36 = V_12;
		SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF * L_37 = V_9;
		List_1_Add_m3A23DE48D4BE459A85DB75B470E3BCFA63662334(L_36, L_37, /*hidden argument*/List_1_Add_m3A23DE48D4BE459A85DB75B470E3BCFA63662334_RuntimeMethod_var);
		// sfs.spriteLibraries = list.ToArray();
		List_1_tD6FC97E7B9C0BAE2041ACEC751C58EFD62D2F9B1 * L_38 = V_12;
		SpriteLibraryAssetU5BU5D_t8208BFA0067B2DFDA16254E72287E09338255B27* L_39;
		L_39 = List_1_ToArray_m0F5F017BABE20CE363FDBE9E9DA9CF4574C2BF12(L_38, /*hidden argument*/List_1_ToArray_m0F5F017BABE20CE363FDBE9E9DA9CF4574C2BF12_RuntimeMethod_var);
		L_33->set_spriteLibraries_4(L_39);
		int32_t L_40 = V_11;
		V_11 = ((int32_t)il2cpp_codegen_add((int32_t)L_40, (int32_t)1));
	}

IL_00d6:
	{
		// foreach (var sfs in swapFullSkin)
		int32_t L_41 = V_11;
		SwapFullSkinU5BU5D_t00CE6E2419CAEB9600A030401C7A056244987CF8* L_42 = V_10;
		if ((((int32_t)L_41) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_42)->max_length))))))
		{
			goto IL_00a9;
		}
	}

IL_00de:
	{
		int32_t L_43 = V_7;
		V_7 = ((int32_t)il2cpp_codegen_add((int32_t)L_43, (int32_t)1));
	}

IL_00e4:
	{
		// foreach (var asset in assets)
		int32_t L_44 = V_7;
		ObjectU5BU5D_t1256A8B00BB71C7F582BF08257BE4F826FF64873* L_45 = V_6;
		if ((((int32_t)L_44) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_45)->max_length))))))
		{
			goto IL_0083;
		}
	}
	{
		int32_t L_46 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_46, (int32_t)1));
	}

IL_00f2:
	{
		// foreach (var assetBundleName in manifest.GetAllAssetBundles())
		int32_t L_47 = V_4;
		StringU5BU5D_tACEBFEDE350025B554CD507C9AE8FFE49359549A* L_48 = V_3;
		if ((((int32_t)L_47) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_48)->max_length))))))
		{
			goto IL_0064;
		}
	}
	{
		// foreach (var sfs in swapFullSkin)
		SwapFullSkinU5BU5D_t00CE6E2419CAEB9600A030401C7A056244987CF8* L_49 = __this->get_swapFullSkin_5();
		V_10 = L_49;
		V_4 = 0;
		goto IL_0119;
	}

IL_0109:
	{
		// foreach (var sfs in swapFullSkin)
		SwapFullSkinU5BU5D_t00CE6E2419CAEB9600A030401C7A056244987CF8* L_50 = V_10;
		int32_t L_51 = V_4;
		int32_t L_52 = L_51;
		SwapFullSkin_t115938BCBBCAED7A2735A385C54CC8C3CE3B82A8 * L_53 = (L_50)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_52));
		// sfs.UpdateSelectionChoice();
		SwapFullSkin_UpdateSelectionChoice_mE20B29107CD6953FEAFCF2707C150CD55AAA3F78(L_53, /*hidden argument*/NULL);
		int32_t L_54 = V_4;
		V_4 = ((int32_t)il2cpp_codegen_add((int32_t)L_54, (int32_t)1));
	}

IL_0119:
	{
		// foreach (var sfs in swapFullSkin)
		int32_t L_55 = V_4;
		SwapFullSkinU5BU5D_t00CE6E2419CAEB9600A030401C7A056244987CF8* L_56 = V_10;
		if ((((int32_t)L_55) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_56)->max_length))))))
		{
			goto IL_0109;
		}
	}
	{
		// }
		return;
	}
}
// System.Void Unity.U2D.Animation.Sample.LoadSwapDLC::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void LoadSwapDLC__ctor_mBBB28374E13DE271CB6D7409EFCA4451F58F6E88 (LoadSwapDLC_t2B7B0C8BE6033FEA11E55BBA53380698F97AFE49 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Unity.U2D.Animation.Sample.SwapFullSkin::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SwapFullSkin_Start_mD1816F2BEEE956E48C51D7F3A84980476472473C (SwapFullSkin_t115938BCBBCAED7A2735A385C54CC8C3CE3B82A8 * __this, const RuntimeMethod* method)
{
	{
		// UpdateSelectionChoice();
		SwapFullSkin_UpdateSelectionChoice_mE20B29107CD6953FEAFCF2707C150CD55AAA3F78(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Unity.U2D.Animation.Sample.SwapFullSkin::OnDropDownValueChanged(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SwapFullSkin_OnDropDownValueChanged_m7083F235642A81B6D2F6B81B43B8C0DED47F2A45 (SwapFullSkin_t115938BCBBCAED7A2735A385C54CC8C3CE3B82A8 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		// spriteLibraryTarget.spriteLibraryAsset = spriteLibraries[value];
		SpriteLibrary_tC62AFE771C7CF1FA407ECE48A4C627139F415C3A * L_0 = __this->get_spriteLibraryTarget_5();
		SpriteLibraryAssetU5BU5D_t8208BFA0067B2DFDA16254E72287E09338255B27* L_1 = __this->get_spriteLibraries_4();
		int32_t L_2 = ___value0;
		int32_t L_3 = L_2;
		SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF * L_4 = (L_1)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_3));
		SpriteLibrary_set_spriteLibraryAsset_m7EF14FF3116EE8AA0A821E55E379E605966AB01B(L_0, L_4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Unity.U2D.Animation.Sample.SwapFullSkin::UpdateSelectionChoice()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SwapFullSkin_UpdateSelectionChoice_mE20B29107CD6953FEAFCF2707C150CD55AAA3F78 (SwapFullSkin_t115938BCBBCAED7A2735A385C54CC8C3CE3B82A8 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mC2611000DA97145D1DFC867ECA604FBB3E8628EC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m32A29DCC109DE9CA0E9445719CB6CEEC99AEDA05_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&SwapFullSkin_OnDropDownValueChanged_m7083F235642A81B6D2F6B81B43B8C0DED47F2A45_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1__ctor_m595DA2ECB187B2DF951D640BFEBCE02F3F800A3F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_t5CF46572372725E6225588C466A7AF5C8597AA79_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_AddListener_mFCFAC8ACA3F75283268DC2629ADEB5504E8FC0C2_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4 * V_0 = NULL;
	int32_t V_1 = 0;
	{
		// dropDownSelection.ClearOptions();
		Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * L_0 = __this->get_dropDownSelection_6();
		Dropdown_ClearOptions_m7F59A8B054698715921D2B0E37EB1808BE53C23C(L_0, /*hidden argument*/NULL);
		// var options = new List<Dropdown.OptionData>(spriteLibraries.Length);
		SpriteLibraryAssetU5BU5D_t8208BFA0067B2DFDA16254E72287E09338255B27* L_1 = __this->get_spriteLibraries_4();
		List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4 * L_2 = (List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4 *)il2cpp_codegen_object_new(List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4_il2cpp_TypeInfo_var);
		List_1__ctor_m32A29DCC109DE9CA0E9445719CB6CEEC99AEDA05(L_2, ((int32_t)((int32_t)(((RuntimeArray*)L_1)->max_length))), /*hidden argument*/List_1__ctor_m32A29DCC109DE9CA0E9445719CB6CEEC99AEDA05_RuntimeMethod_var);
		V_0 = L_2;
		// for (int i = 0; i < spriteLibraries.Length; ++i)
		V_1 = 0;
		goto IL_0039;
	}

IL_001d:
	{
		// options.Add(new Dropdown.OptionData(spriteLibraries[i].name));
		List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4 * L_3 = V_0;
		SpriteLibraryAssetU5BU5D_t8208BFA0067B2DFDA16254E72287E09338255B27* L_4 = __this->get_spriteLibraries_4();
		int32_t L_5 = V_1;
		int32_t L_6 = L_5;
		SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF * L_7 = (L_4)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_6));
		String_t* L_8;
		L_8 = Object_get_name_m0C7BC870ED2F0DC5A2FB09628136CD7D1CB82CFB(L_7, /*hidden argument*/NULL);
		OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857 * L_9 = (OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857 *)il2cpp_codegen_object_new(OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857_il2cpp_TypeInfo_var);
		OptionData__ctor_m5AF14BD8BBF6118AC51A7A9A38AE3AB2DE3C2675(L_9, L_8, /*hidden argument*/NULL);
		List_1_Add_mC2611000DA97145D1DFC867ECA604FBB3E8628EC(L_3, L_9, /*hidden argument*/List_1_Add_mC2611000DA97145D1DFC867ECA604FBB3E8628EC_RuntimeMethod_var);
		// for (int i = 0; i < spriteLibraries.Length; ++i)
		int32_t L_10 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_10, (int32_t)1));
	}

IL_0039:
	{
		// for (int i = 0; i < spriteLibraries.Length; ++i)
		int32_t L_11 = V_1;
		SpriteLibraryAssetU5BU5D_t8208BFA0067B2DFDA16254E72287E09338255B27* L_12 = __this->get_spriteLibraries_4();
		if ((((int32_t)L_11) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_12)->max_length))))))
		{
			goto IL_001d;
		}
	}
	{
		// dropDownSelection.options = options;
		Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * L_13 = __this->get_dropDownSelection_6();
		List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4 * L_14 = V_0;
		Dropdown_set_options_m64FE183D8C988AC643870012A98F3095E2C2C14B(L_13, L_14, /*hidden argument*/NULL);
		// dropDownSelection.onValueChanged.AddListener(OnDropDownValueChanged);
		Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * L_15 = __this->get_dropDownSelection_6();
		DropdownEvent_tEB2C75C3DBC789936B31D9A979FD62E047846CFB * L_16;
		L_16 = Dropdown_get_onValueChanged_m05479714AEB528CF5CE93ED09870E98E6EC94CFA_inline(L_15, /*hidden argument*/NULL);
		UnityAction_1_t5CF46572372725E6225588C466A7AF5C8597AA79 * L_17 = (UnityAction_1_t5CF46572372725E6225588C466A7AF5C8597AA79 *)il2cpp_codegen_object_new(UnityAction_1_t5CF46572372725E6225588C466A7AF5C8597AA79_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m595DA2ECB187B2DF951D640BFEBCE02F3F800A3F(L_17, __this, (intptr_t)((intptr_t)SwapFullSkin_OnDropDownValueChanged_m7083F235642A81B6D2F6B81B43B8C0DED47F2A45_RuntimeMethod_var), /*hidden argument*/UnityAction_1__ctor_m595DA2ECB187B2DF951D640BFEBCE02F3F800A3F_RuntimeMethod_var);
		UnityEvent_1_AddListener_mFCFAC8ACA3F75283268DC2629ADEB5504E8FC0C2(L_16, L_17, /*hidden argument*/UnityEvent_1_AddListener_mFCFAC8ACA3F75283268DC2629ADEB5504E8FC0C2_RuntimeMethod_var);
		// }
		return;
	}
}
// System.Void Unity.U2D.Animation.Sample.SwapFullSkin::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SwapFullSkin__ctor_mA12BC6BA6C3A18C09534BA7C181D7B0D2132B46D (SwapFullSkin_t115938BCBBCAED7A2735A385C54CC8C3CE3B82A8 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// Conversion methods for marshalling of: Unity.U2D.Animation.Sample.SwapOptionData
IL2CPP_EXTERN_C void SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0_marshal_pinvoke(const SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0& unmarshaled, SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0_marshaled_pinvoke& marshaled)
{
	Exception_t* ___dropdown_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'dropdown' of type 'SwapOptionData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___dropdown_0Exception, NULL);
}
IL2CPP_EXTERN_C void SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0_marshal_pinvoke_back(const SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0_marshaled_pinvoke& marshaled, SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0& unmarshaled)
{
	Exception_t* ___dropdown_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'dropdown' of type 'SwapOptionData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___dropdown_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: Unity.U2D.Animation.Sample.SwapOptionData
IL2CPP_EXTERN_C void SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0_marshal_pinvoke_cleanup(SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0_marshaled_pinvoke& marshaled)
{
}
// Conversion methods for marshalling of: Unity.U2D.Animation.Sample.SwapOptionData
IL2CPP_EXTERN_C void SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0_marshal_com(const SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0& unmarshaled, SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0_marshaled_com& marshaled)
{
	Exception_t* ___dropdown_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'dropdown' of type 'SwapOptionData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___dropdown_0Exception, NULL);
}
IL2CPP_EXTERN_C void SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0_marshal_com_back(const SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0_marshaled_com& marshaled, SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0& unmarshaled)
{
	Exception_t* ___dropdown_0Exception = il2cpp_codegen_get_marshal_directive_exception("Cannot marshal field 'dropdown' of type 'SwapOptionData': Reference type field marshaling is not supported.");
	IL2CPP_RAISE_MANAGED_EXCEPTION(___dropdown_0Exception, NULL);
}
// Conversion method for clean up from marshalling of: Unity.U2D.Animation.Sample.SwapOptionData
IL2CPP_EXTERN_C void SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0_marshal_com_cleanup(SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0_marshaled_com& marshaled)
{
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Unity.U2D.Animation.Sample.SwapPart::Start()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SwapPart_Start_m50A8093A53940A60C36B6C2402E6527BB69C93AC (SwapPart_t3D3F08F6133F7F815ABC581DC01B75B8FAB631BF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&Enumerable_Count_TisString_t_m73DFA608B1C4B59DF24B8910F113F39B62FF139D_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerable_1_tBD60400523D840591A17E4CBBACC79397F68FAA2_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_1_t0DE5AA701B682A891412350E63D3E441F98F205C_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_Add_mC2611000DA97145D1DFC867ECA604FBB3E8628EC_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1__ctor_m32A29DCC109DE9CA0E9445719CB6CEEC99AEDA05_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass2_0_U3CStartU3Eb__0_m9083B95FAEDB7D46812287FDB7003029DD4BA255_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CU3Ec__DisplayClass2_0_tE1B0DB639BE84CA15D2CBD9694D84FDEBB76188F_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1__ctor_m595DA2ECB187B2DF951D640BFEBCE02F3F800A3F_RuntimeMethod_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityAction_1_t5CF46572372725E6225588C466A7AF5C8597AA79_il2cpp_TypeInfo_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&UnityEvent_1_AddListener_mFCFAC8ACA3F75283268DC2629ADEB5504E8FC0C2_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	SwapOptionDataU5BU5D_t6D1276EE178F77CE5243BF5799FFAE1BB5F177F2* V_0 = NULL;
	int32_t V_1 = 0;
	U3CU3Ec__DisplayClass2_0_tE1B0DB639BE84CA15D2CBD9694D84FDEBB76188F * V_2 = NULL;
	List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4 * V_3 = NULL;
	RuntimeObject* V_4 = NULL;
	String_t* V_5 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	il2cpp::utils::ExceptionSupportStack<int32_t, 1> __leave_targets;
	{
		// foreach (var swapOption in swapOptionData)
		SwapOptionDataU5BU5D_t6D1276EE178F77CE5243BF5799FFAE1BB5F177F2* L_0 = __this->get_swapOptionData_5();
		V_0 = L_0;
		V_1 = 0;
		goto IL_00bf;
	}

IL_000e:
	{
		U3CU3Ec__DisplayClass2_0_tE1B0DB639BE84CA15D2CBD9694D84FDEBB76188F * L_1 = (U3CU3Ec__DisplayClass2_0_tE1B0DB639BE84CA15D2CBD9694D84FDEBB76188F *)il2cpp_codegen_object_new(U3CU3Ec__DisplayClass2_0_tE1B0DB639BE84CA15D2CBD9694D84FDEBB76188F_il2cpp_TypeInfo_var);
		U3CU3Ec__DisplayClass2_0__ctor_m68376F15EF2246A0E99F1E9513F11E037F84CE1C(L_1, /*hidden argument*/NULL);
		V_2 = L_1;
		// foreach (var swapOption in swapOptionData)
		U3CU3Ec__DisplayClass2_0_tE1B0DB639BE84CA15D2CBD9694D84FDEBB76188F * L_2 = V_2;
		SwapOptionDataU5BU5D_t6D1276EE178F77CE5243BF5799FFAE1BB5F177F2* L_3 = V_0;
		int32_t L_4 = V_1;
		int32_t L_5 = L_4;
		SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0  L_6 = (L_3)->GetAtUnchecked(static_cast<il2cpp_array_size_t>(L_5));
		L_2->set_swapOption_0(L_6);
		// swapOption.dropdown.ClearOptions();
		U3CU3Ec__DisplayClass2_0_tE1B0DB639BE84CA15D2CBD9694D84FDEBB76188F * L_7 = V_2;
		SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0 * L_8 = L_7->get_address_of_swapOption_0();
		Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * L_9 = L_8->get_dropdown_0();
		Dropdown_ClearOptions_m7F59A8B054698715921D2B0E37EB1808BE53C23C(L_9, /*hidden argument*/NULL);
		// var labels = spriteLibraryAsset.GetCategoryLabelNames(swapOption.category);
		SpriteLibraryAsset_t46CA98CB05138679BE01BD19D3CB773A63368BCF * L_10 = __this->get_spriteLibraryAsset_4();
		U3CU3Ec__DisplayClass2_0_tE1B0DB639BE84CA15D2CBD9694D84FDEBB76188F * L_11 = V_2;
		SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0 * L_12 = L_11->get_address_of_swapOption_0();
		String_t* L_13 = L_12->get_category_2();
		RuntimeObject* L_14;
		L_14 = SpriteLibraryAsset_GetCategoryLabelNames_mCBE6FB145B0CFF3C08CCA0720B8114B479B74FCA(L_10, L_13, /*hidden argument*/NULL);
		// var dropDownOption = new List<Dropdown.OptionData>(labels.Count());
		RuntimeObject* L_15 = L_14;
		int32_t L_16;
		L_16 = Enumerable_Count_TisString_t_m73DFA608B1C4B59DF24B8910F113F39B62FF139D(L_15, /*hidden argument*/Enumerable_Count_TisString_t_m73DFA608B1C4B59DF24B8910F113F39B62FF139D_RuntimeMethod_var);
		List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4 * L_17 = (List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4 *)il2cpp_codegen_object_new(List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4_il2cpp_TypeInfo_var);
		List_1__ctor_m32A29DCC109DE9CA0E9445719CB6CEEC99AEDA05(L_17, L_16, /*hidden argument*/List_1__ctor_m32A29DCC109DE9CA0E9445719CB6CEEC99AEDA05_RuntimeMethod_var);
		V_3 = L_17;
		// foreach (var label in labels)
		RuntimeObject* L_18;
		L_18 = InterfaceFuncInvoker0< RuntimeObject* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.String>::GetEnumerator() */, IEnumerable_1_tBD60400523D840591A17E4CBBACC79397F68FAA2_il2cpp_TypeInfo_var, L_15);
		V_4 = L_18;
	}

IL_005a:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0072;
		}

IL_005c:
		{
			// foreach (var label in labels)
			RuntimeObject* L_19 = V_4;
			String_t* L_20;
			L_20 = InterfaceFuncInvoker0< String_t* >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.String>::get_Current() */, IEnumerator_1_t0DE5AA701B682A891412350E63D3E441F98F205C_il2cpp_TypeInfo_var, L_19);
			V_5 = L_20;
			// dropDownOption.Add(new Dropdown.OptionData(label));
			List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4 * L_21 = V_3;
			String_t* L_22 = V_5;
			OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857 * L_23 = (OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857 *)il2cpp_codegen_object_new(OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857_il2cpp_TypeInfo_var);
			OptionData__ctor_m5AF14BD8BBF6118AC51A7A9A38AE3AB2DE3C2675(L_23, L_22, /*hidden argument*/NULL);
			List_1_Add_mC2611000DA97145D1DFC867ECA604FBB3E8628EC(L_21, L_23, /*hidden argument*/List_1_Add_mC2611000DA97145D1DFC867ECA604FBB3E8628EC_RuntimeMethod_var);
		}

IL_0072:
		{
			// foreach (var label in labels)
			RuntimeObject* L_24 = V_4;
			bool L_25;
			L_25 = InterfaceFuncInvoker0< bool >::Invoke(0 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t5956F3AFB7ECF1117E3BC5890E7FC7B7F7A04105_il2cpp_TypeInfo_var, L_24);
			if (L_25)
			{
				goto IL_005c;
			}
		}

IL_007b:
		{
			IL2CPP_LEAVE(0x89, FINALLY_007d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_007d;
	}

FINALLY_007d:
	{ // begin finally (depth: 1)
		{
			RuntimeObject* L_26 = V_4;
			if (!L_26)
			{
				goto IL_0088;
			}
		}

IL_0081:
		{
			RuntimeObject* L_27 = V_4;
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t099785737FC6A1E3699919A94109383715A8D807_il2cpp_TypeInfo_var, L_27);
		}

IL_0088:
		{
			IL2CPP_END_FINALLY(125)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(125)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x89, IL_0089)
	}

IL_0089:
	{
		// swapOption.dropdown.options = dropDownOption;
		U3CU3Ec__DisplayClass2_0_tE1B0DB639BE84CA15D2CBD9694D84FDEBB76188F * L_28 = V_2;
		SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0 * L_29 = L_28->get_address_of_swapOption_0();
		Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * L_30 = L_29->get_dropdown_0();
		List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4 * L_31 = V_3;
		Dropdown_set_options_m64FE183D8C988AC643870012A98F3095E2C2C14B(L_30, L_31, /*hidden argument*/NULL);
		// swapOption.dropdown.onValueChanged.AddListener((x)=>
		// {
		//     swapOption.spriteResolver.SetCategoryAndLabel(swapOption.category, swapOption.dropdown.options[x].text);
		// });
		U3CU3Ec__DisplayClass2_0_tE1B0DB639BE84CA15D2CBD9694D84FDEBB76188F * L_32 = V_2;
		SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0 * L_33 = L_32->get_address_of_swapOption_0();
		Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * L_34 = L_33->get_dropdown_0();
		DropdownEvent_tEB2C75C3DBC789936B31D9A979FD62E047846CFB * L_35;
		L_35 = Dropdown_get_onValueChanged_m05479714AEB528CF5CE93ED09870E98E6EC94CFA_inline(L_34, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_tE1B0DB639BE84CA15D2CBD9694D84FDEBB76188F * L_36 = V_2;
		UnityAction_1_t5CF46572372725E6225588C466A7AF5C8597AA79 * L_37 = (UnityAction_1_t5CF46572372725E6225588C466A7AF5C8597AA79 *)il2cpp_codegen_object_new(UnityAction_1_t5CF46572372725E6225588C466A7AF5C8597AA79_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m595DA2ECB187B2DF951D640BFEBCE02F3F800A3F(L_37, L_36, (intptr_t)((intptr_t)U3CU3Ec__DisplayClass2_0_U3CStartU3Eb__0_m9083B95FAEDB7D46812287FDB7003029DD4BA255_RuntimeMethod_var), /*hidden argument*/UnityAction_1__ctor_m595DA2ECB187B2DF951D640BFEBCE02F3F800A3F_RuntimeMethod_var);
		UnityEvent_1_AddListener_mFCFAC8ACA3F75283268DC2629ADEB5504E8FC0C2(L_35, L_37, /*hidden argument*/UnityEvent_1_AddListener_mFCFAC8ACA3F75283268DC2629ADEB5504E8FC0C2_RuntimeMethod_var);
		int32_t L_38 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_38, (int32_t)1));
	}

IL_00bf:
	{
		// foreach (var swapOption in swapOptionData)
		int32_t L_39 = V_1;
		SwapOptionDataU5BU5D_t6D1276EE178F77CE5243BF5799FFAE1BB5F177F2* L_40 = V_0;
		if ((((int32_t)L_39) < ((int32_t)((int32_t)((int32_t)(((RuntimeArray*)L_40)->max_length))))))
		{
			goto IL_000e;
		}
	}
	{
		// }
		return;
	}
}
// System.Void Unity.U2D.Animation.Sample.SwapPart::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SwapPart__ctor_mA3952C56B7F13FB47C5E3750921B5E81AF43F796 (SwapPart_t3D3F08F6133F7F815ABC581DC01B75B8FAB631BF * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mC0995D847F6A95B1A553652636C38A2AA8B13BED(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Unity.U2D.Animation.Sample.SwapPart/<>c__DisplayClass2_0::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0__ctor_m68376F15EF2246A0E99F1E9513F11E037F84CE1C (U3CU3Ec__DisplayClass2_0_tE1B0DB639BE84CA15D2CBD9694D84FDEBB76188F * __this, const RuntimeMethod* method)
{
	{
		Object__ctor_m88880E0413421D13FD95325EDCE231707CE1F405(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Unity.U2D.Animation.Sample.SwapPart/<>c__DisplayClass2_0::<Start>b__0(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0_U3CStartU3Eb__0_m9083B95FAEDB7D46812287FDB7003029DD4BA255 (U3CU3Ec__DisplayClass2_0_tE1B0DB639BE84CA15D2CBD9694D84FDEBB76188F * __this, int32_t ___x0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&List_1_get_Item_mF979E11037D572E72D866EC260EF0434EF296E67_RuntimeMethod_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		// swapOption.spriteResolver.SetCategoryAndLabel(swapOption.category, swapOption.dropdown.options[x].text);
		SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0 * L_0 = __this->get_address_of_swapOption_0();
		SpriteResolver_t5B3D64444E443E530CC3C9CA5336DED4721F7956 * L_1 = L_0->get_spriteResolver_1();
		SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0 * L_2 = __this->get_address_of_swapOption_0();
		String_t* L_3 = L_2->get_category_2();
		SwapOptionData_t1D1D28503E77CF3603D838085C0B542E5A72CAA0 * L_4 = __this->get_address_of_swapOption_0();
		Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * L_5 = L_4->get_dropdown_0();
		List_1_tAF6577A540702C9F6C407DE69A8FAFB502339DC4 * L_6;
		L_6 = Dropdown_get_options_mF427A2157CDD901C12F1B160C4D1F8207D7111D0(L_5, /*hidden argument*/NULL);
		int32_t L_7 = ___x0;
		OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857 * L_8;
		L_8 = List_1_get_Item_mF979E11037D572E72D866EC260EF0434EF296E67_inline(L_6, L_7, /*hidden argument*/List_1_get_Item_mF979E11037D572E72D866EC260EF0434EF296E67_RuntimeMethod_var);
		String_t* L_9;
		L_9 = OptionData_get_text_m8652FE3866405C4C7C3782659009EF2C7E54D232_inline(L_8, /*hidden argument*/NULL);
		SpriteResolver_SetCategoryAndLabel_m3A76B2CE2DEB1E9985818996176FC83915C7CF37(L_1, L_3, L_9, /*hidden argument*/NULL);
		// });
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR DropdownEvent_tEB2C75C3DBC789936B31D9A979FD62E047846CFB * Dropdown_get_onValueChanged_m05479714AEB528CF5CE93ED09870E98E6EC94CFA_inline (Dropdown_t099F5232BB75810BC79EED6E27DDCED46C3BCD96 * __this, const RuntimeMethod* method)
{
	{
		// public DropdownEvent onValueChanged { get { return m_OnValueChanged; } set { m_OnValueChanged = value; } }
		DropdownEvent_tEB2C75C3DBC789936B31D9A979FD62E047846CFB * L_0 = __this->get_m_OnValueChanged_27();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR String_t* OptionData_get_text_m8652FE3866405C4C7C3782659009EF2C7E54D232_inline (OptionData_t5F665DC13C1E4307727D66ECC1100B3A77E3E857 * __this, const RuntimeMethod* method)
{
	{
		// public string text  { get { return m_Text; }  set { m_Text = value;  } }
		String_t* L_0 = __this->get_m_Text_0();
		return L_0;
	}
}
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mF00B574E58FB078BB753B05A3B86DD0A7A266B63_gshared_inline (List_1_t3F94120C77410A62EAE48421CF166B83AB95A2F5 * __this, int32_t ___index0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___index0;
		int32_t L_1 = (int32_t)__this->get__size_2();
		if ((!(((uint32_t)L_0) >= ((uint32_t)L_1))))
		{
			goto IL_000e;
		}
	}
	{
		ThrowHelper_ThrowArgumentOutOfRangeException_m4841366ABC2B2AFA37C10900551D7E07522C0929(/*hidden argument*/NULL);
	}

IL_000e:
	{
		ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE* L_2 = (ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)__this->get__items_1();
		int32_t L_3 = ___index0;
		RuntimeObject * L_4;
		L_4 = IL2CPP_ARRAY_UNSAFE_LOAD((ObjectU5BU5D_tC1F4EE0DB0B7300255F5FD4AF64FE4C585CF5ADE*)L_2, (int32_t)L_3);
		return (RuntimeObject *)L_4;
	}
}
