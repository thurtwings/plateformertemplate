﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void AllIn1Shader::MakeNewMaterial(System.Boolean,System.String)
extern void AllIn1Shader_MakeNewMaterial_mE7F705EA36336223D8864DD397FAA986C61EBEE3 (void);
// 0x00000002 System.Void AllIn1Shader::MakeCopy()
extern void AllIn1Shader_MakeCopy_m8D3FAF720787E21FD8C6162887DA674915B36C36 (void);
// 0x00000003 System.Void AllIn1Shader::ResetAllProperties(System.Boolean,System.String)
extern void AllIn1Shader_ResetAllProperties_mA039FEF1F00517620D3A3C273EE85B404CFB9E0A (void);
// 0x00000004 System.String AllIn1Shader::GetStringFromShaderType()
extern void AllIn1Shader_GetStringFromShaderType_m54D914538D389E52D72190351D9A664274691D80 (void);
// 0x00000005 System.Void AllIn1Shader::SetMaterial(AllIn1Shader/AfterSetAction,System.Boolean,System.String)
extern void AllIn1Shader_SetMaterial_mC3984D294123C8A0B6940D3F09FFEEEE07C8572A (void);
// 0x00000006 System.Void AllIn1Shader::DoAfterSetAction(AllIn1Shader/AfterSetAction)
extern void AllIn1Shader_DoAfterSetAction_m2123918B31E2C14412D1737E15FA19B81C172EEC (void);
// 0x00000007 System.Void AllIn1Shader::TryCreateNew()
extern void AllIn1Shader_TryCreateNew_m234BC20F58B0DFB4E9647BE34BE90A283099DF93 (void);
// 0x00000008 System.Void AllIn1Shader::ClearAllKeywords()
extern void AllIn1Shader_ClearAllKeywords_mD988454DE618F8E544EF171AD3338F98E24B4333 (void);
// 0x00000009 System.Void AllIn1Shader::SetKeyword(System.String,System.Boolean)
extern void AllIn1Shader_SetKeyword_mE5D052091451244FC46391F625C8C63AF8F0FD8E (void);
// 0x0000000A System.Void AllIn1Shader::FindCurrMaterial()
extern void AllIn1Shader_FindCurrMaterial_mB9EFE076006FA4CCE609D55F34FEDFDD19E03713 (void);
// 0x0000000B System.Void AllIn1Shader::CleanMaterial()
extern void AllIn1Shader_CleanMaterial_m8E500FAE06954447FBD9F205E98573F78DEF2019 (void);
// 0x0000000C System.Void AllIn1Shader::SaveMaterial()
extern void AllIn1Shader_SaveMaterial_mAC0418B7A3142DF46940D577276240D78C8BE251 (void);
// 0x0000000D System.Void AllIn1Shader::SaveMaterialWithOtherName(System.String,System.Int32)
extern void AllIn1Shader_SaveMaterialWithOtherName_m1059A69AEB8181D4E38047AB6E1181AF51D0FF82 (void);
// 0x0000000E System.Void AllIn1Shader::DoSaving(System.String)
extern void AllIn1Shader_DoSaving_m44BABDC9CCF5312136F411570420E7C5EF47ED44 (void);
// 0x0000000F System.Void AllIn1Shader::SetSceneDirty()
extern void AllIn1Shader_SetSceneDirty_m1FFC452FC0782C2108031B6B3BDA16A07EF154C2 (void);
// 0x00000010 System.Void AllIn1Shader::MissingRenderer()
extern void AllIn1Shader_MissingRenderer_m0CF5D69D36B76B33631F42C30F5B685EF7437C57 (void);
// 0x00000011 System.Void AllIn1Shader::ToggleSetAtlasUvs(System.Boolean)
extern void AllIn1Shader_ToggleSetAtlasUvs_mEE86D7544508A24DAF9323D8BF74816E308E6F96 (void);
// 0x00000012 System.Void AllIn1Shader::ApplyMaterialToHierarchy()
extern void AllIn1Shader_ApplyMaterialToHierarchy_m32F0C81AEE934F21D945F387C6E4E6DC6B89C481 (void);
// 0x00000013 System.Void AllIn1Shader::CheckIfValidTarget()
extern void AllIn1Shader_CheckIfValidTarget_mD3FCB5C967D6B6D678926BE784D84A7A761304BA (void);
// 0x00000014 System.Void AllIn1Shader::GetAllChildren(UnityEngine.Transform,System.Collections.Generic.List`1<UnityEngine.Transform>&)
extern void AllIn1Shader_GetAllChildren_m905322E49C9EF2A343D1623090A3B57B9330AF57 (void);
// 0x00000015 System.Void AllIn1Shader::RenderToImage()
extern void AllIn1Shader_RenderToImage_m9ADBB9A484BCAD28619CFC20FC6CB733E09909B9 (void);
// 0x00000016 System.Void AllIn1Shader::RenderAndSaveTexture(UnityEngine.Material,UnityEngine.Texture)
extern void AllIn1Shader_RenderAndSaveTexture_m6A08515FC4F916E1D4349C397E006BB4299905E3 (void);
// 0x00000017 System.String AllIn1Shader::GetNewValidPath(System.String,System.Int32)
extern void AllIn1Shader_GetNewValidPath_m13B5D88B875BB5244D2A47E0D58557F87E005AC9 (void);
// 0x00000018 System.Void AllIn1Shader::OnEnable()
extern void AllIn1Shader_OnEnable_mE9835DC94B4E5B7CC9C06BA0A406A4B565AAAF46 (void);
// 0x00000019 System.Void AllIn1Shader::OnDisable()
extern void AllIn1Shader_OnDisable_mE67BF9B67E77CC0520E942A4FECD5B133FD7A7B1 (void);
// 0x0000001A System.Void AllIn1Shader::OnEditorUpdate()
extern void AllIn1Shader_OnEditorUpdate_m98A1E511589FC578536B47B8E6AD1122BF565DB9 (void);
// 0x0000001B System.Void AllIn1Shader::CreateAndAssignNormalMap()
extern void AllIn1Shader_CreateAndAssignNormalMap_mE09D63046961A4E6852DB6902D751D234A7B65AC (void);
// 0x0000001C System.Void AllIn1Shader::SetNewNormalTexture()
extern void AllIn1Shader_SetNewNormalTexture_mC77EA7A52EFE22351A6AAFEA5681D01EE1342E8C (void);
// 0x0000001D System.Void AllIn1Shader::SetNewNormalTexture2()
extern void AllIn1Shader_SetNewNormalTexture2_mD993E8F9CB4DD2AAA747931FC0A762B45725B8C8 (void);
// 0x0000001E System.Void AllIn1Shader::SetNewNormalTexture3()
extern void AllIn1Shader_SetNewNormalTexture3_m535753B6B39D131A10368250C16C5D9AFDCB9D5D (void);
// 0x0000001F System.Void AllIn1Shader::SetNewNormalTexture4()
extern void AllIn1Shader_SetNewNormalTexture4_mE31E4D0C69A28D4D840D0C02C7EE4A8B4E10CA69 (void);
// 0x00000020 UnityEngine.Texture2D AllIn1Shader::CreateNormalMap(UnityEngine.Texture2D,System.Single,System.Int32)
extern void AllIn1Shader_CreateNormalMap_mB3B664F50B8CCA42FBB5BD5E6C23AD44C731D419 (void);
// 0x00000021 System.Void AllIn1Shader::.ctor()
extern void AllIn1Shader__ctor_mC28FD6F66BE2DF14119F5AC9DA0CA92F48CD42EA (void);
// 0x00000022 System.Void ConfinerSwitcher::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void ConfinerSwitcher_OnTriggerEnter2D_m154905373C7B3015DF1F7A97D9997F0A739DF57C (void);
// 0x00000023 System.Void ConfinerSwitcher::OnTriggerExit2D(UnityEngine.Collider2D)
extern void ConfinerSwitcher_OnTriggerExit2D_mD6169820382DFD57E6AB4094DCD08D67564BA839 (void);
// 0x00000024 System.Void ConfinerSwitcher::.ctor()
extern void ConfinerSwitcher__ctor_mBE91C57EEC4076DC0F5111EA22A19D55C7EB9EBF (void);
// 0x00000025 System.Void EnemyHealthBar::Start()
extern void EnemyHealthBar_Start_mAC5072644CD25461F54283A494648B93E10740AD (void);
// 0x00000026 System.Void EnemyHealthBar::UpdateHealthBar()
extern void EnemyHealthBar_UpdateHealthBar_m6B0617F247EA5BA5FBBF85F060C35F75B8F4FCD2 (void);
// 0x00000027 System.Void EnemyHealthBar::Update()
extern void EnemyHealthBar_Update_m0D89B3B8999217593AA991F95BC0CB9D0DFF3B4B (void);
// 0x00000028 System.Void EnemyHealthBar::.ctor()
extern void EnemyHealthBar__ctor_mA53148773B4E9CA545B3C7D4564ADAF769B50C8B (void);
// 0x00000029 System.Void CameraShakeCinemachine::Awake()
extern void CameraShakeCinemachine_Awake_m5CA148B8FF6AFB8DE443542D071F57882EFCA5DD (void);
// 0x0000002A System.Void CameraShakeCinemachine::Start()
extern void CameraShakeCinemachine_Start_mB42DA2C5794AA38FCC11482B173E55231F067AC2 (void);
// 0x0000002B System.Void CameraShakeCinemachine::Update()
extern void CameraShakeCinemachine_Update_m0A0B14BC4F9D7A94E34CBC78C6C5748EFC3D09D7 (void);
// 0x0000002C System.Void CameraShakeCinemachine::CameraShake(System.Single,System.Single)
extern void CameraShakeCinemachine_CameraShake_mB84711D6B9F4E8E5D594D279BF8816A64B1B93B2 (void);
// 0x0000002D System.Void CameraShakeCinemachine::.ctor()
extern void CameraShakeCinemachine__ctor_m20E67BA2F073C46CE2FC51492A802D05B991C379 (void);
// 0x0000002E Movement Core::get_Movement()
extern void Core_get_Movement_m172A2A762581D45CE756AFD8D8F3A8A810ABD09B (void);
// 0x0000002F System.Void Core::set_Movement(Movement)
extern void Core_set_Movement_m04794317914E765F8DB75A42F4BBBA095A078C01 (void);
// 0x00000030 CollisionSenses Core::get_CollisionSenses()
extern void Core_get_CollisionSenses_mE72AFC5311B2947DCF85CB181BE1BC87CD264E4C (void);
// 0x00000031 System.Void Core::set_CollisionSenses(CollisionSenses)
extern void Core_set_CollisionSenses_mB5076760323379E0214B88D328EE15FC1BE96175 (void);
// 0x00000032 Combat Core::get_Combat()
extern void Core_get_Combat_m6F4DC96E9A98EDC6216BD506E9DD9D626A2110AD (void);
// 0x00000033 System.Void Core::set_Combat(Combat)
extern void Core_set_Combat_mEEC8D3B0EFA0D948AE7F5A92510AC71AA7AD5DE7 (void);
// 0x00000034 Stats Core::get_Stats()
extern void Core_get_Stats_mF10C44DFB82AD0B37BF84FD1690D43332F38530E (void);
// 0x00000035 System.Void Core::set_Stats(Stats)
extern void Core_set_Stats_m3390B6713B79E8ED850DF35C6D0E967BEE9A6B74 (void);
// 0x00000036 System.Void Core::Awake()
extern void Core_Awake_mFDE01B81F891FE5CC0111365322483646A6F34C9 (void);
// 0x00000037 System.Void Core::LogicUpdate()
extern void Core_LogicUpdate_m49FCF14B8B1C721FDA704C440C4FD14E61D458B6 (void);
// 0x00000038 System.Void Core::AddComponent(ILogicUpdate)
extern void Core_AddComponent_mA73D4B30456F2FD76182E6D8ED285CC24C924496 (void);
// 0x00000039 System.Void Core::.ctor()
extern void Core__ctor_mD55440EAEDEF387693B0FA45820CBB23D7D17EA1 (void);
// 0x0000003A UnityEngine.LayerMask CollisionSenses::get_GroundMask()
extern void CollisionSenses_get_GroundMask_m74DF9AD2504A5F90FF3FC9B201B718F37620098E (void);
// 0x0000003B System.Void CollisionSenses::set_GroundMask(UnityEngine.LayerMask)
extern void CollisionSenses_set_GroundMask_mE220BC64B21ACE33D6398C13262D620B21741276 (void);
// 0x0000003C UnityEngine.LayerMask CollisionSenses::get_EnemyMask()
extern void CollisionSenses_get_EnemyMask_m68D99BA24204148E73C6ECBE3972EE9EB3375A7E (void);
// 0x0000003D System.Void CollisionSenses::set_EnemyMask(UnityEngine.LayerMask)
extern void CollisionSenses_set_EnemyMask_mDDBC023875DD52E9129DD4596FAEE421496F9483 (void);
// 0x0000003E System.Single CollisionSenses::get_GroundCheckRadius()
extern void CollisionSenses_get_GroundCheckRadius_m4C1A916647F84610FC94DCAE86650A6137030EE7 (void);
// 0x0000003F System.Void CollisionSenses::set_GroundCheckRadius(System.Single)
extern void CollisionSenses_set_GroundCheckRadius_m3B76801BCCF34884DFFCF414A38962E58A2EF355 (void);
// 0x00000040 System.Single CollisionSenses::get_WallCheckdistance()
extern void CollisionSenses_get_WallCheckdistance_mAE1E6A6B52A3039DE58E7B30F91D2D6D3D9894A4 (void);
// 0x00000041 System.Void CollisionSenses::set_WallCheckdistance(System.Single)
extern void CollisionSenses_set_WallCheckdistance_m5183A4EF4A810A6E448957D11EA1BFB008D61672 (void);
// 0x00000042 UnityEngine.Transform CollisionSenses::get_WallCheck()
extern void CollisionSenses_get_WallCheck_mF1C4E727DA58CC27AA86918C99F7F1EEFB142923 (void);
// 0x00000043 System.Void CollisionSenses::set_WallCheck(UnityEngine.Transform)
extern void CollisionSenses_set_WallCheck_m342A5F5C8E6CE1A9AB69CA69360613B9FC68D43F (void);
// 0x00000044 UnityEngine.Transform CollisionSenses::get_GroundCheck()
extern void CollisionSenses_get_GroundCheck_m496159F04E7490D40A43B154F60DBBEA4DE96C21 (void);
// 0x00000045 System.Void CollisionSenses::set_GroundCheck(UnityEngine.Transform)
extern void CollisionSenses_set_GroundCheck_m7701BFB5ACC47F3070E188C37A20E1B0D36CBA6C (void);
// 0x00000046 UnityEngine.Transform CollisionSenses::get_CeilingCheck()
extern void CollisionSenses_get_CeilingCheck_mACCEBE7CDD51354486B413F05D4B871F0B44A4D5 (void);
// 0x00000047 System.Void CollisionSenses::set_CeilingCheck(UnityEngine.Transform)
extern void CollisionSenses_set_CeilingCheck_mDED07D4536361D3843434E8222F828FF86EC4989 (void);
// 0x00000048 UnityEngine.Transform CollisionSenses::get_LedgeCheckVertical()
extern void CollisionSenses_get_LedgeCheckVertical_mC78C4565A077A9BB55D876CECEFD76EF8ED50EBB (void);
// 0x00000049 System.Void CollisionSenses::set_LedgeCheckVertical(UnityEngine.Transform)
extern void CollisionSenses_set_LedgeCheckVertical_m704C5A0D6EC48893B2C4176236F301E301FB1D5E (void);
// 0x0000004A UnityEngine.Transform CollisionSenses::get_LedgeCheckHorizontal()
extern void CollisionSenses_get_LedgeCheckHorizontal_m887FF88515E0782F19BD73506125364DDC1CFFAF (void);
// 0x0000004B System.Void CollisionSenses::set_LedgeCheckHorizontal(UnityEngine.Transform)
extern void CollisionSenses_set_LedgeCheckHorizontal_m74B3D709EE980723D4DE167B96313F4F1BD0DBCA (void);
// 0x0000004C System.Boolean CollisionSenses::get_Ground()
extern void CollisionSenses_get_Ground_mFB990B8B58EDCDA98F35884C245DF5F46C0C3086 (void);
// 0x0000004D System.Boolean CollisionSenses::get_Ceiling()
extern void CollisionSenses_get_Ceiling_mB494BB581AD6F6380091989CC1EC89DAAB1B5D2A (void);
// 0x0000004E System.Boolean CollisionSenses::get_WallFront()
extern void CollisionSenses_get_WallFront_m266D05D660BBF1E0328A9AA52400201A3B9B2A7E (void);
// 0x0000004F System.Boolean CollisionSenses::get_LedgeHorizontal()
extern void CollisionSenses_get_LedgeHorizontal_m8B3D72FC3CC777928CDCEB0DEE9C05780F5F6085 (void);
// 0x00000050 System.Boolean CollisionSenses::get_LedgeVertical()
extern void CollisionSenses_get_LedgeVertical_m5E81A9AB52A5563D3C99C887FC5361260D2E5329 (void);
// 0x00000051 System.Boolean CollisionSenses::get_WallBack()
extern void CollisionSenses_get_WallBack_m8E715001A7C7795261C09F80E756E7A7EC7332CF (void);
// 0x00000052 System.Boolean CollisionSenses::get_PoundEnemy()
extern void CollisionSenses_get_PoundEnemy_mE0304C010592D0651E5679030EDA79E41720980E (void);
// 0x00000053 System.Void CollisionSenses::.ctor()
extern void CollisionSenses__ctor_mBDB49A8784B242A162F970BE247FD674BAD132E7 (void);
// 0x00000054 System.Void Combat::LogicUpdate()
extern void Combat_LogicUpdate_m94074876B81F53EB1D53409721B74AC668FF6755 (void);
// 0x00000055 System.Void Combat::Damage(System.Single)
extern void Combat_Damage_mB5E8271EDA33B5CC73DB850F74CF6695E1CC1BF0 (void);
// 0x00000056 System.Void Combat::Knockback(UnityEngine.Vector2,System.Single,System.Int32)
extern void Combat_Knockback_m8B13571DDC9E87485F23B1BA46EE51299B8531DE (void);
// 0x00000057 System.Void Combat::CheckKnockback()
extern void Combat_CheckKnockback_m62080CF24CC9EE95C039CA9E78B9BDA0AF4C6DA7 (void);
// 0x00000058 System.Void Combat::.ctor()
extern void Combat__ctor_mD9E519351EDFD3E8CCBF26121CE3066B10013292 (void);
// 0x00000059 System.Void CoreComponent::Awake()
extern void CoreComponent_Awake_m5C815956F5E3EFC674FAF0AA2A4462B0E7459E7B (void);
// 0x0000005A System.Void CoreComponent::LogicUpdate()
extern void CoreComponent_LogicUpdate_mBFFF748AC572D178384A4C28B509C8C66D9F0C6C (void);
// 0x0000005B System.Void CoreComponent::.ctor()
extern void CoreComponent__ctor_m5A67D7612AE7FE4ED44D3F41EF83584F0D6A1E0E (void);
// 0x0000005C UnityEngine.Vector2 Movement::get_CurrentVelocity()
extern void Movement_get_CurrentVelocity_mA760A8ABD437E4779064E74D4FA7E6D31E59C56E (void);
// 0x0000005D System.Void Movement::set_CurrentVelocity(UnityEngine.Vector2)
extern void Movement_set_CurrentVelocity_m3E9C3C5D71014EC23E5C3C1C093F3F556FFFE46F (void);
// 0x0000005E System.Int32 Movement::get_FacingDirection()
extern void Movement_get_FacingDirection_m8DAFF6DCB9249A010654443F603F7C54070BC037 (void);
// 0x0000005F System.Void Movement::set_FacingDirection(System.Int32)
extern void Movement_set_FacingDirection_m614F41019F39CF99C616404D01F280181DD0E5F1 (void);
// 0x00000060 UnityEngine.Rigidbody2D Movement::get_Rb()
extern void Movement_get_Rb_m086D6E6B8CD9818B7C1C5B105266C04C244209C8 (void);
// 0x00000061 System.Void Movement::set_Rb(UnityEngine.Rigidbody2D)
extern void Movement_set_Rb_m354A4212C73B7D7FBC3330642A5B9284F2DCC164 (void);
// 0x00000062 System.Boolean Movement::get_CanSetVelocity()
extern void Movement_get_CanSetVelocity_m0886AD490E0C3A0DCCEC80B3959A31C4A5AD28DD (void);
// 0x00000063 System.Void Movement::set_CanSetVelocity(System.Boolean)
extern void Movement_set_CanSetVelocity_m7319992C6ECF9FFDBC045F194A412B61F1CEA852 (void);
// 0x00000064 System.Void Movement::Awake()
extern void Movement_Awake_mC24012D10E5FE2E1CF2B188AA4EFC347C0DA7A93 (void);
// 0x00000065 System.Void Movement::LogicUpdate()
extern void Movement_LogicUpdate_mE89F7103463D59D9ED1A21016BB25603714BDA9E (void);
// 0x00000066 System.Void Movement::SetVelocityX(System.Single)
extern void Movement_SetVelocityX_m407AB5BCB226BC2A2763D02158A602D098BC6B49 (void);
// 0x00000067 System.Void Movement::SetVelocityY(System.Single)
extern void Movement_SetVelocityY_m153D253AB4DAE87BC03544D4D608CFDD9BE5352C (void);
// 0x00000068 System.Void Movement::SetVelocity(System.Single,UnityEngine.Vector2,System.Int32)
extern void Movement_SetVelocity_m383F8B84DDCEE215F43E68700DCD8E9B9AC50F13 (void);
// 0x00000069 System.Void Movement::SetVelocity(System.Single,UnityEngine.Vector2)
extern void Movement_SetVelocity_m65856EE280DBA5212A0F96A8D36CD676CB4B20A3 (void);
// 0x0000006A System.Void Movement::SetVelocityZero()
extern void Movement_SetVelocityZero_m6FBAF7493CB440202FFEA4B8352451030C9CFDA6 (void);
// 0x0000006B System.Void Movement::SetFinalVelocity()
extern void Movement_SetFinalVelocity_mDB6CD6CDFA8EF43D2CDEEFAEC59287CAA37F9E9B (void);
// 0x0000006C System.Void Movement::CheckIfPlayerShouldFlip(System.Int32)
extern void Movement_CheckIfPlayerShouldFlip_mD735B19086BDF14D5EF10C337B4CE04C613713BC (void);
// 0x0000006D System.Void Movement::Flip()
extern void Movement_Flip_m52ED3BDED8C922EC7D94D4F4B18450D45BF87BEE (void);
// 0x0000006E System.Void Movement::.ctor()
extern void Movement__ctor_mEA4800F5BE98787C0ACA8CDF85918B56DE62A2AB (void);
// 0x0000006F System.Void Stats::Awake()
extern void Stats_Awake_mDCEFEF38E7205C2294049E7D1E362A7E02F68E78 (void);
// 0x00000070 System.Void Stats::DecreaseHealth(System.Single)
extern void Stats_DecreaseHealth_mC558D21506DB4476B79BE3AFEE5A2098A71CA920 (void);
// 0x00000071 System.Void Stats::Die(System.Boolean)
extern void Stats_Die_m1C5224BDB57C1184AECC2FBD588C485F5184F68B (void);
// 0x00000072 System.Void Stats::IncreaseHealth(System.Single)
extern void Stats_IncreaseHealth_m3A293207EEA28B4FEECB84F1EE652DE47A34430F (void);
// 0x00000073 System.Void Stats::DecreaseStamina(System.Single)
extern void Stats_DecreaseStamina_mB89DCE01198A6420E6C077537485D2941CC7AAD8 (void);
// 0x00000074 System.Collections.IEnumerator Stats::HitEffect()
extern void Stats_HitEffect_mAC56690239F0D3CC359F210111F0843FB5198C34 (void);
// 0x00000075 System.Void Stats::.ctor()
extern void Stats__ctor_m86D57BC6FFBC17545B914A808C1C0C5B12E0CD0A (void);
// 0x00000076 System.Void Stats/<HitEffect>d__12::.ctor(System.Int32)
extern void U3CHitEffectU3Ed__12__ctor_mD087739F6EB030EE325EC2DD7FAD22A372778386 (void);
// 0x00000077 System.Void Stats/<HitEffect>d__12::System.IDisposable.Dispose()
extern void U3CHitEffectU3Ed__12_System_IDisposable_Dispose_mA4C9CA68C781C2F79A0ECCDBF058E3147F5958C6 (void);
// 0x00000078 System.Boolean Stats/<HitEffect>d__12::MoveNext()
extern void U3CHitEffectU3Ed__12_MoveNext_mDF3D332A775741036132C6AD75D8ED1DD02347F0 (void);
// 0x00000079 System.Object Stats/<HitEffect>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CHitEffectU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4C4EBFCB4045479CFC11E8BB126EEC35FA73F02E (void);
// 0x0000007A System.Void Stats/<HitEffect>d__12::System.Collections.IEnumerator.Reset()
extern void U3CHitEffectU3Ed__12_System_Collections_IEnumerator_Reset_m487070A46E453A3631ED53956284AE13834E3D1D (void);
// 0x0000007B System.Object Stats/<HitEffect>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CHitEffectU3Ed__12_System_Collections_IEnumerator_get_Current_m4BC6FA39422EE5E507C5E4B847CB98D049B40379 (void);
// 0x0000007C System.Void BasicEnemyManager::Start()
extern void BasicEnemyManager_Start_m0D091E8C8ED1A96BD33718456965D6E2FDB9FDB1 (void);
// 0x0000007D System.Void BasicEnemyManager::Update()
extern void BasicEnemyManager_Update_m59117305BA67C48174EA6155CB11DFC13E727F67 (void);
// 0x0000007E System.Void BasicEnemyManager::EnterMovingState()
extern void BasicEnemyManager_EnterMovingState_m587C136816F0550F54F50D576123F0B62ED08CAF (void);
// 0x0000007F System.Void BasicEnemyManager::UpdateMovingState()
extern void BasicEnemyManager_UpdateMovingState_m4E6A27A0FFDEC7995A460151A1E3CF62BCDA905A (void);
// 0x00000080 System.Void BasicEnemyManager::ExitMovingState()
extern void BasicEnemyManager_ExitMovingState_m1E81EE70D265A291B853EB65107FA209CE5ECC26 (void);
// 0x00000081 System.Void BasicEnemyManager::EnterKnockbackState()
extern void BasicEnemyManager_EnterKnockbackState_m3DA995E83803753503F35611D6386CC1EE21FF0E (void);
// 0x00000082 System.Void BasicEnemyManager::UpdateKnockbackState()
extern void BasicEnemyManager_UpdateKnockbackState_m6234B9B60B410E7EE593A96FD0635E126B35E4E7 (void);
// 0x00000083 System.Void BasicEnemyManager::ExitKnockbackState()
extern void BasicEnemyManager_ExitKnockbackState_m908538721A63E2F3BD63D760DB41267C6F1497DE (void);
// 0x00000084 System.Void BasicEnemyManager::EnterDeadState()
extern void BasicEnemyManager_EnterDeadState_m23B0E521A0F1A50E0CF7AFDFB9E332542BFB4A11 (void);
// 0x00000085 System.Void BasicEnemyManager::UpdateDeadState()
extern void BasicEnemyManager_UpdateDeadState_mF00D3973A266250FCE60C67F9469EA25DA21D368 (void);
// 0x00000086 System.Void BasicEnemyManager::ExitDeadState()
extern void BasicEnemyManager_ExitDeadState_m7D6A7957DA617B00CC27ABAA46CA4BA1F9B74DED (void);
// 0x00000087 System.Void BasicEnemyManager::SwitchState(BasicEnemyManager/State)
extern void BasicEnemyManager_SwitchState_m86D1CD645804EB419BD7B8BB507366CC3F45228B (void);
// 0x00000088 System.Void BasicEnemyManager::Flip()
extern void BasicEnemyManager_Flip_m4DBB15F86EB3BA2B835ABE38E0B7CD706D92E63B (void);
// 0x00000089 System.Void BasicEnemyManager::Damage(System.Single[])
extern void BasicEnemyManager_Damage_mC9A0D55E5B72627429D693558DFE1245D4D55E4F (void);
// 0x0000008A System.Void BasicEnemyManager::CheckTouchDamages()
extern void BasicEnemyManager_CheckTouchDamages_mF8E172BD04FB49758782274CF15B881BD1D2491D (void);
// 0x0000008B System.Void BasicEnemyManager::OnDrawGizmos()
extern void BasicEnemyManager_OnDrawGizmos_m05ACF767E347C163B155EA95C6411F23A2CBB39E (void);
// 0x0000008C System.Void BasicEnemyManager::.ctor()
extern void BasicEnemyManager__ctor_mFF22A118397FB9F99F126511BE111DABD75A5378 (void);
// 0x0000008D System.Void CombatTestDummy::Awake()
extern void CombatTestDummy_Awake_m91F69F1F73B03308A575387018D990BB06C806CD (void);
// 0x0000008E System.Void CombatTestDummy::Update()
extern void CombatTestDummy_Update_m2AD93C31F48F1B2B1981DBF07710EA725DA463E7 (void);
// 0x0000008F System.Void CombatTestDummy::Damage(System.Single)
extern void CombatTestDummy_Damage_mF3A0FED5708785D9C3E7D7E162FCBEB8465C4FB2 (void);
// 0x00000090 System.Void CombatTestDummy::.ctor()
extern void CombatTestDummy__ctor_m463A5D63E81DD26A0F41A3321541CB3DB2F66200 (void);
// 0x00000091 System.Void E1_ChargeState::.ctor(Entity,FiniteStateMachine,System.String,D_ChargeState,Enemy1)
extern void E1_ChargeState__ctor_m2CD18D8FE0B276286ED51A93F7F6531D7D6DF450 (void);
// 0x00000092 System.Void E1_ChargeState::Enter()
extern void E1_ChargeState_Enter_mA4FC9FA2CC549C05D25A9F5FBC7F2DE3D5738873 (void);
// 0x00000093 System.Void E1_ChargeState::Exit()
extern void E1_ChargeState_Exit_mECAF690F538E8C2C9A52B504EE9F1D659C4F264A (void);
// 0x00000094 System.Void E1_ChargeState::LogicUpdate()
extern void E1_ChargeState_LogicUpdate_mC9507F0AF0E698C13E10DC00477C7866E687338E (void);
// 0x00000095 System.Void E1_ChargeState::PhysicsUpdate()
extern void E1_ChargeState_PhysicsUpdate_m56F52C48E7D75AF6F8B43456F67507C279BEBC29 (void);
// 0x00000096 System.Void E1_ChargeState::TickChecks()
extern void E1_ChargeState_TickChecks_m8F9F9BF6D936DEBFD6A6F56DAE90E7E2DEE042BC (void);
// 0x00000097 System.Void E1_DeadState::.ctor(Entity,FiniteStateMachine,System.String,D_DeadState,Enemy1)
extern void E1_DeadState__ctor_m6BD0A61217576D7DD60FDAC62F23B7054D4A5D4E (void);
// 0x00000098 System.Void E1_DeadState::Enter()
extern void E1_DeadState_Enter_m2C7AFC170116D67424DECC2FD3191FFF7834509A (void);
// 0x00000099 System.Void E1_DeadState::Exit()
extern void E1_DeadState_Exit_m0B5787E9FEBAD39A93B9D0E4A9973A15FC2EBDE9 (void);
// 0x0000009A System.Void E1_DeadState::LogicUpdate()
extern void E1_DeadState_LogicUpdate_m5421EAE276E64401B55B5D27E38F34B603F65389 (void);
// 0x0000009B System.Void E1_DeadState::PhysicsUpdate()
extern void E1_DeadState_PhysicsUpdate_m18D715C6017683E7D210937329738E4F39825458 (void);
// 0x0000009C System.Void E1_DeadState::TickChecks()
extern void E1_DeadState_TickChecks_m48AACC76F502E09B7730BC2E05658326EB1C68CD (void);
// 0x0000009D System.Void E1_IdleState::.ctor(Entity,FiniteStateMachine,System.String,D_IdleState,Enemy1)
extern void E1_IdleState__ctor_m668115FE36579930C04918C688B523E260BC7407 (void);
// 0x0000009E System.Void E1_IdleState::LogicUpdate()
extern void E1_IdleState_LogicUpdate_mA80792B788592B7378A2FB81BEBF62AD9DE2732A (void);
// 0x0000009F System.Void E1_LookForPlayerState::.ctor(Entity,FiniteStateMachine,System.String,D_LookForPlayerState,Enemy1)
extern void E1_LookForPlayerState__ctor_m22DCA26F29119A2E87A7CF80C6FE474313543639 (void);
// 0x000000A0 System.Void E1_LookForPlayerState::Enter()
extern void E1_LookForPlayerState_Enter_mD07012885705ACD4379DBC7E8274CE71E2492F77 (void);
// 0x000000A1 System.Void E1_LookForPlayerState::Exit()
extern void E1_LookForPlayerState_Exit_mADF8C5B2F7F6A32CEEEE5E75915432428C02FBFB (void);
// 0x000000A2 System.Void E1_LookForPlayerState::LogicUpdate()
extern void E1_LookForPlayerState_LogicUpdate_m6779C98150AB995723294868F492277B55556C9D (void);
// 0x000000A3 System.Void E1_LookForPlayerState::PhysicsUpdate()
extern void E1_LookForPlayerState_PhysicsUpdate_m67B23ABCDA3F7480E9C9E81A47A39D49D6F6D9C2 (void);
// 0x000000A4 System.Void E1_LookForPlayerState::TickChecks()
extern void E1_LookForPlayerState_TickChecks_mCF1E5C64F173F6A13C67D05DEB81ED0AE17E0B5E (void);
// 0x000000A5 System.Void E1_MeleeAttackState::.ctor(Entity,FiniteStateMachine,System.String,UnityEngine.Transform,D_MeleeAttackState,Enemy1)
extern void E1_MeleeAttackState__ctor_mF82BD38099217FE082647FFC0D3B84B9DE8CB0F0 (void);
// 0x000000A6 System.Void E1_MeleeAttackState::Enter()
extern void E1_MeleeAttackState_Enter_m6BBFA99FD6C92201BDEA69F9BFF0D2546BE6F906 (void);
// 0x000000A7 System.Void E1_MeleeAttackState::Exit()
extern void E1_MeleeAttackState_Exit_m8685E1676EAE0E05121B74A9E7D91ABF9104F696 (void);
// 0x000000A8 System.Void E1_MeleeAttackState::FinishAttack()
extern void E1_MeleeAttackState_FinishAttack_m6E46393F3EC1FFC1B448DB6813583094347C8289 (void);
// 0x000000A9 System.Void E1_MeleeAttackState::LogicUpdate()
extern void E1_MeleeAttackState_LogicUpdate_m82CAA2E97C5D0B882427DBD2D132BF1B755CB92B (void);
// 0x000000AA System.Void E1_MeleeAttackState::PhysicsUpdate()
extern void E1_MeleeAttackState_PhysicsUpdate_mA0E1F5A61C0D1D84BE8D3EC4725E8E475F526155 (void);
// 0x000000AB System.Void E1_MeleeAttackState::TickChecks()
extern void E1_MeleeAttackState_TickChecks_m423D311EF089764C8D358CE8E851EDB0E6E8C992 (void);
// 0x000000AC System.Void E1_MeleeAttackState::TriggerAttack()
extern void E1_MeleeAttackState_TriggerAttack_m8066EB17240303267CFE67DB3A86060E930D721E (void);
// 0x000000AD System.Void E1_MoveState::.ctor(Entity,FiniteStateMachine,System.String,D_MoveState,Enemy1)
extern void E1_MoveState__ctor_m26CE6418EEC0B69943019DB4AEC59B70A8733807 (void);
// 0x000000AE System.Void E1_MoveState::Enter()
extern void E1_MoveState_Enter_mB54D1ABDC20105D4420109994E23DA32B2E15FEA (void);
// 0x000000AF System.Void E1_MoveState::Exit()
extern void E1_MoveState_Exit_m1C5BCFB48700CF87EB2542120C40A996F5F8DF19 (void);
// 0x000000B0 System.Void E1_MoveState::LogicUpdate()
extern void E1_MoveState_LogicUpdate_mFDA8F488C4A89A255795C85554D20BFBA1201945 (void);
// 0x000000B1 System.Void E1_MoveState::PhysicsUpdate()
extern void E1_MoveState_PhysicsUpdate_m4BBEE08CD813A7A451E12329CC97167B44FF526F (void);
// 0x000000B2 System.Void E1_PlayerDetected::.ctor(Entity,FiniteStateMachine,System.String,D_PlayerDetected,Enemy1)
extern void E1_PlayerDetected__ctor_mF3E339439CF847ACC8BE1CF3A1EE1A329DAC9F83 (void);
// 0x000000B3 System.Void E1_PlayerDetected::Enter()
extern void E1_PlayerDetected_Enter_m8CFD1A3A888920D6E90E1A149B7B7F54450D36C8 (void);
// 0x000000B4 System.Void E1_PlayerDetected::Exit()
extern void E1_PlayerDetected_Exit_mF95C059C7E6DD283EE6A5A5BB250C28328C9A142 (void);
// 0x000000B5 System.Void E1_PlayerDetected::LogicUpdate()
extern void E1_PlayerDetected_LogicUpdate_m20EDF1D4DDD4F245B7274F2D05960F4FEEA759B8 (void);
// 0x000000B6 System.Void E1_PlayerDetected::PhysicsUpdate()
extern void E1_PlayerDetected_PhysicsUpdate_mAD84C010C1A0BE67FAFBD0DCB8B032FD1F767471 (void);
// 0x000000B7 System.Void E1_StunState::.ctor(Entity,FiniteStateMachine,System.String,D_StunState,Enemy1)
extern void E1_StunState__ctor_mC30FE26E39AABB7340970EC6F028C2C95C40B192 (void);
// 0x000000B8 System.Void E1_StunState::Enter()
extern void E1_StunState_Enter_mBB060690A2E35FF98ABB819C0DEFC4EB4C183EC1 (void);
// 0x000000B9 System.Void E1_StunState::Exit()
extern void E1_StunState_Exit_m2C9F8493CBB2B9903965A59819824420D52A14E7 (void);
// 0x000000BA System.Void E1_StunState::LogicUpdate()
extern void E1_StunState_LogicUpdate_m84649F8594E0BB6B98EE81A74F853328704F9537 (void);
// 0x000000BB System.Void E1_StunState::PhysicsUpdate()
extern void E1_StunState_PhysicsUpdate_mF3D9149BB9AD5E2D663A99ACEF3138D6101E2DDF (void);
// 0x000000BC System.Void E1_StunState::TickChecks()
extern void E1_StunState_TickChecks_m2F5F262124A83E621D365642B504FB6CAE5A80DC (void);
// 0x000000BD E1_IdleState Enemy1::get_idleState()
extern void Enemy1_get_idleState_mE0C9A3FBF7DA3E4A76E9F88DD28B8F6D68C164F9 (void);
// 0x000000BE System.Void Enemy1::set_idleState(E1_IdleState)
extern void Enemy1_set_idleState_m18DE75CFA65BB8D75A1CE5C6F067EA8F82A08AEB (void);
// 0x000000BF E1_MoveState Enemy1::get_moveState()
extern void Enemy1_get_moveState_m378E69A34987280E78335134C0E753866D32B3C5 (void);
// 0x000000C0 System.Void Enemy1::set_moveState(E1_MoveState)
extern void Enemy1_set_moveState_m29288ED1D140B62734EB80D8DA2A1FBA1B96025C (void);
// 0x000000C1 E1_PlayerDetected Enemy1::get_playerDetectedState()
extern void Enemy1_get_playerDetectedState_mED81BA996974A85CEB20BA5C6C25A3662A9443F6 (void);
// 0x000000C2 System.Void Enemy1::set_playerDetectedState(E1_PlayerDetected)
extern void Enemy1_set_playerDetectedState_mDA5C562D01251A12B0623AA3C989F22F70CA0A8A (void);
// 0x000000C3 E1_ChargeState Enemy1::get_chargeState()
extern void Enemy1_get_chargeState_m4A952E6209BEE246849C61042ED42EE67566AE42 (void);
// 0x000000C4 System.Void Enemy1::set_chargeState(E1_ChargeState)
extern void Enemy1_set_chargeState_mE48C8C21CDA0D75EDE2F3E30B22F2208A56ED7AF (void);
// 0x000000C5 E1_LookForPlayerState Enemy1::get_lookForPlayerState()
extern void Enemy1_get_lookForPlayerState_m0388DAFDA08636CEEFC8E0553D85180CC465E920 (void);
// 0x000000C6 System.Void Enemy1::set_lookForPlayerState(E1_LookForPlayerState)
extern void Enemy1_set_lookForPlayerState_m73386AE9DA9FBD54D7C9218C93B4BA3E5CB03C61 (void);
// 0x000000C7 E1_MeleeAttackState Enemy1::get_meleeAttackState()
extern void Enemy1_get_meleeAttackState_m84EB0BDA5196F46255AF1482F961FB4DB0014F60 (void);
// 0x000000C8 System.Void Enemy1::set_meleeAttackState(E1_MeleeAttackState)
extern void Enemy1_set_meleeAttackState_mD949D5E0AD1FCB6C9610446EF04C81A38CCD51ED (void);
// 0x000000C9 E1_StunState Enemy1::get_stunState()
extern void Enemy1_get_stunState_m130714A54AB28CF9BC83B0C597FBBB5F3045C3BE (void);
// 0x000000CA System.Void Enemy1::set_stunState(E1_StunState)
extern void Enemy1_set_stunState_mFCBD825284AA6FA2F9812845B145B4605982EDC0 (void);
// 0x000000CB E1_DeadState Enemy1::get_deadState()
extern void Enemy1_get_deadState_mDB4C05B960822A6529E2F384060F89D65E57E7B7 (void);
// 0x000000CC System.Void Enemy1::set_deadState(E1_DeadState)
extern void Enemy1_set_deadState_mF5E6748887C6E11FAAF69973A17AD0624C707C33 (void);
// 0x000000CD System.Void Enemy1::Awake()
extern void Enemy1_Awake_mDACED125FF233DF293A45E9AF53FF61C093E696E (void);
// 0x000000CE System.Void Enemy1::Start()
extern void Enemy1_Start_mC33823548F376AEA81FC577AF8FC98BE8D8DA560 (void);
// 0x000000CF System.Void Enemy1::OnDrawGizmos()
extern void Enemy1_OnDrawGizmos_m5392E09EB5950786D62B632D068EE7C277A3AD40 (void);
// 0x000000D0 System.Void Enemy1::.ctor()
extern void Enemy1__ctor_mFB31C5E5C0D17B1858EC762823BE12C6A3D8D9C1 (void);
// 0x000000D1 System.Void E2_DeadState::.ctor(Entity,FiniteStateMachine,System.String,D_DeadState,Enemy2)
extern void E2_DeadState__ctor_m0BA442CB37665A9E09E1F35306FD240D9FCEB899 (void);
// 0x000000D2 System.Void E2_DeadState::Enter()
extern void E2_DeadState_Enter_mE86EEFC6F5EAC431448C52DD5F9C0B75E121407B (void);
// 0x000000D3 System.Void E2_DeadState::Exit()
extern void E2_DeadState_Exit_mDDB7662712390FC7671C6BF3DFE2220813D6186C (void);
// 0x000000D4 System.Void E2_DeadState::LogicUpdate()
extern void E2_DeadState_LogicUpdate_m98D4725EAB9EF5C38413FFE85FF3B1D53FC0102B (void);
// 0x000000D5 System.Void E2_DeadState::PhysicsUpdate()
extern void E2_DeadState_PhysicsUpdate_m608D6880A5DFBCC6A536E354B7AEB94D81C91120 (void);
// 0x000000D6 System.Void E2_DeadState::TickChecks()
extern void E2_DeadState_TickChecks_m11E300E1A8C0E6F5168C1336CDBCDFA4087E6BBF (void);
// 0x000000D7 System.Void E2_DeadState::Start()
extern void E2_DeadState_Start_m1604127BA33253D8A31CC49B6F6D1531B801A990 (void);
// 0x000000D8 System.Void E2_DeadState::Update()
extern void E2_DeadState_Update_m1B1BFFB5B1B8A6306233C98B678FA844FE096B30 (void);
// 0x000000D9 System.Void E2_DodgeState::.ctor(Entity,FiniteStateMachine,System.String,D_DodgeState,Enemy2)
extern void E2_DodgeState__ctor_m4D928DC8CA48A205720147FC268E4885085BB7B4 (void);
// 0x000000DA System.Void E2_DodgeState::TickChecks()
extern void E2_DodgeState_TickChecks_m716F4512FD8DA77BA0704357EAFC5D9C2364B4C0 (void);
// 0x000000DB System.Void E2_DodgeState::Enter()
extern void E2_DodgeState_Enter_m9CE0A951BABAACB24EDD36E0B297C87D0A03F21C (void);
// 0x000000DC System.Void E2_DodgeState::Exit()
extern void E2_DodgeState_Exit_mD965F4E1BA9F47B072DEBCDC4B2FB075E70A73B6 (void);
// 0x000000DD System.Void E2_DodgeState::LogicUpdate()
extern void E2_DodgeState_LogicUpdate_mC1CAB9D385F83E440247412ABAC118086EAC0D2E (void);
// 0x000000DE System.Void E2_DodgeState::PhysicsUpdate()
extern void E2_DodgeState_PhysicsUpdate_mD08C0564FF013980F97458C5F80402509DEC556A (void);
// 0x000000DF System.Void E2_IdleState::.ctor(Entity,FiniteStateMachine,System.String,D_IdleState,Enemy2)
extern void E2_IdleState__ctor_mFC62D4B99C9CCBD5D622872FD14776D5E49DEFA3 (void);
// 0x000000E0 System.Void E2_IdleState::Enter()
extern void E2_IdleState_Enter_m44128D15FC51FF0A4FBFE421546ED2EB2477DA99 (void);
// 0x000000E1 System.Void E2_IdleState::Exit()
extern void E2_IdleState_Exit_m1C30375B3E588E7CF819FB49ED10FDD56D345517 (void);
// 0x000000E2 System.Void E2_IdleState::LogicUpdate()
extern void E2_IdleState_LogicUpdate_m0CBD53C636E58C5A87265BD474561E3E9F7978BB (void);
// 0x000000E3 System.Void E2_IdleState::PhysicsUpdate()
extern void E2_IdleState_PhysicsUpdate_m469740E12DC674880EBD491BB788E561D7B706E8 (void);
// 0x000000E4 System.Void E2_IdleState::TickChecks()
extern void E2_IdleState_TickChecks_m633D7E4D121A028D48146ECA2D28380E5F5BC8B0 (void);
// 0x000000E5 System.Void E2_IdleState::Start()
extern void E2_IdleState_Start_mC9BC1203FFE5F725B98C20454D725CA791897E9F (void);
// 0x000000E6 System.Void E2_IdleState::Update()
extern void E2_IdleState_Update_m35F98A1EBC138B72DD7D3C0F593DC9C66CFD7254 (void);
// 0x000000E7 System.Void E2_LookForPlayerState::.ctor(Entity,FiniteStateMachine,System.String,D_LookForPlayerState,Enemy2)
extern void E2_LookForPlayerState__ctor_m45A291A2878AD38805B6065BC1B0BB99339BF726 (void);
// 0x000000E8 System.Void E2_LookForPlayerState::Enter()
extern void E2_LookForPlayerState_Enter_m6F09DF963F6BAF837B8A5B325D6A9CCAF86DE7A4 (void);
// 0x000000E9 System.Void E2_LookForPlayerState::Exit()
extern void E2_LookForPlayerState_Exit_m4CDE453F9D980F45688F86A47059DD9C270CD82E (void);
// 0x000000EA System.Void E2_LookForPlayerState::LogicUpdate()
extern void E2_LookForPlayerState_LogicUpdate_m279D71C42890C6ECD720D194AD5A3A4146CB6837 (void);
// 0x000000EB System.Void E2_LookForPlayerState::PhysicsUpdate()
extern void E2_LookForPlayerState_PhysicsUpdate_m836F5D1A806B377ED4D6EC995A0A5797A95003C9 (void);
// 0x000000EC System.Void E2_LookForPlayerState::TickChecks()
extern void E2_LookForPlayerState_TickChecks_m4A312CAF2964E29A4C7BA2C036C55AEC68FBA68A (void);
// 0x000000ED System.Void E2_MeleeAttackState::.ctor(Entity,FiniteStateMachine,System.String,UnityEngine.Transform,D_MeleeAttackState,Enemy2)
extern void E2_MeleeAttackState__ctor_m1FB6BFDEB7A5440EAE03E55CFE0C1AB71E79AD91 (void);
// 0x000000EE System.Void E2_MeleeAttackState::Enter()
extern void E2_MeleeAttackState_Enter_mABE1BEE4F981B5220338B11C623A241C2570FBC0 (void);
// 0x000000EF System.Void E2_MeleeAttackState::Exit()
extern void E2_MeleeAttackState_Exit_mFC34DB6E8E86760D2B244D6775AB6FDA2A1CA7C6 (void);
// 0x000000F0 System.Void E2_MeleeAttackState::FinishAttack()
extern void E2_MeleeAttackState_FinishAttack_mC3C45F6E620504546FC313FC836FCCE91D4E8B0F (void);
// 0x000000F1 System.Void E2_MeleeAttackState::LogicUpdate()
extern void E2_MeleeAttackState_LogicUpdate_mFF7AB3FAB1CFB7D8858BFCE467CF24DBE1AF420A (void);
// 0x000000F2 System.Void E2_MeleeAttackState::PhysicsUpdate()
extern void E2_MeleeAttackState_PhysicsUpdate_mF8F3148B74F04B9A9388824AEF1FD9F8DD08935F (void);
// 0x000000F3 System.Void E2_MeleeAttackState::TickChecks()
extern void E2_MeleeAttackState_TickChecks_m6C4A357C7A773C965ADFC07DEB7AE999EB0EC3E8 (void);
// 0x000000F4 System.Void E2_MeleeAttackState::TriggerAttack()
extern void E2_MeleeAttackState_TriggerAttack_mA7559018CC3D68BA86CE277EEB3DC5B1790C08D0 (void);
// 0x000000F5 System.Void E2_MeleeAttackState::Start()
extern void E2_MeleeAttackState_Start_m0A012101F0369FE75DFAC19E021A3F81D67A7C29 (void);
// 0x000000F6 System.Void E2_MeleeAttackState::Update()
extern void E2_MeleeAttackState_Update_m0680E46D870E401C9597439EF3D51EA8EF061AD7 (void);
// 0x000000F7 System.Void E2_MoveState::.ctor(Entity,FiniteStateMachine,System.String,D_MoveState,Enemy2)
extern void E2_MoveState__ctor_m99B828B30BFEDE41C9B9F551DC2A281B0C9E97D6 (void);
// 0x000000F8 System.Void E2_MoveState::Enter()
extern void E2_MoveState_Enter_m5937B152975597427948A0E70EFEC3CC5769C825 (void);
// 0x000000F9 System.Void E2_MoveState::Exit()
extern void E2_MoveState_Exit_m2966266203EC5F4BCCF00D21F43B351B6AA2D90A (void);
// 0x000000FA System.Void E2_MoveState::LogicUpdate()
extern void E2_MoveState_LogicUpdate_m5F2274504D4F0F6790453486F69D6C763C1B7EA6 (void);
// 0x000000FB System.Void E2_MoveState::PhysicsUpdate()
extern void E2_MoveState_PhysicsUpdate_mCB818A2F87D5F498A6DF64CA36E211DA65E202E5 (void);
// 0x000000FC System.Void E2_MoveState::TickChecks()
extern void E2_MoveState_TickChecks_m6F3CE6E0F43043E0247995A7568E92875067F761 (void);
// 0x000000FD System.Void E2_PlayerDetectedState::.ctor(Entity,FiniteStateMachine,System.String,D_PlayerDetected,Enemy2)
extern void E2_PlayerDetectedState__ctor_m52E6FE64491A3FC762A3F18F14BB7435555B5B50 (void);
// 0x000000FE System.Void E2_PlayerDetectedState::Enter()
extern void E2_PlayerDetectedState_Enter_mB82DE29C451A6521E8289FB2AE2793003FFAA5BC (void);
// 0x000000FF System.Void E2_PlayerDetectedState::Exit()
extern void E2_PlayerDetectedState_Exit_m029897FB66BFF886451803B5B89EDB038F30D3D3 (void);
// 0x00000100 System.Void E2_PlayerDetectedState::LogicUpdate()
extern void E2_PlayerDetectedState_LogicUpdate_m75D2236A1A44535BD8D213E13F0D74F993B0D1AF (void);
// 0x00000101 System.Void E2_PlayerDetectedState::PhysicsUpdate()
extern void E2_PlayerDetectedState_PhysicsUpdate_mA17B36074B3842683ECB8DD338007271717CF67B (void);
// 0x00000102 System.Void E2_PlayerDetectedState::TickChecks()
extern void E2_PlayerDetectedState_TickChecks_m81DD6CE340B1CA82D0613D84520E9003773ACA1A (void);
// 0x00000103 System.Void E2_RangedAttackState::.ctor(Entity,FiniteStateMachine,System.String,UnityEngine.Transform,D_RangedAttackState,Enemy2)
extern void E2_RangedAttackState__ctor_mD40D1C163FA4DBBB493D7EA8F5779CEEEFADEA6F (void);
// 0x00000104 System.Void E2_RangedAttackState::TickChecks()
extern void E2_RangedAttackState_TickChecks_m4954BBAC3E20DA7B9F84829ED7A6908450C82E8F (void);
// 0x00000105 System.Void E2_RangedAttackState::Enter()
extern void E2_RangedAttackState_Enter_m3B8846B0C592378B8ED157ED3D841F9A01347DF2 (void);
// 0x00000106 System.Void E2_RangedAttackState::Exit()
extern void E2_RangedAttackState_Exit_m76C117F47B2625D7DADCFD5CC8B475E21CE4C271 (void);
// 0x00000107 System.Void E2_RangedAttackState::LogicUpdate()
extern void E2_RangedAttackState_LogicUpdate_mC9819566903D20AEC6CB83D5CC04827A501970AE (void);
// 0x00000108 System.Void E2_RangedAttackState::PhysicsUpdate()
extern void E2_RangedAttackState_PhysicsUpdate_m8094C4B0FBD1CDB0857655F281CF3E00CB2F5329 (void);
// 0x00000109 System.Void E2_RangedAttackState::TriggerAttack()
extern void E2_RangedAttackState_TriggerAttack_mE3C271D295227332EF813959FE5B0569D373C845 (void);
// 0x0000010A System.Void E2_RangedAttackState::FinishAttack()
extern void E2_RangedAttackState_FinishAttack_m096528D4D8F0F8CBF1BFEF70CF1DB174CC1FCAA2 (void);
// 0x0000010B System.Void E2_StunState::.ctor(Entity,FiniteStateMachine,System.String,D_StunState,Enemy2)
extern void E2_StunState__ctor_mAF638866EF94EB08A137B23F1C032972A495725A (void);
// 0x0000010C System.Void E2_StunState::Enter()
extern void E2_StunState_Enter_mC3F9AB73D9056B37ACABF15E7CF146949E8685CE (void);
// 0x0000010D System.Void E2_StunState::Exit()
extern void E2_StunState_Exit_m2EBBDA512F91729046CE4BB292CDF6607E387BD0 (void);
// 0x0000010E System.Void E2_StunState::LogicUpdate()
extern void E2_StunState_LogicUpdate_mFF1430FA661E793EDDF9A85F24B05A002186BB7B (void);
// 0x0000010F System.Void E2_StunState::PhysicsUpdate()
extern void E2_StunState_PhysicsUpdate_mF5941BD1E2A0E1E74500609B539F13A132BB2D11 (void);
// 0x00000110 System.Void E2_StunState::TickChecks()
extern void E2_StunState_TickChecks_mFC9B8319000C71C9FB16CD60DC43EAD20938B83F (void);
// 0x00000111 E2_IdleState Enemy2::get_idleState()
extern void Enemy2_get_idleState_m9609E996C07D801CDD7D0469BF5E99CEAD058B6C (void);
// 0x00000112 System.Void Enemy2::set_idleState(E2_IdleState)
extern void Enemy2_set_idleState_mC4A62000D63AC6F8139AE307F33EDE046057F043 (void);
// 0x00000113 E2_MoveState Enemy2::get_moveState()
extern void Enemy2_get_moveState_mA57347F419E8CB781BF1303F2290C63436E117BD (void);
// 0x00000114 System.Void Enemy2::set_moveState(E2_MoveState)
extern void Enemy2_set_moveState_mFE81C10785208323812269CA5D1A5C86FD1AA06C (void);
// 0x00000115 E2_PlayerDetectedState Enemy2::get_playerDetectedState()
extern void Enemy2_get_playerDetectedState_mC4E3C395BE954EB1D23B1ED7602FF84C32ED8F7F (void);
// 0x00000116 System.Void Enemy2::set_playerDetectedState(E2_PlayerDetectedState)
extern void Enemy2_set_playerDetectedState_mD99028EB06200A5D7BC949C00AFB8EB2BFF128B7 (void);
// 0x00000117 E2_LookForPlayerState Enemy2::get_lookForPlayerState()
extern void Enemy2_get_lookForPlayerState_mABE40EBEE735BF8C64F1D4CC615CB2D6B59ED3AF (void);
// 0x00000118 System.Void Enemy2::set_lookForPlayerState(E2_LookForPlayerState)
extern void Enemy2_set_lookForPlayerState_m010D59E6FEBF2E6EB46E19919D1C6B0A131F0499 (void);
// 0x00000119 E2_MeleeAttackState Enemy2::get_meleeAttackState()
extern void Enemy2_get_meleeAttackState_m7E4A61D255A594969A6374B9172CCAD1C07C23E0 (void);
// 0x0000011A System.Void Enemy2::set_meleeAttackState(E2_MeleeAttackState)
extern void Enemy2_set_meleeAttackState_m99B79E1DBD6F0AB6D226A83306368644838EC984 (void);
// 0x0000011B E2_StunState Enemy2::get_stunState()
extern void Enemy2_get_stunState_m3897E66DCD78E4312ECB87D9A4E620F4DF920593 (void);
// 0x0000011C System.Void Enemy2::set_stunState(E2_StunState)
extern void Enemy2_set_stunState_mB9C3EAB3B3E3D1240E2D1DC88E59D45B1032D647 (void);
// 0x0000011D E2_DeadState Enemy2::get_deadState()
extern void Enemy2_get_deadState_m85343F6B761082102679F7B8995AD96E2D1B1210 (void);
// 0x0000011E System.Void Enemy2::set_deadState(E2_DeadState)
extern void Enemy2_set_deadState_m85C483452621928C43988A012F38E4B4928BBA7D (void);
// 0x0000011F E2_DodgeState Enemy2::get_dodgeState()
extern void Enemy2_get_dodgeState_mE957B3CE04D174C5EF7436919DD7BC0DC8CD729E (void);
// 0x00000120 System.Void Enemy2::set_dodgeState(E2_DodgeState)
extern void Enemy2_set_dodgeState_m02A6D7A58D9109A52429A8003BE27F24DC6A40A7 (void);
// 0x00000121 E2_RangedAttackState Enemy2::get_rangedAttackState()
extern void Enemy2_get_rangedAttackState_m4375769A9D7BFD8D3D1C9A774B80D031F0FC6FEE (void);
// 0x00000122 System.Void Enemy2::set_rangedAttackState(E2_RangedAttackState)
extern void Enemy2_set_rangedAttackState_m10721EDFD84671B13A333504E2DB920CB0150812 (void);
// 0x00000123 System.Void Enemy2::Awake()
extern void Enemy2_Awake_m63BDF0341F17A7BC738DB10875D5F8C9F0E6F44B (void);
// 0x00000124 System.Void Enemy2::Start()
extern void Enemy2_Start_mECCDD202929D74E082F74026156890AE09E7F535 (void);
// 0x00000125 System.Void Enemy2::OnDrawGizmos()
extern void Enemy2_OnDrawGizmos_mB02E8ECD7D636E093158C557FCE67BD48AF46D32 (void);
// 0x00000126 System.Void Enemy2::.ctor()
extern void Enemy2__ctor_mEF60A06C0696069D82DB6E0A9999741499409559 (void);
// 0x00000127 System.Void E3_ChargeState::.ctor(Entity,FiniteStateMachine,System.String,D_ChargeState,Enemy3)
extern void E3_ChargeState__ctor_m2BD4D11D39B951A2451DA0D3FC0CED18DB804298 (void);
// 0x00000128 System.Void E3_ChargeState::LogicUpdate()
extern void E3_ChargeState_LogicUpdate_mE141EEE5C2B6014EF7E53B466916E5EC1EAD7C52 (void);
// 0x00000129 System.Void E3_DeadState::.ctor(Entity,FiniteStateMachine,System.String,D_DeadState,Enemy3)
extern void E3_DeadState__ctor_m0B9F5EEB570582A6541A802C1DB3069A1A2D8566 (void);
// 0x0000012A System.Void E3_DeadState::Start()
extern void E3_DeadState_Start_m263E555287E4C354A41FCF2D53F14EAA41DE34EA (void);
// 0x0000012B System.Void E3_DeadState::Update()
extern void E3_DeadState_Update_mFA4CE7643C3650EE7766E9872CCDD0AB5B862A18 (void);
// 0x0000012C System.Void E3_EmbushState::.ctor(Entity,FiniteStateMachine,System.String,D_EmbushState,Enemy3)
extern void E3_EmbushState__ctor_m3EA6D16E186542593ADEF710F075375D1D3918F8 (void);
// 0x0000012D System.Void E3_EmbushState::Exit()
extern void E3_EmbushState_Exit_m8FF457FF95907A3BCB0899AAC3DB5050935E9C17 (void);
// 0x0000012E System.Void E3_EmbushState::LogicUpdate()
extern void E3_EmbushState_LogicUpdate_m70F7A932258A353657D77F4C03EF8ED15837CD66 (void);
// 0x0000012F System.Void E3_IdleState::.ctor(Entity,FiniteStateMachine,System.String,D_IdleState,Enemy3)
extern void E3_IdleState__ctor_m125E5EAD8CFFC462B8D5D1329B5AF1A2DC789D7A (void);
// 0x00000130 System.Void E3_IdleState::LogicUpdate()
extern void E3_IdleState_LogicUpdate_mEA09B43E74C23E0CD1598BBA9DB01761033E3498 (void);
// 0x00000131 System.Void E3_LookForPlayerState::.ctor(Entity,FiniteStateMachine,System.String,D_LookForPlayerState,Enemy3)
extern void E3_LookForPlayerState__ctor_mAF537EF5A4C50EECDA6B74859791A2CA0207CCFE (void);
// 0x00000132 System.Void E3_LookForPlayerState::LogicUpdate()
extern void E3_LookForPlayerState_LogicUpdate_mB067EA74592B81664F51D77DA65CA937B55ABF7F (void);
// 0x00000133 System.Void E3_MeleeAttackState::.ctor(Entity,FiniteStateMachine,System.String,UnityEngine.Transform,D_MeleeAttackState,Enemy3)
extern void E3_MeleeAttackState__ctor_m3463B9F8994BAE9551A0BAF3D19DC9422FE8DC65 (void);
// 0x00000134 System.Void E3_MeleeAttackState::Enter()
extern void E3_MeleeAttackState_Enter_m2CB946DC5A120A6262ABA46102C64A51FE489CC3 (void);
// 0x00000135 System.Void E3_MeleeAttackState::Exit()
extern void E3_MeleeAttackState_Exit_mAE38551EE8F673D6FB9C585EBF7265FD9EFBC45A (void);
// 0x00000136 System.Void E3_MeleeAttackState::FinishAttack()
extern void E3_MeleeAttackState_FinishAttack_m1CE01A874168D9E1295D725E214B1CEDCEF194E3 (void);
// 0x00000137 System.Void E3_MeleeAttackState::LogicUpdate()
extern void E3_MeleeAttackState_LogicUpdate_mEA4E05BCD6DEE6F47316C2F6E85BB6BA4288505B (void);
// 0x00000138 System.Void E3_MeleeAttackState::PhysicsUpdate()
extern void E3_MeleeAttackState_PhysicsUpdate_m904AB6B8C82AAACD3785E707AE02C6E91D7CF0EA (void);
// 0x00000139 System.Void E3_MeleeAttackState::TickChecks()
extern void E3_MeleeAttackState_TickChecks_m87749F2218A25F953CDD17459A2A09A1FD071559 (void);
// 0x0000013A System.Void E3_MeleeAttackState::TriggerAttack()
extern void E3_MeleeAttackState_TriggerAttack_m167B4E882D3DE9209A38ACD960B2CF664974DF4D (void);
// 0x0000013B System.Void E3_MoveState::.ctor(Entity,FiniteStateMachine,System.String,D_MoveState,Enemy3)
extern void E3_MoveState__ctor_m775E5380FA5F09EF72B9EB394DCABB2B013BEA41 (void);
// 0x0000013C System.Void E3_MoveState::Enter()
extern void E3_MoveState_Enter_m3E74948E5333E2611F06C4C83164A47740F10EFF (void);
// 0x0000013D System.Void E3_MoveState::Exit()
extern void E3_MoveState_Exit_mE0006CFDA010B3BA0B646E6E94322EBD227D3774 (void);
// 0x0000013E System.Void E3_MoveState::LogicUpdate()
extern void E3_MoveState_LogicUpdate_mD70B8758F2CAE8B23826B8D7659FC7E7E9E0E181 (void);
// 0x0000013F System.Void E3_MoveState::PhysicsUpdate()
extern void E3_MoveState_PhysicsUpdate_mE556DFAC78B891A542ACBFF610A271489D7473B7 (void);
// 0x00000140 System.Void E3_PlayerDetectedState::.ctor(Entity,FiniteStateMachine,System.String,D_PlayerDetected,Enemy3)
extern void E3_PlayerDetectedState__ctor_mCBD54445F4094282C462BA93D3A225AAEACC92CF (void);
// 0x00000141 System.Void E3_PlayerDetectedState::LogicUpdate()
extern void E3_PlayerDetectedState_LogicUpdate_m0ACA0EDC6435C70687D2E001F5E7E12A5F5600BA (void);
// 0x00000142 System.Void E3_RegenState::.ctor(Entity,FiniteStateMachine,System.String,D_RegenState,Enemy3)
extern void E3_RegenState__ctor_m28CF2EF1E89D2DD0952D5EC034C53302DAE5AD17 (void);
// 0x00000143 System.Void E3_RegenState::Enter()
extern void E3_RegenState_Enter_mAC4DB11E4D15374513C087CFB0954B4E270F8D80 (void);
// 0x00000144 System.Void E3_RegenState::Exit()
extern void E3_RegenState_Exit_m790679678503B121787F03F3A325B92F9F7742DA (void);
// 0x00000145 System.Void E3_RegenState::LogicUpdate()
extern void E3_RegenState_LogicUpdate_mF96988C9935D948852E0D488D7BE5B432DDF00EC (void);
// 0x00000146 E3_IdleState Enemy3::get_idleState()
extern void Enemy3_get_idleState_mBA892BA14384A046B8EAD644B406E65B878DE8AB (void);
// 0x00000147 System.Void Enemy3::set_idleState(E3_IdleState)
extern void Enemy3_set_idleState_m2CFEBE780838E96CD7FFF76C995B95863C0DC240 (void);
// 0x00000148 E3_MoveState Enemy3::get_moveState()
extern void Enemy3_get_moveState_m489FFEEAAED821BAD17EC70EC372C25BBBABE58E (void);
// 0x00000149 System.Void Enemy3::set_moveState(E3_MoveState)
extern void Enemy3_set_moveState_mE9AA56220A5C984111C061C9347606D8339E8370 (void);
// 0x0000014A E3_PlayerDetectedState Enemy3::get_playerDetectedState()
extern void Enemy3_get_playerDetectedState_m30A8E36B2EF25D36BA8DA3D1C586DF0C6A82686F (void);
// 0x0000014B System.Void Enemy3::set_playerDetectedState(E3_PlayerDetectedState)
extern void Enemy3_set_playerDetectedState_m0AB6466315E904B35409A52C86CB789750BFC33E (void);
// 0x0000014C E3_ChargeState Enemy3::get_chargeState()
extern void Enemy3_get_chargeState_m8F05F6A0481B8D78481BD0E156A8357B16F81074 (void);
// 0x0000014D System.Void Enemy3::set_chargeState(E3_ChargeState)
extern void Enemy3_set_chargeState_m4D0DE4E63158147B7A323B90B7BF26EA0C39F021 (void);
// 0x0000014E E3_LookForPlayerState Enemy3::get_lookForPlayerState()
extern void Enemy3_get_lookForPlayerState_mBA9F4B30B5C4308D7791835BC74385AC4B87FA56 (void);
// 0x0000014F System.Void Enemy3::set_lookForPlayerState(E3_LookForPlayerState)
extern void Enemy3_set_lookForPlayerState_mF7BBB772A0AB03D0BC3059EA9D411B606EAB70BD (void);
// 0x00000150 E3_MeleeAttackState Enemy3::get_meleeAttackState()
extern void Enemy3_get_meleeAttackState_m0E082BA02FF5BE06E25C574B4D6040EC7DD90BD7 (void);
// 0x00000151 System.Void Enemy3::set_meleeAttackState(E3_MeleeAttackState)
extern void Enemy3_set_meleeAttackState_mCB2C08DD469FEFE5A1B567DBEBE9006966ADD2DE (void);
// 0x00000152 E3_EmbushState Enemy3::get_embushState()
extern void Enemy3_get_embushState_m3B1A27B24B56108736EB5E05E6122406BFC2B999 (void);
// 0x00000153 System.Void Enemy3::set_embushState(E3_EmbushState)
extern void Enemy3_set_embushState_m59B172DBDE3F6CB0B77091B0C1574A9A258BEF51 (void);
// 0x00000154 E3_DeadState Enemy3::get_deadState()
extern void Enemy3_get_deadState_m9A8EF3442D53715D87861084DB0B5776C2955D46 (void);
// 0x00000155 System.Void Enemy3::set_deadState(E3_DeadState)
extern void Enemy3_set_deadState_m4751FBB980207FA3AD0A50749ACE67E9B05D9B48 (void);
// 0x00000156 E3_RegenState Enemy3::get_regenState()
extern void Enemy3_get_regenState_m89F174EC57862C656BFB20F4639FC943F5E03E3C (void);
// 0x00000157 System.Void Enemy3::set_regenState(E3_RegenState)
extern void Enemy3_set_regenState_m2CC3D4610767392F403A0624B8087CFD164FD77C (void);
// 0x00000158 System.Void Enemy3::Awake()
extern void Enemy3_Awake_m3918D7BA0B05354206EE45F409A415E1E8525877 (void);
// 0x00000159 System.Void Enemy3::Start()
extern void Enemy3_Start_mC93658EADC64D22D16190C89686705CC66B2FFFB (void);
// 0x0000015A System.Void Enemy3::OnDrawGizmos()
extern void Enemy3_OnDrawGizmos_m0669995A51E173EBE6D9D4B766EF738590B50C84 (void);
// 0x0000015B System.Void Enemy3::.ctor()
extern void Enemy3__ctor_mD174D0F35ACA96431DF534ACF81753FA74065208 (void);
// 0x0000015C System.Void E4_DeadState::.ctor(Entity,FiniteStateMachine,System.String,D_DeadState,Enemy4)
extern void E4_DeadState__ctor_m65752F23B067113C3E08E3FFE04EEFDD1A5B923E (void);
// 0x0000015D System.Void E4_DeadState::Enter()
extern void E4_DeadState_Enter_m91126292DBF85E6ED6EC3692D8BA1E423E11FE9F (void);
// 0x0000015E System.Void E4_DeadState::Exit()
extern void E4_DeadState_Exit_m9664B0D4FCE824A4CE31A3B73E132F802106E6B7 (void);
// 0x0000015F System.Void E4_DeadState::LogicUpdate()
extern void E4_DeadState_LogicUpdate_mC3FA3437A12E0F933068A5719632996D965E9A00 (void);
// 0x00000160 System.Void E4_DeadState::PhysicsUpdate()
extern void E4_DeadState_PhysicsUpdate_mD3B41B3BE788920116A79A66FC29EC6D121BC101 (void);
// 0x00000161 System.Void E4_DeadState::TickChecks()
extern void E4_DeadState_TickChecks_m128A7055479CF3138940A032D0C93FB4D071C44B (void);
// 0x00000162 System.Void E4_DodgeState::.ctor(Entity,FiniteStateMachine,System.String,D_DodgeState,Enemy4)
extern void E4_DodgeState__ctor_mC0E051BCDB7F13E9374A10C5FE4A5B2F12F7E515 (void);
// 0x00000163 System.Void E4_DodgeState::Enter()
extern void E4_DodgeState_Enter_m8CF96A74AC34B4F482212A0E00E1362EEA9BECA4 (void);
// 0x00000164 System.Void E4_DodgeState::Exit()
extern void E4_DodgeState_Exit_m1A52B7189C08FC0DC14C11A55091600A95BC6885 (void);
// 0x00000165 System.Void E4_DodgeState::LogicUpdate()
extern void E4_DodgeState_LogicUpdate_m7C371F709DFEE7835B19771E936EB418A97DB3EF (void);
// 0x00000166 System.Void E4_DodgeState::PhysicsUpdate()
extern void E4_DodgeState_PhysicsUpdate_mA343599BA0B3E2226AAA0F852FAF4A5AF28B853E (void);
// 0x00000167 System.Void E4_DodgeState::TickChecks()
extern void E4_DodgeState_TickChecks_mDB0ACE9D31A7CE935CE28A4C6C76856A7384BFB8 (void);
// 0x00000168 System.Void E4_IdleState::.ctor(Entity,FiniteStateMachine,System.String,D_IdleState,Enemy4)
extern void E4_IdleState__ctor_m5E81E682449566034E96D99789A14B2D0C52C1B3 (void);
// 0x00000169 System.Void E4_IdleState::Enter()
extern void E4_IdleState_Enter_mA850C9E4455EDC86DE7E8ECB6B99524B67970BD2 (void);
// 0x0000016A System.Void E4_IdleState::Exit()
extern void E4_IdleState_Exit_m35138F31E088561FA924AA0589AAB37E50C8EFDD (void);
// 0x0000016B System.Void E4_IdleState::LogicUpdate()
extern void E4_IdleState_LogicUpdate_m567CFDB7C55C4F848EBA3115FE976F243A939AEB (void);
// 0x0000016C System.Void E4_IdleState::PhysicsUpdate()
extern void E4_IdleState_PhysicsUpdate_m875BCBD787D02ED19A44F91B05CA34D692EEB612 (void);
// 0x0000016D System.Void E4_IdleState::TickChecks()
extern void E4_IdleState_TickChecks_mD22D70ED6762F21475DA32EEC3B705A99330AD66 (void);
// 0x0000016E System.Void E4_LookForPlayerState::.ctor(Entity,FiniteStateMachine,System.String,D_LookForPlayerState,Enemy4)
extern void E4_LookForPlayerState__ctor_m269DA8DD6B445548B9D4E5A4CF3B1DB943846132 (void);
// 0x0000016F System.Void E4_LookForPlayerState::Enter()
extern void E4_LookForPlayerState_Enter_m06C70F337A67935107C868B7B028B176B3BC1B56 (void);
// 0x00000170 System.Void E4_LookForPlayerState::Exit()
extern void E4_LookForPlayerState_Exit_m379928D0D69F58BC4F1F47ED70A3298F9B10B33C (void);
// 0x00000171 System.Void E4_LookForPlayerState::LogicUpdate()
extern void E4_LookForPlayerState_LogicUpdate_mACA066722DB0AA4E066E41A20570ECAB1943A5C1 (void);
// 0x00000172 System.Void E4_LookForPlayerState::PhysicsUpdate()
extern void E4_LookForPlayerState_PhysicsUpdate_m9E7F5B23473512FA7BAE2ECD2A1F7A48F6B7DEEC (void);
// 0x00000173 System.Void E4_LookForPlayerState::TickChecks()
extern void E4_LookForPlayerState_TickChecks_m2015EE1FD788A93B985D1F60A6CD508DC9FBF54C (void);
// 0x00000174 System.Void E4_MeleeAttackState::.ctor(Entity,FiniteStateMachine,System.String,UnityEngine.Transform,D_MeleeAttackState,Enemy4)
extern void E4_MeleeAttackState__ctor_mB5B29F30323E834CCDADE4A5B8DCEC0FC063E91D (void);
// 0x00000175 System.Void E4_MeleeAttackState::Enter()
extern void E4_MeleeAttackState_Enter_mFF00928DE6B168C56DDF3F700E91C3E4EAD5E973 (void);
// 0x00000176 System.Void E4_MeleeAttackState::Exit()
extern void E4_MeleeAttackState_Exit_m49073ADD2C1CD5294AA0823764DB920D4C76AED6 (void);
// 0x00000177 System.Void E4_MeleeAttackState::FinishAttack()
extern void E4_MeleeAttackState_FinishAttack_m226B61744921BC64170B36FB24EEA178210040F4 (void);
// 0x00000178 System.Void E4_MeleeAttackState::LogicUpdate()
extern void E4_MeleeAttackState_LogicUpdate_m71690018794E9828FDCE9BF6A159AAB50BD916FD (void);
// 0x00000179 System.Void E4_MeleeAttackState::PhysicsUpdate()
extern void E4_MeleeAttackState_PhysicsUpdate_mDE7C2E2FBF57E6E376D96147D4E7713114E4F061 (void);
// 0x0000017A System.Void E4_MeleeAttackState::TickChecks()
extern void E4_MeleeAttackState_TickChecks_mBCA15AE37F144C7BEAE967EF02F88877041D990E (void);
// 0x0000017B System.Void E4_MeleeAttackState::TriggerAttack()
extern void E4_MeleeAttackState_TriggerAttack_m78D8FF0886AF7ABF060D9DAF9B10C24A0E5C2F17 (void);
// 0x0000017C System.Void E4_MoveState::.ctor(Entity,FiniteStateMachine,System.String,D_MoveState,Enemy4)
extern void E4_MoveState__ctor_m9D62EB852E5AC79353059A466CACAFC2301F432F (void);
// 0x0000017D System.Void E4_MoveState::Enter()
extern void E4_MoveState_Enter_m36EADB2633F4488C2C82A3D3FA58E52AE8B00B95 (void);
// 0x0000017E System.Void E4_MoveState::Exit()
extern void E4_MoveState_Exit_m8F08BDF734C602C4B390311922F8ACFCD9B49260 (void);
// 0x0000017F System.Void E4_MoveState::LogicUpdate()
extern void E4_MoveState_LogicUpdate_m295EA3E0CBB014C9F2ED35A78A6DA939EE3BD58A (void);
// 0x00000180 System.Void E4_MoveState::PhysicsUpdate()
extern void E4_MoveState_PhysicsUpdate_mD50BA0F1C2E35E7576A5AA7F2C4E86E3C103614F (void);
// 0x00000181 System.Void E4_MoveState::TickChecks()
extern void E4_MoveState_TickChecks_m8E3BE61B62D3E4149900405BF7C4C4DD86D68BD1 (void);
// 0x00000182 System.Void E4_PlayerDetectedState::.ctor(Entity,FiniteStateMachine,System.String,D_PlayerDetected,Enemy4)
extern void E4_PlayerDetectedState__ctor_m2E575F8060CDB5178F0D731FD90F99FD2A456A9E (void);
// 0x00000183 System.Void E4_PlayerDetectedState::Enter()
extern void E4_PlayerDetectedState_Enter_m2A932A4C457170354D2F3B7BE93AAE689B447F5A (void);
// 0x00000184 System.Void E4_PlayerDetectedState::Exit()
extern void E4_PlayerDetectedState_Exit_m8396DE06529C226926FC6882C0304D2829E70530 (void);
// 0x00000185 System.Void E4_PlayerDetectedState::LogicUpdate()
extern void E4_PlayerDetectedState_LogicUpdate_m91547C9A614FBB8F80DF1C7AAC5FF2677BBED520 (void);
// 0x00000186 System.Void E4_PlayerDetectedState::PhysicsUpdate()
extern void E4_PlayerDetectedState_PhysicsUpdate_mA91D07E02E054E63D6EACEE7D6F36D980800ADA2 (void);
// 0x00000187 System.Void E4_PlayerDetectedState::TickChecks()
extern void E4_PlayerDetectedState_TickChecks_mEB6D2315DEA9D1C2BE78C63E0007D1878DF57B4A (void);
// 0x00000188 System.Void E4_RangedAttackState::.ctor(Entity,FiniteStateMachine,System.String,UnityEngine.Transform,D_RangedAttackState,Enemy4)
extern void E4_RangedAttackState__ctor_m1E5798BF40C3699F5D7432F9DE92420DBAB01121 (void);
// 0x00000189 System.Void E4_RangedAttackState::Enter()
extern void E4_RangedAttackState_Enter_mA97D1ED423C1480022CF0D14035BFBC39CC645DB (void);
// 0x0000018A System.Void E4_RangedAttackState::Exit()
extern void E4_RangedAttackState_Exit_mB99A84C188AF7F867FDA740AA7E87905C2080138 (void);
// 0x0000018B System.Void E4_RangedAttackState::FinishAttack()
extern void E4_RangedAttackState_FinishAttack_mE6C9F958D0D97122269E79B2A59E873C6DB285D7 (void);
// 0x0000018C System.Void E4_RangedAttackState::LogicUpdate()
extern void E4_RangedAttackState_LogicUpdate_m7799D19C9AF35235A85414A7E04948E5F710A28F (void);
// 0x0000018D System.Void E4_RangedAttackState::PhysicsUpdate()
extern void E4_RangedAttackState_PhysicsUpdate_mBFB736434A16ECD49A9FD88E0D0F98081C757D58 (void);
// 0x0000018E System.Void E4_RangedAttackState::TickChecks()
extern void E4_RangedAttackState_TickChecks_m5F531991A9E9A64D082DBDDFBEEE0025E198BDB7 (void);
// 0x0000018F System.Void E4_RangedAttackState::TriggerAttack()
extern void E4_RangedAttackState_TriggerAttack_m8F95627F3E8A148D5CAFB0C036539A85A9EA3338 (void);
// 0x00000190 E4_IdleState Enemy4::get_idleState()
extern void Enemy4_get_idleState_mEA127CCA7F311EDDC2354B16718DCF8160903A4D (void);
// 0x00000191 System.Void Enemy4::set_idleState(E4_IdleState)
extern void Enemy4_set_idleState_m7D23B1B973EA57FFCC3D39AD119A146E2BA80729 (void);
// 0x00000192 E4_MoveState Enemy4::get_moveState()
extern void Enemy4_get_moveState_m9DA4F359FB57E3B393EADB3B9B4293AC23BDE374 (void);
// 0x00000193 System.Void Enemy4::set_moveState(E4_MoveState)
extern void Enemy4_set_moveState_m634AF993CE5C7524147C0F13C3805681838EA02A (void);
// 0x00000194 E4_PlayerDetectedState Enemy4::get_playerDetectedState()
extern void Enemy4_get_playerDetectedState_mF8E0ADAB977232B11304B082A0970E6E5459A5DB (void);
// 0x00000195 System.Void Enemy4::set_playerDetectedState(E4_PlayerDetectedState)
extern void Enemy4_set_playerDetectedState_m1F060C49975799B2287CEDAA08E998C7A5CB7D09 (void);
// 0x00000196 E4_LookForPlayerState Enemy4::get_lookForPlayerState()
extern void Enemy4_get_lookForPlayerState_m97516991E5977210C5B40886EC21245622CF0CFD (void);
// 0x00000197 System.Void Enemy4::set_lookForPlayerState(E4_LookForPlayerState)
extern void Enemy4_set_lookForPlayerState_m551D22917375E48FD13361850E264661EF26EDC5 (void);
// 0x00000198 E4_MeleeAttackState Enemy4::get_meleeAttackState()
extern void Enemy4_get_meleeAttackState_m699DBBD4F7DEC7530565CAD05274FB3F041B9D53 (void);
// 0x00000199 System.Void Enemy4::set_meleeAttackState(E4_MeleeAttackState)
extern void Enemy4_set_meleeAttackState_mE63392F34524CC7051CC554C0E620CEE28EE0B4D (void);
// 0x0000019A E4_DeadState Enemy4::get_deadState()
extern void Enemy4_get_deadState_m834819D3DBD25F6BD813A6FDB0CC203C88E0423B (void);
// 0x0000019B System.Void Enemy4::set_deadState(E4_DeadState)
extern void Enemy4_set_deadState_m3E412779FF8D8FC143F6994850B31E1CA706A97F (void);
// 0x0000019C E4_DodgeState Enemy4::get_dodgeState()
extern void Enemy4_get_dodgeState_mC0AECFF38394E15590AE3C45C0B966B244504F5B (void);
// 0x0000019D System.Void Enemy4::set_dodgeState(E4_DodgeState)
extern void Enemy4_set_dodgeState_m44EC625B6106CCBCDD4B16BAE97A17FC232B0CA3 (void);
// 0x0000019E E4_RangedAttackState Enemy4::get_rangedAttackState()
extern void Enemy4_get_rangedAttackState_m5ABC78B51C930ACE717DB5D3F5570A78789B8452 (void);
// 0x0000019F System.Void Enemy4::set_rangedAttackState(E4_RangedAttackState)
extern void Enemy4_set_rangedAttackState_m1DC22E410FCB47990A196753452001C021630C88 (void);
// 0x000001A0 System.Void Enemy4::Awake()
extern void Enemy4_Awake_m797D25E7D6D2BF79CB03953FC2F36E62A10F67FA (void);
// 0x000001A1 System.Void Enemy4::Start()
extern void Enemy4_Start_m5DF1FC3EB8E01B4E1C8B11983C4A96D0CD2D3E2C (void);
// 0x000001A2 System.Void Enemy4::OnDrawGizmos()
extern void Enemy4_OnDrawGizmos_m933D5F7B9E98A98855ABCDC766D0AAB3BD7440D7 (void);
// 0x000001A3 System.Void Enemy4::.ctor()
extern void Enemy4__ctor_mA002C4E651A5331BB7938BC264B17B4D783D5DE3 (void);
// 0x000001A4 UnityEngine.Animator Entity::get_Animator()
extern void Entity_get_Animator_mE3A186D7422082C3AAF044F212E817B9F0D32134 (void);
// 0x000001A5 System.Void Entity::set_Animator(UnityEngine.Animator)
extern void Entity_set_Animator_m4C82F97F9F16843B78F608DBF3AADAB2BFFFB57A (void);
// 0x000001A6 AnimationToFSM Entity::get_Atfsm()
extern void Entity_get_Atfsm_mF891A5BF42EC5AD0848A01B619020AF82AC351F7 (void);
// 0x000001A7 System.Void Entity::set_Atfsm(AnimationToFSM)
extern void Entity_set_Atfsm_m51BFBE4D06B77B9D197D7EC748DA71CD120AC8F5 (void);
// 0x000001A8 System.Int32 Entity::get_LastDamageDirection()
extern void Entity_get_LastDamageDirection_mA726D86ED179AEBEA41920096FE4FBBEB7A90B24 (void);
// 0x000001A9 System.Void Entity::set_LastDamageDirection(System.Int32)
extern void Entity_set_LastDamageDirection_m3AF2A6CBC1DF07591AD80FA71210EDE736ABF2C1 (void);
// 0x000001AA Core Entity::get_Core()
extern void Entity_get_Core_m969A58FED6B929FF5D3CAAEA51098DB218D4B806 (void);
// 0x000001AB System.Void Entity::set_Core(Core)
extern void Entity_set_Core_mDCD8DEC2594D09500F6DDB1EB16543DB1BFE51F0 (void);
// 0x000001AC System.Void Entity::Awake()
extern void Entity_Awake_m53067BAC9D02222618330E6DC07F457399362C8A (void);
// 0x000001AD System.Void Entity::Update()
extern void Entity_Update_m4FAC0E7988D34B19805DBB519889B857020642CA (void);
// 0x000001AE System.Void Entity::FixedUpdate()
extern void Entity_FixedUpdate_m36DAEE494A40A2BA13A9C36FC9D5704D9FB8C5A6 (void);
// 0x000001AF System.Boolean Entity::CheckPlayerInMinimumRange()
extern void Entity_CheckPlayerInMinimumRange_mA89B6027827F219FFCF04AFABA82C27024DDB5BA (void);
// 0x000001B0 System.Boolean Entity::CheckPlayerInMaximumRange()
extern void Entity_CheckPlayerInMaximumRange_mB1D2C20904B5A31C9010C6B07DDBF77426F940A5 (void);
// 0x000001B1 System.Boolean Entity::CheckPlayerInCloseRangeAction()
extern void Entity_CheckPlayerInCloseRangeAction_m95802D3048DB0A25B78C041AA1B51D0F402F1A70 (void);
// 0x000001B2 System.Boolean Entity::CheckForPlayerToEmbush()
extern void Entity_CheckForPlayerToEmbush_m91C8873D88FDCAB13BDB9458A077B13F36835599 (void);
// 0x000001B3 System.Void Entity::ResetStunResistance()
extern void Entity_ResetStunResistance_m0E85D826B9E93D9A502D0E8E853ADC224CF28B23 (void);
// 0x000001B4 System.Void Entity::DamageHop(System.Single)
extern void Entity_DamageHop_m46336AA3D97F06D2E06AFC4228404D752D1C9C1A (void);
// 0x000001B5 System.Void Entity::OnDrawGizmos()
extern void Entity_OnDrawGizmos_m670BFCD0FBA0D4402A95C8CCEB88377375F2AB01 (void);
// 0x000001B6 System.Void Entity::.ctor()
extern void Entity__ctor_mA22D51D64B97E8F6F879E72C8E0DE7D26ED0C824 (void);
// 0x000001B7 State FiniteStateMachine::get_currentState()
extern void FiniteStateMachine_get_currentState_mCEC096E0E07CDE37B0FB5CAB0A18245E9FB6D35D (void);
// 0x000001B8 System.Void FiniteStateMachine::set_currentState(State)
extern void FiniteStateMachine_set_currentState_m40116BF9AD6CC400EEE4F17FE9183873A976746B (void);
// 0x000001B9 System.Void FiniteStateMachine::Initialize(State)
extern void FiniteStateMachine_Initialize_mF5425B5BF1AEDF9436FCBFBF95C165A2DDEA62F1 (void);
// 0x000001BA System.Void FiniteStateMachine::ChangeState(State)
extern void FiniteStateMachine_ChangeState_m6BA6695EDC31E88A2795C8F9B6D786CFEBAB5E48 (void);
// 0x000001BB System.Void FiniteStateMachine::.ctor()
extern void FiniteStateMachine__ctor_mF83459ECF9E991F57293D41F7B261EF11E8B9E41 (void);
// 0x000001BC System.Single State::get_startTime()
extern void State_get_startTime_m3881926F7D9F6AF6CE767EBC9E53CEC7D5FB1B53 (void);
// 0x000001BD System.Void State::set_startTime(System.Single)
extern void State_set_startTime_m75EF6DDE4B43E8D40F2584A1D05793AA1AE158F7 (void);
// 0x000001BE System.Void State::.ctor(Entity,FiniteStateMachine,System.String)
extern void State__ctor_m7922784812838F4A31FEE66D76C8DCAB41A960FE (void);
// 0x000001BF System.Void State::TickChecks()
extern void State_TickChecks_m6F8FDB299E3F93EB0061AB520C8F900124B6EE89 (void);
// 0x000001C0 System.Void State::Enter()
extern void State_Enter_m3433FED643AB93ED2C51B1510BDD19BEF7F5DD09 (void);
// 0x000001C1 System.Void State::Exit()
extern void State_Exit_m4DE1BF421094D707A14DFB8D154CE52C8097AB29 (void);
// 0x000001C2 System.Void State::LogicUpdate()
extern void State_LogicUpdate_m164478ECC97B11CFA59D33C9E156A430C9775916 (void);
// 0x000001C3 System.Void State::PhysicsUpdate()
extern void State_PhysicsUpdate_m0554F42942B71FA0EB8824397590EF99B884C16F (void);
// 0x000001C4 System.Void AttackState::.ctor(Entity,FiniteStateMachine,System.String,UnityEngine.Transform)
extern void AttackState__ctor_m3C44FDB406BAE32540299F13802909D33B51CA86 (void);
// 0x000001C5 System.Void AttackState::TickChecks()
extern void AttackState_TickChecks_mBA73A247F2767DBBEBF0D44FDFD69A078F13CDBD (void);
// 0x000001C6 System.Void AttackState::Enter()
extern void AttackState_Enter_m3506EDFCBBC2600F3DC6268174D7165F69353B4B (void);
// 0x000001C7 System.Void AttackState::Exit()
extern void AttackState_Exit_m2D73ECF95A8E8A1773B09DDEE9156E35F367F5EC (void);
// 0x000001C8 System.Void AttackState::LogicUpdate()
extern void AttackState_LogicUpdate_m32CC25C997780DAC7DB0FF94F7D012B6486EE611 (void);
// 0x000001C9 System.Void AttackState::PhysicsUpdate()
extern void AttackState_PhysicsUpdate_m0F4C8C9B751EB446A37F8F1950396AF52365D6C7 (void);
// 0x000001CA System.Void AttackState::TriggerAttack()
extern void AttackState_TriggerAttack_m6771D1C2CB90C977CAD302DE345861FBF928C228 (void);
// 0x000001CB System.Void AttackState::FinishAttack()
extern void AttackState_FinishAttack_m4A955DB10635495234A203B603B505371BF4B362 (void);
// 0x000001CC System.Void ChargeState::.ctor(Entity,FiniteStateMachine,System.String,D_ChargeState)
extern void ChargeState__ctor_m7B756F79B3AF698C7024C2AB978307A4FC639D1B (void);
// 0x000001CD System.Void ChargeState::Enter()
extern void ChargeState_Enter_m3F0DD13C47C6A10C91DEA35A8342905AB527FF85 (void);
// 0x000001CE System.Void ChargeState::Exit()
extern void ChargeState_Exit_m85DC1D2730E911F1D924DC641822917C65249D5B (void);
// 0x000001CF System.Void ChargeState::LogicUpdate()
extern void ChargeState_LogicUpdate_mF4E811B953305AF48832785EAFAE9D50814BCDBA (void);
// 0x000001D0 System.Void ChargeState::PhysicsUpdate()
extern void ChargeState_PhysicsUpdate_m9BF2FAA7E8BA4675CCA8BEDA55D244EEDB1356C3 (void);
// 0x000001D1 System.Void ChargeState::TickChecks()
extern void ChargeState_TickChecks_mFF794AC08563F78DE708FE34D468A9ACE5B15999 (void);
// 0x000001D2 System.Void D_ChargeState::.ctor()
extern void D_ChargeState__ctor_m49103D516126F9CAAC625E7227CC8BB3A9A7DEE5 (void);
// 0x000001D3 System.Void D_DeadState::.ctor()
extern void D_DeadState__ctor_m1BC99FAD4D2FF11569BAB68C0FD54E20BFD203E2 (void);
// 0x000001D4 System.Void D_DodgeState::.ctor()
extern void D_DodgeState__ctor_m60A8C18DCEE68419881C1605C45AB474B943CA32 (void);
// 0x000001D5 System.Void D_EmbushState::.ctor()
extern void D_EmbushState__ctor_m1580DBDD9776E245EAC4777BBBD79205E05A1E21 (void);
// 0x000001D6 System.Void D_Entity::.ctor()
extern void D_Entity__ctor_mDF027D8680FA962248A1D660E1E80FC4F10BC8B0 (void);
// 0x000001D7 System.Void D_IdleState::.ctor()
extern void D_IdleState__ctor_mED8037E108CB92FAB47F93A6967A061ABAE23DC9 (void);
// 0x000001D8 System.Void D_LookForPlayerState::.ctor()
extern void D_LookForPlayerState__ctor_m0AD91F340CB0595B7FF0292F32CE6F23BDE78078 (void);
// 0x000001D9 System.Void D_MeleeAttackState::.ctor()
extern void D_MeleeAttackState__ctor_m31A368F4C80F4EC1738CE57E56CE9D3762EDD4B5 (void);
// 0x000001DA System.Void D_MoveState::.ctor()
extern void D_MoveState__ctor_m7F58A56EA079E8AC9FC708517632EB9E8D867D5E (void);
// 0x000001DB System.Void D_PlayerDetected::.ctor()
extern void D_PlayerDetected__ctor_m94B2AB6DE983E10684584EE1A5A3E286E0EDCE07 (void);
// 0x000001DC System.Void D_RangedAttackState::.ctor()
extern void D_RangedAttackState__ctor_m9D8EEB756E1AFD0426F7B9F2A30C28787D433927 (void);
// 0x000001DD System.Void D_RegenState::.ctor()
extern void D_RegenState__ctor_mFB4D35279A73C9BFA4ADDB3D13123038EBA1D553 (void);
// 0x000001DE System.Void D_StunState::.ctor()
extern void D_StunState__ctor_mC0423E7C9B2C8885F3EE6EF0F41F909F4AD193FF (void);
// 0x000001DF System.Void DeadState::.ctor(Entity,FiniteStateMachine,System.String,D_DeadState)
extern void DeadState__ctor_m82AF591480AE53CD925635B2007905D46B77B324 (void);
// 0x000001E0 System.Void DeadState::Enter()
extern void DeadState_Enter_m94FEEF4A54717C667D64F67809EE7A8BC1DCE3EB (void);
// 0x000001E1 System.Void DeadState::Exit()
extern void DeadState_Exit_mEFC9F4BE65AA67EE5B06935F5911771D339CDABA (void);
// 0x000001E2 System.Void DeadState::LogicUpdate()
extern void DeadState_LogicUpdate_mD25E6AC37B87006BBFA41E1ABAF74CD2E0B31ADF (void);
// 0x000001E3 System.Void DeadState::PhysicsUpdate()
extern void DeadState_PhysicsUpdate_mE9A146266C055BF045887D578C0B9ED62D01DC2B (void);
// 0x000001E4 System.Void DeadState::TickChecks()
extern void DeadState_TickChecks_m19EF67020D07C69223C780E24B51495C56907160 (void);
// 0x000001E5 System.Void DodgeState::.ctor(Entity,FiniteStateMachine,System.String,D_DodgeState)
extern void DodgeState__ctor_m9E5199514F39EAD99FFAF5191380286FE5E7B134 (void);
// 0x000001E6 System.Void DodgeState::TickChecks()
extern void DodgeState_TickChecks_m32BDF6236BAD06F1FC8DCA5DFE2706726102F610 (void);
// 0x000001E7 System.Void DodgeState::Enter()
extern void DodgeState_Enter_mC98799871A3E17A0B7D7CA7086AA6FCB622F9B45 (void);
// 0x000001E8 System.Void DodgeState::Exit()
extern void DodgeState_Exit_m1C24E82297DF7C4B0532FBEA74770E839CD17F59 (void);
// 0x000001E9 System.Void DodgeState::LogicUpdate()
extern void DodgeState_LogicUpdate_mACE185E911D269CC42E156A18EBD1DED6A965D01 (void);
// 0x000001EA System.Void DodgeState::PhysicsUpdate()
extern void DodgeState_PhysicsUpdate_m72E26F891C0978A078CD3400D3AE079FCE7975A3 (void);
// 0x000001EB System.Void EmbushState::.ctor(Entity,FiniteStateMachine,System.String,D_EmbushState)
extern void EmbushState__ctor_mA6AC939D7D6D2029B998171411CF8FA8DEC9653C (void);
// 0x000001EC System.Void EmbushState::Enter()
extern void EmbushState_Enter_mBD461E15EDEDA2407E6E9FA8A5A03EDEB484B7DD (void);
// 0x000001ED System.Void EmbushState::Exit()
extern void EmbushState_Exit_mC6D43A3AF933B1C070FDD111078ED7E3772BBA4A (void);
// 0x000001EE System.Void EmbushState::LogicUpdate()
extern void EmbushState_LogicUpdate_m51930CA8413387F7BACEB850F5E1623CDF94503C (void);
// 0x000001EF System.Void EmbushState::PhysicsUpdate()
extern void EmbushState_PhysicsUpdate_mC5A101BEA218EE0062705EB5E3A1451042BF32AD (void);
// 0x000001F0 System.Void EmbushState::TickChecks()
extern void EmbushState_TickChecks_mFE06E0B7D101090A6A34E7EC04537B7F21AE8158 (void);
// 0x000001F1 System.Void IdleState::.ctor(Entity,FiniteStateMachine,System.String,D_IdleState)
extern void IdleState__ctor_mF2362D03E8F851DF3D81D7E9B348124047B5DB36 (void);
// 0x000001F2 System.Void IdleState::Enter()
extern void IdleState_Enter_m51072142AC823A7C2FB871048D754F5299D17C94 (void);
// 0x000001F3 System.Void IdleState::Exit()
extern void IdleState_Exit_m9B85F8778772174C65C1B778350E52D2F4AAFC30 (void);
// 0x000001F4 System.Void IdleState::LogicUpdate()
extern void IdleState_LogicUpdate_mCEE0C6B9E98CB35527E8ED4A45F63E2914D3CDDA (void);
// 0x000001F5 System.Void IdleState::PhysicsUpdate()
extern void IdleState_PhysicsUpdate_mA4F06FF1E82934D08DE3AFBB1E2F1D369CC56F35 (void);
// 0x000001F6 System.Void IdleState::SetFlipAfterIdle(System.Boolean)
extern void IdleState_SetFlipAfterIdle_mDE60F5B1316B9AE7E8DD28F9267182DACA9AB693 (void);
// 0x000001F7 System.Void IdleState::TickChecks()
extern void IdleState_TickChecks_m0D1499F22370960C420FABBB21F0D613112A96AD (void);
// 0x000001F8 System.Void IdleState::SetRandomIdleTime()
extern void IdleState_SetRandomIdleTime_m10D3FEC0293C4B639724672A137DFC6DA4031455 (void);
// 0x000001F9 System.Void LookForPlayerState::.ctor(Entity,FiniteStateMachine,System.String,D_LookForPlayerState)
extern void LookForPlayerState__ctor_mA1CA9D40B3FBD3AE0C6B19CAC2CA7B9A53F2008E (void);
// 0x000001FA System.Void LookForPlayerState::TickChecks()
extern void LookForPlayerState_TickChecks_m35F1F6575EC6E009A103FFE1072267C2774F969A (void);
// 0x000001FB System.Void LookForPlayerState::Enter()
extern void LookForPlayerState_Enter_m583C106D344CDF25B6C4D42CD92CA60EBD3DBB0B (void);
// 0x000001FC System.Void LookForPlayerState::Exit()
extern void LookForPlayerState_Exit_m42C55374F41F3D35B61224E47273C87524C111C3 (void);
// 0x000001FD System.Void LookForPlayerState::LogicUpdate()
extern void LookForPlayerState_LogicUpdate_m5BB1DBAD33CDD9FC9DE1C75270955703094BE338 (void);
// 0x000001FE System.Void LookForPlayerState::PhysicsUpdate()
extern void LookForPlayerState_PhysicsUpdate_m6780D11A592EA103B375B92632C4EAE8C84B002A (void);
// 0x000001FF System.Void LookForPlayerState::SetTurnImmeditely(System.Boolean)
extern void LookForPlayerState_SetTurnImmeditely_m21241B79EE43257A6BE5C521062359963C8ED26A (void);
// 0x00000200 System.Void MeleeAttackState::.ctor(Entity,FiniteStateMachine,System.String,UnityEngine.Transform,D_MeleeAttackState)
extern void MeleeAttackState__ctor_mFDFC1C278BE8443C8C713EBEEDCC36DEB9AC5A5F (void);
// 0x00000201 System.Void MeleeAttackState::Enter()
extern void MeleeAttackState_Enter_m9873973935277D815B98A86E5347E2DB89DAE546 (void);
// 0x00000202 System.Void MeleeAttackState::Exit()
extern void MeleeAttackState_Exit_mE1B2B9D3CBC3CCEF7F850AA0C9FED724F511532F (void);
// 0x00000203 System.Void MeleeAttackState::FinishAttack()
extern void MeleeAttackState_FinishAttack_mBBDA06914CECE6A8AB0040B411BA06D8D4FA163D (void);
// 0x00000204 System.Void MeleeAttackState::LogicUpdate()
extern void MeleeAttackState_LogicUpdate_m3EC01B7E6E7379F52FF6E46643B2C4E06A7ABA15 (void);
// 0x00000205 System.Void MeleeAttackState::PhysicsUpdate()
extern void MeleeAttackState_PhysicsUpdate_mE0B8BD9BBCB8C2E453D5758AE3D3DBA5628696EC (void);
// 0x00000206 System.Void MeleeAttackState::TickChecks()
extern void MeleeAttackState_TickChecks_mB7526C2457A5DA60B83EC75417699EAF28C5163F (void);
// 0x00000207 System.Void MeleeAttackState::TriggerAttack()
extern void MeleeAttackState_TriggerAttack_m5203228B06F9139364252BCFB9B685C0D623470A (void);
// 0x00000208 System.Void MoveState::.ctor(Entity,FiniteStateMachine,System.String,D_MoveState)
extern void MoveState__ctor_m09EC17DFDBEA293B87C8C6782AA6293997683CCC (void);
// 0x00000209 System.Void MoveState::Enter()
extern void MoveState_Enter_m9949CC1758C872C442E37D4D78927BEB9966165B (void);
// 0x0000020A System.Void MoveState::Exit()
extern void MoveState_Exit_mFD8332161E17822D03DA0577058DAAB79FF34699 (void);
// 0x0000020B System.Void MoveState::LogicUpdate()
extern void MoveState_LogicUpdate_m2B84E9CB40E745F8A9FAF82C072C856D8B4BEDE0 (void);
// 0x0000020C System.Void MoveState::PhysicsUpdate()
extern void MoveState_PhysicsUpdate_mCB0BEE6BDB976A01E06CBD98AF04FB4983D05020 (void);
// 0x0000020D System.Void MoveState::TickChecks()
extern void MoveState_TickChecks_mF15FB174785C967DA5ABD59EC38FDB9ED04B503C (void);
// 0x0000020E System.Void PlayerDetectedState::.ctor(Entity,FiniteStateMachine,System.String,D_PlayerDetected)
extern void PlayerDetectedState__ctor_m0896D0F12F8E351E3588A10C7A84D33A2063C821 (void);
// 0x0000020F System.Void PlayerDetectedState::Enter()
extern void PlayerDetectedState_Enter_m66A439724B1FD86C231453061F43AC7559DA2CB8 (void);
// 0x00000210 System.Void PlayerDetectedState::Exit()
extern void PlayerDetectedState_Exit_m9568F894FA587EA3C8198CAEA36A9D799C25EE2D (void);
// 0x00000211 System.Void PlayerDetectedState::LogicUpdate()
extern void PlayerDetectedState_LogicUpdate_mFCBEED5F981BF08A1BE9660E6C8A076607937AD3 (void);
// 0x00000212 System.Void PlayerDetectedState::PhysicsUpdate()
extern void PlayerDetectedState_PhysicsUpdate_m18AB880AF598426230CED16DDBEFAACAA0E82CD2 (void);
// 0x00000213 System.Void PlayerDetectedState::TickChecks()
extern void PlayerDetectedState_TickChecks_mA6117411A96FE143CAEDC78B298A1B98ED3DCB82 (void);
// 0x00000214 System.Void RangedAttackState::.ctor(Entity,FiniteStateMachine,System.String,UnityEngine.Transform,D_RangedAttackState)
extern void RangedAttackState__ctor_m7EA2EC0C30DA1104F29AEC083C49BB9C79466063 (void);
// 0x00000215 System.Void RangedAttackState::TickChecks()
extern void RangedAttackState_TickChecks_m48A6606907DCCC6891403900032FB527876F6D50 (void);
// 0x00000216 System.Void RangedAttackState::Enter()
extern void RangedAttackState_Enter_mD6F1AE174F8630C0F753FFD35414A506FC4A459F (void);
// 0x00000217 System.Void RangedAttackState::Exit()
extern void RangedAttackState_Exit_m10345FE1AF9EC8830EFD511B118E407622CE5F7F (void);
// 0x00000218 System.Void RangedAttackState::LogicUpdate()
extern void RangedAttackState_LogicUpdate_m74BEEA408526D5F76DF8B6570FBC0D1FFA2524EC (void);
// 0x00000219 System.Void RangedAttackState::PhysicsUpdate()
extern void RangedAttackState_PhysicsUpdate_mF40078770E3BDA22E6CA594F0C04A1A3628C1210 (void);
// 0x0000021A System.Void RangedAttackState::TriggerAttack()
extern void RangedAttackState_TriggerAttack_m6342D4D9CC27A041285CDE7F8D2665F93FEB107F (void);
// 0x0000021B System.Void RangedAttackState::FinishAttack()
extern void RangedAttackState_FinishAttack_m6E85E3020A1ADBE46F5D029E5EFE4DB6437099F8 (void);
// 0x0000021C System.Void RegenState::.ctor(Entity,FiniteStateMachine,System.String,D_RegenState)
extern void RegenState__ctor_m2724C41CAB68845007621FD4CC223A50D23FA6E8 (void);
// 0x0000021D System.Void RegenState::Enter()
extern void RegenState_Enter_m07C57B29071DB3082DBF4D1AFA2BB0982E9D1735 (void);
// 0x0000021E System.Void RegenState::Exit()
extern void RegenState_Exit_mCCF1D830B84864CB55E2AECD45D4D3676996ADCA (void);
// 0x0000021F System.Void RegenState::LogicUpdate()
extern void RegenState_LogicUpdate_mF38762B83A3524B862BF188ED6891CCD523BE674 (void);
// 0x00000220 System.Void RegenState::PhysicsUpdate()
extern void RegenState_PhysicsUpdate_m430D5A6F910D95A65BFF044E78B3BE8075083693 (void);
// 0x00000221 System.Void RegenState::TickChecks()
extern void RegenState_TickChecks_m020174B48761805D3B04194616E9506D220F74E3 (void);
// 0x00000222 System.Void StunState::.ctor(Entity,FiniteStateMachine,System.String,D_StunState)
extern void StunState__ctor_m42812B04B838B6A8154DF1D2DF0E5DEB8C20EFCA (void);
// 0x00000223 System.Void StunState::Enter()
extern void StunState_Enter_m4BC2DE784AFE74A7C4F34A22A0E4B36EE20733DC (void);
// 0x00000224 System.Void StunState::Exit()
extern void StunState_Exit_m6AA575AD9843084BEC2D27DF451E4D42292FE033 (void);
// 0x00000225 System.Void StunState::LogicUpdate()
extern void StunState_LogicUpdate_mB746CB8073E72985ED6CF8DB0341324FE1187508 (void);
// 0x00000226 System.Void StunState::PhysicsUpdate()
extern void StunState_PhysicsUpdate_m467137F2F9240926FCCB1E512B2E4C396F89F898 (void);
// 0x00000227 System.Void StunState::TickChecks()
extern void StunState_TickChecks_m29DDCC15F8AB5A0A61519956D71E09A691B2A0A2 (void);
// 0x00000228 System.Void TrainingDummyManager::.ctor()
extern void TrainingDummyManager__ctor_m9FFB009E988E0A8E98BB6F5D808A9351BA259E0C (void);
// 0x00000229 System.Void GameManager::Awake()
extern void GameManager_Awake_m22F42B2A82708B10F652CAD8F2E0A4767110FF30 (void);
// 0x0000022A System.Void GameManager::Respawn()
extern void GameManager_Respawn_m3B6501D139E5DD8182689FF742BAE77C2295216D (void);
// 0x0000022B System.Void GameManager::CheckRespawn()
extern void GameManager_CheckRespawn_m5C4C4E2884D273703F7BBC8AFFC66F378B3357D5 (void);
// 0x0000022C System.Void GameManager::Start()
extern void GameManager_Start_m26461AEF27E44DB8FECCBC19D6C9E228B658BF8E (void);
// 0x0000022D System.Void GameManager::Update()
extern void GameManager_Update_mC9303BA7C3117BD861F49F8E36151CC52117E6C1 (void);
// 0x0000022E System.Void GameManager::Quit()
extern void GameManager_Quit_m3E5652D9A2DA57E3E7A31BECB511D9BCDD55BD75 (void);
// 0x0000022F System.Void GameManager::.ctor()
extern void GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D (void);
// 0x00000230 System.Void RespawnManager::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void RespawnManager_OnTriggerEnter2D_m4471BF0806295EB599F0675E12F7174262C66A6D (void);
// 0x00000231 System.Void RespawnManager::.ctor()
extern void RespawnManager__ctor_mB6363BB8AA7BDB0B7A69C57A719D345326B36A1C (void);
// 0x00000232 System.Void ParticleToTarget::.ctor()
extern void ParticleToTarget__ctor_m558AD5B220853D0CF2D02BF182C8D4BB145556A1 (void);
// 0x00000233 System.Void ParticulesManager::Start()
extern void ParticulesManager_Start_mA94055C82B0DBF63935E06DAE04C573C920EB450 (void);
// 0x00000234 System.Void ParticulesManager::OnParticleCollision(UnityEngine.GameObject)
extern void ParticulesManager_OnParticleCollision_mC534CABAD89E34DA6CEDC762CD5F529AF03AF8EB (void);
// 0x00000235 System.Void ParticulesManager::.ctor()
extern void ParticulesManager__ctor_mC87F0B8D0A7085E1201AD26FDC92D46757C45EEA (void);
// 0x00000236 System.Void PlayerData::.ctor()
extern void PlayerData__ctor_mC2DA6B32832AB92B19E799148968590FF1F28C1B (void);
// 0x00000237 UnityEngine.Vector2 PlayerInputHandler::get_RawMovementInput()
extern void PlayerInputHandler_get_RawMovementInput_m3AF315DDF95FB1856FB0B32630D2AA8465A01EB4 (void);
// 0x00000238 System.Void PlayerInputHandler::set_RawMovementInput(UnityEngine.Vector2)
extern void PlayerInputHandler_set_RawMovementInput_m249F8838F9EFBF919C0C984FBDF4802B30A8EB92 (void);
// 0x00000239 UnityEngine.Vector2 PlayerInputHandler::get_RawDashDirectionInput()
extern void PlayerInputHandler_get_RawDashDirectionInput_m57B516A85E44CC80878A4C6448028D89244AC1A1 (void);
// 0x0000023A System.Void PlayerInputHandler::set_RawDashDirectionInput(UnityEngine.Vector2)
extern void PlayerInputHandler_set_RawDashDirectionInput_m0592FE1B872D77A37D77D6ED61A244CEC8C93255 (void);
// 0x0000023B UnityEngine.Vector2Int PlayerInputHandler::get_DashDirectionInput()
extern void PlayerInputHandler_get_DashDirectionInput_mB0E58145AB8E3CB565361DF7E868497DC405CB2F (void);
// 0x0000023C System.Void PlayerInputHandler::set_DashDirectionInput(UnityEngine.Vector2Int)
extern void PlayerInputHandler_set_DashDirectionInput_m26B2D25468CC3DAC5D95B80FAD08B307B6671679 (void);
// 0x0000023D System.Int32 PlayerInputHandler::get_NormalizedInputX()
extern void PlayerInputHandler_get_NormalizedInputX_mA055B906BDAFED306F18B6890C9F8AEC6EBC4C1B (void);
// 0x0000023E System.Void PlayerInputHandler::set_NormalizedInputX(System.Int32)
extern void PlayerInputHandler_set_NormalizedInputX_mD15166140EA451985E30D83FD75C79191044D713 (void);
// 0x0000023F System.Int32 PlayerInputHandler::get_NormalizedInputY()
extern void PlayerInputHandler_get_NormalizedInputY_m314BDE45CC47AC6D3AC427116222A46F4AA3B35F (void);
// 0x00000240 System.Void PlayerInputHandler::set_NormalizedInputY(System.Int32)
extern void PlayerInputHandler_set_NormalizedInputY_m7B72D4E8D5184C43BA55074C4A6001845B61964E (void);
// 0x00000241 System.Boolean PlayerInputHandler::get_JumpInput()
extern void PlayerInputHandler_get_JumpInput_m26BC043384005AD5F83600E9F29EA18D20F7FC7D (void);
// 0x00000242 System.Void PlayerInputHandler::set_JumpInput(System.Boolean)
extern void PlayerInputHandler_set_JumpInput_m8DA9FE7EE38724040D9798EBC8650AFA025C8365 (void);
// 0x00000243 System.Boolean PlayerInputHandler::get_GrabInput()
extern void PlayerInputHandler_get_GrabInput_m6B25B0BA68400B221515C83A2764AB339D3FDEFC (void);
// 0x00000244 System.Void PlayerInputHandler::set_GrabInput(System.Boolean)
extern void PlayerInputHandler_set_GrabInput_m74C4166D924773CB349B54AE66F7067F01F0BFE5 (void);
// 0x00000245 System.Boolean PlayerInputHandler::get_JumpInputStop()
extern void PlayerInputHandler_get_JumpInputStop_mA82CD3965C6E34FC1774E70C31E2E3FA66DB34FB (void);
// 0x00000246 System.Void PlayerInputHandler::set_JumpInputStop(System.Boolean)
extern void PlayerInputHandler_set_JumpInputStop_mBA621FCBD43FB41CE8E02157D2473E42BB773AE8 (void);
// 0x00000247 System.Boolean PlayerInputHandler::get_DashInput()
extern void PlayerInputHandler_get_DashInput_mCA79E1D77F9D4EB5642491FEE12A99B61DB9CDBC (void);
// 0x00000248 System.Void PlayerInputHandler::set_DashInput(System.Boolean)
extern void PlayerInputHandler_set_DashInput_m05E8DB0DF6ADF472CD4D53526C1F6813CA20EC3E (void);
// 0x00000249 System.Boolean PlayerInputHandler::get_DashInputStop()
extern void PlayerInputHandler_get_DashInputStop_m489F0B3E7E92B2843A4FE0B48224161A469CBA72 (void);
// 0x0000024A System.Void PlayerInputHandler::set_DashInputStop(System.Boolean)
extern void PlayerInputHandler_set_DashInputStop_m7DE101AF637FE87A3AC933A3293A263ABC423B88 (void);
// 0x0000024B System.Boolean PlayerInputHandler::get_RollInput()
extern void PlayerInputHandler_get_RollInput_mD0B385A5857FE99B8E09694397A651C5A1A5251B (void);
// 0x0000024C System.Void PlayerInputHandler::set_RollInput(System.Boolean)
extern void PlayerInputHandler_set_RollInput_m5EBEA8DFCB5AA109869EC6857D4C93BF2BF69B5E (void);
// 0x0000024D System.Boolean[] PlayerInputHandler::get_AttackInputs()
extern void PlayerInputHandler_get_AttackInputs_mFE74D1E6EDA13AF8F9B6749BC33DAE6B2BE63CED (void);
// 0x0000024E System.Void PlayerInputHandler::set_AttackInputs(System.Boolean[])
extern void PlayerInputHandler_set_AttackInputs_m6EE4797101A202E44849101A478FC5BECE62C85D (void);
// 0x0000024F System.Void PlayerInputHandler::Start()
extern void PlayerInputHandler_Start_mF2E5EBC5BB402A32542EA6B1487048DD83ECFDF0 (void);
// 0x00000250 System.Void PlayerInputHandler::Update()
extern void PlayerInputHandler_Update_mF283E8945B879FBEA7530EB3F5C626B4CA1C8949 (void);
// 0x00000251 System.Void PlayerInputHandler::OnPrimaryAttackInput(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void PlayerInputHandler_OnPrimaryAttackInput_m5BC5D64E7DDA95564F95DAC94A719675C9B3D2E9 (void);
// 0x00000252 System.Void PlayerInputHandler::OnMoveInput(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void PlayerInputHandler_OnMoveInput_m97461836C5180660F88BC89CE428B724E9E2F9BC (void);
// 0x00000253 System.Void PlayerInputHandler::OnJumpInput(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void PlayerInputHandler_OnJumpInput_mBA10B81503CC3E121C3C605EBC467AEE5365CA40 (void);
// 0x00000254 System.Void PlayerInputHandler::OnDashInput(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void PlayerInputHandler_OnDashInput_m08888E52DE467D072C873EE64BEBB6FB5706630D (void);
// 0x00000255 System.Void PlayerInputHandler::OnGrabInput(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void PlayerInputHandler_OnGrabInput_m0704CF75910E66289FCB0EF24FC20B47F85887D9 (void);
// 0x00000256 System.Void PlayerInputHandler::OnDashDirectionInput(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void PlayerInputHandler_OnDashDirectionInput_m363569B92E16DCABAC5B19EDA91B9BDA66C75167 (void);
// 0x00000257 System.Void PlayerInputHandler::OnRollInput(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void PlayerInputHandler_OnRollInput_m6C2981FC15CD4C552DD539F2A4446173595922D2 (void);
// 0x00000258 System.Void PlayerInputHandler::UseJumpInput()
extern void PlayerInputHandler_UseJumpInput_m618F6ADFA138E2D1615EBE34C2E77BC8B1F321E1 (void);
// 0x00000259 System.Void PlayerInputHandler::UseDashInput()
extern void PlayerInputHandler_UseDashInput_mDA9FCA59DBAEE514B4FC912796E96819A55DEBAD (void);
// 0x0000025A System.Void PlayerInputHandler::UseRollInput()
extern void PlayerInputHandler_UseRollInput_m6172E234BA679656907165FEFA2B7516EC94F572 (void);
// 0x0000025B System.Void PlayerInputHandler::CheckJumpInputHoldTime()
extern void PlayerInputHandler_CheckJumpInputHoldTime_mE034D66ECF20A583F098BBA6A3C1B22287E84489 (void);
// 0x0000025C System.Void PlayerInputHandler::CheckDashInputHoldTime()
extern void PlayerInputHandler_CheckDashInputHoldTime_mB1D4AE9C00F9BBF3D7564FAA455B11E75B2EE5C9 (void);
// 0x0000025D System.Void PlayerInputHandler::.ctor()
extern void PlayerInputHandler__ctor_m5A813BC103A59C67103EC4B8F9976D47C2E5F1EC (void);
// 0x0000025E System.Void PlayerAfterImage::OnEnable()
extern void PlayerAfterImage_OnEnable_mF72041CF8EB96FEA16CDEF68FA9CEA5DAC59D3B8 (void);
// 0x0000025F System.Void PlayerAfterImage::Update()
extern void PlayerAfterImage_Update_mA8F423CFC7BB69629245E7440DA66989BA808D71 (void);
// 0x00000260 System.Void PlayerAfterImage::.ctor()
extern void PlayerAfterImage__ctor_mCFB7F4339C5F10065D66FF900F54E382B8FFEFA1 (void);
// 0x00000261 PlayerAfterImagePool PlayerAfterImagePool::get_Instance()
extern void PlayerAfterImagePool_get_Instance_mC0331F5A143E84CBEC306312A464081139A3BA07 (void);
// 0x00000262 System.Void PlayerAfterImagePool::set_Instance(PlayerAfterImagePool)
extern void PlayerAfterImagePool_set_Instance_m2594D883D50656B1D24270EE78F5F6F8E88C3F4B (void);
// 0x00000263 System.Void PlayerAfterImagePool::Awake()
extern void PlayerAfterImagePool_Awake_m59BA8EF9BD86EE7ACF6A95C7D104C306376113A8 (void);
// 0x00000264 System.Void PlayerAfterImagePool::GrowPool()
extern void PlayerAfterImagePool_GrowPool_m4CB61CA940F5D6D670076486720512A20A262897 (void);
// 0x00000265 System.Void PlayerAfterImagePool::AddToPool(UnityEngine.GameObject)
extern void PlayerAfterImagePool_AddToPool_m450E9DA28BDA6EF86223BA81867EB30EAA3F9AE7 (void);
// 0x00000266 UnityEngine.GameObject PlayerAfterImagePool::GetFromPool()
extern void PlayerAfterImagePool_GetFromPool_m426FD43C0F3F37BC58B4604CF813B0AC48629485 (void);
// 0x00000267 System.Void PlayerAfterImagePool::.ctor()
extern void PlayerAfterImagePool__ctor_m114A98B7728C0675C5244D25B08B5DB9DEA6D513 (void);
// 0x00000268 System.Void PlayerCombatController::.ctor()
extern void PlayerCombatController__ctor_m78980447F79B11082A2D7C9B8B1A3C1666204FF5 (void);
// 0x00000269 System.Void PlayerController::Start()
extern void PlayerController_Start_m9531F30EC892BDD1758A2EEC724E86EFBDA150A3 (void);
// 0x0000026A System.Void PlayerController::Update()
extern void PlayerController_Update_mB31159CAD7DD2329859472554BC9154A83D8E794 (void);
// 0x0000026B System.Void PlayerController::FixedUpdate()
extern void PlayerController_FixedUpdate_m54EE3ADAA7597303B1F69849B233D1A68D880B14 (void);
// 0x0000026C System.Void PlayerController::GetInput()
extern void PlayerController_GetInput_m4AFA3AE682A7A53D856CA648934BB322CEC6EB31 (void);
// 0x0000026D System.Void PlayerController::ApplyMovement()
extern void PlayerController_ApplyMovement_mEE8E1B38C5A305FD1FCA4B480CEB545E7D0FA1DF (void);
// 0x0000026E System.Void PlayerController::CheckParticules()
extern void PlayerController_CheckParticules_m66D4BE5163AE2408F0342D7BDD598AFF8930A610 (void);
// 0x0000026F System.Void PlayerController::ActivateJumpParticles()
extern void PlayerController_ActivateJumpParticles_m892B0F1AFD078A2E9D1FBD1980B0BFB76F61035A (void);
// 0x00000270 System.Void PlayerController::DeactivateJumpParticles()
extern void PlayerController_DeactivateJumpParticles_mD07C73C6C8B31428E287507CC4000FD055B96263 (void);
// 0x00000271 System.Void PlayerController::NormalJump()
extern void PlayerController_NormalJump_mA5A7C8373E2B7C88F4734F3BCDE9AB5C69E88D27 (void);
// 0x00000272 System.Collections.IEnumerator PlayerController::JumpSqueeze(System.Single,System.Single,System.Single)
extern void PlayerController_JumpSqueeze_mF96893621E21786FD3103599811EFCAEE38E5309 (void);
// 0x00000273 System.Void PlayerController::WallJump()
extern void PlayerController_WallJump_m79BBF84ACFF8FDF4CFC69C5D6BAE9A87993C0F1C (void);
// 0x00000274 System.Void PlayerController::CheckJump()
extern void PlayerController_CheckJump_m84A9DED55F65FDCAAC3ADBE0CED00A5531657A29 (void);
// 0x00000275 System.Void PlayerController::Knockback(System.Int32)
extern void PlayerController_Knockback_m9301868379DFAD25C0F24E88112CBBF8A5AB6DAC (void);
// 0x00000276 System.Void PlayerController::CheckKnockback()
extern void PlayerController_CheckKnockback_m79E84818DAE6A2F10C6C782EE1376E4DB326FE68 (void);
// 0x00000277 System.Void PlayerController::CheckLedgeClimb()
extern void PlayerController_CheckLedgeClimb_m167F961F395FB06EAF39430E2781439D646CA4A8 (void);
// 0x00000278 System.Void PlayerController::FinishLedgeClimb()
extern void PlayerController_FinishLedgeClimb_mF3FBB827B7DE082FECFE82D56609C1AE3B41D705 (void);
// 0x00000279 System.Void PlayerController::CheckIfCanJump()
extern void PlayerController_CheckIfCanJump_mE68E35B78B6D8DB6935D4FA5CB5AE72DE4ED817B (void);
// 0x0000027A System.Void PlayerController::CheckIfWallSliding()
extern void PlayerController_CheckIfWallSliding_m42F08E725399873BEA6DAE515CAE3A21C867F11D (void);
// 0x0000027B System.Void PlayerController::CheckMovementDirection()
extern void PlayerController_CheckMovementDirection_m65436EDDF53E79E0D7407DA74371B03A7D5C4330 (void);
// 0x0000027C System.Void PlayerController::AttemptToDash()
extern void PlayerController_AttemptToDash_mE6A3367618A9EB68D924784EA971D1375A921AE0 (void);
// 0x0000027D System.Void PlayerController::CheckDash()
extern void PlayerController_CheckDash_mACAE3867180E01018925178589DBD74063A9A428 (void);
// 0x0000027E System.Boolean PlayerController::GetDashStatus()
extern void PlayerController_GetDashStatus_m9391AF5A488814FF81D5432DAAC864FD67E9E961 (void);
// 0x0000027F System.Void PlayerController::CheckSurroundings()
extern void PlayerController_CheckSurroundings_m2638F4C6D34AC2DF775A1BBB39910C21BE7A244F (void);
// 0x00000280 System.Void PlayerController::Flip()
extern void PlayerController_Flip_m78C6F4DC8127219F853348B761BD0EDEDEC88DD0 (void);
// 0x00000281 System.Int32 PlayerController::GetFacingDirection()
extern void PlayerController_GetFacingDirection_m34476102285862EB1F9F3D9D1A7EC2FD675F8CBF (void);
// 0x00000282 System.Void PlayerController::DisableFlip()
extern void PlayerController_DisableFlip_m02C0DB710A9DABDBDAAABD98FC1C72EBAB019834 (void);
// 0x00000283 System.Void PlayerController::EnableFlip()
extern void PlayerController_EnableFlip_m774FE2842B1B105FC490E676C65EA36FF2CE006F (void);
// 0x00000284 System.Void PlayerController::UpdateAnimations()
extern void PlayerController_UpdateAnimations_m6C34457263AD9886F900BC66A5F2EE7351054F56 (void);
// 0x00000285 System.Void PlayerController::OnDrawGizmos()
extern void PlayerController_OnDrawGizmos_mDB5E3315C6F60D4C95063F50B7FDC74B5B9E3C62 (void);
// 0x00000286 System.Void PlayerController::.ctor()
extern void PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB (void);
// 0x00000287 System.Void PlayerController/<JumpSqueeze>d__80::.ctor(System.Int32)
extern void U3CJumpSqueezeU3Ed__80__ctor_m5AAE49D54907DFBCD338FFCA511D9AB1C379C699 (void);
// 0x00000288 System.Void PlayerController/<JumpSqueeze>d__80::System.IDisposable.Dispose()
extern void U3CJumpSqueezeU3Ed__80_System_IDisposable_Dispose_m5E07A734A0A78FF968D0C9510EC9CD566B8B581F (void);
// 0x00000289 System.Boolean PlayerController/<JumpSqueeze>d__80::MoveNext()
extern void U3CJumpSqueezeU3Ed__80_MoveNext_m3AD5EA6ED368A9556A52B658F82E64AD5C2A260A (void);
// 0x0000028A System.Object PlayerController/<JumpSqueeze>d__80::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CJumpSqueezeU3Ed__80_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m06702FBA3AED7C7B6CAEB9B9F20D6C5CEC04DFED (void);
// 0x0000028B System.Void PlayerController/<JumpSqueeze>d__80::System.Collections.IEnumerator.Reset()
extern void U3CJumpSqueezeU3Ed__80_System_Collections_IEnumerator_Reset_m710EE58E3AC36491F6B7336784809E8F028F7496 (void);
// 0x0000028C System.Object PlayerController/<JumpSqueeze>d__80::System.Collections.IEnumerator.get_Current()
extern void U3CJumpSqueezeU3Ed__80_System_Collections_IEnumerator_get_Current_mCB3B01623B59C881F0F256BA0980497B693D0981 (void);
// 0x0000028D System.Void PlayerParticulesManager::Start()
extern void PlayerParticulesManager_Start_mC5D2BA338ADFB74B66EA6696302F727A8C11961D (void);
// 0x0000028E System.Void PlayerParticulesManager::ActivateJumpParticules()
extern void PlayerParticulesManager_ActivateJumpParticules_mBC579250B4A81784D0DBE412EFFE54E71574636B (void);
// 0x0000028F System.Void PlayerParticulesManager::ActivateWallSlideParticules()
extern void PlayerParticulesManager_ActivateWallSlideParticules_mDD20E077F1352B5D6DEEF4DF029DB3B0EF421A72 (void);
// 0x00000290 System.Void PlayerParticulesManager::ActivateRunParticules()
extern void PlayerParticulesManager_ActivateRunParticules_m673B198A7065AFF75164072235BF1D1D605A9606 (void);
// 0x00000291 System.Void PlayerParticulesManager::DeactivateJumpParticules()
extern void PlayerParticulesManager_DeactivateJumpParticules_m43EFF69867C548BFA8EF181F0FE68F7EB2317791 (void);
// 0x00000292 System.Void PlayerParticulesManager::DeactivateWallSlideParticules()
extern void PlayerParticulesManager_DeactivateWallSlideParticules_mB3B0C7E4BA046C34BF386E53E519AB132DFD83CD (void);
// 0x00000293 System.Void PlayerParticulesManager::DeactivateRunParticules()
extern void PlayerParticulesManager_DeactivateRunParticules_m1FCD874528D407D1BD28170EB29BFFCEE3F56E16 (void);
// 0x00000294 System.Void PlayerParticulesManager::.ctor()
extern void PlayerParticulesManager__ctor_m49368CB727149BBC6BB0BCD349D44E1A78708852 (void);
// 0x00000295 System.Void PlayerStats::Start()
extern void PlayerStats_Start_m4D4AE70D5DDA0FC33DE88634198F70275FF512DC (void);
// 0x00000296 System.Void PlayerStats::TakeDamage(System.Single)
extern void PlayerStats_TakeDamage_m5E9B290AE295F8BCD9F72BC3754D9B940F581A7E (void);
// 0x00000297 System.Void PlayerStats::Die()
extern void PlayerStats_Die_m9DD2B794182DBA6221B9A2B6D193D36621CC2B16 (void);
// 0x00000298 System.Void PlayerStats::.ctor()
extern void PlayerStats__ctor_mE0D15E1136221C95781FA81A050F868076E05620 (void);
// 0x00000299 PlayerStateMachine Player::get_StateMachine()
extern void Player_get_StateMachine_m2198B858EAFA76B007558A67FE1800D043EEDA7F (void);
// 0x0000029A System.Void Player::set_StateMachine(PlayerStateMachine)
extern void Player_set_StateMachine_mF55EAB7CEFDAB9B9375A6CE98C8ED4857095C817 (void);
// 0x0000029B PlayerIdleState Player::get_IdleState()
extern void Player_get_IdleState_m4B4159BE756438FAF50BA682E94CD97EB3359CEB (void);
// 0x0000029C System.Void Player::set_IdleState(PlayerIdleState)
extern void Player_set_IdleState_m1EE2B0F91F5A91BD0A627D60C72577C9DD166515 (void);
// 0x0000029D PlayerMoveState Player::get_MoveState()
extern void Player_get_MoveState_m2C72FC94E47A1983A5C54C61D78172EFC9C7FFEA (void);
// 0x0000029E System.Void Player::set_MoveState(PlayerMoveState)
extern void Player_set_MoveState_mAA36A7247EFFD915F255D9D9D21CD75DEF4B92F4 (void);
// 0x0000029F PlayerJumpState Player::get_JumpState()
extern void Player_get_JumpState_mDCA6ABA3DD458DACF52879CB57FDF3F2F49A8C19 (void);
// 0x000002A0 System.Void Player::set_JumpState(PlayerJumpState)
extern void Player_set_JumpState_mC83D41A9D9309166C7B64C32CC826716396A4336 (void);
// 0x000002A1 PlayerLandState Player::get_LandState()
extern void Player_get_LandState_mC544BBC946DD7A8946BD110AAFFD63BFD75D5A05 (void);
// 0x000002A2 System.Void Player::set_LandState(PlayerLandState)
extern void Player_set_LandState_m3F4AA4D6217A9AC555D3AC47713B9877DE042FC8 (void);
// 0x000002A3 PlayerDashState Player::get_DashState()
extern void Player_get_DashState_mB0A4A3373F0BC1914A8C29F9B74198E988A5551B (void);
// 0x000002A4 System.Void Player::set_DashState(PlayerDashState)
extern void Player_set_DashState_m5E4CF4807B68E74EA16A2E2C7582A93CEFEC899F (void);
// 0x000002A5 PlayerRollState Player::get_RollState()
extern void Player_get_RollState_mC5A5F1F61962B883EB24D2432EBB9098D61940C4 (void);
// 0x000002A6 System.Void Player::set_RollState(PlayerRollState)
extern void Player_set_RollState_mE37EBB798B56DCF56F8192933DD8F74324917E66 (void);
// 0x000002A7 PlayerInAirState Player::get_InAirState()
extern void Player_get_InAirState_m08BD915E02EE2B66D2AB5DC812E576D6A4669E78 (void);
// 0x000002A8 System.Void Player::set_InAirState(PlayerInAirState)
extern void Player_set_InAirState_m1F44F2A60DF33B57D699FEE7E6A95DB064659B77 (void);
// 0x000002A9 PlayerAttackState Player::get_PrimaryAttackState()
extern void Player_get_PrimaryAttackState_m807460CA47D3824FA6796238F7B04F01CA3313A3 (void);
// 0x000002AA System.Void Player::set_PrimaryAttackState(PlayerAttackState)
extern void Player_set_PrimaryAttackState_m353DD6D335A366EDD0A083D2DC4374377CD224D5 (void);
// 0x000002AB PlayerAttackState Player::get_SecondaryAttackState()
extern void Player_get_SecondaryAttackState_m7824B1833AE6BB387028FFB4ADE12DD88DBECE44 (void);
// 0x000002AC System.Void Player::set_SecondaryAttackState(PlayerAttackState)
extern void Player_set_SecondaryAttackState_m4CB9971C06C0332E70B8896A516A3B4FC6A730EE (void);
// 0x000002AD PlayerWallGrabState Player::get_WallGrabState()
extern void Player_get_WallGrabState_m7224F71AD0CBD49F5D9E207E4C9415BA8DECBDE7 (void);
// 0x000002AE System.Void Player::set_WallGrabState(PlayerWallGrabState)
extern void Player_set_WallGrabState_mFBB5753BBC6E5C989214CD5721CFEEA8C8932709 (void);
// 0x000002AF PlayerWallJumpState Player::get_WallJumpState()
extern void Player_get_WallJumpState_mAF0DF68CEF7E155E54442E0E5656F2D65A82C903 (void);
// 0x000002B0 System.Void Player::set_WallJumpState(PlayerWallJumpState)
extern void Player_set_WallJumpState_m7B1D2F1AAE95CC1B13CCDB4542C488EC1E38BD3A (void);
// 0x000002B1 PlayerWallSlideState Player::get_WallSlideState()
extern void Player_get_WallSlideState_mF9D7F216CF8603587C057EE999FEED40FA5BD5E8 (void);
// 0x000002B2 System.Void Player::set_WallSlideState(PlayerWallSlideState)
extern void Player_set_WallSlideState_m0CA05530916005E69FB08774E73227B8C920681C (void);
// 0x000002B3 PlayerWallClimbState Player::get_WallClimbState()
extern void Player_get_WallClimbState_m542A476F6A239889AFE80076B9EE369D89CDFB34 (void);
// 0x000002B4 System.Void Player::set_WallClimbState(PlayerWallClimbState)
extern void Player_set_WallClimbState_m42742C45FD72FCF3C9056681D2E5E63452129FEC (void);
// 0x000002B5 PlayerLedgeClimbState Player::get_LedgeClimbState()
extern void Player_get_LedgeClimbState_mBEA2BAA8F845E81BAD0FC6E211F9A5F9D2D23591 (void);
// 0x000002B6 System.Void Player::set_LedgeClimbState(PlayerLedgeClimbState)
extern void Player_set_LedgeClimbState_mA1AB9E354332E6A9778B00D7974D7C2FF7131C4E (void);
// 0x000002B7 PlayerCrouchIdleState Player::get_CrouchIdleState()
extern void Player_get_CrouchIdleState_mB32BA08F68A49B4759821B0642D88961A75072DD (void);
// 0x000002B8 System.Void Player::set_CrouchIdleState(PlayerCrouchIdleState)
extern void Player_set_CrouchIdleState_m0F5297FBE37CB7CBAB5184F85B67D739EA4D6171 (void);
// 0x000002B9 PlayerCrouchMoveState Player::get_CrouchMoveState()
extern void Player_get_CrouchMoveState_m988D07B15EE791346C447F268A580A8CBD425CB1 (void);
// 0x000002BA System.Void Player::set_CrouchMoveState(PlayerCrouchMoveState)
extern void Player_set_CrouchMoveState_mE50AF05E29A4B93852F2A6D182CD4965F5C2AED4 (void);
// 0x000002BB PlayerGroundPoundState Player::get_GroundPoundState()
extern void Player_get_GroundPoundState_mC9CFE723E0B384D69A41B9AECF0C775CDAE066F4 (void);
// 0x000002BC System.Void Player::set_GroundPoundState(PlayerGroundPoundState)
extern void Player_set_GroundPoundState_m2075D69193DC02A711BACCDF89E6D766E10859A4 (void);
// 0x000002BD UnityEngine.Transform Player::get_dashDirectionIndicator()
extern void Player_get_dashDirectionIndicator_mB22CB5447A611B5E1CDAD8A48276AC6890A5A12E (void);
// 0x000002BE System.Void Player::set_dashDirectionIndicator(UnityEngine.Transform)
extern void Player_set_dashDirectionIndicator_m78FE4F2A6E91DE1BE4EFC576382C570B5B42C1D6 (void);
// 0x000002BF UnityEngine.Animator Player::get_Anim()
extern void Player_get_Anim_m9221694B09CA9B7C315CE0FEB3655FB1BFE19112 (void);
// 0x000002C0 System.Void Player::set_Anim(UnityEngine.Animator)
extern void Player_set_Anim_mCB2981090965D89CAB5605C26E223637379C2BC6 (void);
// 0x000002C1 UnityEngine.Rigidbody2D Player::get_Rb()
extern void Player_get_Rb_m2B4FF51AC6E82017EA8B69AD0379F7078BC31144 (void);
// 0x000002C2 System.Void Player::set_Rb(UnityEngine.Rigidbody2D)
extern void Player_set_Rb_m787D661181B65CC25AB5F1B1FEC06AA4BC565B90 (void);
// 0x000002C3 PlayerInputHandler Player::get_InputHandler()
extern void Player_get_InputHandler_mCFA3AEA34BE76A7F4020A4ADF30EFAE0011CE695 (void);
// 0x000002C4 System.Void Player::set_InputHandler(PlayerInputHandler)
extern void Player_set_InputHandler_mD2DD228A45907B3AFA5AEBD74DBFCD0BD08F045E (void);
// 0x000002C5 UnityEngine.BoxCollider2D Player::get_MovementCollider()
extern void Player_get_MovementCollider_m1F5621F1F42114456C9A7FBDB225BBF35C7727C3 (void);
// 0x000002C6 System.Void Player::set_MovementCollider(UnityEngine.BoxCollider2D)
extern void Player_set_MovementCollider_mA5520C56E1609EB0CCBD8FD744FCF7213A0AA2FD (void);
// 0x000002C7 PlayerInventory Player::get_Inventory()
extern void Player_get_Inventory_m9860D2EAF7984A6B94831D1822AA54EAA4BB06EF (void);
// 0x000002C8 System.Void Player::set_Inventory(PlayerInventory)
extern void Player_set_Inventory_m5EAAFF570CCD7B758B2D58A1E486C61AB90A671A (void);
// 0x000002C9 Core Player::get_Core()
extern void Player_get_Core_mA72B07C402649E359C5DAD0323EE65E9605A5FE8 (void);
// 0x000002CA System.Void Player::set_Core(Core)
extern void Player_set_Core_mDD73A7FE2B282CEC0F489509082C4A6A545C535B (void);
// 0x000002CB System.Void Player::Awake()
extern void Player_Awake_m1131F11CF6BF6FBE6454601C7D9A94AC8F468A24 (void);
// 0x000002CC System.Void Player::Start()
extern void Player_Start_mBD692B64AAC791B93A589E7D3596F36787EAF021 (void);
// 0x000002CD System.Void Player::Update()
extern void Player_Update_mBA04F1D6FE3C18037EA95DFAEEAA9977BFD49CD3 (void);
// 0x000002CE System.Void Player::FixedUpdate()
extern void Player_FixedUpdate_mD7447EDFC86F29A3E5FBDEF7E0139535BD4C5088 (void);
// 0x000002CF System.Void Player::SetColliderHeight(System.Single)
extern void Player_SetColliderHeight_m8547071A8C8336D256AB495B61D7244BFCC63459 (void);
// 0x000002D0 System.Void Player::AnimationTrigger()
extern void Player_AnimationTrigger_mBE4430E63FC5BCE0702D8FB97E4AC2FAEA871DFD (void);
// 0x000002D1 System.Void Player::AnimationFinishTrigger()
extern void Player_AnimationFinishTrigger_mBCC1891FF169F290CF363089D3F43F557B047CC5 (void);
// 0x000002D2 System.Void Player::ActiveRollParticle()
extern void Player_ActiveRollParticle_m798123F4ABCD3FC17789921D7FD2CDC094FC359B (void);
// 0x000002D3 System.Void Player::DeactivateRollParticles()
extern void Player_DeactivateRollParticles_m5DF6C55726FA1FF641B681F15414080BED4F171D (void);
// 0x000002D4 System.Void Player::.ctor()
extern void Player__ctor_mDE8EB5B351975D4E7E24DE341B8B49B8A29CC2B7 (void);
// 0x000002D5 System.Void PlayerState::.ctor(Player,PlayerStateMachine,PlayerData,System.Int32)
extern void PlayerState__ctor_mBBD1B6CA2B248B98E457773E4EC58332256FCA59 (void);
// 0x000002D6 System.Void PlayerState::DoChecks()
extern void PlayerState_DoChecks_m0493A20EBFB37FC588EEF3E4C694C03DFB677D51 (void);
// 0x000002D7 System.Void PlayerState::Enter()
extern void PlayerState_Enter_mBF1461BBD6A3D0F11ED2333152B89614D757F4B4 (void);
// 0x000002D8 System.Void PlayerState::Exit()
extern void PlayerState_Exit_m8A7AE4B644E5E38A57BD9EB52F1C1C53EDEC0A3A (void);
// 0x000002D9 System.Void PlayerState::LogicUpdate()
extern void PlayerState_LogicUpdate_m5AFB229BAF38D34B611E2CAA3CA60F05E32156B1 (void);
// 0x000002DA System.Void PlayerState::PhysicUpdate()
extern void PlayerState_PhysicUpdate_m0B2619B3295B08A5921DE790504E90819BE624CA (void);
// 0x000002DB System.Void PlayerState::AnimationTrigger()
extern void PlayerState_AnimationTrigger_mD3BC05F52C83DD46D74A09CC1A31A2D1C64AE972 (void);
// 0x000002DC System.Void PlayerState::AnimationFinishTrigger()
extern void PlayerState_AnimationFinishTrigger_m9F77E914D6DEB987DCFC7E2B3CF12729E0DD0803 (void);
// 0x000002DD PlayerState PlayerStateMachine::get_CurrentState()
extern void PlayerStateMachine_get_CurrentState_mF308023C3A5D7FE149663D92BD483E9528EA060D (void);
// 0x000002DE System.Void PlayerStateMachine::set_CurrentState(PlayerState)
extern void PlayerStateMachine_set_CurrentState_m8E18DA6734485836D3D40FF276CE728788FDF764 (void);
// 0x000002DF System.Void PlayerStateMachine::Initialize(PlayerState)
extern void PlayerStateMachine_Initialize_m047039DD815DAD874A94ADE4F9085331FAAF8B41 (void);
// 0x000002E0 System.Void PlayerStateMachine::ChangeStateTo(PlayerState)
extern void PlayerStateMachine_ChangeStateTo_m7D8DFDD769B2995C181F3E106A510E6C773C5490 (void);
// 0x000002E1 System.Void PlayerStateMachine::.ctor()
extern void PlayerStateMachine__ctor_mEC680EE294695F01B04B9B57D933DD5F063D1C84 (void);
// 0x000002E2 System.Void PlayerAttackState::.ctor(Player,PlayerStateMachine,PlayerData,System.Int32)
extern void PlayerAttackState__ctor_m7C9145298561F41D23135ED2D99CD3C5485CD8E2 (void);
// 0x000002E3 System.Void PlayerAttackState::Enter()
extern void PlayerAttackState_Enter_mA4B8C2C351CB0F594546B1269A06D12E1104A52D (void);
// 0x000002E4 System.Void PlayerAttackState::Exit()
extern void PlayerAttackState_Exit_mD4DE091D4575745CC8D9D30740788D3EE7B76D8A (void);
// 0x000002E5 System.Void PlayerAttackState::SetWeapon(Weapon)
extern void PlayerAttackState_SetWeapon_m114E638CE46797F74119F0654ECC4CBF4D31013A (void);
// 0x000002E6 System.Void PlayerAttackState::SetPlayerVelocity(System.Single)
extern void PlayerAttackState_SetPlayerVelocity_m10D9988CB820294D061637A206F9DFCF3B79D19F (void);
// 0x000002E7 System.Void PlayerAttackState::SetFlipCheck(System.Boolean)
extern void PlayerAttackState_SetFlipCheck_mE38CBF231E57625F0FE84434F347B57F7CDE284B (void);
// 0x000002E8 System.Void PlayerAttackState::AnimationFinishTrigger()
extern void PlayerAttackState_AnimationFinishTrigger_mAC7E3A4826EE06084368B1CA02CC66C4496089D4 (void);
// 0x000002E9 System.Void PlayerAttackState::LogicUpdate()
extern void PlayerAttackState_LogicUpdate_m090B12EF63A90AC722E2240F68CF679818F44CD9 (void);
// 0x000002EA System.Void PlayerAttackState::DoChecks()
extern void PlayerAttackState_DoChecks_m88AEF25CF639FA3D9EF9DA523BBD7A81FDE1C664 (void);
// 0x000002EB System.Boolean PlayerDashState::get_CanDash()
extern void PlayerDashState_get_CanDash_mA262276ADE682D009EFFE00E43C45E68BE6F520C (void);
// 0x000002EC System.Void PlayerDashState::set_CanDash(System.Boolean)
extern void PlayerDashState_set_CanDash_mC48669CC87DE45F3614F5C7944220EE488015911 (void);
// 0x000002ED System.Void PlayerDashState::.ctor(Player,PlayerStateMachine,PlayerData,System.Int32)
extern void PlayerDashState__ctor_mF277FAC947195100B57EB8FF51076A1B5C1C5BFC (void);
// 0x000002EE System.Void PlayerDashState::Enter()
extern void PlayerDashState_Enter_m247D12F4D49AD755836A4F3FB4341D1233EF9C19 (void);
// 0x000002EF System.Void PlayerDashState::Exit()
extern void PlayerDashState_Exit_m4A91B3814DFD5AC8ED1207B3775923BCC763BCCF (void);
// 0x000002F0 System.Void PlayerDashState::LogicUpdate()
extern void PlayerDashState_LogicUpdate_mC958921EC03AD403D1292DA3A21E0E691E7F6041 (void);
// 0x000002F1 System.Void PlayerDashState::PlaceAfterImage()
extern void PlayerDashState_PlaceAfterImage_mA7FE344D09FDFABEB8E2B185FAACE525FDCABBEE (void);
// 0x000002F2 System.Void PlayerDashState::CheckIfShouldPlaceAfterImage()
extern void PlayerDashState_CheckIfShouldPlaceAfterImage_mCEEC762DF33427038A542C8F1C04A80384AE398A (void);
// 0x000002F3 System.Boolean PlayerDashState::CheckIfCanDash()
extern void PlayerDashState_CheckIfCanDash_mF29CBE3B0F66DF394D1A119F73CFA2977D2308D0 (void);
// 0x000002F4 System.Void PlayerDashState::ResetCanDash()
extern void PlayerDashState_ResetCanDash_m3BD6D13A2DF877F61A66853F80FE66507921B25C (void);
// 0x000002F5 System.Void PlayerJumpState::.ctor(Player,PlayerStateMachine,PlayerData,System.Int32)
extern void PlayerJumpState__ctor_mFFDA8B78652E5A6BFB20F590506CC5C755CD8C9C (void);
// 0x000002F6 System.Void PlayerJumpState::Enter()
extern void PlayerJumpState_Enter_mFF3D4664A054849912D7BE1C71DD86AB0907733A (void);
// 0x000002F7 System.Boolean PlayerJumpState::CanJump()
extern void PlayerJumpState_CanJump_m1B53FC213A579C553F7773856D282F750827D109 (void);
// 0x000002F8 System.Void PlayerJumpState::ResetAmountOfJumpsLeft()
extern void PlayerJumpState_ResetAmountOfJumpsLeft_m32DBB19FDC09B9C5CCB7606C267856FB627E15C0 (void);
// 0x000002F9 System.Void PlayerJumpState::DecreaseAmountOfJumpsLeft()
extern void PlayerJumpState_DecreaseAmountOfJumpsLeft_m8CBE45F6353BE4CDB7F0911AC36F9EFA96B91718 (void);
// 0x000002FA System.Void PlayerJumpState::DecreaseAmountOfJumpsLeft(System.Int32)
extern void PlayerJumpState_DecreaseAmountOfJumpsLeft_mD6370F9C76EB365E2B4F300F721EF743FA23E0D3 (void);
// 0x000002FB System.Void PlayerWallJumpState::.ctor(Player,PlayerStateMachine,PlayerData,System.Int32)
extern void PlayerWallJumpState__ctor_m78B8D9EAF011BFC3D1A0BA0F976B9326F21DF181 (void);
// 0x000002FC System.Void PlayerWallJumpState::Enter()
extern void PlayerWallJumpState_Enter_mDB828AE7CA9C5395A4E1AC9BDEC4422E26833E5C (void);
// 0x000002FD System.Void PlayerWallJumpState::LogicUpdate()
extern void PlayerWallJumpState_LogicUpdate_m6287177A521525FDC0E7143D090C5EB2BF882DA9 (void);
// 0x000002FE System.Void PlayerWallJumpState::DetermineWallJumpDirection(System.Boolean)
extern void PlayerWallJumpState_DetermineWallJumpDirection_mAFBF966CA65D6E7E31343D39ABA01C7FDB2140D1 (void);
// 0x000002FF System.Void PlayerCrouchIdleState::.ctor(Player,PlayerStateMachine,PlayerData,System.Int32)
extern void PlayerCrouchIdleState__ctor_m95A13333D66EFAF76EF16DAF8D497F483F326AF0 (void);
// 0x00000300 System.Void PlayerCrouchIdleState::Enter()
extern void PlayerCrouchIdleState_Enter_mA9578EA05088B735D3BFD5DFA1B5888ADCDF761A (void);
// 0x00000301 System.Void PlayerCrouchIdleState::Exit()
extern void PlayerCrouchIdleState_Exit_mEA524886A5367A6F7D4251DBEF293E8065997218 (void);
// 0x00000302 System.Void PlayerCrouchIdleState::LogicUpdate()
extern void PlayerCrouchIdleState_LogicUpdate_mBCB232513214F8E61F3BF4F6166F7793DB82DC68 (void);
// 0x00000303 System.Void PlayerCrouchMoveState::.ctor(Player,PlayerStateMachine,PlayerData,System.Int32)
extern void PlayerCrouchMoveState__ctor_m3956151970D421F0C3DFB624E6D0B8F2112532A5 (void);
// 0x00000304 System.Void PlayerCrouchMoveState::Enter()
extern void PlayerCrouchMoveState_Enter_m464116C03059D01C4CF9EF61BD578A7F08EAC8D6 (void);
// 0x00000305 System.Void PlayerCrouchMoveState::Exit()
extern void PlayerCrouchMoveState_Exit_m7827506D50396A0B3B2EF2254F134072C0F1D8DB (void);
// 0x00000306 System.Void PlayerCrouchMoveState::LogicUpdate()
extern void PlayerCrouchMoveState_LogicUpdate_mD578517912D63ACDDF4078FB77A8F4D1FCB14D1F (void);
// 0x00000307 System.Void PlayerIdleState::.ctor(Player,PlayerStateMachine,PlayerData,System.Int32)
extern void PlayerIdleState__ctor_m488850B99AA8E87D24B5A9C71C93E976BC232E6B (void);
// 0x00000308 System.Void PlayerIdleState::DoChecks()
extern void PlayerIdleState_DoChecks_mA49A8D1CFCCCA946F6441DEC96035B8CC8A9CE9D (void);
// 0x00000309 System.Void PlayerIdleState::Enter()
extern void PlayerIdleState_Enter_mF9FFABBD1BAA897F4B26E7068A6406BA34BED0C8 (void);
// 0x0000030A System.Void PlayerIdleState::Exit()
extern void PlayerIdleState_Exit_mC7CA6F8C0136D65341E50B6F1BF28BBF3CC88D88 (void);
// 0x0000030B System.Void PlayerIdleState::LogicUpdate()
extern void PlayerIdleState_LogicUpdate_mAB09D5BB6C5F713605C75E5364109D8BB16840DD (void);
// 0x0000030C System.Void PlayerIdleState::PhysicUpdate()
extern void PlayerIdleState_PhysicUpdate_mC9A0A9F447626BA656D888325B8EA422931C7AC3 (void);
// 0x0000030D System.Void PlayerLandState::.ctor(Player,PlayerStateMachine,PlayerData,System.Int32)
extern void PlayerLandState__ctor_m07A9132A7946F638545C0C07B432C222C0952ABB (void);
// 0x0000030E System.Void PlayerLandState::DoChecks()
extern void PlayerLandState_DoChecks_m0F18F534716FB3D6F1B520A45315004BB7F28BBF (void);
// 0x0000030F System.Void PlayerLandState::Enter()
extern void PlayerLandState_Enter_m711D3F152ECD9B22105B57FB9E45F335CA88B456 (void);
// 0x00000310 System.Void PlayerLandState::Exit()
extern void PlayerLandState_Exit_m2F85F8F12B384F6B0437C70C22D7009037DE4FE7 (void);
// 0x00000311 System.Void PlayerLandState::LogicUpdate()
extern void PlayerLandState_LogicUpdate_m00E15B18BC7270F9EF04215EDF2E9D10555CF83B (void);
// 0x00000312 System.Void PlayerLandState::PhysicUpdate()
extern void PlayerLandState_PhysicUpdate_m4E74532C6DFD93F9D577C0A8B679D17F2DE5DF11 (void);
// 0x00000313 System.Void PlayerMoveState::.ctor(Player,PlayerStateMachine,PlayerData,System.Int32)
extern void PlayerMoveState__ctor_mD5A70EE29E482F9AF783CD11235F10C706E1EF5F (void);
// 0x00000314 System.Void PlayerMoveState::DoChecks()
extern void PlayerMoveState_DoChecks_m7062B511BBC685183F1AE34237B2C3C1F6736835 (void);
// 0x00000315 System.Void PlayerMoveState::Enter()
extern void PlayerMoveState_Enter_mFC5D54C7CC3694500D03DD4429D6B98492D81EE6 (void);
// 0x00000316 System.Void PlayerMoveState::Exit()
extern void PlayerMoveState_Exit_mF979FA0C8EC5D17A91757F67F110D276DD7E840E (void);
// 0x00000317 System.Void PlayerMoveState::LogicUpdate()
extern void PlayerMoveState_LogicUpdate_m51BD7E1FB796BE25AEACBBAD3B529B5920BB3A59 (void);
// 0x00000318 System.Void PlayerMoveState::PhysicUpdate()
extern void PlayerMoveState_PhysicUpdate_m0AA69F2DAF345F4A405F39D1C4B971B82FFE1741 (void);
// 0x00000319 System.Void PlayerGroundPoundState::.ctor(Player,PlayerStateMachine,PlayerData,System.Int32)
extern void PlayerGroundPoundState__ctor_mAD0CD91C11FFABBA89FCE22361E8824488489711 (void);
// 0x0000031A System.Void PlayerGroundPoundState::DoChecks()
extern void PlayerGroundPoundState_DoChecks_mCC7645E6787E2E30790C0F6CA777C32F5CAE828F (void);
// 0x0000031B System.Void PlayerGroundPoundState::Enter()
extern void PlayerGroundPoundState_Enter_mC34A63A489CA7C798B51CFD1FDE06394A2D99FCC (void);
// 0x0000031C System.Void PlayerGroundPoundState::Exit()
extern void PlayerGroundPoundState_Exit_m49DA5F4C4D658A2737A2A30BD31B60B7C71A76C2 (void);
// 0x0000031D System.Void PlayerGroundPoundState::LogicUpdate()
extern void PlayerGroundPoundState_LogicUpdate_m55A4DE466DF5A4C16F12ADBF67E355D84D95A9AD (void);
// 0x0000031E System.Void PlayerInAirState::.ctor(Player,PlayerStateMachine,PlayerData,System.Int32)
extern void PlayerInAirState__ctor_mC55E393C74EECD473F357E7B3B325E9157D3A7F8 (void);
// 0x0000031F System.Void PlayerInAirState::DoChecks()
extern void PlayerInAirState_DoChecks_mEBAE8897C190FA9490EBC7A89B3D25AD45ECE60C (void);
// 0x00000320 System.Void PlayerInAirState::LogicUpdate()
extern void PlayerInAirState_LogicUpdate_mE24B63A690557FD8902C035D224D90C14D590053 (void);
// 0x00000321 System.Void PlayerInAirState::Exit()
extern void PlayerInAirState_Exit_m0E2F012A1396BE0B3659D4172B3CF1B3CDA6D479 (void);
// 0x00000322 System.Void PlayerInAirState::CheckJumpMultiplier()
extern void PlayerInAirState_CheckJumpMultiplier_m25B746B93F0DE4FCAEA316BEC004423542CE522D (void);
// 0x00000323 System.Void PlayerInAirState::CheckCoyoteTime()
extern void PlayerInAirState_CheckCoyoteTime_mF958F210F4AB3BEE3F4696B2684ED7701D28FDD4 (void);
// 0x00000324 System.Void PlayerInAirState::SetIsJumping()
extern void PlayerInAirState_SetIsJumping_m4717E283CE01E0C745469B5A6E6C6EDBFA88CCB3 (void);
// 0x00000325 System.Void PlayerInAirState::StartCoyoteTime()
extern void PlayerInAirState_StartCoyoteTime_mE052E9C1FA3386B86827F3900ACCBA80DECB01D6 (void);
// 0x00000326 System.Void PlayerInAirState::StartWallJumpCoyoteTime()
extern void PlayerInAirState_StartWallJumpCoyoteTime_m92B62CC05212A6D43D554A90082F463206286973 (void);
// 0x00000327 System.Void PlayerInAirState::StopWallJumpCoyoteTime()
extern void PlayerInAirState_StopWallJumpCoyoteTime_m72FDFE076F7AE733E21479793F9CDD5DAB7977A5 (void);
// 0x00000328 System.Void PlayerInAirState::CheckWallJumpCoyoteTime()
extern void PlayerInAirState_CheckWallJumpCoyoteTime_mA68C0EFB38A6D99734395580F04FC08C954F5754 (void);
// 0x00000329 System.Void PlayerLedgeClimbState::.ctor(Player,PlayerStateMachine,PlayerData,System.Int32)
extern void PlayerLedgeClimbState__ctor_m9C41AE29A34E709AA2F75485BA0918A9AB3148BF (void);
// 0x0000032A System.Void PlayerLedgeClimbState::SetDetectedPosition(UnityEngine.Vector2)
extern void PlayerLedgeClimbState_SetDetectedPosition_m4A20E87870C24357264DC8ECFB908F8AC2B17ACD (void);
// 0x0000032B UnityEngine.Vector2 PlayerLedgeClimbState::DetermineCornerPosition()
extern void PlayerLedgeClimbState_DetermineCornerPosition_m347C8E0AC299F20E1D180D538E7791A5640F6C03 (void);
// 0x0000032C System.Void PlayerLedgeClimbState::AnimationFinishTrigger()
extern void PlayerLedgeClimbState_AnimationFinishTrigger_m2F4B48AEF3B733C52DC063FBF5A1273C296B2DB2 (void);
// 0x0000032D System.Void PlayerLedgeClimbState::AnimationTrigger()
extern void PlayerLedgeClimbState_AnimationTrigger_mC7F33003ABA2EB927BFEAC53DD948FA75ABE0964 (void);
// 0x0000032E System.Void PlayerLedgeClimbState::Enter()
extern void PlayerLedgeClimbState_Enter_mB83DDEAC3728FD40C0A7C01619A8DDA805EABF8E (void);
// 0x0000032F System.Void PlayerLedgeClimbState::Exit()
extern void PlayerLedgeClimbState_Exit_mF3E1C5F4ABC1B2910A0FB6499710FD0A6741803B (void);
// 0x00000330 System.Void PlayerLedgeClimbState::LogicUpdate()
extern void PlayerLedgeClimbState_LogicUpdate_mCAED3EB4976E17E89C3F428B5323406C5E88CE57 (void);
// 0x00000331 System.Void PlayerLedgeClimbState::CheckForSpace()
extern void PlayerLedgeClimbState_CheckForSpace_mAE7093D57A3BB11DE315E4722CE8A32B6231E17D (void);
// 0x00000332 System.Boolean PlayerRollState::get_CanRoll()
extern void PlayerRollState_get_CanRoll_m8D12AD049072AC064E787C5AD7DDF338A38D80EC (void);
// 0x00000333 System.Void PlayerRollState::set_CanRoll(System.Boolean)
extern void PlayerRollState_set_CanRoll_mDABB853ADB7608625E4029A265AFF798F1EE73E7 (void);
// 0x00000334 System.Void PlayerRollState::.ctor(Player,PlayerStateMachine,PlayerData,System.Int32)
extern void PlayerRollState__ctor_m116862B5DE4374687AA95317F13F78EC3A68AC83 (void);
// 0x00000335 System.Void PlayerRollState::Enter()
extern void PlayerRollState_Enter_m43EB59AEAFDCE5D4A8674BDD6E3CEBA414693000 (void);
// 0x00000336 System.Void PlayerRollState::Exit()
extern void PlayerRollState_Exit_m288396623841CB1A753BE73AB2ADAEC89DAF150E (void);
// 0x00000337 System.Void PlayerRollState::LogicUpdate()
extern void PlayerRollState_LogicUpdate_mF832E86A8F885A7513A5113CEAF7927DB6B9F523 (void);
// 0x00000338 System.Void PlayerRollState::AnimationFinishTrigger()
extern void PlayerRollState_AnimationFinishTrigger_m36DAC24F6D0780117EBE57D99A901F1D810C34BB (void);
// 0x00000339 System.Void PlayerRollState::PhysicUpdate()
extern void PlayerRollState_PhysicUpdate_m4577E60F74D29B0F8028A2575ACC00857A5B7C08 (void);
// 0x0000033A System.Boolean PlayerRollState::CheckIfCanRoll()
extern void PlayerRollState_CheckIfCanRoll_m3F52F9969F959B4B3AF62363DC4C8A1BD4292193 (void);
// 0x0000033B System.Void PlayerRollState::ResetCanRoll()
extern void PlayerRollState_ResetCanRoll_m7FEE29B1A9069FC87D3DDD694123DF67ABC34440 (void);
// 0x0000033C System.Void PlayerRollState::DoChecks()
extern void PlayerRollState_DoChecks_m885BEFEF0A21A33FC460C759AE7195447F3A1C0F (void);
// 0x0000033D System.Void PlayerWallClimbState::.ctor(Player,PlayerStateMachine,PlayerData,System.Int32)
extern void PlayerWallClimbState__ctor_m6C93051D24EA881B5465E9FDFC7F2E3800C21C33 (void);
// 0x0000033E System.Void PlayerWallClimbState::Enter()
extern void PlayerWallClimbState_Enter_m21D40F72E5F61A050D1587E14C5AB4C45024C2BA (void);
// 0x0000033F System.Void PlayerWallClimbState::Exit()
extern void PlayerWallClimbState_Exit_mAD42F6AD9A3636615D227825CD2516D5FFCDCAD6 (void);
// 0x00000340 System.Void PlayerWallClimbState::LogicUpdate()
extern void PlayerWallClimbState_LogicUpdate_mD46FAF47BF9A85407F2550CEFB3EADE0CC80C25E (void);
// 0x00000341 System.Void PlayerWallGrabState::.ctor(Player,PlayerStateMachine,PlayerData,System.Int32)
extern void PlayerWallGrabState__ctor_m570DA0870A52D6A163DD2005E1393485F9E64EA8 (void);
// 0x00000342 System.Void PlayerWallGrabState::Enter()
extern void PlayerWallGrabState_Enter_m78DC3F39508640BB47CD74FBC53A585D83DE5C22 (void);
// 0x00000343 System.Void PlayerWallGrabState::Exit()
extern void PlayerWallGrabState_Exit_m41C63259015BC7144B6DD2FD9EABB8BD4064A67D (void);
// 0x00000344 System.Void PlayerWallGrabState::LogicUpdate()
extern void PlayerWallGrabState_LogicUpdate_mC3A93A9AABFC61480D69FAC96D060F1B437A08B7 (void);
// 0x00000345 System.Void PlayerWallGrabState::HoldPosition()
extern void PlayerWallGrabState_HoldPosition_m7433AD54FBFE1DD5A83F3963362A21A0569E9330 (void);
// 0x00000346 System.Void PlayerWallSlideState::.ctor(Player,PlayerStateMachine,PlayerData,System.Int32)
extern void PlayerWallSlideState__ctor_m9903A9937CD5F6321D914D577C16969046A7F07C (void);
// 0x00000347 System.Void PlayerWallSlideState::Enter()
extern void PlayerWallSlideState_Enter_mEE16386BD6A908D6CAFDDE1D5E788EAA7FF0E3F2 (void);
// 0x00000348 System.Void PlayerWallSlideState::Exit()
extern void PlayerWallSlideState_Exit_m6942C199021E1D02F591A69907AF5069C1A99C27 (void);
// 0x00000349 System.Void PlayerWallSlideState::LogicUpdate()
extern void PlayerWallSlideState_LogicUpdate_m98004D4882107007D84E3A02448A7BBE87F1665B (void);
// 0x0000034A System.Void PlayerAbilityState::.ctor(Player,PlayerStateMachine,PlayerData,System.Int32)
extern void PlayerAbilityState__ctor_mEA5C9D00057030D53B714FDFF62E673989157787 (void);
// 0x0000034B System.Void PlayerAbilityState::DoChecks()
extern void PlayerAbilityState_DoChecks_mCDD808CB26E6772848E67AB183A30DD34379FBBC (void);
// 0x0000034C System.Void PlayerAbilityState::Enter()
extern void PlayerAbilityState_Enter_m40E748F18F02CD94AC6E6DF6EA3106F15D057C78 (void);
// 0x0000034D System.Void PlayerAbilityState::LogicUpdate()
extern void PlayerAbilityState_LogicUpdate_m51138343B66E0DB8AEB3BC527F6A7C1300436D71 (void);
// 0x0000034E System.Void PlayerAbilityState::PhysicUpdate()
extern void PlayerAbilityState_PhysicUpdate_mA4AB2EA4D0E45977EFCF121D37B51EA31C70B658 (void);
// 0x0000034F System.Void PlayerAbilityState::Exit()
extern void PlayerAbilityState_Exit_m0AF77B26376A7E5D2E34A200B5680C1A3F4D1C85 (void);
// 0x00000350 System.Void PlayerGroundedState::.ctor(Player,PlayerStateMachine,PlayerData,System.Int32)
extern void PlayerGroundedState__ctor_m38E11EB17C9646647FCD796FD7BDB8E03865DE91 (void);
// 0x00000351 System.Void PlayerGroundedState::DoChecks()
extern void PlayerGroundedState_DoChecks_m898912949CF3BA356289A12A8C010F0F09E9D9AE (void);
// 0x00000352 System.Void PlayerGroundedState::Enter()
extern void PlayerGroundedState_Enter_mBA3C213E0FA68FCB44C0CFC269E9EFB595A56C2E (void);
// 0x00000353 System.Void PlayerGroundedState::Exit()
extern void PlayerGroundedState_Exit_m3E5548079501BFBCB6B49937584E14B5ABA1B13F (void);
// 0x00000354 System.Void PlayerGroundedState::LogicUpdate()
extern void PlayerGroundedState_LogicUpdate_m222127BB700087F1AC5DC0CC49061D6D183AF5A6 (void);
// 0x00000355 System.Void PlayerGroundedState::PhysicUpdate()
extern void PlayerGroundedState_PhysicUpdate_m1BE494577B76C0F82DDD4B95594F86EF2D508BB4 (void);
// 0x00000356 System.Void PlayerTouchingWallState::.ctor(Player,PlayerStateMachine,PlayerData,System.Int32)
extern void PlayerTouchingWallState__ctor_m9D4A2ED21FA22F634089A7F32F9C2E9D287FC070 (void);
// 0x00000357 System.Void PlayerTouchingWallState::AnimationFinishTrigger()
extern void PlayerTouchingWallState_AnimationFinishTrigger_m9921B66168F923E4A4045B59D1A3BE47FFFD71DA (void);
// 0x00000358 System.Void PlayerTouchingWallState::AnimationTrigger()
extern void PlayerTouchingWallState_AnimationTrigger_m9A3372BE80B3E7C300A47352DE119FEA6801683D (void);
// 0x00000359 System.Void PlayerTouchingWallState::DoChecks()
extern void PlayerTouchingWallState_DoChecks_m793AB5C27C547E81CE7873065C482E2B1BA759A2 (void);
// 0x0000035A System.Void PlayerTouchingWallState::Enter()
extern void PlayerTouchingWallState_Enter_m5EB8BA304DB10FBBE1672F0AA7F4941BC72E4833 (void);
// 0x0000035B System.Void PlayerTouchingWallState::Exit()
extern void PlayerTouchingWallState_Exit_mAF79B2575EE936A8A40E242010C680A9AF36AA68 (void);
// 0x0000035C System.Void PlayerTouchingWallState::LogicUpdate()
extern void PlayerTouchingWallState_LogicUpdate_mFFF80A30CFAF4AF2E29413796259D9FB227752CB (void);
// 0x0000035D System.Void PlayerTouchingWallState::PhysicUpdate()
extern void PlayerTouchingWallState_PhysicUpdate_m471EEA27224D52FCA85E86FE3968549BF7A35205 (void);
// 0x0000035E System.Void PlayerHealthbar::Start()
extern void PlayerHealthbar_Start_m359AA2749E600E417607E53D594D78FFF45E955E (void);
// 0x0000035F System.Void PlayerHealthbar::UpdateHealthBar()
extern void PlayerHealthbar_UpdateHealthBar_m5E9A22A15D0F8AFE1ED6F3BF81E3E207284CA4A8 (void);
// 0x00000360 System.Void PlayerHealthbar::Update()
extern void PlayerHealthbar_Update_m1F3D0929B94538067B23282D6B5910807956D00B (void);
// 0x00000361 System.Void PlayerHealthbar::.ctor()
extern void PlayerHealthbar__ctor_mF8F4E1428FC3FE537DB3F705A205A8AE4127A980 (void);
// 0x00000362 System.Void PlayerInventory::.ctor()
extern void PlayerInventory__ctor_m61E3BB66C1E62760440A0BF22DFCE0B247AC1D8D (void);
// 0x00000363 System.Void AggressiveWeapon::Awake()
extern void AggressiveWeapon_Awake_m674698FCE8BBAE39AC0B75E3FD49A2A5F0E62757 (void);
// 0x00000364 System.Void AggressiveWeapon::AnimationActionTrigger()
extern void AggressiveWeapon_AnimationActionTrigger_m174AD8B699F773DDF11C8940D2955785E1886F5E (void);
// 0x00000365 System.Void AggressiveWeapon::CheckMeleeAttack()
extern void AggressiveWeapon_CheckMeleeAttack_m281FC6666B2231EA923CC0358A8657EA53C63EE7 (void);
// 0x00000366 System.Void AggressiveWeapon::AddToDetected(UnityEngine.Collider2D)
extern void AggressiveWeapon_AddToDetected_m7E1F3B18B760C5AA2E24C15A3797859EBFE525B8 (void);
// 0x00000367 System.Void AggressiveWeapon::RemoveFromDetected(UnityEngine.Collider2D)
extern void AggressiveWeapon_RemoveFromDetected_m184626228DE15FE60710932C20FE047BEAD44D0D (void);
// 0x00000368 System.Void AggressiveWeapon::.ctor()
extern void AggressiveWeapon__ctor_m0EDBC926A9E34BF85EDF9E77CBB184263F6FCF0C (void);
// 0x00000369 WeaponAttackDetails[] SO_AggressiveWeaponData::get_AttackDetails()
extern void SO_AggressiveWeaponData_get_AttackDetails_m466D82DC22DA83C637F7FF76A4FD9E87EE5E3575 (void);
// 0x0000036A System.Void SO_AggressiveWeaponData::set_AttackDetails(WeaponAttackDetails[])
extern void SO_AggressiveWeaponData_set_AttackDetails_mDE6C0D04379A649F8DC95FF1526E26FC4DC1AFCD (void);
// 0x0000036B System.Void SO_AggressiveWeaponData::OnEnable()
extern void SO_AggressiveWeaponData_OnEnable_m1CB88620492457FE2AA4723144C7417B1D3C471D (void);
// 0x0000036C System.Void SO_AggressiveWeaponData::.ctor()
extern void SO_AggressiveWeaponData__ctor_m6F94B89F734F667C7B400E4527C0E1A38D5106B0 (void);
// 0x0000036D System.Int32 SO_WeaponData::get_amountOfAttacks()
extern void SO_WeaponData_get_amountOfAttacks_m43D85164E4E01F5DA4862C6F994E2DFA7ED099A3 (void);
// 0x0000036E System.Void SO_WeaponData::set_amountOfAttacks(System.Int32)
extern void SO_WeaponData_set_amountOfAttacks_m8E605A5F587C238F35CC3F0DDCF5273D6CE0B6C8 (void);
// 0x0000036F System.Single[] SO_WeaponData::get_movementSpeed()
extern void SO_WeaponData_get_movementSpeed_m68D54FAA18F29BE81FEB3B5CF85DDBA76BC71790 (void);
// 0x00000370 System.Void SO_WeaponData::set_movementSpeed(System.Single[])
extern void SO_WeaponData_set_movementSpeed_m832F6C640FEB51C01445F1FA27D3E53972111A1E (void);
// 0x00000371 System.Void SO_WeaponData::.ctor()
extern void SO_WeaponData__ctor_mB19489CDDB758BA6E49A53DEC6793FBEE60CBD62 (void);
// 0x00000372 System.Void Weapon::Awake()
extern void Weapon_Awake_mCFA0CE996E9FAA699B578ABB8796DED719536A5B (void);
// 0x00000373 System.Void Weapon::EnterWeapon()
extern void Weapon_EnterWeapon_m7088D4FBC7443AD739E3F5EAF62D4C21DA78B397 (void);
// 0x00000374 System.Void Weapon::ExitWeapon()
extern void Weapon_ExitWeapon_mCCE913A59A9611B6711AAEDBAB6DD89C656FD2B5 (void);
// 0x00000375 System.Void Weapon::AnimationFinishTrigger()
extern void Weapon_AnimationFinishTrigger_m7D77C3F4F97BB179AC7B4792D903B228398A8966 (void);
// 0x00000376 System.Void Weapon::AnimationStartMovementTrigger()
extern void Weapon_AnimationStartMovementTrigger_m161DF91F29F965F3E4509C17B262E478AF75BE96 (void);
// 0x00000377 System.Void Weapon::AnimationStopMovementTrigger()
extern void Weapon_AnimationStopMovementTrigger_m217753007AD7E9DA2E763913B77EDF0AFF77CE5A (void);
// 0x00000378 System.Void Weapon::AnimationTurnOnFlipTrigger()
extern void Weapon_AnimationTurnOnFlipTrigger_mC3B0DED763FF7A3A890B779ABE37A8442A92C024 (void);
// 0x00000379 System.Void Weapon::AnimationTurnOffFlipTrigger()
extern void Weapon_AnimationTurnOffFlipTrigger_mBD8C12FB7BF1DE16EE8B9E9857F4CDD53B63D66C (void);
// 0x0000037A System.Void Weapon::AnimationActionTrigger()
extern void Weapon_AnimationActionTrigger_m85C267206A4386E0E24B6D2EC60250747D249C24 (void);
// 0x0000037B System.Void Weapon::InitializeWeapon(PlayerAttackState,Core)
extern void Weapon_InitializeWeapon_m854207E6153645B0FA1F314DD47A8FD8AFE5EBC2 (void);
// 0x0000037C System.Void Weapon::.ctor()
extern void Weapon__ctor_m643DE56148B24BD987E564400E443ACDF43CDB97 (void);
// 0x0000037D System.Void Projectile::Start()
extern void Projectile_Start_m065C53350564E17D5A0A0322FF064F8C9697DAB6 (void);
// 0x0000037E System.Void Projectile::Update()
extern void Projectile_Update_m382C5B499BD4599FE34A04DA3DA0701077C710B2 (void);
// 0x0000037F System.Void Projectile::FixedUpdate()
extern void Projectile_FixedUpdate_m18A97CA278CB70690315AFF74AC809179B2A94B1 (void);
// 0x00000380 System.Void Projectile::FireProjectile(System.Single,System.Single,System.Single)
extern void Projectile_FireProjectile_m9AE7A2D3E94C499EBC6EA0EA8AB251DBC29AAD95 (void);
// 0x00000381 System.Void Projectile::OnDrawGizmos()
extern void Projectile_OnDrawGizmos_m0F4890032EFAD3EE1F22111B5941CDA89FD61E71 (void);
// 0x00000382 System.Void Projectile::.ctor()
extern void Projectile__ctor_m22DAC83BA9B394316027755FD2ADFCA806EE39BB (void);
// 0x00000383 System.Void Restart::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Restart_OnTriggerEnter2D_m1D4019138EAB429FC9B0D70E37C95D2E9627B279 (void);
// 0x00000384 System.Void Restart::.ctor()
extern void Restart__ctor_mC040D6C84CCAA3DF8BE8CDBF1C030FBAA1331FAE (void);
// 0x00000385 System.Void Spikes::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void Spikes_OnTriggerEnter2D_mFE312F753147C6FFDBEA498886ED1501B19DDEAA (void);
// 0x00000386 System.Void Spikes::.ctor()
extern void Spikes__ctor_m97F99FE6120BD57A3783EEEB43244791DCE0511D (void);
// 0x00000387 T GenericNotImplementedError`1::TryToGet(T,System.String)
// 0x00000388 System.Void IDamageable::Damage(System.Single)
// 0x00000389 System.Void IKnockbackable::Knockback(UnityEngine.Vector2,System.Single,System.Int32)
// 0x0000038A System.Void ILogicUpdate::LogicUpdate()
// 0x0000038B System.Void AnimationToFSM::TriggerAttack()
extern void AnimationToFSM_TriggerAttack_m50D927BAB09702F735F6DC293240CC326D1773AB (void);
// 0x0000038C System.Void AnimationToFSM::FinishAttack()
extern void AnimationToFSM_FinishAttack_m15F4B3990243E65C56C3130EB9A0382A8C02DC0E (void);
// 0x0000038D System.Void AnimationToFSM::.ctor()
extern void AnimationToFSM__ctor_mD2712448244F972BEE4CCBEFE10A36C762097E00 (void);
// 0x0000038E System.Void WeaponAnimationToWeapon::Start()
extern void WeaponAnimationToWeapon_Start_m0D39E00F77A9ECA2E98E58A49938975FC299B77A (void);
// 0x0000038F System.Void WeaponAnimationToWeapon::AnimationFinishTrigger()
extern void WeaponAnimationToWeapon_AnimationFinishTrigger_m453082E0A0AA5787477C203ACF3006272F0FA882 (void);
// 0x00000390 System.Void WeaponAnimationToWeapon::AnimationStartMovementTrigger()
extern void WeaponAnimationToWeapon_AnimationStartMovementTrigger_m5ACCD871C6E32E56C0FE6366E6121652C566170C (void);
// 0x00000391 System.Void WeaponAnimationToWeapon::AnimationStopMovementTrigger()
extern void WeaponAnimationToWeapon_AnimationStopMovementTrigger_m83C8AA63F240B9B80A10594B983EA41A7BB14378 (void);
// 0x00000392 System.Void WeaponAnimationToWeapon::AnimationTurnOnFlipTrigger()
extern void WeaponAnimationToWeapon_AnimationTurnOnFlipTrigger_m028F7DDEB8079C35A0D52D67B7F7CCC7BC1FA212 (void);
// 0x00000393 System.Void WeaponAnimationToWeapon::AnimationTurnOffFlipTrigger()
extern void WeaponAnimationToWeapon_AnimationTurnOffFlipTrigger_m38278DC3E5963B58D77C377D6CD26A91EBD4BBDD (void);
// 0x00000394 System.Void WeaponAnimationToWeapon::AnimationActionTrigger()
extern void WeaponAnimationToWeapon_AnimationActionTrigger_m98697E1848D8513F571F47D24ED6E26C9300CFA1 (void);
// 0x00000395 System.Void WeaponAnimationToWeapon::.ctor()
extern void WeaponAnimationToWeapon__ctor_mC7DC3574700B82D532EC8DEAB4B30532E0F80BDB (void);
// 0x00000396 System.Void WeaponHitboxToWeapon::Awake()
extern void WeaponHitboxToWeapon_Awake_m8C2CE82D19ED70F01CC7F23A33DB3AEA3667982E (void);
// 0x00000397 System.Void WeaponHitboxToWeapon::OnTriggerEnter2D(UnityEngine.Collider2D)
extern void WeaponHitboxToWeapon_OnTriggerEnter2D_mC7AD74F0DE45E8C488F3A7B164B877EA9DFA6266 (void);
// 0x00000398 System.Void WeaponHitboxToWeapon::OnTriggerStay2D(UnityEngine.Collider2D)
extern void WeaponHitboxToWeapon_OnTriggerStay2D_mDCEAF9C124CB9D7EB99BB06DE5B131B4B966FDC1 (void);
// 0x00000399 System.Void WeaponHitboxToWeapon::OnTriggerExit2D(UnityEngine.Collider2D)
extern void WeaponHitboxToWeapon_OnTriggerExit2D_m8A44B5668F56ED64507639B2DDC738C6050A1758 (void);
// 0x0000039A System.Void WeaponHitboxToWeapon::.ctor()
extern void WeaponHitboxToWeapon__ctor_mF1A8F9C8827DC66E129A9E9258915B7AD3CB0491 (void);
// 0x0000039B System.Void ChatController::OnEnable()
extern void ChatController_OnEnable_mBC3BFC05A1D47069DFA58A4F0E39CFF8FAD44FF2 (void);
// 0x0000039C System.Void ChatController::OnDisable()
extern void ChatController_OnDisable_m268A7488A3FDEB850FC3BC91A0BBDA767D42876B (void);
// 0x0000039D System.Void ChatController::AddToChatOutput(System.String)
extern void ChatController_AddToChatOutput_m43856EEA133E04C24701A2616E7F10D7FFA68371 (void);
// 0x0000039E System.Void ChatController::.ctor()
extern void ChatController__ctor_m3B66A5F749B457D865E8BDA1DE481C8CF1158026 (void);
// 0x0000039F System.Void DropdownSample::OnButtonClick()
extern void DropdownSample_OnButtonClick_m97641451A2EAC4FC6682897A948A37FDD6BF9EA6 (void);
// 0x000003A0 System.Void DropdownSample::.ctor()
extern void DropdownSample__ctor_m87664E3DC75A1A9AA19FEE6ED6B993AB1E50E2B9 (void);
// 0x000003A1 System.Void EnvMapAnimator::Awake()
extern void EnvMapAnimator_Awake_mFFC04BC5320F83CA8C45FF36A11BF43AC17C6B93 (void);
// 0x000003A2 System.Collections.IEnumerator EnvMapAnimator::Start()
extern void EnvMapAnimator_Start_mC0D348CAB0F0DC920EF3D3A008688B533F66D1BE (void);
// 0x000003A3 System.Void EnvMapAnimator::.ctor()
extern void EnvMapAnimator__ctor_mC151D673A394E2E7CEA8774C4004985028C5C3EC (void);
// 0x000003A4 System.Void EnvMapAnimator/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m4D65F4FC2207AE4B6BE963AF9B5EDC55C7E29B23 (void);
// 0x000003A5 System.Void EnvMapAnimator/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_mFEE2ACED70A3D825988E28CC61FEF8DCD7660A5B (void);
// 0x000003A6 System.Boolean EnvMapAnimator/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_m2F1A8053A32AD86DA80F86391EC32EDC1C396AEE (void);
// 0x000003A7 System.Object EnvMapAnimator/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03E2A57659B8F598AC450183F55D43F903C0A1E (void);
// 0x000003A8 System.Void EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m268BA538CF6812C56EB281C0CE29D5AA2E9A2CAB (void);
// 0x000003A9 System.Object EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mFE9CA9F52F9AFA1E4C9DBADA2064F854D6931CFF (void);
// 0x000003AA System.Char TMPro.TMP_DigitValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_DigitValidator_Validate_m5D303EB8CD6E9E7D526D633BA1021884051FE226 (void);
// 0x000003AB System.Void TMPro.TMP_DigitValidator::.ctor()
extern void TMP_DigitValidator__ctor_m1E838567EF38170662F1BAF52A847CC2C258333E (void);
// 0x000003AC System.Char TMPro.TMP_PhoneNumberValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_PhoneNumberValidator_Validate_mF0E90A277E9E91BC213DD02DC60088D03C9436B1 (void);
// 0x000003AD System.Void TMPro.TMP_PhoneNumberValidator::.ctor()
extern void TMP_PhoneNumberValidator__ctor_mB3C36CAAE3B52554C44A1D19194F0176B5A8EED3 (void);
// 0x000003AE TMPro.TMP_TextEventHandler/CharacterSelectionEvent TMPro.TMP_TextEventHandler::get_onCharacterSelection()
extern void TMP_TextEventHandler_get_onCharacterSelection_m90C39320C726E8E542D91F4BBD690697349F5385 (void);
// 0x000003AF System.Void TMPro.TMP_TextEventHandler::set_onCharacterSelection(TMPro.TMP_TextEventHandler/CharacterSelectionEvent)
extern void TMP_TextEventHandler_set_onCharacterSelection_m5ED7658EB101C6740A921FA150DE18C443BDA0C4 (void);
// 0x000003B0 TMPro.TMP_TextEventHandler/SpriteSelectionEvent TMPro.TMP_TextEventHandler::get_onSpriteSelection()
extern void TMP_TextEventHandler_get_onSpriteSelection_m0E645AE1DFE19B011A3319474D0CF0DA612C8B7B (void);
// 0x000003B1 System.Void TMPro.TMP_TextEventHandler::set_onSpriteSelection(TMPro.TMP_TextEventHandler/SpriteSelectionEvent)
extern void TMP_TextEventHandler_set_onSpriteSelection_m4AFC6772C3357218956A5D33B3CD19F3AAF39788 (void);
// 0x000003B2 TMPro.TMP_TextEventHandler/WordSelectionEvent TMPro.TMP_TextEventHandler::get_onWordSelection()
extern void TMP_TextEventHandler_get_onWordSelection_mA42B89A37810FB659FCFA8539339A3BB8037203A (void);
// 0x000003B3 System.Void TMPro.TMP_TextEventHandler::set_onWordSelection(TMPro.TMP_TextEventHandler/WordSelectionEvent)
extern void TMP_TextEventHandler_set_onWordSelection_m4A839FAFFABAFECD073B82BA8826E1CD033C0076 (void);
// 0x000003B4 TMPro.TMP_TextEventHandler/LineSelectionEvent TMPro.TMP_TextEventHandler::get_onLineSelection()
extern void TMP_TextEventHandler_get_onLineSelection_mB701B6C713AD4EFC61E1B30A564EE54ADE31F58D (void);
// 0x000003B5 System.Void TMPro.TMP_TextEventHandler::set_onLineSelection(TMPro.TMP_TextEventHandler/LineSelectionEvent)
extern void TMP_TextEventHandler_set_onLineSelection_m0B2337598350E51D0A17B8FCB3AAA533F312F3DA (void);
// 0x000003B6 TMPro.TMP_TextEventHandler/LinkSelectionEvent TMPro.TMP_TextEventHandler::get_onLinkSelection()
extern void TMP_TextEventHandler_get_onLinkSelection_mF5C3875D661F5B1E3712566FE16D332EA37D15BB (void);
// 0x000003B7 System.Void TMPro.TMP_TextEventHandler::set_onLinkSelection(TMPro.TMP_TextEventHandler/LinkSelectionEvent)
extern void TMP_TextEventHandler_set_onLinkSelection_m200566EDEB2C9299647F3EEAC588B51818D360A5 (void);
// 0x000003B8 System.Void TMPro.TMP_TextEventHandler::Awake()
extern void TMP_TextEventHandler_Awake_m43EB03A4A6776A624F79457EC49E78E7B5BA1C70 (void);
// 0x000003B9 System.Void TMPro.TMP_TextEventHandler::LateUpdate()
extern void TMP_TextEventHandler_LateUpdate_mE1D989C40DA8E54E116E3C60217DFCAADD6FDE11 (void);
// 0x000003BA System.Void TMPro.TMP_TextEventHandler::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerEnter_m8FC88F25858B24CE68BE80C727A3F0227A8EE5AC (void);
// 0x000003BB System.Void TMPro.TMP_TextEventHandler::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerExit_mBB2A74D55741F631A678A5D6997D40247FE75D44 (void);
// 0x000003BC System.Void TMPro.TMP_TextEventHandler::SendOnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnCharacterSelection_m78983D3590F1B0C242BEAB0A11FDBDABDD1814EC (void);
// 0x000003BD System.Void TMPro.TMP_TextEventHandler::SendOnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnSpriteSelection_m10C257A74F121B95E7077F7E488FBC52380A6C53 (void);
// 0x000003BE System.Void TMPro.TMP_TextEventHandler::SendOnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnWordSelection_m95A4E5A00E339E5B8BA9AA63B98DB3E81C8B8F66 (void);
// 0x000003BF System.Void TMPro.TMP_TextEventHandler::SendOnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnLineSelection_mE85BA6ECE1188B666FD36839B8970C3E0130BC43 (void);
// 0x000003C0 System.Void TMPro.TMP_TextEventHandler::SendOnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventHandler_SendOnLinkSelection_m299E6620DFD835C8258A3F48B4EA076C304E9B77 (void);
// 0x000003C1 System.Void TMPro.TMP_TextEventHandler::.ctor()
extern void TMP_TextEventHandler__ctor_m6345DC1CEA2E4209E928AD1E61C3ACA4227DD9B8 (void);
// 0x000003C2 System.Void TMPro.TMP_TextEventHandler/CharacterSelectionEvent::.ctor()
extern void CharacterSelectionEvent__ctor_m06BC183AF31BA4A2055A44514BC3FF0539DD04C7 (void);
// 0x000003C3 System.Void TMPro.TMP_TextEventHandler/SpriteSelectionEvent::.ctor()
extern void SpriteSelectionEvent__ctor_m0F760052E9A5AF44A7AF7AC006CB4B24809590F2 (void);
// 0x000003C4 System.Void TMPro.TMP_TextEventHandler/WordSelectionEvent::.ctor()
extern void WordSelectionEvent__ctor_m106CDEB17C520C9D20CE7120DE6BBBDEDB48886C (void);
// 0x000003C5 System.Void TMPro.TMP_TextEventHandler/LineSelectionEvent::.ctor()
extern void LineSelectionEvent__ctor_m5D735FDA9B71B9147C6F791B331498F145D75018 (void);
// 0x000003C6 System.Void TMPro.TMP_TextEventHandler/LinkSelectionEvent::.ctor()
extern void LinkSelectionEvent__ctor_m7AB7977D0D0F8883C5A98DD6BB2D390BC3CAB8E0 (void);
// 0x000003C7 System.Collections.IEnumerator TMPro.Examples.Benchmark01::Start()
extern void Benchmark01_Start_mE7E5146B0D8D926CC410CAA48F3B617A0A7D955C (void);
// 0x000003C8 System.Void TMPro.Examples.Benchmark01::.ctor()
extern void Benchmark01__ctor_mB92568AA7A9E13B92315B6270FCA23584A7D0F7F (void);
// 0x000003C9 System.Void TMPro.Examples.Benchmark01/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_mBE5D8B4B98C372BD6DC3936437999DF3048DE3AB (void);
// 0x000003CA System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_m1F4180A7FDAE9AC5F11F30B67F813B3D7CD56D73 (void);
// 0x000003CB System.Boolean TMPro.Examples.Benchmark01/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mF3B1D38CD37FD5187C3192141DB380747C66AD3C (void);
// 0x000003CC System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22B2975D42326E3DE437174BBE4F0E8790CB6591 (void);
// 0x000003CD System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m514DA545ECA10AA62BFC239BB582FF0017B91D2B (void);
// 0x000003CE System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m6904A738F04118BA24E473F1809B3D69E711627E (void);
// 0x000003CF System.Collections.IEnumerator TMPro.Examples.Benchmark01_UGUI::Start()
extern void Benchmark01_UGUI_Start_m2F8F9E1798943DA8086A7C7E73BA9D7C67482BD4 (void);
// 0x000003D0 System.Void TMPro.Examples.Benchmark01_UGUI::.ctor()
extern void Benchmark01_UGUI__ctor_mACFE8D997EAB50FDBD7F671C1A25A892D9F78376 (void);
// 0x000003D1 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m653B757B49A674E239C804FFF4EDEF325B5DB651 (void);
// 0x000003D2 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_mAD0BC985E1E01C8369A7A955A60D3E545B0B3561 (void);
// 0x000003D3 System.Boolean TMPro.Examples.Benchmark01_UGUI/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mF9B2C8D290035BC9A79C4347772B5B7B9D404DE1 (void);
// 0x000003D4 System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF438D42D7C5A811FF9A283F8780F015FF55826CD (void);
// 0x000003D5 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m65545EB5B6DDDDAB5ACF92600137C1C754B322BA (void);
// 0x000003D6 System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD4523BE1D1014AAF1C0709EB767C0B0F47D739D2 (void);
// 0x000003D7 System.Void TMPro.Examples.Benchmark02::Start()
extern void Benchmark02_Start_m315C0DF3A9AC66B4F5802FDAED056E7E5F3D2046 (void);
// 0x000003D8 System.Void TMPro.Examples.Benchmark02::.ctor()
extern void Benchmark02__ctor_m526B38D3B7E75905208E4E57B168D8AC1900F4E5 (void);
// 0x000003D9 System.Void TMPro.Examples.Benchmark03::Awake()
extern void Benchmark03_Awake_mD23429C1EE428EEF4ED9A753385BF194BAA41A77 (void);
// 0x000003DA System.Void TMPro.Examples.Benchmark03::Start()
extern void Benchmark03_Start_m19E638DED29CB7F96362F3C346DC47217C4AF516 (void);
// 0x000003DB System.Void TMPro.Examples.Benchmark03::.ctor()
extern void Benchmark03__ctor_m89F9102259BE1694EC418E9023DC563F3999B0BE (void);
// 0x000003DC System.Void TMPro.Examples.Benchmark04::Start()
extern void Benchmark04_Start_mEABD467C12547066E33359FDC433E8B5BAD43DB9 (void);
// 0x000003DD System.Void TMPro.Examples.Benchmark04::.ctor()
extern void Benchmark04__ctor_m5015375E0C8D19060CB5DE14CBF4AC02DDD70E7C (void);
// 0x000003DE System.Void TMPro.Examples.CameraController::Awake()
extern void CameraController_Awake_m420393892377B9703EC97764B34E32890FC5283E (void);
// 0x000003DF System.Void TMPro.Examples.CameraController::Start()
extern void CameraController_Start_m32D90EE7232BE6DE08D224F824F8EB9571655610 (void);
// 0x000003E0 System.Void TMPro.Examples.CameraController::LateUpdate()
extern void CameraController_LateUpdate_m6D81DEBA4E8B443CF2AD8288F0E76E3A6B3B5373 (void);
// 0x000003E1 System.Void TMPro.Examples.CameraController::GetPlayerInput()
extern void CameraController_GetPlayerInput_m6695FE20CFC691585A6AC279EDB338EC9DD13FE3 (void);
// 0x000003E2 System.Void TMPro.Examples.CameraController::.ctor()
extern void CameraController__ctor_m09187FB27B590118043D4DC7B89B93164124C124 (void);
// 0x000003E3 System.Void TMPro.Examples.ObjectSpin::Awake()
extern void ObjectSpin_Awake_mC1EB9630B6D3BAE645D3DD79C264F71F7B18A1AA (void);
// 0x000003E4 System.Void TMPro.Examples.ObjectSpin::Update()
extern void ObjectSpin_Update_mD39DCBA0789DC0116037C442F0BA1EE6752E36D3 (void);
// 0x000003E5 System.Void TMPro.Examples.ObjectSpin::.ctor()
extern void ObjectSpin__ctor_m1827B9648659746252026432DFED907AEC6007FC (void);
// 0x000003E6 System.Void TMPro.Examples.ShaderPropAnimator::Awake()
extern void ShaderPropAnimator_Awake_mE04C66A41CA53AB73733E7D2CCD21B30A360C5A8 (void);
// 0x000003E7 System.Void TMPro.Examples.ShaderPropAnimator::Start()
extern void ShaderPropAnimator_Start_mC5AC59C59AF2F71E0CF65F11ACD787488140EDD2 (void);
// 0x000003E8 System.Collections.IEnumerator TMPro.Examples.ShaderPropAnimator::AnimateProperties()
extern void ShaderPropAnimator_AnimateProperties_mAF49CD157AD41377CE00AA10F0C06C8BF5AA0469 (void);
// 0x000003E9 System.Void TMPro.Examples.ShaderPropAnimator::.ctor()
extern void ShaderPropAnimator__ctor_mAA626BC8AEEB00C5AE362FE8690D3F2200CE6E64 (void);
// 0x000003EA System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::.ctor(System.Int32)
extern void U3CAnimatePropertiesU3Ed__6__ctor_mFAC0F8A7368D9D35AD2780C118E13414DA79B56A (void);
// 0x000003EB System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.IDisposable.Dispose()
extern void U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m628EECBFCBC49087298185F17BC2AE7A73FC8A7B (void);
// 0x000003EC System.Boolean TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::MoveNext()
extern void U3CAnimatePropertiesU3Ed__6_MoveNext_m8A58CCFDAE59F55AB1BB2C103800886E62C807CF (void);
// 0x000003ED System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0742A0FA62C518EBD119ED5FEBF849F918E12390 (void);
// 0x000003EE System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.Reset()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mC076F281DD17668D6CE42EB04EA974DE1FFB3F6B (void);
// 0x000003EF System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mA585E786DA86D2589EBCA029AD9E64E8B5135362 (void);
// 0x000003F0 System.Void TMPro.Examples.SimpleScript::Start()
extern void SimpleScript_Start_m75CD9CEDCAFA9A991753478D7C28407C7329FB8F (void);
// 0x000003F1 System.Void TMPro.Examples.SimpleScript::Update()
extern void SimpleScript_Update_mE8C3930DCC1767C2DF59B622FABD188E6FB91BE5 (void);
// 0x000003F2 System.Void TMPro.Examples.SimpleScript::.ctor()
extern void SimpleScript__ctor_mEB370A69544227EF04C48C604A28502ADAA73697 (void);
// 0x000003F3 System.Void TMPro.Examples.SkewTextExample::Awake()
extern void SkewTextExample_Awake_m6FBA7E7DC9AFDD3099F0D0B9CDE91574551105DB (void);
// 0x000003F4 System.Void TMPro.Examples.SkewTextExample::Start()
extern void SkewTextExample_Start_m536F82F3A5D229332694688C59F396E07F88153F (void);
// 0x000003F5 UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void SkewTextExample_CopyAnimationCurve_m555177255F5828DBC7E677ED06F7EFFC052886B3 (void);
// 0x000003F6 System.Collections.IEnumerator TMPro.Examples.SkewTextExample::WarpText()
extern void SkewTextExample_WarpText_m0E0C46988600673F0E5DFA3133534DC6CA5950D3 (void);
// 0x000003F7 System.Void TMPro.Examples.SkewTextExample::.ctor()
extern void SkewTextExample__ctor_m945059906734DD38CF860CD32B3E07635D0E1F86 (void);
// 0x000003F8 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__7__ctor_mEAD3C39209B75514446A44B6C2FA76F8097EBD6F (void);
// 0x000003F9 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m32AA7120BE15547799BDC515FA3486488952BDCF (void);
// 0x000003FA System.Boolean TMPro.Examples.SkewTextExample/<WarpText>d__7::MoveNext()
extern void U3CWarpTextU3Ed__7_MoveNext_mB832E4A3DFDDFECC460A510BBC664F218B3D7FCF (void);
// 0x000003FB System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CF506B740C16FEDE58B6E1BE4556CAC2C6AEAD1 (void);
// 0x000003FC System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mDDCDBB794F21DF70178A75747D8D8398F38A9C2F (void);
// 0x000003FD System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m6D89CF1910F76577E23C924B535E93FD1B01CF0A (void);
// 0x000003FE System.Void TMPro.Examples.TMP_ExampleScript_01::Awake()
extern void TMP_ExampleScript_01_Awake_m381EF5B50E1D012B8CA1883DEFF604EEAE6D95F4 (void);
// 0x000003FF System.Void TMPro.Examples.TMP_ExampleScript_01::Update()
extern void TMP_ExampleScript_01_Update_m31611F22A608784F164A24444195B33B714567FB (void);
// 0x00000400 System.Void TMPro.Examples.TMP_ExampleScript_01::.ctor()
extern void TMP_ExampleScript_01__ctor_m3641F2E0D25B6666CE77773A4A493C4C4D3D3A12 (void);
// 0x00000401 System.Void TMPro.Examples.TMP_FrameRateCounter::Awake()
extern void TMP_FrameRateCounter_Awake_m958B668086DB4A38D9C56E3F8C2DCCB6FF11FAA3 (void);
// 0x00000402 System.Void TMPro.Examples.TMP_FrameRateCounter::Start()
extern void TMP_FrameRateCounter_Start_mC642CA714D0FB8482D2AC6192D976DA179EE3720 (void);
// 0x00000403 System.Void TMPro.Examples.TMP_FrameRateCounter::Update()
extern void TMP_FrameRateCounter_Update_m5E41B55573D86C3D345BFE11C489B00229BCEE21 (void);
// 0x00000404 System.Void TMPro.Examples.TMP_FrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_FrameRateCounter_Set_FrameCounter_Position_mA72ECDE464E3290E4F533491FC8113B8D4068BE1 (void);
// 0x00000405 System.Void TMPro.Examples.TMP_FrameRateCounter::.ctor()
extern void TMP_FrameRateCounter__ctor_mC79D5BF3FAE2BB7D22D9955A75E3483725BA619C (void);
// 0x00000406 System.Void TMPro.Examples.TMP_TextEventCheck::OnEnable()
extern void TMP_TextEventCheck_OnEnable_mE6D5125EEE0720E6F414978A496066E7CF1592DF (void);
// 0x00000407 System.Void TMPro.Examples.TMP_TextEventCheck::OnDisable()
extern void TMP_TextEventCheck_OnDisable_m94F087C259890C48D4F5464ABA4CD1D0E5863A9A (void);
// 0x00000408 System.Void TMPro.Examples.TMP_TextEventCheck::OnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnCharacterSelection_mDA044F0808D61A99CDA374075943CEB1C92C253D (void);
// 0x00000409 System.Void TMPro.Examples.TMP_TextEventCheck::OnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnSpriteSelection_mE8FFA550F38F5447CA37205698A30A41ADE901E0 (void);
// 0x0000040A System.Void TMPro.Examples.TMP_TextEventCheck::OnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnWordSelection_m6037166D18A938678A2B06F28A5DCB3E09FAC61B (void);
// 0x0000040B System.Void TMPro.Examples.TMP_TextEventCheck::OnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnLineSelection_mD2BAA6C8ABD61F0442A40C5448FA7774D782C363 (void);
// 0x0000040C System.Void TMPro.Examples.TMP_TextEventCheck::OnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventCheck_OnLinkSelection_m184B11D5E35AC4EA7CDCE3340AB9FEE3C14BF41C (void);
// 0x0000040D System.Void TMPro.Examples.TMP_TextEventCheck::.ctor()
extern void TMP_TextEventCheck__ctor_mA5E82EE7CE8F8836FC6CF3823CBC7356005B898B (void);
// 0x0000040E System.Void TMPro.Examples.TMP_TextInfoDebugTool::.ctor()
extern void TMP_TextInfoDebugTool__ctor_mF6AA30660FBD4CE708B6147833853498993CB9EE (void);
// 0x0000040F System.Void TMPro.Examples.TMP_TextSelector_A::Awake()
extern void TMP_TextSelector_A_Awake_mD7252A5075E30E3BF9BEF4353F7AA2A9203FC943 (void);
// 0x00000410 System.Void TMPro.Examples.TMP_TextSelector_A::LateUpdate()
extern void TMP_TextSelector_A_LateUpdate_mDBBC09726332EDDEAF7C30AB6C08FB33261F79FD (void);
// 0x00000411 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerEnter_mC19B85FB5B4E6EB47832C39F0115A513B85060D0 (void);
// 0x00000412 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerExit_mE51C850F54B18A5C96E3BE015DFBB0F079E5AE66 (void);
// 0x00000413 System.Void TMPro.Examples.TMP_TextSelector_A::.ctor()
extern void TMP_TextSelector_A__ctor_mEB83D3952B32CEE9A871EC60E3AE9B79048302BF (void);
// 0x00000414 System.Void TMPro.Examples.TMP_TextSelector_B::Awake()
extern void TMP_TextSelector_B_Awake_m7E413A43C54DF9E0FE70C4E69FC682391B47205A (void);
// 0x00000415 System.Void TMPro.Examples.TMP_TextSelector_B::OnEnable()
extern void TMP_TextSelector_B_OnEnable_m0828D13E2D407B90038442228D54FB0D7B3D29FB (void);
// 0x00000416 System.Void TMPro.Examples.TMP_TextSelector_B::OnDisable()
extern void TMP_TextSelector_B_OnDisable_m2B559A85B52C8CAFC7350CC7B4F8E5BC773EF781 (void);
// 0x00000417 System.Void TMPro.Examples.TMP_TextSelector_B::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TMP_TextSelector_B_ON_TEXT_CHANGED_m1597DBE7C7EBE7CFA4DC395897A4779387B59910 (void);
// 0x00000418 System.Void TMPro.Examples.TMP_TextSelector_B::LateUpdate()
extern void TMP_TextSelector_B_LateUpdate_m8BA10E368C7F3483E9EC05589BBE45D7EFC75691 (void);
// 0x00000419 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerEnter_m0A0064632E4C0E0ADCD4358AD5BC168BFB74AC4D (void);
// 0x0000041A System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerExit_m667659102B5B17FBAF56593B7034E5EC1C48D43F (void);
// 0x0000041B System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerClick_m8DB2D22EE2F4D3965115791C81F985D82021469F (void);
// 0x0000041C System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerUp_m303500BE309B56F3ADCE8B461CEC50C8D5ED70BC (void);
// 0x0000041D System.Void TMPro.Examples.TMP_TextSelector_B::RestoreCachedVertexAttributes(System.Int32)
extern void TMP_TextSelector_B_RestoreCachedVertexAttributes_m3D637CF2C6CA663922EBE56FFD672BE582E9F1F2 (void);
// 0x0000041E System.Void TMPro.Examples.TMP_TextSelector_B::.ctor()
extern void TMP_TextSelector_B__ctor_mE654F9F1570570C4BACDA79640B8DB3033D91C33 (void);
// 0x0000041F System.Void TMPro.Examples.TMP_UiFrameRateCounter::Awake()
extern void TMP_UiFrameRateCounter_Awake_m83DBE22B6CC551BE5E385048B92067679716179E (void);
// 0x00000420 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Start()
extern void TMP_UiFrameRateCounter_Start_m59DA342A492C9EF8AD5C4512753211BF3796C944 (void);
// 0x00000421 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Update()
extern void TMP_UiFrameRateCounter_Update_m28FC233C475AA15A3BE399BF9345977089997749 (void);
// 0x00000422 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_UiFrameRateCounter_Set_FrameCounter_Position_m2025933902F312C0EC4B6A864196A8BA8D545647 (void);
// 0x00000423 System.Void TMPro.Examples.TMP_UiFrameRateCounter::.ctor()
extern void TMP_UiFrameRateCounter__ctor_m5774BF4B9389770FC34991095557B24417600F84 (void);
// 0x00000424 System.Void TMPro.Examples.TMPro_InstructionOverlay::Awake()
extern void TMPro_InstructionOverlay_Awake_m2444EA8749D72BCEA4DC30A328E3745AB19062EC (void);
// 0x00000425 System.Void TMPro.Examples.TMPro_InstructionOverlay::Set_FrameCounter_Position(TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions)
extern void TMPro_InstructionOverlay_Set_FrameCounter_Position_m62B897E4DB2D6B1936833EC54D9C246D68D50EEB (void);
// 0x00000426 System.Void TMPro.Examples.TMPro_InstructionOverlay::.ctor()
extern void TMPro_InstructionOverlay__ctor_m7AB5B851B4BFB07547E460D6B7B9D969DEB4A7CF (void);
// 0x00000427 System.Void TMPro.Examples.TeleType::Awake()
extern void TeleType_Awake_m099FCB202F2A8299B133DC56ECB9D01A16C08DFE (void);
// 0x00000428 System.Collections.IEnumerator TMPro.Examples.TeleType::Start()
extern void TeleType_Start_m72DE9DE597F4FA0B1CA02115CBC1E74EB53F63F7 (void);
// 0x00000429 System.Void TMPro.Examples.TeleType::.ctor()
extern void TeleType__ctor_m12A79B34F66CBFDCA8F582F0689D1731586B90AF (void);
// 0x0000042A System.Void TMPro.Examples.TeleType/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_mF3575DBEBF4F153F6A899AF3362940298C11B629 (void);
// 0x0000042B System.Void TMPro.Examples.TeleType/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m6DABFDBC2A313BF6DD90AA1C43B4EF9D6249CBE9 (void);
// 0x0000042C System.Boolean TMPro.Examples.TeleType/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_mEF7A3215376BDFB52C3DA9D26FF0459074E89715 (void);
// 0x0000042D System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DC06D52865D30DAA78DBD70B6554D13E2EA70B (void);
// 0x0000042E System.Void TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mE854A2CECA0E01F3C2D90DA4F720CF0F387994F8 (void);
// 0x0000042F System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m77D3936B94684C817EE9F6D2C903238A457D5261 (void);
// 0x00000430 System.Void TMPro.Examples.TextConsoleSimulator::Awake()
extern void TextConsoleSimulator_Awake_mA51EE182A528CCA5CAEA8DBE8AD30E43FDFAE7B0 (void);
// 0x00000431 System.Void TMPro.Examples.TextConsoleSimulator::Start()
extern void TextConsoleSimulator_Start_m429701BE4C9A52AA6B257172A4D85D58E091E7DA (void);
// 0x00000432 System.Void TMPro.Examples.TextConsoleSimulator::OnEnable()
extern void TextConsoleSimulator_OnEnable_m3397FBCDA8D9DA264D2149288BE4DCF63368AB50 (void);
// 0x00000433 System.Void TMPro.Examples.TextConsoleSimulator::OnDisable()
extern void TextConsoleSimulator_OnDisable_m43DF869E864789E215873192ECFCDC51ABA79712 (void);
// 0x00000434 System.Void TMPro.Examples.TextConsoleSimulator::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TextConsoleSimulator_ON_TEXT_CHANGED_m51F06EE5DD9B32FEFB529743F209C6E6C41BE3B8 (void);
// 0x00000435 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealCharacters(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealCharacters_mE8E415644F7BD2056D0809F7BB0FA9A2F9FE8534 (void);
// 0x00000436 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealWords(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealWords_mFDBC863D30BC63ADCF2860F303AF252E27D0F4F4 (void);
// 0x00000437 System.Void TMPro.Examples.TextConsoleSimulator::.ctor()
extern void TextConsoleSimulator__ctor_m4719FB9D4D89F37234757D93876DF0193E8E2848 (void);
// 0x00000438 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::.ctor(System.Int32)
extern void U3CRevealCharactersU3Ed__7__ctor_mD45A85F5F50909F70C80AC8CE460F4FD261CDE9D (void);
// 0x00000439 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.IDisposable.Dispose()
extern void U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m85F270FDC11A4D79E9CF47AADC9FA1FBF032F86C (void);
// 0x0000043A System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::MoveNext()
extern void U3CRevealCharactersU3Ed__7_MoveNext_mA9A6555E50889A7AA73F20749F25989165023DE3 (void);
// 0x0000043B System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BBAE6868EB7F1C11BE5DF001641E18C9D625F83 (void);
// 0x0000043C System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.Reset()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mA9F4893381AB24E01EAEBFC38A88AF363A9A0691 (void);
// 0x0000043D System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_mFD0B7538B1A650FB389FFE9296B0E51AEA5B6B6F (void);
// 0x0000043E System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::.ctor(System.Int32)
extern void U3CRevealWordsU3Ed__8__ctor_m22FF9E770988107A928C5D1EA639F60239BFFEF0 (void);
// 0x0000043F System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.IDisposable.Dispose()
extern void U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m3EAF6EF4A8C99A71FEED251BF3F28A26BF6AD7F8 (void);
// 0x00000440 System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::MoveNext()
extern void U3CRevealWordsU3Ed__8_MoveNext_m3811753E2384D4CBBAD6BD712EBA4FAF00D73210 (void);
// 0x00000441 System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3F93E4AA532F15D52D68B7121805C014AB2D7FB (void);
// 0x00000442 System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.Reset()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mCDC8982948ED5F7743567569CA1D4A354218714F (void);
// 0x00000443 System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mF39E492D576810F58DB9031556CFD6806FD32E27 (void);
// 0x00000444 System.Void TMPro.Examples.TextMeshProFloatingText::Awake()
extern void TextMeshProFloatingText_Awake_m679E597FF18E192C1FBD263D0E5ECEA392412046 (void);
// 0x00000445 System.Void TMPro.Examples.TextMeshProFloatingText::Start()
extern void TextMeshProFloatingText_Start_m68E511DEEDA883FE0F0999B329451F3A8A7269B1 (void);
// 0x00000446 System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshProFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshProFloatingText_m3C381B8A53C58CF001D7A9212B8BDA6F368CC2C7 (void);
// 0x00000447 System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshFloatingText_m22691E6EA41B7FCF782B03954865D8E3B890E4DF (void);
// 0x00000448 System.Void TMPro.Examples.TextMeshProFloatingText::.ctor()
extern void TextMeshProFloatingText__ctor_m968E2691E21A93C010CF8205BC3666ADF712457E (void);
// 0x00000449 System.Void TMPro.Examples.TextMeshProFloatingText::.cctor()
extern void TextMeshProFloatingText__cctor_m5FF3949FBDB0FD13908FB7349F64DDB3012512B8 (void);
// 0x0000044A System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::.ctor(System.Int32)
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_m3F7F9561735FF8403090A93E708E781B3997B81B (void);
// 0x0000044B System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m6F24E1DC5777D4682375D8742BC9272EE6D4EA2C (void);
// 0x0000044C System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::MoveNext()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_MoveNext_m04B1BB9074CBA76BA7F63849721F4CAEA1045A28 (void);
// 0x0000044D System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14A7C9DEBD0FF0C7A6979AFEC78B0BEE9E4C3963 (void);
// 0x0000044E System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mC0D5548A92F57550F60EE76E17B58125B57B94A8 (void);
// 0x0000044F System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mD3CBFD6F8558EF09F2B4A3D8D1B76BD7FDB3A98B (void);
// 0x00000450 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::.ctor(System.Int32)
extern void U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m0466586E44A01852547C6CECCA91C14673B98C20 (void);
// 0x00000451 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m8730A3A2EFBF759A51F0E5330C3BBF8EFC1736A3 (void);
// 0x00000452 System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::MoveNext()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_MoveNext_m93E19CF96C4EBC57CC4E03E2D3D0B56B3E92A36A (void);
// 0x00000453 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4076BE73343A473EFE46E73ABA5DE66CC797598 (void);
// 0x00000454 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_mC33B355BB392C5A52DCA0AAF2588E1E78AF6D3A5 (void);
// 0x00000455 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m28CCE2E51295AD5F368C504BADDDFF008C2CC0A6 (void);
// 0x00000456 System.Void TMPro.Examples.TextMeshSpawner::Awake()
extern void TextMeshSpawner_Awake_mDF8CCC9C6B7380D6FA027263CDC3BC00EF75AEFB (void);
// 0x00000457 System.Void TMPro.Examples.TextMeshSpawner::Start()
extern void TextMeshSpawner_Start_m7CC21883CA786A846A44921D2E37C201C25439BA (void);
// 0x00000458 System.Void TMPro.Examples.TextMeshSpawner::.ctor()
extern void TextMeshSpawner__ctor_m1255777673BE591F62B4FC55EB999DBD7A7CB5A1 (void);
// 0x00000459 System.Void TMPro.Examples.VertexColorCycler::Awake()
extern void VertexColorCycler_Awake_m84B4548078500DA811F3ADFF66186373BE8EDABC (void);
// 0x0000045A System.Void TMPro.Examples.VertexColorCycler::Start()
extern void VertexColorCycler_Start_mC3FF90808EDD6A02F08375E909254778D5268B66 (void);
// 0x0000045B System.Collections.IEnumerator TMPro.Examples.VertexColorCycler::AnimateVertexColors()
extern void VertexColorCycler_AnimateVertexColors_m6960F777E4876DFCA726BCAE7A8163850D58FA42 (void);
// 0x0000045C System.Void TMPro.Examples.VertexColorCycler::.ctor()
extern void VertexColorCycler__ctor_m09990A8066C8A957A96CDBEBDA980399283B45E9 (void);
// 0x0000045D System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__3__ctor_m0038B1054BCC928D35F8C0021ED7D2E1C533E35F (void);
// 0x0000045E System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5BAD394A0B09B3E0FF19E91521E02C2B3ADD6007 (void);
// 0x0000045F System.Boolean TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__3_MoveNext_m2842AF5B12AFC17112D1AE75E46AB1B12776D2A6 (void);
// 0x00000460 System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE810D7968957A09C88E61C29DAAEC68E4AF1E51 (void);
// 0x00000461 System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m72ABAC50E9E4D972FB44CAFF387F3E23FEC5D932 (void);
// 0x00000462 System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m8DE595A1D01F3A507A356F8BCE020D0851412B52 (void);
// 0x00000463 System.Void TMPro.Examples.VertexJitter::Awake()
extern void VertexJitter_Awake_m9C5586A35BD9C928455D6479C93C1CE095447D8F (void);
// 0x00000464 System.Void TMPro.Examples.VertexJitter::OnEnable()
extern void VertexJitter_OnEnable_m97ED60DBD350C72D1436ADFB8009A6F33A78C825 (void);
// 0x00000465 System.Void TMPro.Examples.VertexJitter::OnDisable()
extern void VertexJitter_OnDisable_mF8D32D6E02E41A73C3E958FB6EE5D1D659D2A846 (void);
// 0x00000466 System.Void TMPro.Examples.VertexJitter::Start()
extern void VertexJitter_Start_m8A0FED7ED16F90DBF287E09BFCBD7B26E07DBF97 (void);
// 0x00000467 System.Void TMPro.Examples.VertexJitter::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexJitter_ON_TEXT_CHANGED_mC7AB0113F6823D4FF415A096314B55D9C1BE9549 (void);
// 0x00000468 System.Collections.IEnumerator TMPro.Examples.VertexJitter::AnimateVertexColors()
extern void VertexJitter_AnimateVertexColors_mECAE037FC0CBA52CAC71C0B61E88829FF18BCC16 (void);
// 0x00000469 System.Void TMPro.Examples.VertexJitter::.ctor()
extern void VertexJitter__ctor_m550C9169D6FCD6F60D6AABCB8B4955DF58A12DCE (void);
// 0x0000046A System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m0222C3457F5ACA497FE3A8EC829DE4AD11A169F8 (void);
// 0x0000046B System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m717E79A39A8161ADDA9E62F7CDFB67B8F2D65099 (void);
// 0x0000046C System.Boolean TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_m169A75C7E2147372CB933520B670AF77907C1C6B (void);
// 0x0000046D System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920EBA43D59A1A88E27FED92CF0AC0DF90179479 (void);
// 0x0000046E System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m6C045A6DD0B60F1512457448E615877EAB86D75D (void);
// 0x0000046F System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m017345FE58B497EAFC9D0CB1FB733F76EB3449AF (void);
// 0x00000470 System.Void TMPro.Examples.VertexShakeA::Awake()
extern void VertexShakeA_Awake_m005C6F9EE8A8FD816CD3A736D6AF199CD2B615A2 (void);
// 0x00000471 System.Void TMPro.Examples.VertexShakeA::OnEnable()
extern void VertexShakeA_OnEnable_m9F60F3951A7D0DF4201A0409F0ADC05243D324EA (void);
// 0x00000472 System.Void TMPro.Examples.VertexShakeA::OnDisable()
extern void VertexShakeA_OnDisable_m59B86895C03B278B2E634A7C68CC942344A8D2D2 (void);
// 0x00000473 System.Void TMPro.Examples.VertexShakeA::Start()
extern void VertexShakeA_Start_m3E623C28F873F54F4AC7579433C7C50B2A3319DE (void);
// 0x00000474 System.Void TMPro.Examples.VertexShakeA::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeA_ON_TEXT_CHANGED_m7AD31F3239351E0916E4D02148F46A80DC148D7E (void);
// 0x00000475 System.Collections.IEnumerator TMPro.Examples.VertexShakeA::AnimateVertexColors()
extern void VertexShakeA_AnimateVertexColors_mC451E2732A4E3E90E2553811AD75903EEF974599 (void);
// 0x00000476 System.Void TMPro.Examples.VertexShakeA::.ctor()
extern void VertexShakeA__ctor_m7E6FD1700BD08616532AF22B3B489A18B88DFB62 (void);
// 0x00000477 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m92612416BEC0EBF5E9849FB603629C0F2F95FEF2 (void);
// 0x00000478 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m77D966994D4717EAFD8EFE169F3E8A4EE8B05B81 (void);
// 0x00000479 System.Boolean TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_mCEEDE03667D329386BB4AE7B8252B7A9B54F443F (void);
// 0x0000047A System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCCD3645ACF9B18D760B341C863F853996FA9BCE (void);
// 0x0000047B System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mB9444B5E58B4E97105C447F547C0F74C51BCFBFA (void);
// 0x0000047C System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m5CC464185D5C0251C6206E20AFFA681BA0525A7E (void);
// 0x0000047D System.Void TMPro.Examples.VertexShakeB::Awake()
extern void VertexShakeB_Awake_m99C9A0474DBFE462DC88C898F958C1805376F9D5 (void);
// 0x0000047E System.Void TMPro.Examples.VertexShakeB::OnEnable()
extern void VertexShakeB_OnEnable_m64299258797D745CDFE7F3CBEC4708BDBC7C3971 (void);
// 0x0000047F System.Void TMPro.Examples.VertexShakeB::OnDisable()
extern void VertexShakeB_OnDisable_m07D520A8D7BCD8D188CE9F5CC7845F47D5AD6EF4 (void);
// 0x00000480 System.Void TMPro.Examples.VertexShakeB::Start()
extern void VertexShakeB_Start_m9642281210FA5F701A324B78850331E4638B2DD1 (void);
// 0x00000481 System.Void TMPro.Examples.VertexShakeB::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeB_ON_TEXT_CHANGED_m5650D69D258D0EEF35AF390D6D5086B327B308A5 (void);
// 0x00000482 System.Collections.IEnumerator TMPro.Examples.VertexShakeB::AnimateVertexColors()
extern void VertexShakeB_AnimateVertexColors_mDF7A7E7028361D288DF588D9301541E4FA1EFA87 (void);
// 0x00000483 System.Void TMPro.Examples.VertexShakeB::.ctor()
extern void VertexShakeB__ctor_m605A778C36B506B5763A1AE17B01F8DCCBCD51EC (void);
// 0x00000484 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m3E3B1D286DEBED2BC028AD490308568B930C3760 (void);
// 0x00000485 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m0F9D4B2A6ED0500C2120DBA29932CC279E8908DC (void);
// 0x00000486 System.Boolean TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m0534E436514E663BF024364E524EE5716FF15C8E (void);
// 0x00000487 System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2A8BAFC0261ACBBE8EEA74DE4B498D30C68AE3D (void);
// 0x00000488 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5F12D7C6F2523BEB6326FE49AE972116D6157CBB (void);
// 0x00000489 System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mB9C12786011A3B78517AEFAE8D5A78B95A4219AD (void);
// 0x0000048A System.Void TMPro.Examples.VertexZoom::Awake()
extern void VertexZoom_Awake_m1B5D386B98CF2EB05A8155B238D6F6E8275D181C (void);
// 0x0000048B System.Void TMPro.Examples.VertexZoom::OnEnable()
extern void VertexZoom_OnEnable_m7F980FC038FC2534C428A5FD33E3E13AEAEB4EEC (void);
// 0x0000048C System.Void TMPro.Examples.VertexZoom::OnDisable()
extern void VertexZoom_OnDisable_m880C38B62B4BB05AF49F12F75B83FFA2F0519884 (void);
// 0x0000048D System.Void TMPro.Examples.VertexZoom::Start()
extern void VertexZoom_Start_m10A182DCEF8D430DADAFBFFA3F04171F8E60C84C (void);
// 0x0000048E System.Void TMPro.Examples.VertexZoom::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexZoom_ON_TEXT_CHANGED_mB696E443C61D2212AC93A7615AA58106DD25250F (void);
// 0x0000048F System.Collections.IEnumerator TMPro.Examples.VertexZoom::AnimateVertexColors()
extern void VertexZoom_AnimateVertexColors_mEFBA6C940DFECB485236C247358A32011A7963ED (void);
// 0x00000490 System.Void TMPro.Examples.VertexZoom::.ctor()
extern void VertexZoom__ctor_mE7B36F2D3EC8EF397AB0AD7A19E988C06927A3B2 (void);
// 0x00000491 System.Void TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m4EC5F8042B5AC645EA7D0913E50FD22144DBD348 (void);
// 0x00000492 System.Int32 TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::<AnimateVertexColors>b__0(System.Int32,System.Int32)
extern void U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m959A7E1D6162B5AAF28B953AFB04D7943BCAB107 (void);
// 0x00000493 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m537CF0A5ADF5BBC2DF784BF526E3F32EB528E1B2 (void);
// 0x00000494 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mF0CD944C393771101D2718A660E1DFF22385819F (void);
// 0x00000495 System.Boolean TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m8340EFEB2B2CF6EA1545CFB6967968CA171FEE66 (void);
// 0x00000496 System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093E922A5A046910B2A7EE826D804CA3064A7BD9 (void);
// 0x00000497 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDED63FD52741D58D8D5A3E53415F91B6560F680C (void);
// 0x00000498 System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m933EBF7C4300C7C1E3EE60E4741110025877CA5F (void);
// 0x00000499 System.Void TMPro.Examples.WarpTextExample::Awake()
extern void WarpTextExample_Awake_mEDB072A485F3F37270FC736E1A201624D696B4B0 (void);
// 0x0000049A System.Void TMPro.Examples.WarpTextExample::Start()
extern void WarpTextExample_Start_m36448AEA494658D7C129C3B71BF3A64CC4DFEDC4 (void);
// 0x0000049B UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void WarpTextExample_CopyAnimationCurve_m744026E638662DA48AC81ED21E0589E3E4E05FAB (void);
// 0x0000049C System.Collections.IEnumerator TMPro.Examples.WarpTextExample::WarpText()
extern void WarpTextExample_WarpText_mA29C98CF3B92F253C2DD2BADFC76618F8324BEF2 (void);
// 0x0000049D System.Void TMPro.Examples.WarpTextExample::.ctor()
extern void WarpTextExample__ctor_m62299DFDB704027267321007E16DB87D93D0F099 (void);
// 0x0000049E System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__8__ctor_m9FADE04C27A0034C5A276232FCA187AECDC6BF49 (void);
// 0x0000049F System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m5AB0DCF4B3DE6C290487F3DBBC7BAE931130DE60 (void);
// 0x000004A0 System.Boolean TMPro.Examples.WarpTextExample/<WarpText>d__8::MoveNext()
extern void U3CWarpTextU3Ed__8_MoveNext_m9C83C8455FF8ADFBAA8D05958C3048612A96EB84 (void);
// 0x000004A1 System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D5776AF80C4227401ADD7E13D98F2530CB9E7A1 (void);
// 0x000004A2 System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4F2FBBE7B375E6FC837736B4275A7B601A01F535 (void);
// 0x000004A3 System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m01FA935D93D737C9DDD88A051F7DFC393DD7BD25 (void);
// 0x000004A4 System.Void AllIn1SpriteShader.All1ShaderDemoController::Start()
extern void All1ShaderDemoController_Start_mEEC3CAE48A5261CB2CD689EC5D84A2D0660F16AB (void);
// 0x000004A5 System.Void AllIn1SpriteShader.All1ShaderDemoController::Update()
extern void All1ShaderDemoController_Update_mBE75BD58B1BBC36FC1B3E75A8726E714724A8670 (void);
// 0x000004A6 System.Void AllIn1SpriteShader.All1ShaderDemoController::GetInput()
extern void All1ShaderDemoController_GetInput_m6073AC5247202DE21350D05BA66835A8CB7E658E (void);
// 0x000004A7 System.Void AllIn1SpriteShader.All1ShaderDemoController::ChangeExpositor(System.Int32)
extern void All1ShaderDemoController_ChangeExpositor_mA47C171B722C938B020A66A4DFD595911762BC68 (void);
// 0x000004A8 System.Void AllIn1SpriteShader.All1ShaderDemoController::SetExpositorText()
extern void All1ShaderDemoController_SetExpositorText_m2449D154B641AB7FD4FFCDF4A20E80EE15028A3B (void);
// 0x000004A9 System.Int32 AllIn1SpriteShader.All1ShaderDemoController::GetCurrExpositor()
extern void All1ShaderDemoController_GetCurrExpositor_m4183C3946ABF9B688E29C8C03B0C80B20DA65304 (void);
// 0x000004AA System.Void AllIn1SpriteShader.All1ShaderDemoController::.ctor()
extern void All1ShaderDemoController__ctor_m4AA052973914A05EB579354C2C30ED258D9965B3 (void);
// 0x000004AB System.Void AllIn1SpriteShader.All1TextureOffsetOverTime::Start()
extern void All1TextureOffsetOverTime_Start_m077B271F3A70CBE1CFEB63B14BE1F839B4CFBCE0 (void);
// 0x000004AC System.Void AllIn1SpriteShader.All1TextureOffsetOverTime::Update()
extern void All1TextureOffsetOverTime_Update_m006EF87314FBB31C5282570A8F8200DFAA654250 (void);
// 0x000004AD System.Void AllIn1SpriteShader.All1TextureOffsetOverTime::DestroyComponentAndLogError(System.String)
extern void All1TextureOffsetOverTime_DestroyComponentAndLogError_m21D043B2F255B83C420B2D1865663F067B3F707F (void);
// 0x000004AE System.Void AllIn1SpriteShader.All1TextureOffsetOverTime::.ctor()
extern void All1TextureOffsetOverTime__ctor_m17D0106F116F9EDA4F1AEF77BADCD69EC6913110 (void);
// 0x000004AF System.Void AllIn1SpriteShader.AllIn1ScrollProperty::Start()
extern void AllIn1ScrollProperty_Start_m5941A1504E99412B1CE17548E002BD336132A5C1 (void);
// 0x000004B0 System.Void AllIn1SpriteShader.AllIn1ScrollProperty::Update()
extern void AllIn1ScrollProperty_Update_mF2B333FA53FA2347D0463187B6B0FE5C0B85F661 (void);
// 0x000004B1 System.Void AllIn1SpriteShader.AllIn1ScrollProperty::DestroyComponentAndLogError(System.String)
extern void AllIn1ScrollProperty_DestroyComponentAndLogError_mB50B67930AB289F8FDC6E5063C58DF52D095786A (void);
// 0x000004B2 System.Void AllIn1SpriteShader.AllIn1ScrollProperty::.ctor()
extern void AllIn1ScrollProperty__ctor_m5148B11659A91C216AE3AE3B85778E71C857429B (void);
// 0x000004B3 System.Void AllIn1SpriteShader.Demo2AutoScroll::Start()
extern void Demo2AutoScroll_Start_m4AEE65959275348130AE119246FF1C33FEB00F66 (void);
// 0x000004B4 System.Collections.IEnumerator AllIn1SpriteShader.Demo2AutoScroll::ScrollElements()
extern void Demo2AutoScroll_ScrollElements_mF866F08A3586845E528E753205F2B65A8767EAF1 (void);
// 0x000004B5 System.Void AllIn1SpriteShader.Demo2AutoScroll::.ctor()
extern void Demo2AutoScroll__ctor_mF6BEB75AB83B28B7CA50FB2BD6F7F8C9EAD8CC3E (void);
// 0x000004B6 System.Void AllIn1SpriteShader.Demo2AutoScroll/<ScrollElements>d__4::.ctor(System.Int32)
extern void U3CScrollElementsU3Ed__4__ctor_m1F1BD6DCB9CA445D10DE6B2AF051AF11BD9AC02A (void);
// 0x000004B7 System.Void AllIn1SpriteShader.Demo2AutoScroll/<ScrollElements>d__4::System.IDisposable.Dispose()
extern void U3CScrollElementsU3Ed__4_System_IDisposable_Dispose_m34188717DA14DF12B0E29B18A60F135381015DBD (void);
// 0x000004B8 System.Boolean AllIn1SpriteShader.Demo2AutoScroll/<ScrollElements>d__4::MoveNext()
extern void U3CScrollElementsU3Ed__4_MoveNext_m1F69AE110D7DAD42C71B82AD9FE7355D770B7249 (void);
// 0x000004B9 System.Object AllIn1SpriteShader.Demo2AutoScroll/<ScrollElements>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CScrollElementsU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m13230DA8EF32AA27571693E059C0AA976BCABAAA (void);
// 0x000004BA System.Void AllIn1SpriteShader.Demo2AutoScroll/<ScrollElements>d__4::System.Collections.IEnumerator.Reset()
extern void U3CScrollElementsU3Ed__4_System_Collections_IEnumerator_Reset_m7BEF0DE9D77F41B1DB4C48C89D506FFC04E0B6BC (void);
// 0x000004BB System.Object AllIn1SpriteShader.Demo2AutoScroll/<ScrollElements>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CScrollElementsU3Ed__4_System_Collections_IEnumerator_get_Current_m580B00C2C8C6CB363A03D46AF29E653CD6A69DE5 (void);
// 0x000004BC System.Void AllIn1SpriteShader.DemoCamera::Awake()
extern void DemoCamera_Awake_m4ECA403039A6C68D7048F875C9846C2F2F845E7F (void);
// 0x000004BD System.Void AllIn1SpriteShader.DemoCamera::Update()
extern void DemoCamera_Update_mEABD0D432561814631853E4CFD6127C05DA44231 (void);
// 0x000004BE System.Collections.IEnumerator AllIn1SpriteShader.DemoCamera::SetCamAfterStart()
extern void DemoCamera_SetCamAfterStart_m96A3BE2CDF47F4D52320617D2FFAF0CA33AA8773 (void);
// 0x000004BF System.Void AllIn1SpriteShader.DemoCamera::.ctor()
extern void DemoCamera__ctor_m7298363D9C905F16A40F4B02225B2B3406574773 (void);
// 0x000004C0 System.Void AllIn1SpriteShader.DemoCamera/<SetCamAfterStart>d__8::.ctor(System.Int32)
extern void U3CSetCamAfterStartU3Ed__8__ctor_mA8337C68757B7D40FAFF6E81E64A03E89715DC6C (void);
// 0x000004C1 System.Void AllIn1SpriteShader.DemoCamera/<SetCamAfterStart>d__8::System.IDisposable.Dispose()
extern void U3CSetCamAfterStartU3Ed__8_System_IDisposable_Dispose_m5993ADE0141B93FCBED8DCE77C4C8285A477C86B (void);
// 0x000004C2 System.Boolean AllIn1SpriteShader.DemoCamera/<SetCamAfterStart>d__8::MoveNext()
extern void U3CSetCamAfterStartU3Ed__8_MoveNext_m52B1967F30180CFE83B28C71F5BBC32612538076 (void);
// 0x000004C3 System.Object AllIn1SpriteShader.DemoCamera/<SetCamAfterStart>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSetCamAfterStartU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0E94003660DA4E83C91F84E8055A6627CFB56B0A (void);
// 0x000004C4 System.Void AllIn1SpriteShader.DemoCamera/<SetCamAfterStart>d__8::System.Collections.IEnumerator.Reset()
extern void U3CSetCamAfterStartU3Ed__8_System_Collections_IEnumerator_Reset_m7400F544CC8BE1E2C846322EDBD54765C8C3654B (void);
// 0x000004C5 System.Object AllIn1SpriteShader.DemoCamera/<SetCamAfterStart>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CSetCamAfterStartU3Ed__8_System_Collections_IEnumerator_get_Current_m2753443439BEB359F0D4991B19ECB0479FFBBE0C (void);
// 0x000004C6 System.Void AllIn1SpriteShader.DemoCircleExpositor::Start()
extern void DemoCircleExpositor_Start_m0B710706757CA7D195C82649D39619394ACC86BE (void);
// 0x000004C7 System.Void AllIn1SpriteShader.DemoCircleExpositor::Update()
extern void DemoCircleExpositor_Update_m4699A9A58BDB0F195BA3090CCBADC35BC04CFF74 (void);
// 0x000004C8 System.Void AllIn1SpriteShader.DemoCircleExpositor::ChangeTarget(System.Int32)
extern void DemoCircleExpositor_ChangeTarget_m119354E8A96778A683F8C43043E37F4FDDED57B1 (void);
// 0x000004C9 System.Void AllIn1SpriteShader.DemoCircleExpositor::.ctor()
extern void DemoCircleExpositor__ctor_m83F52F9F725417DBB3980F9AFDB8F962AF4C670F (void);
// 0x000004CA System.Void AllIn1SpriteShader.DemoItem::Update()
extern void DemoItem_Update_mB33B944D0C5E63E68B40BC4D67F573F51D332B50 (void);
// 0x000004CB System.Void AllIn1SpriteShader.DemoItem::.ctor()
extern void DemoItem__ctor_m6C6224B09B25A50F9D2DE6553CADDC0A5E4F4140 (void);
// 0x000004CC System.Void AllIn1SpriteShader.DemoItem::.cctor()
extern void DemoItem__cctor_m9A552D77A553FA30F3D0AA90A89DC964E7892F44 (void);
// 0x000004CD System.Void AllIn1SpriteShader.DemoRandomColorSwap::Start()
extern void DemoRandomColorSwap_Start_m94684B7699A9DFCDC803280D5F0A88E52D39AEE7 (void);
// 0x000004CE System.Void AllIn1SpriteShader.DemoRandomColorSwap::NewColor()
extern void DemoRandomColorSwap_NewColor_m32EC1ABB92C3863FF51E05B6F4A6ADD03E0DCB6B (void);
// 0x000004CF UnityEngine.Color AllIn1SpriteShader.DemoRandomColorSwap::GenerateColor()
extern void DemoRandomColorSwap_GenerateColor_mD5ED1C41EA7B3F993ACE60E082EA3EF41D9B4E6E (void);
// 0x000004D0 System.Void AllIn1SpriteShader.DemoRandomColorSwap::.ctor()
extern void DemoRandomColorSwap__ctor_mA82A8FA6AC1D7CDED3D578301E68013F1C31E0D6 (void);
// 0x000004D1 System.Void AllIn1SpriteShader.DemoRepositionExpositor::RepositionExpositor()
extern void DemoRepositionExpositor_RepositionExpositor_m79FF08534B92E056E53EA21EA6B66E5BC63D4084 (void);
// 0x000004D2 System.Void AllIn1SpriteShader.DemoRepositionExpositor::.ctor()
extern void DemoRepositionExpositor__ctor_mA818DA5BD14FD598F148E4FE5C69E901D4045520 (void);
// 0x000004D3 System.Void AllIn1SpriteShader.All1CreateUnifiedOutline::Update()
extern void All1CreateUnifiedOutline_Update_m5641EFED7018929F1D58C04A4F850DD272764CEC (void);
// 0x000004D4 System.Void AllIn1SpriteShader.All1CreateUnifiedOutline::CreateOutlineSpriteDuplicate(UnityEngine.GameObject)
extern void All1CreateUnifiedOutline_CreateOutlineSpriteDuplicate_m480A6FD6DDB94F456A7CE3E2026053BE9023F6C3 (void);
// 0x000004D5 System.Void AllIn1SpriteShader.All1CreateUnifiedOutline::MissingMaterial()
extern void All1CreateUnifiedOutline_MissingMaterial_m3A82377519175A2D96AFF31E5EDD3A328D63BACB (void);
// 0x000004D6 System.Void AllIn1SpriteShader.All1CreateUnifiedOutline::GetAllChildren(UnityEngine.Transform,System.Collections.Generic.List`1<UnityEngine.Transform>&)
extern void All1CreateUnifiedOutline_GetAllChildren_mAAD7C28A2A5A8F6BDEDF235DB026C7451BD82F57 (void);
// 0x000004D7 System.Void AllIn1SpriteShader.All1CreateUnifiedOutline::.ctor()
extern void All1CreateUnifiedOutline__ctor_m189CDB191BB8F2F101E3A4E9F930F2D84981F0B8 (void);
// 0x000004D8 System.Void AllIn1SpriteShader.RandomSeed::Start()
extern void RandomSeed_Start_mFC6E494BAB8F987A2994660771B42B9D11BCE86F (void);
// 0x000004D9 System.Void AllIn1SpriteShader.RandomSeed::.ctor()
extern void RandomSeed__ctor_mB7DC90975D873C9CF6D8BECF875EA44D8D2AA302 (void);
// 0x000004DA System.Void AllIn1SpriteShader.SetAtlasUvs::Start()
extern void SetAtlasUvs_Start_m4AF6C9B645238AE5FF6B10438A8DEA87719F157A (void);
// 0x000004DB System.Void AllIn1SpriteShader.SetAtlasUvs::Reset()
extern void SetAtlasUvs_Reset_m6D9E04258D341C27F0ECB36D9F342CA3D3D5B3BC (void);
// 0x000004DC System.Void AllIn1SpriteShader.SetAtlasUvs::Setup()
extern void SetAtlasUvs_Setup_mB74C08D364BA97A6F16F170433CA50636E737745 (void);
// 0x000004DD System.Void AllIn1SpriteShader.SetAtlasUvs::OnWillRenderObject()
extern void SetAtlasUvs_OnWillRenderObject_mFFB7A45341EE9FE7E9E2D742F6523AF4C67CD8C7 (void);
// 0x000004DE System.Void AllIn1SpriteShader.SetAtlasUvs::GetAndSetUVs()
extern void SetAtlasUvs_GetAndSetUVs_mE82DE67BC9FE2E00501E177AE72C14D6692ADCB0 (void);
// 0x000004DF System.Void AllIn1SpriteShader.SetAtlasUvs::ResetAtlasUvs()
extern void SetAtlasUvs_ResetAtlasUvs_m30348C40877A06CCF73A3169306F6C5DF40406EB (void);
// 0x000004E0 System.Void AllIn1SpriteShader.SetAtlasUvs::UpdateEveryFrame(System.Boolean)
extern void SetAtlasUvs_UpdateEveryFrame_m507682AEAEB64356EBE7486739B702025ADE20D3 (void);
// 0x000004E1 System.Boolean AllIn1SpriteShader.SetAtlasUvs::GetRendererReferencesIfNeeded()
extern void SetAtlasUvs_GetRendererReferencesIfNeeded_mFA52628BDE471B7EAB32096B3CBA4F448AA45DD7 (void);
// 0x000004E2 System.Void AllIn1SpriteShader.SetAtlasUvs::.ctor()
extern void SetAtlasUvs__ctor_m042D771698F0B8BB203C900E7119BB6519CAB283 (void);
// 0x000004E3 System.Void AllIn1SpriteShader.SetGlobalTime::Start()
extern void SetGlobalTime_Start_m8F51B543660EF7280297678A1ECB14FD403EF007 (void);
// 0x000004E4 System.Void AllIn1SpriteShader.SetGlobalTime::Update()
extern void SetGlobalTime_Update_mE9CE1843FC11FC2EAB533E50F0D650FF1C1BB0DC (void);
// 0x000004E5 System.Void AllIn1SpriteShader.SetGlobalTime::.ctor()
extern void SetGlobalTime__ctor_mB3B514DAD04B536F81F563259EE75654F6029513 (void);
// 0x000004E6 System.Void AllIn1SpriteShader.All1DemoChangeScene::Update()
extern void All1DemoChangeScene_Update_m5A3884916B6641F07AE5250499F0BD943574B5F5 (void);
// 0x000004E7 System.Void AllIn1SpriteShader.All1DemoChangeScene::NextScene()
extern void All1DemoChangeScene_NextScene_mFF3DFBB30601DA5AC8F6191D7E8D17C62A2F780E (void);
// 0x000004E8 System.Void AllIn1SpriteShader.All1DemoChangeScene::LoadScene(System.String)
extern void All1DemoChangeScene_LoadScene_m894077928CA689EB5D62947A5E912761197E2AF9 (void);
// 0x000004E9 System.Void AllIn1SpriteShader.All1DemoChangeScene::.ctor()
extern void All1DemoChangeScene__ctor_m971C27B3CB025D12132CC3EF1B3F84E4E325AC02 (void);
// 0x000004EA System.Void AllIn1SpriteShader.All1DemoClaw::Start()
extern void All1DemoClaw_Start_m7F8EABD2497BDEE79FCC09E56E0A1C71BF9656B9 (void);
// 0x000004EB System.Void AllIn1SpriteShader.All1DemoClaw::Update()
extern void All1DemoClaw_Update_m7AFB8117C3AA03EF88C68EEBE0A9548F473D00A2 (void);
// 0x000004EC System.Void AllIn1SpriteShader.All1DemoClaw::.ctor()
extern void All1DemoClaw__ctor_mBCEE8397E3E75BC8210D7BECBCD15EB00093AE42 (void);
// 0x000004ED System.Void AllIn1SpriteShader.All1DemoRandomZ::Start()
extern void All1DemoRandomZ_Start_m130E074ABE1614D4BC687B2EF9A340383398657E (void);
// 0x000004EE System.Void AllIn1SpriteShader.All1DemoRandomZ::.ctor()
extern void All1DemoRandomZ__ctor_m22F81ED995ED7FFA0EC0F8859F52A54B2B4DA6F2 (void);
// 0x000004EF System.Void AllIn1SpriteShader.All1DemoUpAndDown::Start()
extern void All1DemoUpAndDown_Start_m6BAED669B9C7C67FB53F331D94FEA7989FA540C2 (void);
// 0x000004F0 System.Void AllIn1SpriteShader.All1DemoUpAndDown::Update()
extern void All1DemoUpAndDown_Update_m7D574BDE0D181FE6C89CE4C2442DBA984D6F41EA (void);
// 0x000004F1 System.Void AllIn1SpriteShader.All1DemoUpAndDown::.ctor()
extern void All1DemoUpAndDown__ctor_m2D65F826D83D2CC364F901C5A6445FB54F0DEFBE (void);
// 0x000004F2 System.Void AllIn1SpriteShader.All1DemoUrpCamMove::Start()
extern void All1DemoUrpCamMove_Start_m6F378D55541F497B7C055E5891FB3EEDDB086B46 (void);
// 0x000004F3 System.Void AllIn1SpriteShader.All1DemoUrpCamMove::FixedUpdate()
extern void All1DemoUrpCamMove_FixedUpdate_m5F27951E3A2DADEDA3E8733020C550866FFDFFAA (void);
// 0x000004F4 System.Void AllIn1SpriteShader.All1DemoUrpCamMove::.ctor()
extern void All1DemoUrpCamMove__ctor_m4ED75302A93663B3B19B01B8A8AD1396B3DB8379 (void);
static Il2CppMethodPointer s_methodPointers[1268] = 
{
	AllIn1Shader_MakeNewMaterial_mE7F705EA36336223D8864DD397FAA986C61EBEE3,
	AllIn1Shader_MakeCopy_m8D3FAF720787E21FD8C6162887DA674915B36C36,
	AllIn1Shader_ResetAllProperties_mA039FEF1F00517620D3A3C273EE85B404CFB9E0A,
	AllIn1Shader_GetStringFromShaderType_m54D914538D389E52D72190351D9A664274691D80,
	AllIn1Shader_SetMaterial_mC3984D294123C8A0B6940D3F09FFEEEE07C8572A,
	AllIn1Shader_DoAfterSetAction_m2123918B31E2C14412D1737E15FA19B81C172EEC,
	AllIn1Shader_TryCreateNew_m234BC20F58B0DFB4E9647BE34BE90A283099DF93,
	AllIn1Shader_ClearAllKeywords_mD988454DE618F8E544EF171AD3338F98E24B4333,
	AllIn1Shader_SetKeyword_mE5D052091451244FC46391F625C8C63AF8F0FD8E,
	AllIn1Shader_FindCurrMaterial_mB9EFE076006FA4CCE609D55F34FEDFDD19E03713,
	AllIn1Shader_CleanMaterial_m8E500FAE06954447FBD9F205E98573F78DEF2019,
	AllIn1Shader_SaveMaterial_mAC0418B7A3142DF46940D577276240D78C8BE251,
	AllIn1Shader_SaveMaterialWithOtherName_m1059A69AEB8181D4E38047AB6E1181AF51D0FF82,
	AllIn1Shader_DoSaving_m44BABDC9CCF5312136F411570420E7C5EF47ED44,
	AllIn1Shader_SetSceneDirty_m1FFC452FC0782C2108031B6B3BDA16A07EF154C2,
	AllIn1Shader_MissingRenderer_m0CF5D69D36B76B33631F42C30F5B685EF7437C57,
	AllIn1Shader_ToggleSetAtlasUvs_mEE86D7544508A24DAF9323D8BF74816E308E6F96,
	AllIn1Shader_ApplyMaterialToHierarchy_m32F0C81AEE934F21D945F387C6E4E6DC6B89C481,
	AllIn1Shader_CheckIfValidTarget_mD3FCB5C967D6B6D678926BE784D84A7A761304BA,
	AllIn1Shader_GetAllChildren_m905322E49C9EF2A343D1623090A3B57B9330AF57,
	AllIn1Shader_RenderToImage_m9ADBB9A484BCAD28619CFC20FC6CB733E09909B9,
	AllIn1Shader_RenderAndSaveTexture_m6A08515FC4F916E1D4349C397E006BB4299905E3,
	AllIn1Shader_GetNewValidPath_m13B5D88B875BB5244D2A47E0D58557F87E005AC9,
	AllIn1Shader_OnEnable_mE9835DC94B4E5B7CC9C06BA0A406A4B565AAAF46,
	AllIn1Shader_OnDisable_mE67BF9B67E77CC0520E942A4FECD5B133FD7A7B1,
	AllIn1Shader_OnEditorUpdate_m98A1E511589FC578536B47B8E6AD1122BF565DB9,
	AllIn1Shader_CreateAndAssignNormalMap_mE09D63046961A4E6852DB6902D751D234A7B65AC,
	AllIn1Shader_SetNewNormalTexture_mC77EA7A52EFE22351A6AAFEA5681D01EE1342E8C,
	AllIn1Shader_SetNewNormalTexture2_mD993E8F9CB4DD2AAA747931FC0A762B45725B8C8,
	AllIn1Shader_SetNewNormalTexture3_m535753B6B39D131A10368250C16C5D9AFDCB9D5D,
	AllIn1Shader_SetNewNormalTexture4_mE31E4D0C69A28D4D840D0C02C7EE4A8B4E10CA69,
	AllIn1Shader_CreateNormalMap_mB3B664F50B8CCA42FBB5BD5E6C23AD44C731D419,
	AllIn1Shader__ctor_mC28FD6F66BE2DF14119F5AC9DA0CA92F48CD42EA,
	ConfinerSwitcher_OnTriggerEnter2D_m154905373C7B3015DF1F7A97D9997F0A739DF57C,
	ConfinerSwitcher_OnTriggerExit2D_mD6169820382DFD57E6AB4094DCD08D67564BA839,
	ConfinerSwitcher__ctor_mBE91C57EEC4076DC0F5111EA22A19D55C7EB9EBF,
	EnemyHealthBar_Start_mAC5072644CD25461F54283A494648B93E10740AD,
	EnemyHealthBar_UpdateHealthBar_m6B0617F247EA5BA5FBBF85F060C35F75B8F4FCD2,
	EnemyHealthBar_Update_m0D89B3B8999217593AA991F95BC0CB9D0DFF3B4B,
	EnemyHealthBar__ctor_mA53148773B4E9CA545B3C7D4564ADAF769B50C8B,
	CameraShakeCinemachine_Awake_m5CA148B8FF6AFB8DE443542D071F57882EFCA5DD,
	CameraShakeCinemachine_Start_mB42DA2C5794AA38FCC11482B173E55231F067AC2,
	CameraShakeCinemachine_Update_m0A0B14BC4F9D7A94E34CBC78C6C5748EFC3D09D7,
	CameraShakeCinemachine_CameraShake_mB84711D6B9F4E8E5D594D279BF8816A64B1B93B2,
	CameraShakeCinemachine__ctor_m20E67BA2F073C46CE2FC51492A802D05B991C379,
	Core_get_Movement_m172A2A762581D45CE756AFD8D8F3A8A810ABD09B,
	Core_set_Movement_m04794317914E765F8DB75A42F4BBBA095A078C01,
	Core_get_CollisionSenses_mE72AFC5311B2947DCF85CB181BE1BC87CD264E4C,
	Core_set_CollisionSenses_mB5076760323379E0214B88D328EE15FC1BE96175,
	Core_get_Combat_m6F4DC96E9A98EDC6216BD506E9DD9D626A2110AD,
	Core_set_Combat_mEEC8D3B0EFA0D948AE7F5A92510AC71AA7AD5DE7,
	Core_get_Stats_mF10C44DFB82AD0B37BF84FD1690D43332F38530E,
	Core_set_Stats_m3390B6713B79E8ED850DF35C6D0E967BEE9A6B74,
	Core_Awake_mFDE01B81F891FE5CC0111365322483646A6F34C9,
	Core_LogicUpdate_m49FCF14B8B1C721FDA704C440C4FD14E61D458B6,
	Core_AddComponent_mA73D4B30456F2FD76182E6D8ED285CC24C924496,
	Core__ctor_mD55440EAEDEF387693B0FA45820CBB23D7D17EA1,
	CollisionSenses_get_GroundMask_m74DF9AD2504A5F90FF3FC9B201B718F37620098E,
	CollisionSenses_set_GroundMask_mE220BC64B21ACE33D6398C13262D620B21741276,
	CollisionSenses_get_EnemyMask_m68D99BA24204148E73C6ECBE3972EE9EB3375A7E,
	CollisionSenses_set_EnemyMask_mDDBC023875DD52E9129DD4596FAEE421496F9483,
	CollisionSenses_get_GroundCheckRadius_m4C1A916647F84610FC94DCAE86650A6137030EE7,
	CollisionSenses_set_GroundCheckRadius_m3B76801BCCF34884DFFCF414A38962E58A2EF355,
	CollisionSenses_get_WallCheckdistance_mAE1E6A6B52A3039DE58E7B30F91D2D6D3D9894A4,
	CollisionSenses_set_WallCheckdistance_m5183A4EF4A810A6E448957D11EA1BFB008D61672,
	CollisionSenses_get_WallCheck_mF1C4E727DA58CC27AA86918C99F7F1EEFB142923,
	CollisionSenses_set_WallCheck_m342A5F5C8E6CE1A9AB69CA69360613B9FC68D43F,
	CollisionSenses_get_GroundCheck_m496159F04E7490D40A43B154F60DBBEA4DE96C21,
	CollisionSenses_set_GroundCheck_m7701BFB5ACC47F3070E188C37A20E1B0D36CBA6C,
	CollisionSenses_get_CeilingCheck_mACCEBE7CDD51354486B413F05D4B871F0B44A4D5,
	CollisionSenses_set_CeilingCheck_mDED07D4536361D3843434E8222F828FF86EC4989,
	CollisionSenses_get_LedgeCheckVertical_mC78C4565A077A9BB55D876CECEFD76EF8ED50EBB,
	CollisionSenses_set_LedgeCheckVertical_m704C5A0D6EC48893B2C4176236F301E301FB1D5E,
	CollisionSenses_get_LedgeCheckHorizontal_m887FF88515E0782F19BD73506125364DDC1CFFAF,
	CollisionSenses_set_LedgeCheckHorizontal_m74B3D709EE980723D4DE167B96313F4F1BD0DBCA,
	CollisionSenses_get_Ground_mFB990B8B58EDCDA98F35884C245DF5F46C0C3086,
	CollisionSenses_get_Ceiling_mB494BB581AD6F6380091989CC1EC89DAAB1B5D2A,
	CollisionSenses_get_WallFront_m266D05D660BBF1E0328A9AA52400201A3B9B2A7E,
	CollisionSenses_get_LedgeHorizontal_m8B3D72FC3CC777928CDCEB0DEE9C05780F5F6085,
	CollisionSenses_get_LedgeVertical_m5E81A9AB52A5563D3C99C887FC5361260D2E5329,
	CollisionSenses_get_WallBack_m8E715001A7C7795261C09F80E756E7A7EC7332CF,
	CollisionSenses_get_PoundEnemy_mE0304C010592D0651E5679030EDA79E41720980E,
	CollisionSenses__ctor_mBDB49A8784B242A162F970BE247FD674BAD132E7,
	Combat_LogicUpdate_m94074876B81F53EB1D53409721B74AC668FF6755,
	Combat_Damage_mB5E8271EDA33B5CC73DB850F74CF6695E1CC1BF0,
	Combat_Knockback_m8B13571DDC9E87485F23B1BA46EE51299B8531DE,
	Combat_CheckKnockback_m62080CF24CC9EE95C039CA9E78B9BDA0AF4C6DA7,
	Combat__ctor_mD9E519351EDFD3E8CCBF26121CE3066B10013292,
	CoreComponent_Awake_m5C815956F5E3EFC674FAF0AA2A4462B0E7459E7B,
	CoreComponent_LogicUpdate_mBFFF748AC572D178384A4C28B509C8C66D9F0C6C,
	CoreComponent__ctor_m5A67D7612AE7FE4ED44D3F41EF83584F0D6A1E0E,
	Movement_get_CurrentVelocity_mA760A8ABD437E4779064E74D4FA7E6D31E59C56E,
	Movement_set_CurrentVelocity_m3E9C3C5D71014EC23E5C3C1C093F3F556FFFE46F,
	Movement_get_FacingDirection_m8DAFF6DCB9249A010654443F603F7C54070BC037,
	Movement_set_FacingDirection_m614F41019F39CF99C616404D01F280181DD0E5F1,
	Movement_get_Rb_m086D6E6B8CD9818B7C1C5B105266C04C244209C8,
	Movement_set_Rb_m354A4212C73B7D7FBC3330642A5B9284F2DCC164,
	Movement_get_CanSetVelocity_m0886AD490E0C3A0DCCEC80B3959A31C4A5AD28DD,
	Movement_set_CanSetVelocity_m7319992C6ECF9FFDBC045F194A412B61F1CEA852,
	Movement_Awake_mC24012D10E5FE2E1CF2B188AA4EFC347C0DA7A93,
	Movement_LogicUpdate_mE89F7103463D59D9ED1A21016BB25603714BDA9E,
	Movement_SetVelocityX_m407AB5BCB226BC2A2763D02158A602D098BC6B49,
	Movement_SetVelocityY_m153D253AB4DAE87BC03544D4D608CFDD9BE5352C,
	Movement_SetVelocity_m383F8B84DDCEE215F43E68700DCD8E9B9AC50F13,
	Movement_SetVelocity_m65856EE280DBA5212A0F96A8D36CD676CB4B20A3,
	Movement_SetVelocityZero_m6FBAF7493CB440202FFEA4B8352451030C9CFDA6,
	Movement_SetFinalVelocity_mDB6CD6CDFA8EF43D2CDEEFAEC59287CAA37F9E9B,
	Movement_CheckIfPlayerShouldFlip_mD735B19086BDF14D5EF10C337B4CE04C613713BC,
	Movement_Flip_m52ED3BDED8C922EC7D94D4F4B18450D45BF87BEE,
	Movement__ctor_mEA4800F5BE98787C0ACA8CDF85918B56DE62A2AB,
	Stats_Awake_mDCEFEF38E7205C2294049E7D1E362A7E02F68E78,
	Stats_DecreaseHealth_mC558D21506DB4476B79BE3AFEE5A2098A71CA920,
	Stats_Die_m1C5224BDB57C1184AECC2FBD588C485F5184F68B,
	Stats_IncreaseHealth_m3A293207EEA28B4FEECB84F1EE652DE47A34430F,
	Stats_DecreaseStamina_mB89DCE01198A6420E6C077537485D2941CC7AAD8,
	Stats_HitEffect_mAC56690239F0D3CC359F210111F0843FB5198C34,
	Stats__ctor_m86D57BC6FFBC17545B914A808C1C0C5B12E0CD0A,
	U3CHitEffectU3Ed__12__ctor_mD087739F6EB030EE325EC2DD7FAD22A372778386,
	U3CHitEffectU3Ed__12_System_IDisposable_Dispose_mA4C9CA68C781C2F79A0ECCDBF058E3147F5958C6,
	U3CHitEffectU3Ed__12_MoveNext_mDF3D332A775741036132C6AD75D8ED1DD02347F0,
	U3CHitEffectU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4C4EBFCB4045479CFC11E8BB126EEC35FA73F02E,
	U3CHitEffectU3Ed__12_System_Collections_IEnumerator_Reset_m487070A46E453A3631ED53956284AE13834E3D1D,
	U3CHitEffectU3Ed__12_System_Collections_IEnumerator_get_Current_m4BC6FA39422EE5E507C5E4B847CB98D049B40379,
	BasicEnemyManager_Start_m0D091E8C8ED1A96BD33718456965D6E2FDB9FDB1,
	BasicEnemyManager_Update_m59117305BA67C48174EA6155CB11DFC13E727F67,
	BasicEnemyManager_EnterMovingState_m587C136816F0550F54F50D576123F0B62ED08CAF,
	BasicEnemyManager_UpdateMovingState_m4E6A27A0FFDEC7995A460151A1E3CF62BCDA905A,
	BasicEnemyManager_ExitMovingState_m1E81EE70D265A291B853EB65107FA209CE5ECC26,
	BasicEnemyManager_EnterKnockbackState_m3DA995E83803753503F35611D6386CC1EE21FF0E,
	BasicEnemyManager_UpdateKnockbackState_m6234B9B60B410E7EE593A96FD0635E126B35E4E7,
	BasicEnemyManager_ExitKnockbackState_m908538721A63E2F3BD63D760DB41267C6F1497DE,
	BasicEnemyManager_EnterDeadState_m23B0E521A0F1A50E0CF7AFDFB9E332542BFB4A11,
	BasicEnemyManager_UpdateDeadState_mF00D3973A266250FCE60C67F9469EA25DA21D368,
	BasicEnemyManager_ExitDeadState_m7D6A7957DA617B00CC27ABAA46CA4BA1F9B74DED,
	BasicEnemyManager_SwitchState_m86D1CD645804EB419BD7B8BB507366CC3F45228B,
	BasicEnemyManager_Flip_m4DBB15F86EB3BA2B835ABE38E0B7CD706D92E63B,
	BasicEnemyManager_Damage_mC9A0D55E5B72627429D693558DFE1245D4D55E4F,
	BasicEnemyManager_CheckTouchDamages_mF8E172BD04FB49758782274CF15B881BD1D2491D,
	BasicEnemyManager_OnDrawGizmos_m05ACF767E347C163B155EA95C6411F23A2CBB39E,
	BasicEnemyManager__ctor_mFF22A118397FB9F99F126511BE111DABD75A5378,
	CombatTestDummy_Awake_m91F69F1F73B03308A575387018D990BB06C806CD,
	CombatTestDummy_Update_m2AD93C31F48F1B2B1981DBF07710EA725DA463E7,
	CombatTestDummy_Damage_mF3A0FED5708785D9C3E7D7E162FCBEB8465C4FB2,
	CombatTestDummy__ctor_m463A5D63E81DD26A0F41A3321541CB3DB2F66200,
	E1_ChargeState__ctor_m2CD18D8FE0B276286ED51A93F7F6531D7D6DF450,
	E1_ChargeState_Enter_mA4FC9FA2CC549C05D25A9F5FBC7F2DE3D5738873,
	E1_ChargeState_Exit_mECAF690F538E8C2C9A52B504EE9F1D659C4F264A,
	E1_ChargeState_LogicUpdate_mC9507F0AF0E698C13E10DC00477C7866E687338E,
	E1_ChargeState_PhysicsUpdate_m56F52C48E7D75AF6F8B43456F67507C279BEBC29,
	E1_ChargeState_TickChecks_m8F9F9BF6D936DEBFD6A6F56DAE90E7E2DEE042BC,
	E1_DeadState__ctor_m6BD0A61217576D7DD60FDAC62F23B7054D4A5D4E,
	E1_DeadState_Enter_m2C7AFC170116D67424DECC2FD3191FFF7834509A,
	E1_DeadState_Exit_m0B5787E9FEBAD39A93B9D0E4A9973A15FC2EBDE9,
	E1_DeadState_LogicUpdate_m5421EAE276E64401B55B5D27E38F34B603F65389,
	E1_DeadState_PhysicsUpdate_m18D715C6017683E7D210937329738E4F39825458,
	E1_DeadState_TickChecks_m48AACC76F502E09B7730BC2E05658326EB1C68CD,
	E1_IdleState__ctor_m668115FE36579930C04918C688B523E260BC7407,
	E1_IdleState_LogicUpdate_mA80792B788592B7378A2FB81BEBF62AD9DE2732A,
	E1_LookForPlayerState__ctor_m22DCA26F29119A2E87A7CF80C6FE474313543639,
	E1_LookForPlayerState_Enter_mD07012885705ACD4379DBC7E8274CE71E2492F77,
	E1_LookForPlayerState_Exit_mADF8C5B2F7F6A32CEEEE5E75915432428C02FBFB,
	E1_LookForPlayerState_LogicUpdate_m6779C98150AB995723294868F492277B55556C9D,
	E1_LookForPlayerState_PhysicsUpdate_m67B23ABCDA3F7480E9C9E81A47A39D49D6F6D9C2,
	E1_LookForPlayerState_TickChecks_mCF1E5C64F173F6A13C67D05DEB81ED0AE17E0B5E,
	E1_MeleeAttackState__ctor_mF82BD38099217FE082647FFC0D3B84B9DE8CB0F0,
	E1_MeleeAttackState_Enter_m6BBFA99FD6C92201BDEA69F9BFF0D2546BE6F906,
	E1_MeleeAttackState_Exit_m8685E1676EAE0E05121B74A9E7D91ABF9104F696,
	E1_MeleeAttackState_FinishAttack_m6E46393F3EC1FFC1B448DB6813583094347C8289,
	E1_MeleeAttackState_LogicUpdate_m82CAA2E97C5D0B882427DBD2D132BF1B755CB92B,
	E1_MeleeAttackState_PhysicsUpdate_mA0E1F5A61C0D1D84BE8D3EC4725E8E475F526155,
	E1_MeleeAttackState_TickChecks_m423D311EF089764C8D358CE8E851EDB0E6E8C992,
	E1_MeleeAttackState_TriggerAttack_m8066EB17240303267CFE67DB3A86060E930D721E,
	E1_MoveState__ctor_m26CE6418EEC0B69943019DB4AEC59B70A8733807,
	E1_MoveState_Enter_mB54D1ABDC20105D4420109994E23DA32B2E15FEA,
	E1_MoveState_Exit_m1C5BCFB48700CF87EB2542120C40A996F5F8DF19,
	E1_MoveState_LogicUpdate_mFDA8F488C4A89A255795C85554D20BFBA1201945,
	E1_MoveState_PhysicsUpdate_m4BBEE08CD813A7A451E12329CC97167B44FF526F,
	E1_PlayerDetected__ctor_mF3E339439CF847ACC8BE1CF3A1EE1A329DAC9F83,
	E1_PlayerDetected_Enter_m8CFD1A3A888920D6E90E1A149B7B7F54450D36C8,
	E1_PlayerDetected_Exit_mF95C059C7E6DD283EE6A5A5BB250C28328C9A142,
	E1_PlayerDetected_LogicUpdate_m20EDF1D4DDD4F245B7274F2D05960F4FEEA759B8,
	E1_PlayerDetected_PhysicsUpdate_mAD84C010C1A0BE67FAFBD0DCB8B032FD1F767471,
	E1_StunState__ctor_mC30FE26E39AABB7340970EC6F028C2C95C40B192,
	E1_StunState_Enter_mBB060690A2E35FF98ABB819C0DEFC4EB4C183EC1,
	E1_StunState_Exit_m2C9F8493CBB2B9903965A59819824420D52A14E7,
	E1_StunState_LogicUpdate_m84649F8594E0BB6B98EE81A74F853328704F9537,
	E1_StunState_PhysicsUpdate_mF3D9149BB9AD5E2D663A99ACEF3138D6101E2DDF,
	E1_StunState_TickChecks_m2F5F262124A83E621D365642B504FB6CAE5A80DC,
	Enemy1_get_idleState_mE0C9A3FBF7DA3E4A76E9F88DD28B8F6D68C164F9,
	Enemy1_set_idleState_m18DE75CFA65BB8D75A1CE5C6F067EA8F82A08AEB,
	Enemy1_get_moveState_m378E69A34987280E78335134C0E753866D32B3C5,
	Enemy1_set_moveState_m29288ED1D140B62734EB80D8DA2A1FBA1B96025C,
	Enemy1_get_playerDetectedState_mED81BA996974A85CEB20BA5C6C25A3662A9443F6,
	Enemy1_set_playerDetectedState_mDA5C562D01251A12B0623AA3C989F22F70CA0A8A,
	Enemy1_get_chargeState_m4A952E6209BEE246849C61042ED42EE67566AE42,
	Enemy1_set_chargeState_mE48C8C21CDA0D75EDE2F3E30B22F2208A56ED7AF,
	Enemy1_get_lookForPlayerState_m0388DAFDA08636CEEFC8E0553D85180CC465E920,
	Enemy1_set_lookForPlayerState_m73386AE9DA9FBD54D7C9218C93B4BA3E5CB03C61,
	Enemy1_get_meleeAttackState_m84EB0BDA5196F46255AF1482F961FB4DB0014F60,
	Enemy1_set_meleeAttackState_mD949D5E0AD1FCB6C9610446EF04C81A38CCD51ED,
	Enemy1_get_stunState_m130714A54AB28CF9BC83B0C597FBBB5F3045C3BE,
	Enemy1_set_stunState_mFCBD825284AA6FA2F9812845B145B4605982EDC0,
	Enemy1_get_deadState_mDB4C05B960822A6529E2F384060F89D65E57E7B7,
	Enemy1_set_deadState_mF5E6748887C6E11FAAF69973A17AD0624C707C33,
	Enemy1_Awake_mDACED125FF233DF293A45E9AF53FF61C093E696E,
	Enemy1_Start_mC33823548F376AEA81FC577AF8FC98BE8D8DA560,
	Enemy1_OnDrawGizmos_m5392E09EB5950786D62B632D068EE7C277A3AD40,
	Enemy1__ctor_mFB31C5E5C0D17B1858EC762823BE12C6A3D8D9C1,
	E2_DeadState__ctor_m0BA442CB37665A9E09E1F35306FD240D9FCEB899,
	E2_DeadState_Enter_mE86EEFC6F5EAC431448C52DD5F9C0B75E121407B,
	E2_DeadState_Exit_mDDB7662712390FC7671C6BF3DFE2220813D6186C,
	E2_DeadState_LogicUpdate_m98D4725EAB9EF5C38413FFE85FF3B1D53FC0102B,
	E2_DeadState_PhysicsUpdate_m608D6880A5DFBCC6A536E354B7AEB94D81C91120,
	E2_DeadState_TickChecks_m11E300E1A8C0E6F5168C1336CDBCDFA4087E6BBF,
	E2_DeadState_Start_m1604127BA33253D8A31CC49B6F6D1531B801A990,
	E2_DeadState_Update_m1B1BFFB5B1B8A6306233C98B678FA844FE096B30,
	E2_DodgeState__ctor_m4D928DC8CA48A205720147FC268E4885085BB7B4,
	E2_DodgeState_TickChecks_m716F4512FD8DA77BA0704357EAFC5D9C2364B4C0,
	E2_DodgeState_Enter_m9CE0A951BABAACB24EDD36E0B297C87D0A03F21C,
	E2_DodgeState_Exit_mD965F4E1BA9F47B072DEBCDC4B2FB075E70A73B6,
	E2_DodgeState_LogicUpdate_mC1CAB9D385F83E440247412ABAC118086EAC0D2E,
	E2_DodgeState_PhysicsUpdate_mD08C0564FF013980F97458C5F80402509DEC556A,
	E2_IdleState__ctor_mFC62D4B99C9CCBD5D622872FD14776D5E49DEFA3,
	E2_IdleState_Enter_m44128D15FC51FF0A4FBFE421546ED2EB2477DA99,
	E2_IdleState_Exit_m1C30375B3E588E7CF819FB49ED10FDD56D345517,
	E2_IdleState_LogicUpdate_m0CBD53C636E58C5A87265BD474561E3E9F7978BB,
	E2_IdleState_PhysicsUpdate_m469740E12DC674880EBD491BB788E561D7B706E8,
	E2_IdleState_TickChecks_m633D7E4D121A028D48146ECA2D28380E5F5BC8B0,
	E2_IdleState_Start_mC9BC1203FFE5F725B98C20454D725CA791897E9F,
	E2_IdleState_Update_m35F98A1EBC138B72DD7D3C0F593DC9C66CFD7254,
	E2_LookForPlayerState__ctor_m45A291A2878AD38805B6065BC1B0BB99339BF726,
	E2_LookForPlayerState_Enter_m6F09DF963F6BAF837B8A5B325D6A9CCAF86DE7A4,
	E2_LookForPlayerState_Exit_m4CDE453F9D980F45688F86A47059DD9C270CD82E,
	E2_LookForPlayerState_LogicUpdate_m279D71C42890C6ECD720D194AD5A3A4146CB6837,
	E2_LookForPlayerState_PhysicsUpdate_m836F5D1A806B377ED4D6EC995A0A5797A95003C9,
	E2_LookForPlayerState_TickChecks_m4A312CAF2964E29A4C7BA2C036C55AEC68FBA68A,
	E2_MeleeAttackState__ctor_m1FB6BFDEB7A5440EAE03E55CFE0C1AB71E79AD91,
	E2_MeleeAttackState_Enter_mABE1BEE4F981B5220338B11C623A241C2570FBC0,
	E2_MeleeAttackState_Exit_mFC34DB6E8E86760D2B244D6775AB6FDA2A1CA7C6,
	E2_MeleeAttackState_FinishAttack_mC3C45F6E620504546FC313FC836FCCE91D4E8B0F,
	E2_MeleeAttackState_LogicUpdate_mFF7AB3FAB1CFB7D8858BFCE467CF24DBE1AF420A,
	E2_MeleeAttackState_PhysicsUpdate_mF8F3148B74F04B9A9388824AEF1FD9F8DD08935F,
	E2_MeleeAttackState_TickChecks_m6C4A357C7A773C965ADFC07DEB7AE999EB0EC3E8,
	E2_MeleeAttackState_TriggerAttack_mA7559018CC3D68BA86CE277EEB3DC5B1790C08D0,
	E2_MeleeAttackState_Start_m0A012101F0369FE75DFAC19E021A3F81D67A7C29,
	E2_MeleeAttackState_Update_m0680E46D870E401C9597439EF3D51EA8EF061AD7,
	E2_MoveState__ctor_m99B828B30BFEDE41C9B9F551DC2A281B0C9E97D6,
	E2_MoveState_Enter_m5937B152975597427948A0E70EFEC3CC5769C825,
	E2_MoveState_Exit_m2966266203EC5F4BCCF00D21F43B351B6AA2D90A,
	E2_MoveState_LogicUpdate_m5F2274504D4F0F6790453486F69D6C763C1B7EA6,
	E2_MoveState_PhysicsUpdate_mCB818A2F87D5F498A6DF64CA36E211DA65E202E5,
	E2_MoveState_TickChecks_m6F3CE6E0F43043E0247995A7568E92875067F761,
	E2_PlayerDetectedState__ctor_m52E6FE64491A3FC762A3F18F14BB7435555B5B50,
	E2_PlayerDetectedState_Enter_mB82DE29C451A6521E8289FB2AE2793003FFAA5BC,
	E2_PlayerDetectedState_Exit_m029897FB66BFF886451803B5B89EDB038F30D3D3,
	E2_PlayerDetectedState_LogicUpdate_m75D2236A1A44535BD8D213E13F0D74F993B0D1AF,
	E2_PlayerDetectedState_PhysicsUpdate_mA17B36074B3842683ECB8DD338007271717CF67B,
	E2_PlayerDetectedState_TickChecks_m81DD6CE340B1CA82D0613D84520E9003773ACA1A,
	E2_RangedAttackState__ctor_mD40D1C163FA4DBBB493D7EA8F5779CEEEFADEA6F,
	E2_RangedAttackState_TickChecks_m4954BBAC3E20DA7B9F84829ED7A6908450C82E8F,
	E2_RangedAttackState_Enter_m3B8846B0C592378B8ED157ED3D841F9A01347DF2,
	E2_RangedAttackState_Exit_m76C117F47B2625D7DADCFD5CC8B475E21CE4C271,
	E2_RangedAttackState_LogicUpdate_mC9819566903D20AEC6CB83D5CC04827A501970AE,
	E2_RangedAttackState_PhysicsUpdate_m8094C4B0FBD1CDB0857655F281CF3E00CB2F5329,
	E2_RangedAttackState_TriggerAttack_mE3C271D295227332EF813959FE5B0569D373C845,
	E2_RangedAttackState_FinishAttack_m096528D4D8F0F8CBF1BFEF70CF1DB174CC1FCAA2,
	E2_StunState__ctor_mAF638866EF94EB08A137B23F1C032972A495725A,
	E2_StunState_Enter_mC3F9AB73D9056B37ACABF15E7CF146949E8685CE,
	E2_StunState_Exit_m2EBBDA512F91729046CE4BB292CDF6607E387BD0,
	E2_StunState_LogicUpdate_mFF1430FA661E793EDDF9A85F24B05A002186BB7B,
	E2_StunState_PhysicsUpdate_mF5941BD1E2A0E1E74500609B539F13A132BB2D11,
	E2_StunState_TickChecks_mFC9B8319000C71C9FB16CD60DC43EAD20938B83F,
	Enemy2_get_idleState_m9609E996C07D801CDD7D0469BF5E99CEAD058B6C,
	Enemy2_set_idleState_mC4A62000D63AC6F8139AE307F33EDE046057F043,
	Enemy2_get_moveState_mA57347F419E8CB781BF1303F2290C63436E117BD,
	Enemy2_set_moveState_mFE81C10785208323812269CA5D1A5C86FD1AA06C,
	Enemy2_get_playerDetectedState_mC4E3C395BE954EB1D23B1ED7602FF84C32ED8F7F,
	Enemy2_set_playerDetectedState_mD99028EB06200A5D7BC949C00AFB8EB2BFF128B7,
	Enemy2_get_lookForPlayerState_mABE40EBEE735BF8C64F1D4CC615CB2D6B59ED3AF,
	Enemy2_set_lookForPlayerState_m010D59E6FEBF2E6EB46E19919D1C6B0A131F0499,
	Enemy2_get_meleeAttackState_m7E4A61D255A594969A6374B9172CCAD1C07C23E0,
	Enemy2_set_meleeAttackState_m99B79E1DBD6F0AB6D226A83306368644838EC984,
	Enemy2_get_stunState_m3897E66DCD78E4312ECB87D9A4E620F4DF920593,
	Enemy2_set_stunState_mB9C3EAB3B3E3D1240E2D1DC88E59D45B1032D647,
	Enemy2_get_deadState_m85343F6B761082102679F7B8995AD96E2D1B1210,
	Enemy2_set_deadState_m85C483452621928C43988A012F38E4B4928BBA7D,
	Enemy2_get_dodgeState_mE957B3CE04D174C5EF7436919DD7BC0DC8CD729E,
	Enemy2_set_dodgeState_m02A6D7A58D9109A52429A8003BE27F24DC6A40A7,
	Enemy2_get_rangedAttackState_m4375769A9D7BFD8D3D1C9A774B80D031F0FC6FEE,
	Enemy2_set_rangedAttackState_m10721EDFD84671B13A333504E2DB920CB0150812,
	Enemy2_Awake_m63BDF0341F17A7BC738DB10875D5F8C9F0E6F44B,
	Enemy2_Start_mECCDD202929D74E082F74026156890AE09E7F535,
	Enemy2_OnDrawGizmos_mB02E8ECD7D636E093158C557FCE67BD48AF46D32,
	Enemy2__ctor_mEF60A06C0696069D82DB6E0A9999741499409559,
	E3_ChargeState__ctor_m2BD4D11D39B951A2451DA0D3FC0CED18DB804298,
	E3_ChargeState_LogicUpdate_mE141EEE5C2B6014EF7E53B466916E5EC1EAD7C52,
	E3_DeadState__ctor_m0B9F5EEB570582A6541A802C1DB3069A1A2D8566,
	E3_DeadState_Start_m263E555287E4C354A41FCF2D53F14EAA41DE34EA,
	E3_DeadState_Update_mFA4CE7643C3650EE7766E9872CCDD0AB5B862A18,
	E3_EmbushState__ctor_m3EA6D16E186542593ADEF710F075375D1D3918F8,
	E3_EmbushState_Exit_m8FF457FF95907A3BCB0899AAC3DB5050935E9C17,
	E3_EmbushState_LogicUpdate_m70F7A932258A353657D77F4C03EF8ED15837CD66,
	E3_IdleState__ctor_m125E5EAD8CFFC462B8D5D1329B5AF1A2DC789D7A,
	E3_IdleState_LogicUpdate_mEA09B43E74C23E0CD1598BBA9DB01761033E3498,
	E3_LookForPlayerState__ctor_mAF537EF5A4C50EECDA6B74859791A2CA0207CCFE,
	E3_LookForPlayerState_LogicUpdate_mB067EA74592B81664F51D77DA65CA937B55ABF7F,
	E3_MeleeAttackState__ctor_m3463B9F8994BAE9551A0BAF3D19DC9422FE8DC65,
	E3_MeleeAttackState_Enter_m2CB946DC5A120A6262ABA46102C64A51FE489CC3,
	E3_MeleeAttackState_Exit_mAE38551EE8F673D6FB9C585EBF7265FD9EFBC45A,
	E3_MeleeAttackState_FinishAttack_m1CE01A874168D9E1295D725E214B1CEDCEF194E3,
	E3_MeleeAttackState_LogicUpdate_mEA4E05BCD6DEE6F47316C2F6E85BB6BA4288505B,
	E3_MeleeAttackState_PhysicsUpdate_m904AB6B8C82AAACD3785E707AE02C6E91D7CF0EA,
	E3_MeleeAttackState_TickChecks_m87749F2218A25F953CDD17459A2A09A1FD071559,
	E3_MeleeAttackState_TriggerAttack_m167B4E882D3DE9209A38ACD960B2CF664974DF4D,
	E3_MoveState__ctor_m775E5380FA5F09EF72B9EB394DCABB2B013BEA41,
	E3_MoveState_Enter_m3E74948E5333E2611F06C4C83164A47740F10EFF,
	E3_MoveState_Exit_mE0006CFDA010B3BA0B646E6E94322EBD227D3774,
	E3_MoveState_LogicUpdate_mD70B8758F2CAE8B23826B8D7659FC7E7E9E0E181,
	E3_MoveState_PhysicsUpdate_mE556DFAC78B891A542ACBFF610A271489D7473B7,
	E3_PlayerDetectedState__ctor_mCBD54445F4094282C462BA93D3A225AAEACC92CF,
	E3_PlayerDetectedState_LogicUpdate_m0ACA0EDC6435C70687D2E001F5E7E12A5F5600BA,
	E3_RegenState__ctor_m28CF2EF1E89D2DD0952D5EC034C53302DAE5AD17,
	E3_RegenState_Enter_mAC4DB11E4D15374513C087CFB0954B4E270F8D80,
	E3_RegenState_Exit_m790679678503B121787F03F3A325B92F9F7742DA,
	E3_RegenState_LogicUpdate_mF96988C9935D948852E0D488D7BE5B432DDF00EC,
	Enemy3_get_idleState_mBA892BA14384A046B8EAD644B406E65B878DE8AB,
	Enemy3_set_idleState_m2CFEBE780838E96CD7FFF76C995B95863C0DC240,
	Enemy3_get_moveState_m489FFEEAAED821BAD17EC70EC372C25BBBABE58E,
	Enemy3_set_moveState_mE9AA56220A5C984111C061C9347606D8339E8370,
	Enemy3_get_playerDetectedState_m30A8E36B2EF25D36BA8DA3D1C586DF0C6A82686F,
	Enemy3_set_playerDetectedState_m0AB6466315E904B35409A52C86CB789750BFC33E,
	Enemy3_get_chargeState_m8F05F6A0481B8D78481BD0E156A8357B16F81074,
	Enemy3_set_chargeState_m4D0DE4E63158147B7A323B90B7BF26EA0C39F021,
	Enemy3_get_lookForPlayerState_mBA9F4B30B5C4308D7791835BC74385AC4B87FA56,
	Enemy3_set_lookForPlayerState_mF7BBB772A0AB03D0BC3059EA9D411B606EAB70BD,
	Enemy3_get_meleeAttackState_m0E082BA02FF5BE06E25C574B4D6040EC7DD90BD7,
	Enemy3_set_meleeAttackState_mCB2C08DD469FEFE5A1B567DBEBE9006966ADD2DE,
	Enemy3_get_embushState_m3B1A27B24B56108736EB5E05E6122406BFC2B999,
	Enemy3_set_embushState_m59B172DBDE3F6CB0B77091B0C1574A9A258BEF51,
	Enemy3_get_deadState_m9A8EF3442D53715D87861084DB0B5776C2955D46,
	Enemy3_set_deadState_m4751FBB980207FA3AD0A50749ACE67E9B05D9B48,
	Enemy3_get_regenState_m89F174EC57862C656BFB20F4639FC943F5E03E3C,
	Enemy3_set_regenState_m2CC3D4610767392F403A0624B8087CFD164FD77C,
	Enemy3_Awake_m3918D7BA0B05354206EE45F409A415E1E8525877,
	Enemy3_Start_mC93658EADC64D22D16190C89686705CC66B2FFFB,
	Enemy3_OnDrawGizmos_m0669995A51E173EBE6D9D4B766EF738590B50C84,
	Enemy3__ctor_mD174D0F35ACA96431DF534ACF81753FA74065208,
	E4_DeadState__ctor_m65752F23B067113C3E08E3FFE04EEFDD1A5B923E,
	E4_DeadState_Enter_m91126292DBF85E6ED6EC3692D8BA1E423E11FE9F,
	E4_DeadState_Exit_m9664B0D4FCE824A4CE31A3B73E132F802106E6B7,
	E4_DeadState_LogicUpdate_mC3FA3437A12E0F933068A5719632996D965E9A00,
	E4_DeadState_PhysicsUpdate_mD3B41B3BE788920116A79A66FC29EC6D121BC101,
	E4_DeadState_TickChecks_m128A7055479CF3138940A032D0C93FB4D071C44B,
	E4_DodgeState__ctor_mC0E051BCDB7F13E9374A10C5FE4A5B2F12F7E515,
	E4_DodgeState_Enter_m8CF96A74AC34B4F482212A0E00E1362EEA9BECA4,
	E4_DodgeState_Exit_m1A52B7189C08FC0DC14C11A55091600A95BC6885,
	E4_DodgeState_LogicUpdate_m7C371F709DFEE7835B19771E936EB418A97DB3EF,
	E4_DodgeState_PhysicsUpdate_mA343599BA0B3E2226AAA0F852FAF4A5AF28B853E,
	E4_DodgeState_TickChecks_mDB0ACE9D31A7CE935CE28A4C6C76856A7384BFB8,
	E4_IdleState__ctor_m5E81E682449566034E96D99789A14B2D0C52C1B3,
	E4_IdleState_Enter_mA850C9E4455EDC86DE7E8ECB6B99524B67970BD2,
	E4_IdleState_Exit_m35138F31E088561FA924AA0589AAB37E50C8EFDD,
	E4_IdleState_LogicUpdate_m567CFDB7C55C4F848EBA3115FE976F243A939AEB,
	E4_IdleState_PhysicsUpdate_m875BCBD787D02ED19A44F91B05CA34D692EEB612,
	E4_IdleState_TickChecks_mD22D70ED6762F21475DA32EEC3B705A99330AD66,
	E4_LookForPlayerState__ctor_m269DA8DD6B445548B9D4E5A4CF3B1DB943846132,
	E4_LookForPlayerState_Enter_m06C70F337A67935107C868B7B028B176B3BC1B56,
	E4_LookForPlayerState_Exit_m379928D0D69F58BC4F1F47ED70A3298F9B10B33C,
	E4_LookForPlayerState_LogicUpdate_mACA066722DB0AA4E066E41A20570ECAB1943A5C1,
	E4_LookForPlayerState_PhysicsUpdate_m9E7F5B23473512FA7BAE2ECD2A1F7A48F6B7DEEC,
	E4_LookForPlayerState_TickChecks_m2015EE1FD788A93B985D1F60A6CD508DC9FBF54C,
	E4_MeleeAttackState__ctor_mB5B29F30323E834CCDADE4A5B8DCEC0FC063E91D,
	E4_MeleeAttackState_Enter_mFF00928DE6B168C56DDF3F700E91C3E4EAD5E973,
	E4_MeleeAttackState_Exit_m49073ADD2C1CD5294AA0823764DB920D4C76AED6,
	E4_MeleeAttackState_FinishAttack_m226B61744921BC64170B36FB24EEA178210040F4,
	E4_MeleeAttackState_LogicUpdate_m71690018794E9828FDCE9BF6A159AAB50BD916FD,
	E4_MeleeAttackState_PhysicsUpdate_mDE7C2E2FBF57E6E376D96147D4E7713114E4F061,
	E4_MeleeAttackState_TickChecks_mBCA15AE37F144C7BEAE967EF02F88877041D990E,
	E4_MeleeAttackState_TriggerAttack_m78D8FF0886AF7ABF060D9DAF9B10C24A0E5C2F17,
	E4_MoveState__ctor_m9D62EB852E5AC79353059A466CACAFC2301F432F,
	E4_MoveState_Enter_m36EADB2633F4488C2C82A3D3FA58E52AE8B00B95,
	E4_MoveState_Exit_m8F08BDF734C602C4B390311922F8ACFCD9B49260,
	E4_MoveState_LogicUpdate_m295EA3E0CBB014C9F2ED35A78A6DA939EE3BD58A,
	E4_MoveState_PhysicsUpdate_mD50BA0F1C2E35E7576A5AA7F2C4E86E3C103614F,
	E4_MoveState_TickChecks_m8E3BE61B62D3E4149900405BF7C4C4DD86D68BD1,
	E4_PlayerDetectedState__ctor_m2E575F8060CDB5178F0D731FD90F99FD2A456A9E,
	E4_PlayerDetectedState_Enter_m2A932A4C457170354D2F3B7BE93AAE689B447F5A,
	E4_PlayerDetectedState_Exit_m8396DE06529C226926FC6882C0304D2829E70530,
	E4_PlayerDetectedState_LogicUpdate_m91547C9A614FBB8F80DF1C7AAC5FF2677BBED520,
	E4_PlayerDetectedState_PhysicsUpdate_mA91D07E02E054E63D6EACEE7D6F36D980800ADA2,
	E4_PlayerDetectedState_TickChecks_mEB6D2315DEA9D1C2BE78C63E0007D1878DF57B4A,
	E4_RangedAttackState__ctor_m1E5798BF40C3699F5D7432F9DE92420DBAB01121,
	E4_RangedAttackState_Enter_mA97D1ED423C1480022CF0D14035BFBC39CC645DB,
	E4_RangedAttackState_Exit_mB99A84C188AF7F867FDA740AA7E87905C2080138,
	E4_RangedAttackState_FinishAttack_mE6C9F958D0D97122269E79B2A59E873C6DB285D7,
	E4_RangedAttackState_LogicUpdate_m7799D19C9AF35235A85414A7E04948E5F710A28F,
	E4_RangedAttackState_PhysicsUpdate_mBFB736434A16ECD49A9FD88E0D0F98081C757D58,
	E4_RangedAttackState_TickChecks_m5F531991A9E9A64D082DBDDFBEEE0025E198BDB7,
	E4_RangedAttackState_TriggerAttack_m8F95627F3E8A148D5CAFB0C036539A85A9EA3338,
	Enemy4_get_idleState_mEA127CCA7F311EDDC2354B16718DCF8160903A4D,
	Enemy4_set_idleState_m7D23B1B973EA57FFCC3D39AD119A146E2BA80729,
	Enemy4_get_moveState_m9DA4F359FB57E3B393EADB3B9B4293AC23BDE374,
	Enemy4_set_moveState_m634AF993CE5C7524147C0F13C3805681838EA02A,
	Enemy4_get_playerDetectedState_mF8E0ADAB977232B11304B082A0970E6E5459A5DB,
	Enemy4_set_playerDetectedState_m1F060C49975799B2287CEDAA08E998C7A5CB7D09,
	Enemy4_get_lookForPlayerState_m97516991E5977210C5B40886EC21245622CF0CFD,
	Enemy4_set_lookForPlayerState_m551D22917375E48FD13361850E264661EF26EDC5,
	Enemy4_get_meleeAttackState_m699DBBD4F7DEC7530565CAD05274FB3F041B9D53,
	Enemy4_set_meleeAttackState_mE63392F34524CC7051CC554C0E620CEE28EE0B4D,
	Enemy4_get_deadState_m834819D3DBD25F6BD813A6FDB0CC203C88E0423B,
	Enemy4_set_deadState_m3E412779FF8D8FC143F6994850B31E1CA706A97F,
	Enemy4_get_dodgeState_mC0AECFF38394E15590AE3C45C0B966B244504F5B,
	Enemy4_set_dodgeState_m44EC625B6106CCBCDD4B16BAE97A17FC232B0CA3,
	Enemy4_get_rangedAttackState_m5ABC78B51C930ACE717DB5D3F5570A78789B8452,
	Enemy4_set_rangedAttackState_m1DC22E410FCB47990A196753452001C021630C88,
	Enemy4_Awake_m797D25E7D6D2BF79CB03953FC2F36E62A10F67FA,
	Enemy4_Start_m5DF1FC3EB8E01B4E1C8B11983C4A96D0CD2D3E2C,
	Enemy4_OnDrawGizmos_m933D5F7B9E98A98855ABCDC766D0AAB3BD7440D7,
	Enemy4__ctor_mA002C4E651A5331BB7938BC264B17B4D783D5DE3,
	Entity_get_Animator_mE3A186D7422082C3AAF044F212E817B9F0D32134,
	Entity_set_Animator_m4C82F97F9F16843B78F608DBF3AADAB2BFFFB57A,
	Entity_get_Atfsm_mF891A5BF42EC5AD0848A01B619020AF82AC351F7,
	Entity_set_Atfsm_m51BFBE4D06B77B9D197D7EC748DA71CD120AC8F5,
	Entity_get_LastDamageDirection_mA726D86ED179AEBEA41920096FE4FBBEB7A90B24,
	Entity_set_LastDamageDirection_m3AF2A6CBC1DF07591AD80FA71210EDE736ABF2C1,
	Entity_get_Core_m969A58FED6B929FF5D3CAAEA51098DB218D4B806,
	Entity_set_Core_mDCD8DEC2594D09500F6DDB1EB16543DB1BFE51F0,
	Entity_Awake_m53067BAC9D02222618330E6DC07F457399362C8A,
	Entity_Update_m4FAC0E7988D34B19805DBB519889B857020642CA,
	Entity_FixedUpdate_m36DAEE494A40A2BA13A9C36FC9D5704D9FB8C5A6,
	Entity_CheckPlayerInMinimumRange_mA89B6027827F219FFCF04AFABA82C27024DDB5BA,
	Entity_CheckPlayerInMaximumRange_mB1D2C20904B5A31C9010C6B07DDBF77426F940A5,
	Entity_CheckPlayerInCloseRangeAction_m95802D3048DB0A25B78C041AA1B51D0F402F1A70,
	Entity_CheckForPlayerToEmbush_m91C8873D88FDCAB13BDB9458A077B13F36835599,
	Entity_ResetStunResistance_m0E85D826B9E93D9A502D0E8E853ADC224CF28B23,
	Entity_DamageHop_m46336AA3D97F06D2E06AFC4228404D752D1C9C1A,
	Entity_OnDrawGizmos_m670BFCD0FBA0D4402A95C8CCEB88377375F2AB01,
	Entity__ctor_mA22D51D64B97E8F6F879E72C8E0DE7D26ED0C824,
	FiniteStateMachine_get_currentState_mCEC096E0E07CDE37B0FB5CAB0A18245E9FB6D35D,
	FiniteStateMachine_set_currentState_m40116BF9AD6CC400EEE4F17FE9183873A976746B,
	FiniteStateMachine_Initialize_mF5425B5BF1AEDF9436FCBFBF95C165A2DDEA62F1,
	FiniteStateMachine_ChangeState_m6BA6695EDC31E88A2795C8F9B6D786CFEBAB5E48,
	FiniteStateMachine__ctor_mF83459ECF9E991F57293D41F7B261EF11E8B9E41,
	State_get_startTime_m3881926F7D9F6AF6CE767EBC9E53CEC7D5FB1B53,
	State_set_startTime_m75EF6DDE4B43E8D40F2584A1D05793AA1AE158F7,
	State__ctor_m7922784812838F4A31FEE66D76C8DCAB41A960FE,
	State_TickChecks_m6F8FDB299E3F93EB0061AB520C8F900124B6EE89,
	State_Enter_m3433FED643AB93ED2C51B1510BDD19BEF7F5DD09,
	State_Exit_m4DE1BF421094D707A14DFB8D154CE52C8097AB29,
	State_LogicUpdate_m164478ECC97B11CFA59D33C9E156A430C9775916,
	State_PhysicsUpdate_m0554F42942B71FA0EB8824397590EF99B884C16F,
	AttackState__ctor_m3C44FDB406BAE32540299F13802909D33B51CA86,
	AttackState_TickChecks_mBA73A247F2767DBBEBF0D44FDFD69A078F13CDBD,
	AttackState_Enter_m3506EDFCBBC2600F3DC6268174D7165F69353B4B,
	AttackState_Exit_m2D73ECF95A8E8A1773B09DDEE9156E35F367F5EC,
	AttackState_LogicUpdate_m32CC25C997780DAC7DB0FF94F7D012B6486EE611,
	AttackState_PhysicsUpdate_m0F4C8C9B751EB446A37F8F1950396AF52365D6C7,
	AttackState_TriggerAttack_m6771D1C2CB90C977CAD302DE345861FBF928C228,
	AttackState_FinishAttack_m4A955DB10635495234A203B603B505371BF4B362,
	ChargeState__ctor_m7B756F79B3AF698C7024C2AB978307A4FC639D1B,
	ChargeState_Enter_m3F0DD13C47C6A10C91DEA35A8342905AB527FF85,
	ChargeState_Exit_m85DC1D2730E911F1D924DC641822917C65249D5B,
	ChargeState_LogicUpdate_mF4E811B953305AF48832785EAFAE9D50814BCDBA,
	ChargeState_PhysicsUpdate_m9BF2FAA7E8BA4675CCA8BEDA55D244EEDB1356C3,
	ChargeState_TickChecks_mFF794AC08563F78DE708FE34D468A9ACE5B15999,
	D_ChargeState__ctor_m49103D516126F9CAAC625E7227CC8BB3A9A7DEE5,
	D_DeadState__ctor_m1BC99FAD4D2FF11569BAB68C0FD54E20BFD203E2,
	D_DodgeState__ctor_m60A8C18DCEE68419881C1605C45AB474B943CA32,
	D_EmbushState__ctor_m1580DBDD9776E245EAC4777BBBD79205E05A1E21,
	D_Entity__ctor_mDF027D8680FA962248A1D660E1E80FC4F10BC8B0,
	D_IdleState__ctor_mED8037E108CB92FAB47F93A6967A061ABAE23DC9,
	D_LookForPlayerState__ctor_m0AD91F340CB0595B7FF0292F32CE6F23BDE78078,
	D_MeleeAttackState__ctor_m31A368F4C80F4EC1738CE57E56CE9D3762EDD4B5,
	D_MoveState__ctor_m7F58A56EA079E8AC9FC708517632EB9E8D867D5E,
	D_PlayerDetected__ctor_m94B2AB6DE983E10684584EE1A5A3E286E0EDCE07,
	D_RangedAttackState__ctor_m9D8EEB756E1AFD0426F7B9F2A30C28787D433927,
	D_RegenState__ctor_mFB4D35279A73C9BFA4ADDB3D13123038EBA1D553,
	D_StunState__ctor_mC0423E7C9B2C8885F3EE6EF0F41F909F4AD193FF,
	DeadState__ctor_m82AF591480AE53CD925635B2007905D46B77B324,
	DeadState_Enter_m94FEEF4A54717C667D64F67809EE7A8BC1DCE3EB,
	DeadState_Exit_mEFC9F4BE65AA67EE5B06935F5911771D339CDABA,
	DeadState_LogicUpdate_mD25E6AC37B87006BBFA41E1ABAF74CD2E0B31ADF,
	DeadState_PhysicsUpdate_mE9A146266C055BF045887D578C0B9ED62D01DC2B,
	DeadState_TickChecks_m19EF67020D07C69223C780E24B51495C56907160,
	DodgeState__ctor_m9E5199514F39EAD99FFAF5191380286FE5E7B134,
	DodgeState_TickChecks_m32BDF6236BAD06F1FC8DCA5DFE2706726102F610,
	DodgeState_Enter_mC98799871A3E17A0B7D7CA7086AA6FCB622F9B45,
	DodgeState_Exit_m1C24E82297DF7C4B0532FBEA74770E839CD17F59,
	DodgeState_LogicUpdate_mACE185E911D269CC42E156A18EBD1DED6A965D01,
	DodgeState_PhysicsUpdate_m72E26F891C0978A078CD3400D3AE079FCE7975A3,
	EmbushState__ctor_mA6AC939D7D6D2029B998171411CF8FA8DEC9653C,
	EmbushState_Enter_mBD461E15EDEDA2407E6E9FA8A5A03EDEB484B7DD,
	EmbushState_Exit_mC6D43A3AF933B1C070FDD111078ED7E3772BBA4A,
	EmbushState_LogicUpdate_m51930CA8413387F7BACEB850F5E1623CDF94503C,
	EmbushState_PhysicsUpdate_mC5A101BEA218EE0062705EB5E3A1451042BF32AD,
	EmbushState_TickChecks_mFE06E0B7D101090A6A34E7EC04537B7F21AE8158,
	IdleState__ctor_mF2362D03E8F851DF3D81D7E9B348124047B5DB36,
	IdleState_Enter_m51072142AC823A7C2FB871048D754F5299D17C94,
	IdleState_Exit_m9B85F8778772174C65C1B778350E52D2F4AAFC30,
	IdleState_LogicUpdate_mCEE0C6B9E98CB35527E8ED4A45F63E2914D3CDDA,
	IdleState_PhysicsUpdate_mA4F06FF1E82934D08DE3AFBB1E2F1D369CC56F35,
	IdleState_SetFlipAfterIdle_mDE60F5B1316B9AE7E8DD28F9267182DACA9AB693,
	IdleState_TickChecks_m0D1499F22370960C420FABBB21F0D613112A96AD,
	IdleState_SetRandomIdleTime_m10D3FEC0293C4B639724672A137DFC6DA4031455,
	LookForPlayerState__ctor_mA1CA9D40B3FBD3AE0C6B19CAC2CA7B9A53F2008E,
	LookForPlayerState_TickChecks_m35F1F6575EC6E009A103FFE1072267C2774F969A,
	LookForPlayerState_Enter_m583C106D344CDF25B6C4D42CD92CA60EBD3DBB0B,
	LookForPlayerState_Exit_m42C55374F41F3D35B61224E47273C87524C111C3,
	LookForPlayerState_LogicUpdate_m5BB1DBAD33CDD9FC9DE1C75270955703094BE338,
	LookForPlayerState_PhysicsUpdate_m6780D11A592EA103B375B92632C4EAE8C84B002A,
	LookForPlayerState_SetTurnImmeditely_m21241B79EE43257A6BE5C521062359963C8ED26A,
	MeleeAttackState__ctor_mFDFC1C278BE8443C8C713EBEEDCC36DEB9AC5A5F,
	MeleeAttackState_Enter_m9873973935277D815B98A86E5347E2DB89DAE546,
	MeleeAttackState_Exit_mE1B2B9D3CBC3CCEF7F850AA0C9FED724F511532F,
	MeleeAttackState_FinishAttack_mBBDA06914CECE6A8AB0040B411BA06D8D4FA163D,
	MeleeAttackState_LogicUpdate_m3EC01B7E6E7379F52FF6E46643B2C4E06A7ABA15,
	MeleeAttackState_PhysicsUpdate_mE0B8BD9BBCB8C2E453D5758AE3D3DBA5628696EC,
	MeleeAttackState_TickChecks_mB7526C2457A5DA60B83EC75417699EAF28C5163F,
	MeleeAttackState_TriggerAttack_m5203228B06F9139364252BCFB9B685C0D623470A,
	MoveState__ctor_m09EC17DFDBEA293B87C8C6782AA6293997683CCC,
	MoveState_Enter_m9949CC1758C872C442E37D4D78927BEB9966165B,
	MoveState_Exit_mFD8332161E17822D03DA0577058DAAB79FF34699,
	MoveState_LogicUpdate_m2B84E9CB40E745F8A9FAF82C072C856D8B4BEDE0,
	MoveState_PhysicsUpdate_mCB0BEE6BDB976A01E06CBD98AF04FB4983D05020,
	MoveState_TickChecks_mF15FB174785C967DA5ABD59EC38FDB9ED04B503C,
	PlayerDetectedState__ctor_m0896D0F12F8E351E3588A10C7A84D33A2063C821,
	PlayerDetectedState_Enter_m66A439724B1FD86C231453061F43AC7559DA2CB8,
	PlayerDetectedState_Exit_m9568F894FA587EA3C8198CAEA36A9D799C25EE2D,
	PlayerDetectedState_LogicUpdate_mFCBEED5F981BF08A1BE9660E6C8A076607937AD3,
	PlayerDetectedState_PhysicsUpdate_m18AB880AF598426230CED16DDBEFAACAA0E82CD2,
	PlayerDetectedState_TickChecks_mA6117411A96FE143CAEDC78B298A1B98ED3DCB82,
	RangedAttackState__ctor_m7EA2EC0C30DA1104F29AEC083C49BB9C79466063,
	RangedAttackState_TickChecks_m48A6606907DCCC6891403900032FB527876F6D50,
	RangedAttackState_Enter_mD6F1AE174F8630C0F753FFD35414A506FC4A459F,
	RangedAttackState_Exit_m10345FE1AF9EC8830EFD511B118E407622CE5F7F,
	RangedAttackState_LogicUpdate_m74BEEA408526D5F76DF8B6570FBC0D1FFA2524EC,
	RangedAttackState_PhysicsUpdate_mF40078770E3BDA22E6CA594F0C04A1A3628C1210,
	RangedAttackState_TriggerAttack_m6342D4D9CC27A041285CDE7F8D2665F93FEB107F,
	RangedAttackState_FinishAttack_m6E85E3020A1ADBE46F5D029E5EFE4DB6437099F8,
	RegenState__ctor_m2724C41CAB68845007621FD4CC223A50D23FA6E8,
	RegenState_Enter_m07C57B29071DB3082DBF4D1AFA2BB0982E9D1735,
	RegenState_Exit_mCCF1D830B84864CB55E2AECD45D4D3676996ADCA,
	RegenState_LogicUpdate_mF38762B83A3524B862BF188ED6891CCD523BE674,
	RegenState_PhysicsUpdate_m430D5A6F910D95A65BFF044E78B3BE8075083693,
	RegenState_TickChecks_m020174B48761805D3B04194616E9506D220F74E3,
	StunState__ctor_m42812B04B838B6A8154DF1D2DF0E5DEB8C20EFCA,
	StunState_Enter_m4BC2DE784AFE74A7C4F34A22A0E4B36EE20733DC,
	StunState_Exit_m6AA575AD9843084BEC2D27DF451E4D42292FE033,
	StunState_LogicUpdate_mB746CB8073E72985ED6CF8DB0341324FE1187508,
	StunState_PhysicsUpdate_m467137F2F9240926FCCB1E512B2E4C396F89F898,
	StunState_TickChecks_m29DDCC15F8AB5A0A61519956D71E09A691B2A0A2,
	TrainingDummyManager__ctor_m9FFB009E988E0A8E98BB6F5D808A9351BA259E0C,
	GameManager_Awake_m22F42B2A82708B10F652CAD8F2E0A4767110FF30,
	GameManager_Respawn_m3B6501D139E5DD8182689FF742BAE77C2295216D,
	GameManager_CheckRespawn_m5C4C4E2884D273703F7BBC8AFFC66F378B3357D5,
	GameManager_Start_m26461AEF27E44DB8FECCBC19D6C9E228B658BF8E,
	GameManager_Update_mC9303BA7C3117BD861F49F8E36151CC52117E6C1,
	GameManager_Quit_m3E5652D9A2DA57E3E7A31BECB511D9BCDD55BD75,
	GameManager__ctor_mE8666F6D0CA9C31E16B719F79780DC4B0245B64D,
	RespawnManager_OnTriggerEnter2D_m4471BF0806295EB599F0675E12F7174262C66A6D,
	RespawnManager__ctor_mB6363BB8AA7BDB0B7A69C57A719D345326B36A1C,
	ParticleToTarget__ctor_m558AD5B220853D0CF2D02BF182C8D4BB145556A1,
	ParticulesManager_Start_mA94055C82B0DBF63935E06DAE04C573C920EB450,
	ParticulesManager_OnParticleCollision_mC534CABAD89E34DA6CEDC762CD5F529AF03AF8EB,
	ParticulesManager__ctor_mC87F0B8D0A7085E1201AD26FDC92D46757C45EEA,
	PlayerData__ctor_mC2DA6B32832AB92B19E799148968590FF1F28C1B,
	PlayerInputHandler_get_RawMovementInput_m3AF315DDF95FB1856FB0B32630D2AA8465A01EB4,
	PlayerInputHandler_set_RawMovementInput_m249F8838F9EFBF919C0C984FBDF4802B30A8EB92,
	PlayerInputHandler_get_RawDashDirectionInput_m57B516A85E44CC80878A4C6448028D89244AC1A1,
	PlayerInputHandler_set_RawDashDirectionInput_m0592FE1B872D77A37D77D6ED61A244CEC8C93255,
	PlayerInputHandler_get_DashDirectionInput_mB0E58145AB8E3CB565361DF7E868497DC405CB2F,
	PlayerInputHandler_set_DashDirectionInput_m26B2D25468CC3DAC5D95B80FAD08B307B6671679,
	PlayerInputHandler_get_NormalizedInputX_mA055B906BDAFED306F18B6890C9F8AEC6EBC4C1B,
	PlayerInputHandler_set_NormalizedInputX_mD15166140EA451985E30D83FD75C79191044D713,
	PlayerInputHandler_get_NormalizedInputY_m314BDE45CC47AC6D3AC427116222A46F4AA3B35F,
	PlayerInputHandler_set_NormalizedInputY_m7B72D4E8D5184C43BA55074C4A6001845B61964E,
	PlayerInputHandler_get_JumpInput_m26BC043384005AD5F83600E9F29EA18D20F7FC7D,
	PlayerInputHandler_set_JumpInput_m8DA9FE7EE38724040D9798EBC8650AFA025C8365,
	PlayerInputHandler_get_GrabInput_m6B25B0BA68400B221515C83A2764AB339D3FDEFC,
	PlayerInputHandler_set_GrabInput_m74C4166D924773CB349B54AE66F7067F01F0BFE5,
	PlayerInputHandler_get_JumpInputStop_mA82CD3965C6E34FC1774E70C31E2E3FA66DB34FB,
	PlayerInputHandler_set_JumpInputStop_mBA621FCBD43FB41CE8E02157D2473E42BB773AE8,
	PlayerInputHandler_get_DashInput_mCA79E1D77F9D4EB5642491FEE12A99B61DB9CDBC,
	PlayerInputHandler_set_DashInput_m05E8DB0DF6ADF472CD4D53526C1F6813CA20EC3E,
	PlayerInputHandler_get_DashInputStop_m489F0B3E7E92B2843A4FE0B48224161A469CBA72,
	PlayerInputHandler_set_DashInputStop_m7DE101AF637FE87A3AC933A3293A263ABC423B88,
	PlayerInputHandler_get_RollInput_mD0B385A5857FE99B8E09694397A651C5A1A5251B,
	PlayerInputHandler_set_RollInput_m5EBEA8DFCB5AA109869EC6857D4C93BF2BF69B5E,
	PlayerInputHandler_get_AttackInputs_mFE74D1E6EDA13AF8F9B6749BC33DAE6B2BE63CED,
	PlayerInputHandler_set_AttackInputs_m6EE4797101A202E44849101A478FC5BECE62C85D,
	PlayerInputHandler_Start_mF2E5EBC5BB402A32542EA6B1487048DD83ECFDF0,
	PlayerInputHandler_Update_mF283E8945B879FBEA7530EB3F5C626B4CA1C8949,
	PlayerInputHandler_OnPrimaryAttackInput_m5BC5D64E7DDA95564F95DAC94A719675C9B3D2E9,
	PlayerInputHandler_OnMoveInput_m97461836C5180660F88BC89CE428B724E9E2F9BC,
	PlayerInputHandler_OnJumpInput_mBA10B81503CC3E121C3C605EBC467AEE5365CA40,
	PlayerInputHandler_OnDashInput_m08888E52DE467D072C873EE64BEBB6FB5706630D,
	PlayerInputHandler_OnGrabInput_m0704CF75910E66289FCB0EF24FC20B47F85887D9,
	PlayerInputHandler_OnDashDirectionInput_m363569B92E16DCABAC5B19EDA91B9BDA66C75167,
	PlayerInputHandler_OnRollInput_m6C2981FC15CD4C552DD539F2A4446173595922D2,
	PlayerInputHandler_UseJumpInput_m618F6ADFA138E2D1615EBE34C2E77BC8B1F321E1,
	PlayerInputHandler_UseDashInput_mDA9FCA59DBAEE514B4FC912796E96819A55DEBAD,
	PlayerInputHandler_UseRollInput_m6172E234BA679656907165FEFA2B7516EC94F572,
	PlayerInputHandler_CheckJumpInputHoldTime_mE034D66ECF20A583F098BBA6A3C1B22287E84489,
	PlayerInputHandler_CheckDashInputHoldTime_mB1D4AE9C00F9BBF3D7564FAA455B11E75B2EE5C9,
	PlayerInputHandler__ctor_m5A813BC103A59C67103EC4B8F9976D47C2E5F1EC,
	PlayerAfterImage_OnEnable_mF72041CF8EB96FEA16CDEF68FA9CEA5DAC59D3B8,
	PlayerAfterImage_Update_mA8F423CFC7BB69629245E7440DA66989BA808D71,
	PlayerAfterImage__ctor_mCFB7F4339C5F10065D66FF900F54E382B8FFEFA1,
	PlayerAfterImagePool_get_Instance_mC0331F5A143E84CBEC306312A464081139A3BA07,
	PlayerAfterImagePool_set_Instance_m2594D883D50656B1D24270EE78F5F6F8E88C3F4B,
	PlayerAfterImagePool_Awake_m59BA8EF9BD86EE7ACF6A95C7D104C306376113A8,
	PlayerAfterImagePool_GrowPool_m4CB61CA940F5D6D670076486720512A20A262897,
	PlayerAfterImagePool_AddToPool_m450E9DA28BDA6EF86223BA81867EB30EAA3F9AE7,
	PlayerAfterImagePool_GetFromPool_m426FD43C0F3F37BC58B4604CF813B0AC48629485,
	PlayerAfterImagePool__ctor_m114A98B7728C0675C5244D25B08B5DB9DEA6D513,
	PlayerCombatController__ctor_m78980447F79B11082A2D7C9B8B1A3C1666204FF5,
	PlayerController_Start_m9531F30EC892BDD1758A2EEC724E86EFBDA150A3,
	PlayerController_Update_mB31159CAD7DD2329859472554BC9154A83D8E794,
	PlayerController_FixedUpdate_m54EE3ADAA7597303B1F69849B233D1A68D880B14,
	PlayerController_GetInput_m4AFA3AE682A7A53D856CA648934BB322CEC6EB31,
	PlayerController_ApplyMovement_mEE8E1B38C5A305FD1FCA4B480CEB545E7D0FA1DF,
	PlayerController_CheckParticules_m66D4BE5163AE2408F0342D7BDD598AFF8930A610,
	PlayerController_ActivateJumpParticles_m892B0F1AFD078A2E9D1FBD1980B0BFB76F61035A,
	PlayerController_DeactivateJumpParticles_mD07C73C6C8B31428E287507CC4000FD055B96263,
	PlayerController_NormalJump_mA5A7C8373E2B7C88F4734F3BCDE9AB5C69E88D27,
	PlayerController_JumpSqueeze_mF96893621E21786FD3103599811EFCAEE38E5309,
	PlayerController_WallJump_m79BBF84ACFF8FDF4CFC69C5D6BAE9A87993C0F1C,
	PlayerController_CheckJump_m84A9DED55F65FDCAAC3ADBE0CED00A5531657A29,
	PlayerController_Knockback_m9301868379DFAD25C0F24E88112CBBF8A5AB6DAC,
	PlayerController_CheckKnockback_m79E84818DAE6A2F10C6C782EE1376E4DB326FE68,
	PlayerController_CheckLedgeClimb_m167F961F395FB06EAF39430E2781439D646CA4A8,
	PlayerController_FinishLedgeClimb_mF3FBB827B7DE082FECFE82D56609C1AE3B41D705,
	PlayerController_CheckIfCanJump_mE68E35B78B6D8DB6935D4FA5CB5AE72DE4ED817B,
	PlayerController_CheckIfWallSliding_m42F08E725399873BEA6DAE515CAE3A21C867F11D,
	PlayerController_CheckMovementDirection_m65436EDDF53E79E0D7407DA74371B03A7D5C4330,
	PlayerController_AttemptToDash_mE6A3367618A9EB68D924784EA971D1375A921AE0,
	PlayerController_CheckDash_mACAE3867180E01018925178589DBD74063A9A428,
	PlayerController_GetDashStatus_m9391AF5A488814FF81D5432DAAC864FD67E9E961,
	PlayerController_CheckSurroundings_m2638F4C6D34AC2DF775A1BBB39910C21BE7A244F,
	PlayerController_Flip_m78C6F4DC8127219F853348B761BD0EDEDEC88DD0,
	PlayerController_GetFacingDirection_m34476102285862EB1F9F3D9D1A7EC2FD675F8CBF,
	PlayerController_DisableFlip_m02C0DB710A9DABDBDAAABD98FC1C72EBAB019834,
	PlayerController_EnableFlip_m774FE2842B1B105FC490E676C65EA36FF2CE006F,
	PlayerController_UpdateAnimations_m6C34457263AD9886F900BC66A5F2EE7351054F56,
	PlayerController_OnDrawGizmos_mDB5E3315C6F60D4C95063F50B7FDC74B5B9E3C62,
	PlayerController__ctor_mF30385729DAFDFCB895C4939F6051DCE6C0327FB,
	U3CJumpSqueezeU3Ed__80__ctor_m5AAE49D54907DFBCD338FFCA511D9AB1C379C699,
	U3CJumpSqueezeU3Ed__80_System_IDisposable_Dispose_m5E07A734A0A78FF968D0C9510EC9CD566B8B581F,
	U3CJumpSqueezeU3Ed__80_MoveNext_m3AD5EA6ED368A9556A52B658F82E64AD5C2A260A,
	U3CJumpSqueezeU3Ed__80_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m06702FBA3AED7C7B6CAEB9B9F20D6C5CEC04DFED,
	U3CJumpSqueezeU3Ed__80_System_Collections_IEnumerator_Reset_m710EE58E3AC36491F6B7336784809E8F028F7496,
	U3CJumpSqueezeU3Ed__80_System_Collections_IEnumerator_get_Current_mCB3B01623B59C881F0F256BA0980497B693D0981,
	PlayerParticulesManager_Start_mC5D2BA338ADFB74B66EA6696302F727A8C11961D,
	PlayerParticulesManager_ActivateJumpParticules_mBC579250B4A81784D0DBE412EFFE54E71574636B,
	PlayerParticulesManager_ActivateWallSlideParticules_mDD20E077F1352B5D6DEEF4DF029DB3B0EF421A72,
	PlayerParticulesManager_ActivateRunParticules_m673B198A7065AFF75164072235BF1D1D605A9606,
	PlayerParticulesManager_DeactivateJumpParticules_m43EFF69867C548BFA8EF181F0FE68F7EB2317791,
	PlayerParticulesManager_DeactivateWallSlideParticules_mB3B0C7E4BA046C34BF386E53E519AB132DFD83CD,
	PlayerParticulesManager_DeactivateRunParticules_m1FCD874528D407D1BD28170EB29BFFCEE3F56E16,
	PlayerParticulesManager__ctor_m49368CB727149BBC6BB0BCD349D44E1A78708852,
	PlayerStats_Start_m4D4AE70D5DDA0FC33DE88634198F70275FF512DC,
	PlayerStats_TakeDamage_m5E9B290AE295F8BCD9F72BC3754D9B940F581A7E,
	PlayerStats_Die_m9DD2B794182DBA6221B9A2B6D193D36621CC2B16,
	PlayerStats__ctor_mE0D15E1136221C95781FA81A050F868076E05620,
	Player_get_StateMachine_m2198B858EAFA76B007558A67FE1800D043EEDA7F,
	Player_set_StateMachine_mF55EAB7CEFDAB9B9375A6CE98C8ED4857095C817,
	Player_get_IdleState_m4B4159BE756438FAF50BA682E94CD97EB3359CEB,
	Player_set_IdleState_m1EE2B0F91F5A91BD0A627D60C72577C9DD166515,
	Player_get_MoveState_m2C72FC94E47A1983A5C54C61D78172EFC9C7FFEA,
	Player_set_MoveState_mAA36A7247EFFD915F255D9D9D21CD75DEF4B92F4,
	Player_get_JumpState_mDCA6ABA3DD458DACF52879CB57FDF3F2F49A8C19,
	Player_set_JumpState_mC83D41A9D9309166C7B64C32CC826716396A4336,
	Player_get_LandState_mC544BBC946DD7A8946BD110AAFFD63BFD75D5A05,
	Player_set_LandState_m3F4AA4D6217A9AC555D3AC47713B9877DE042FC8,
	Player_get_DashState_mB0A4A3373F0BC1914A8C29F9B74198E988A5551B,
	Player_set_DashState_m5E4CF4807B68E74EA16A2E2C7582A93CEFEC899F,
	Player_get_RollState_mC5A5F1F61962B883EB24D2432EBB9098D61940C4,
	Player_set_RollState_mE37EBB798B56DCF56F8192933DD8F74324917E66,
	Player_get_InAirState_m08BD915E02EE2B66D2AB5DC812E576D6A4669E78,
	Player_set_InAirState_m1F44F2A60DF33B57D699FEE7E6A95DB064659B77,
	Player_get_PrimaryAttackState_m807460CA47D3824FA6796238F7B04F01CA3313A3,
	Player_set_PrimaryAttackState_m353DD6D335A366EDD0A083D2DC4374377CD224D5,
	Player_get_SecondaryAttackState_m7824B1833AE6BB387028FFB4ADE12DD88DBECE44,
	Player_set_SecondaryAttackState_m4CB9971C06C0332E70B8896A516A3B4FC6A730EE,
	Player_get_WallGrabState_m7224F71AD0CBD49F5D9E207E4C9415BA8DECBDE7,
	Player_set_WallGrabState_mFBB5753BBC6E5C989214CD5721CFEEA8C8932709,
	Player_get_WallJumpState_mAF0DF68CEF7E155E54442E0E5656F2D65A82C903,
	Player_set_WallJumpState_m7B1D2F1AAE95CC1B13CCDB4542C488EC1E38BD3A,
	Player_get_WallSlideState_mF9D7F216CF8603587C057EE999FEED40FA5BD5E8,
	Player_set_WallSlideState_m0CA05530916005E69FB08774E73227B8C920681C,
	Player_get_WallClimbState_m542A476F6A239889AFE80076B9EE369D89CDFB34,
	Player_set_WallClimbState_m42742C45FD72FCF3C9056681D2E5E63452129FEC,
	Player_get_LedgeClimbState_mBEA2BAA8F845E81BAD0FC6E211F9A5F9D2D23591,
	Player_set_LedgeClimbState_mA1AB9E354332E6A9778B00D7974D7C2FF7131C4E,
	Player_get_CrouchIdleState_mB32BA08F68A49B4759821B0642D88961A75072DD,
	Player_set_CrouchIdleState_m0F5297FBE37CB7CBAB5184F85B67D739EA4D6171,
	Player_get_CrouchMoveState_m988D07B15EE791346C447F268A580A8CBD425CB1,
	Player_set_CrouchMoveState_mE50AF05E29A4B93852F2A6D182CD4965F5C2AED4,
	Player_get_GroundPoundState_mC9CFE723E0B384D69A41B9AECF0C775CDAE066F4,
	Player_set_GroundPoundState_m2075D69193DC02A711BACCDF89E6D766E10859A4,
	Player_get_dashDirectionIndicator_mB22CB5447A611B5E1CDAD8A48276AC6890A5A12E,
	Player_set_dashDirectionIndicator_m78FE4F2A6E91DE1BE4EFC576382C570B5B42C1D6,
	Player_get_Anim_m9221694B09CA9B7C315CE0FEB3655FB1BFE19112,
	Player_set_Anim_mCB2981090965D89CAB5605C26E223637379C2BC6,
	Player_get_Rb_m2B4FF51AC6E82017EA8B69AD0379F7078BC31144,
	Player_set_Rb_m787D661181B65CC25AB5F1B1FEC06AA4BC565B90,
	Player_get_InputHandler_mCFA3AEA34BE76A7F4020A4ADF30EFAE0011CE695,
	Player_set_InputHandler_mD2DD228A45907B3AFA5AEBD74DBFCD0BD08F045E,
	Player_get_MovementCollider_m1F5621F1F42114456C9A7FBDB225BBF35C7727C3,
	Player_set_MovementCollider_mA5520C56E1609EB0CCBD8FD744FCF7213A0AA2FD,
	Player_get_Inventory_m9860D2EAF7984A6B94831D1822AA54EAA4BB06EF,
	Player_set_Inventory_m5EAAFF570CCD7B758B2D58A1E486C61AB90A671A,
	Player_get_Core_mA72B07C402649E359C5DAD0323EE65E9605A5FE8,
	Player_set_Core_mDD73A7FE2B282CEC0F489509082C4A6A545C535B,
	Player_Awake_m1131F11CF6BF6FBE6454601C7D9A94AC8F468A24,
	Player_Start_mBD692B64AAC791B93A589E7D3596F36787EAF021,
	Player_Update_mBA04F1D6FE3C18037EA95DFAEEAA9977BFD49CD3,
	Player_FixedUpdate_mD7447EDFC86F29A3E5FBDEF7E0139535BD4C5088,
	Player_SetColliderHeight_m8547071A8C8336D256AB495B61D7244BFCC63459,
	Player_AnimationTrigger_mBE4430E63FC5BCE0702D8FB97E4AC2FAEA871DFD,
	Player_AnimationFinishTrigger_mBCC1891FF169F290CF363089D3F43F557B047CC5,
	Player_ActiveRollParticle_m798123F4ABCD3FC17789921D7FD2CDC094FC359B,
	Player_DeactivateRollParticles_m5DF6C55726FA1FF641B681F15414080BED4F171D,
	Player__ctor_mDE8EB5B351975D4E7E24DE341B8B49B8A29CC2B7,
	PlayerState__ctor_mBBD1B6CA2B248B98E457773E4EC58332256FCA59,
	PlayerState_DoChecks_m0493A20EBFB37FC588EEF3E4C694C03DFB677D51,
	PlayerState_Enter_mBF1461BBD6A3D0F11ED2333152B89614D757F4B4,
	PlayerState_Exit_m8A7AE4B644E5E38A57BD9EB52F1C1C53EDEC0A3A,
	PlayerState_LogicUpdate_m5AFB229BAF38D34B611E2CAA3CA60F05E32156B1,
	PlayerState_PhysicUpdate_m0B2619B3295B08A5921DE790504E90819BE624CA,
	PlayerState_AnimationTrigger_mD3BC05F52C83DD46D74A09CC1A31A2D1C64AE972,
	PlayerState_AnimationFinishTrigger_m9F77E914D6DEB987DCFC7E2B3CF12729E0DD0803,
	PlayerStateMachine_get_CurrentState_mF308023C3A5D7FE149663D92BD483E9528EA060D,
	PlayerStateMachine_set_CurrentState_m8E18DA6734485836D3D40FF276CE728788FDF764,
	PlayerStateMachine_Initialize_m047039DD815DAD874A94ADE4F9085331FAAF8B41,
	PlayerStateMachine_ChangeStateTo_m7D8DFDD769B2995C181F3E106A510E6C773C5490,
	PlayerStateMachine__ctor_mEC680EE294695F01B04B9B57D933DD5F063D1C84,
	PlayerAttackState__ctor_m7C9145298561F41D23135ED2D99CD3C5485CD8E2,
	PlayerAttackState_Enter_mA4B8C2C351CB0F594546B1269A06D12E1104A52D,
	PlayerAttackState_Exit_mD4DE091D4575745CC8D9D30740788D3EE7B76D8A,
	PlayerAttackState_SetWeapon_m114E638CE46797F74119F0654ECC4CBF4D31013A,
	PlayerAttackState_SetPlayerVelocity_m10D9988CB820294D061637A206F9DFCF3B79D19F,
	PlayerAttackState_SetFlipCheck_mE38CBF231E57625F0FE84434F347B57F7CDE284B,
	PlayerAttackState_AnimationFinishTrigger_mAC7E3A4826EE06084368B1CA02CC66C4496089D4,
	PlayerAttackState_LogicUpdate_m090B12EF63A90AC722E2240F68CF679818F44CD9,
	PlayerAttackState_DoChecks_m88AEF25CF639FA3D9EF9DA523BBD7A81FDE1C664,
	PlayerDashState_get_CanDash_mA262276ADE682D009EFFE00E43C45E68BE6F520C,
	PlayerDashState_set_CanDash_mC48669CC87DE45F3614F5C7944220EE488015911,
	PlayerDashState__ctor_mF277FAC947195100B57EB8FF51076A1B5C1C5BFC,
	PlayerDashState_Enter_m247D12F4D49AD755836A4F3FB4341D1233EF9C19,
	PlayerDashState_Exit_m4A91B3814DFD5AC8ED1207B3775923BCC763BCCF,
	PlayerDashState_LogicUpdate_mC958921EC03AD403D1292DA3A21E0E691E7F6041,
	PlayerDashState_PlaceAfterImage_mA7FE344D09FDFABEB8E2B185FAACE525FDCABBEE,
	PlayerDashState_CheckIfShouldPlaceAfterImage_mCEEC762DF33427038A542C8F1C04A80384AE398A,
	PlayerDashState_CheckIfCanDash_mF29CBE3B0F66DF394D1A119F73CFA2977D2308D0,
	PlayerDashState_ResetCanDash_m3BD6D13A2DF877F61A66853F80FE66507921B25C,
	PlayerJumpState__ctor_mFFDA8B78652E5A6BFB20F590506CC5C755CD8C9C,
	PlayerJumpState_Enter_mFF3D4664A054849912D7BE1C71DD86AB0907733A,
	PlayerJumpState_CanJump_m1B53FC213A579C553F7773856D282F750827D109,
	PlayerJumpState_ResetAmountOfJumpsLeft_m32DBB19FDC09B9C5CCB7606C267856FB627E15C0,
	PlayerJumpState_DecreaseAmountOfJumpsLeft_m8CBE45F6353BE4CDB7F0911AC36F9EFA96B91718,
	PlayerJumpState_DecreaseAmountOfJumpsLeft_mD6370F9C76EB365E2B4F300F721EF743FA23E0D3,
	PlayerWallJumpState__ctor_m78B8D9EAF011BFC3D1A0BA0F976B9326F21DF181,
	PlayerWallJumpState_Enter_mDB828AE7CA9C5395A4E1AC9BDEC4422E26833E5C,
	PlayerWallJumpState_LogicUpdate_m6287177A521525FDC0E7143D090C5EB2BF882DA9,
	PlayerWallJumpState_DetermineWallJumpDirection_mAFBF966CA65D6E7E31343D39ABA01C7FDB2140D1,
	PlayerCrouchIdleState__ctor_m95A13333D66EFAF76EF16DAF8D497F483F326AF0,
	PlayerCrouchIdleState_Enter_mA9578EA05088B735D3BFD5DFA1B5888ADCDF761A,
	PlayerCrouchIdleState_Exit_mEA524886A5367A6F7D4251DBEF293E8065997218,
	PlayerCrouchIdleState_LogicUpdate_mBCB232513214F8E61F3BF4F6166F7793DB82DC68,
	PlayerCrouchMoveState__ctor_m3956151970D421F0C3DFB624E6D0B8F2112532A5,
	PlayerCrouchMoveState_Enter_m464116C03059D01C4CF9EF61BD578A7F08EAC8D6,
	PlayerCrouchMoveState_Exit_m7827506D50396A0B3B2EF2254F134072C0F1D8DB,
	PlayerCrouchMoveState_LogicUpdate_mD578517912D63ACDDF4078FB77A8F4D1FCB14D1F,
	PlayerIdleState__ctor_m488850B99AA8E87D24B5A9C71C93E976BC232E6B,
	PlayerIdleState_DoChecks_mA49A8D1CFCCCA946F6441DEC96035B8CC8A9CE9D,
	PlayerIdleState_Enter_mF9FFABBD1BAA897F4B26E7068A6406BA34BED0C8,
	PlayerIdleState_Exit_mC7CA6F8C0136D65341E50B6F1BF28BBF3CC88D88,
	PlayerIdleState_LogicUpdate_mAB09D5BB6C5F713605C75E5364109D8BB16840DD,
	PlayerIdleState_PhysicUpdate_mC9A0A9F447626BA656D888325B8EA422931C7AC3,
	PlayerLandState__ctor_m07A9132A7946F638545C0C07B432C222C0952ABB,
	PlayerLandState_DoChecks_m0F18F534716FB3D6F1B520A45315004BB7F28BBF,
	PlayerLandState_Enter_m711D3F152ECD9B22105B57FB9E45F335CA88B456,
	PlayerLandState_Exit_m2F85F8F12B384F6B0437C70C22D7009037DE4FE7,
	PlayerLandState_LogicUpdate_m00E15B18BC7270F9EF04215EDF2E9D10555CF83B,
	PlayerLandState_PhysicUpdate_m4E74532C6DFD93F9D577C0A8B679D17F2DE5DF11,
	PlayerMoveState__ctor_mD5A70EE29E482F9AF783CD11235F10C706E1EF5F,
	PlayerMoveState_DoChecks_m7062B511BBC685183F1AE34237B2C3C1F6736835,
	PlayerMoveState_Enter_mFC5D54C7CC3694500D03DD4429D6B98492D81EE6,
	PlayerMoveState_Exit_mF979FA0C8EC5D17A91757F67F110D276DD7E840E,
	PlayerMoveState_LogicUpdate_m51BD7E1FB796BE25AEACBBAD3B529B5920BB3A59,
	PlayerMoveState_PhysicUpdate_m0AA69F2DAF345F4A405F39D1C4B971B82FFE1741,
	PlayerGroundPoundState__ctor_mAD0CD91C11FFABBA89FCE22361E8824488489711,
	PlayerGroundPoundState_DoChecks_mCC7645E6787E2E30790C0F6CA777C32F5CAE828F,
	PlayerGroundPoundState_Enter_mC34A63A489CA7C798B51CFD1FDE06394A2D99FCC,
	PlayerGroundPoundState_Exit_m49DA5F4C4D658A2737A2A30BD31B60B7C71A76C2,
	PlayerGroundPoundState_LogicUpdate_m55A4DE466DF5A4C16F12ADBF67E355D84D95A9AD,
	PlayerInAirState__ctor_mC55E393C74EECD473F357E7B3B325E9157D3A7F8,
	PlayerInAirState_DoChecks_mEBAE8897C190FA9490EBC7A89B3D25AD45ECE60C,
	PlayerInAirState_LogicUpdate_mE24B63A690557FD8902C035D224D90C14D590053,
	PlayerInAirState_Exit_m0E2F012A1396BE0B3659D4172B3CF1B3CDA6D479,
	PlayerInAirState_CheckJumpMultiplier_m25B746B93F0DE4FCAEA316BEC004423542CE522D,
	PlayerInAirState_CheckCoyoteTime_mF958F210F4AB3BEE3F4696B2684ED7701D28FDD4,
	PlayerInAirState_SetIsJumping_m4717E283CE01E0C745469B5A6E6C6EDBFA88CCB3,
	PlayerInAirState_StartCoyoteTime_mE052E9C1FA3386B86827F3900ACCBA80DECB01D6,
	PlayerInAirState_StartWallJumpCoyoteTime_m92B62CC05212A6D43D554A90082F463206286973,
	PlayerInAirState_StopWallJumpCoyoteTime_m72FDFE076F7AE733E21479793F9CDD5DAB7977A5,
	PlayerInAirState_CheckWallJumpCoyoteTime_mA68C0EFB38A6D99734395580F04FC08C954F5754,
	PlayerLedgeClimbState__ctor_m9C41AE29A34E709AA2F75485BA0918A9AB3148BF,
	PlayerLedgeClimbState_SetDetectedPosition_m4A20E87870C24357264DC8ECFB908F8AC2B17ACD,
	PlayerLedgeClimbState_DetermineCornerPosition_m347C8E0AC299F20E1D180D538E7791A5640F6C03,
	PlayerLedgeClimbState_AnimationFinishTrigger_m2F4B48AEF3B733C52DC063FBF5A1273C296B2DB2,
	PlayerLedgeClimbState_AnimationTrigger_mC7F33003ABA2EB927BFEAC53DD948FA75ABE0964,
	PlayerLedgeClimbState_Enter_mB83DDEAC3728FD40C0A7C01619A8DDA805EABF8E,
	PlayerLedgeClimbState_Exit_mF3E1C5F4ABC1B2910A0FB6499710FD0A6741803B,
	PlayerLedgeClimbState_LogicUpdate_mCAED3EB4976E17E89C3F428B5323406C5E88CE57,
	PlayerLedgeClimbState_CheckForSpace_mAE7093D57A3BB11DE315E4722CE8A32B6231E17D,
	PlayerRollState_get_CanRoll_m8D12AD049072AC064E787C5AD7DDF338A38D80EC,
	PlayerRollState_set_CanRoll_mDABB853ADB7608625E4029A265AFF798F1EE73E7,
	PlayerRollState__ctor_m116862B5DE4374687AA95317F13F78EC3A68AC83,
	PlayerRollState_Enter_m43EB59AEAFDCE5D4A8674BDD6E3CEBA414693000,
	PlayerRollState_Exit_m288396623841CB1A753BE73AB2ADAEC89DAF150E,
	PlayerRollState_LogicUpdate_mF832E86A8F885A7513A5113CEAF7927DB6B9F523,
	PlayerRollState_AnimationFinishTrigger_m36DAC24F6D0780117EBE57D99A901F1D810C34BB,
	PlayerRollState_PhysicUpdate_m4577E60F74D29B0F8028A2575ACC00857A5B7C08,
	PlayerRollState_CheckIfCanRoll_m3F52F9969F959B4B3AF62363DC4C8A1BD4292193,
	PlayerRollState_ResetCanRoll_m7FEE29B1A9069FC87D3DDD694123DF67ABC34440,
	PlayerRollState_DoChecks_m885BEFEF0A21A33FC460C759AE7195447F3A1C0F,
	PlayerWallClimbState__ctor_m6C93051D24EA881B5465E9FDFC7F2E3800C21C33,
	PlayerWallClimbState_Enter_m21D40F72E5F61A050D1587E14C5AB4C45024C2BA,
	PlayerWallClimbState_Exit_mAD42F6AD9A3636615D227825CD2516D5FFCDCAD6,
	PlayerWallClimbState_LogicUpdate_mD46FAF47BF9A85407F2550CEFB3EADE0CC80C25E,
	PlayerWallGrabState__ctor_m570DA0870A52D6A163DD2005E1393485F9E64EA8,
	PlayerWallGrabState_Enter_m78DC3F39508640BB47CD74FBC53A585D83DE5C22,
	PlayerWallGrabState_Exit_m41C63259015BC7144B6DD2FD9EABB8BD4064A67D,
	PlayerWallGrabState_LogicUpdate_mC3A93A9AABFC61480D69FAC96D060F1B437A08B7,
	PlayerWallGrabState_HoldPosition_m7433AD54FBFE1DD5A83F3963362A21A0569E9330,
	PlayerWallSlideState__ctor_m9903A9937CD5F6321D914D577C16969046A7F07C,
	PlayerWallSlideState_Enter_mEE16386BD6A908D6CAFDDE1D5E788EAA7FF0E3F2,
	PlayerWallSlideState_Exit_m6942C199021E1D02F591A69907AF5069C1A99C27,
	PlayerWallSlideState_LogicUpdate_m98004D4882107007D84E3A02448A7BBE87F1665B,
	PlayerAbilityState__ctor_mEA5C9D00057030D53B714FDFF62E673989157787,
	PlayerAbilityState_DoChecks_mCDD808CB26E6772848E67AB183A30DD34379FBBC,
	PlayerAbilityState_Enter_m40E748F18F02CD94AC6E6DF6EA3106F15D057C78,
	PlayerAbilityState_LogicUpdate_m51138343B66E0DB8AEB3BC527F6A7C1300436D71,
	PlayerAbilityState_PhysicUpdate_mA4AB2EA4D0E45977EFCF121D37B51EA31C70B658,
	PlayerAbilityState_Exit_m0AF77B26376A7E5D2E34A200B5680C1A3F4D1C85,
	PlayerGroundedState__ctor_m38E11EB17C9646647FCD796FD7BDB8E03865DE91,
	PlayerGroundedState_DoChecks_m898912949CF3BA356289A12A8C010F0F09E9D9AE,
	PlayerGroundedState_Enter_mBA3C213E0FA68FCB44C0CFC269E9EFB595A56C2E,
	PlayerGroundedState_Exit_m3E5548079501BFBCB6B49937584E14B5ABA1B13F,
	PlayerGroundedState_LogicUpdate_m222127BB700087F1AC5DC0CC49061D6D183AF5A6,
	PlayerGroundedState_PhysicUpdate_m1BE494577B76C0F82DDD4B95594F86EF2D508BB4,
	PlayerTouchingWallState__ctor_m9D4A2ED21FA22F634089A7F32F9C2E9D287FC070,
	PlayerTouchingWallState_AnimationFinishTrigger_m9921B66168F923E4A4045B59D1A3BE47FFFD71DA,
	PlayerTouchingWallState_AnimationTrigger_m9A3372BE80B3E7C300A47352DE119FEA6801683D,
	PlayerTouchingWallState_DoChecks_m793AB5C27C547E81CE7873065C482E2B1BA759A2,
	PlayerTouchingWallState_Enter_m5EB8BA304DB10FBBE1672F0AA7F4941BC72E4833,
	PlayerTouchingWallState_Exit_mAF79B2575EE936A8A40E242010C680A9AF36AA68,
	PlayerTouchingWallState_LogicUpdate_mFFF80A30CFAF4AF2E29413796259D9FB227752CB,
	PlayerTouchingWallState_PhysicUpdate_m471EEA27224D52FCA85E86FE3968549BF7A35205,
	PlayerHealthbar_Start_m359AA2749E600E417607E53D594D78FFF45E955E,
	PlayerHealthbar_UpdateHealthBar_m5E9A22A15D0F8AFE1ED6F3BF81E3E207284CA4A8,
	PlayerHealthbar_Update_m1F3D0929B94538067B23282D6B5910807956D00B,
	PlayerHealthbar__ctor_mF8F4E1428FC3FE537DB3F705A205A8AE4127A980,
	PlayerInventory__ctor_m61E3BB66C1E62760440A0BF22DFCE0B247AC1D8D,
	AggressiveWeapon_Awake_m674698FCE8BBAE39AC0B75E3FD49A2A5F0E62757,
	AggressiveWeapon_AnimationActionTrigger_m174AD8B699F773DDF11C8940D2955785E1886F5E,
	AggressiveWeapon_CheckMeleeAttack_m281FC6666B2231EA923CC0358A8657EA53C63EE7,
	AggressiveWeapon_AddToDetected_m7E1F3B18B760C5AA2E24C15A3797859EBFE525B8,
	AggressiveWeapon_RemoveFromDetected_m184626228DE15FE60710932C20FE047BEAD44D0D,
	AggressiveWeapon__ctor_m0EDBC926A9E34BF85EDF9E77CBB184263F6FCF0C,
	SO_AggressiveWeaponData_get_AttackDetails_m466D82DC22DA83C637F7FF76A4FD9E87EE5E3575,
	SO_AggressiveWeaponData_set_AttackDetails_mDE6C0D04379A649F8DC95FF1526E26FC4DC1AFCD,
	SO_AggressiveWeaponData_OnEnable_m1CB88620492457FE2AA4723144C7417B1D3C471D,
	SO_AggressiveWeaponData__ctor_m6F94B89F734F667C7B400E4527C0E1A38D5106B0,
	SO_WeaponData_get_amountOfAttacks_m43D85164E4E01F5DA4862C6F994E2DFA7ED099A3,
	SO_WeaponData_set_amountOfAttacks_m8E605A5F587C238F35CC3F0DDCF5273D6CE0B6C8,
	SO_WeaponData_get_movementSpeed_m68D54FAA18F29BE81FEB3B5CF85DDBA76BC71790,
	SO_WeaponData_set_movementSpeed_m832F6C640FEB51C01445F1FA27D3E53972111A1E,
	SO_WeaponData__ctor_mB19489CDDB758BA6E49A53DEC6793FBEE60CBD62,
	Weapon_Awake_mCFA0CE996E9FAA699B578ABB8796DED719536A5B,
	Weapon_EnterWeapon_m7088D4FBC7443AD739E3F5EAF62D4C21DA78B397,
	Weapon_ExitWeapon_mCCE913A59A9611B6711AAEDBAB6DD89C656FD2B5,
	Weapon_AnimationFinishTrigger_m7D77C3F4F97BB179AC7B4792D903B228398A8966,
	Weapon_AnimationStartMovementTrigger_m161DF91F29F965F3E4509C17B262E478AF75BE96,
	Weapon_AnimationStopMovementTrigger_m217753007AD7E9DA2E763913B77EDF0AFF77CE5A,
	Weapon_AnimationTurnOnFlipTrigger_mC3B0DED763FF7A3A890B779ABE37A8442A92C024,
	Weapon_AnimationTurnOffFlipTrigger_mBD8C12FB7BF1DE16EE8B9E9857F4CDD53B63D66C,
	Weapon_AnimationActionTrigger_m85C267206A4386E0E24B6D2EC60250747D249C24,
	Weapon_InitializeWeapon_m854207E6153645B0FA1F314DD47A8FD8AFE5EBC2,
	Weapon__ctor_m643DE56148B24BD987E564400E443ACDF43CDB97,
	Projectile_Start_m065C53350564E17D5A0A0322FF064F8C9697DAB6,
	Projectile_Update_m382C5B499BD4599FE34A04DA3DA0701077C710B2,
	Projectile_FixedUpdate_m18A97CA278CB70690315AFF74AC809179B2A94B1,
	Projectile_FireProjectile_m9AE7A2D3E94C499EBC6EA0EA8AB251DBC29AAD95,
	Projectile_OnDrawGizmos_m0F4890032EFAD3EE1F22111B5941CDA89FD61E71,
	Projectile__ctor_m22DAC83BA9B394316027755FD2ADFCA806EE39BB,
	Restart_OnTriggerEnter2D_m1D4019138EAB429FC9B0D70E37C95D2E9627B279,
	Restart__ctor_mC040D6C84CCAA3DF8BE8CDBF1C030FBAA1331FAE,
	Spikes_OnTriggerEnter2D_mFE312F753147C6FFDBEA498886ED1501B19DDEAA,
	Spikes__ctor_m97F99FE6120BD57A3783EEEB43244791DCE0511D,
	NULL,
	NULL,
	NULL,
	NULL,
	AnimationToFSM_TriggerAttack_m50D927BAB09702F735F6DC293240CC326D1773AB,
	AnimationToFSM_FinishAttack_m15F4B3990243E65C56C3130EB9A0382A8C02DC0E,
	AnimationToFSM__ctor_mD2712448244F972BEE4CCBEFE10A36C762097E00,
	WeaponAnimationToWeapon_Start_m0D39E00F77A9ECA2E98E58A49938975FC299B77A,
	WeaponAnimationToWeapon_AnimationFinishTrigger_m453082E0A0AA5787477C203ACF3006272F0FA882,
	WeaponAnimationToWeapon_AnimationStartMovementTrigger_m5ACCD871C6E32E56C0FE6366E6121652C566170C,
	WeaponAnimationToWeapon_AnimationStopMovementTrigger_m83C8AA63F240B9B80A10594B983EA41A7BB14378,
	WeaponAnimationToWeapon_AnimationTurnOnFlipTrigger_m028F7DDEB8079C35A0D52D67B7F7CCC7BC1FA212,
	WeaponAnimationToWeapon_AnimationTurnOffFlipTrigger_m38278DC3E5963B58D77C377D6CD26A91EBD4BBDD,
	WeaponAnimationToWeapon_AnimationActionTrigger_m98697E1848D8513F571F47D24ED6E26C9300CFA1,
	WeaponAnimationToWeapon__ctor_mC7DC3574700B82D532EC8DEAB4B30532E0F80BDB,
	WeaponHitboxToWeapon_Awake_m8C2CE82D19ED70F01CC7F23A33DB3AEA3667982E,
	WeaponHitboxToWeapon_OnTriggerEnter2D_mC7AD74F0DE45E8C488F3A7B164B877EA9DFA6266,
	WeaponHitboxToWeapon_OnTriggerStay2D_mDCEAF9C124CB9D7EB99BB06DE5B131B4B966FDC1,
	WeaponHitboxToWeapon_OnTriggerExit2D_m8A44B5668F56ED64507639B2DDC738C6050A1758,
	WeaponHitboxToWeapon__ctor_mF1A8F9C8827DC66E129A9E9258915B7AD3CB0491,
	ChatController_OnEnable_mBC3BFC05A1D47069DFA58A4F0E39CFF8FAD44FF2,
	ChatController_OnDisable_m268A7488A3FDEB850FC3BC91A0BBDA767D42876B,
	ChatController_AddToChatOutput_m43856EEA133E04C24701A2616E7F10D7FFA68371,
	ChatController__ctor_m3B66A5F749B457D865E8BDA1DE481C8CF1158026,
	DropdownSample_OnButtonClick_m97641451A2EAC4FC6682897A948A37FDD6BF9EA6,
	DropdownSample__ctor_m87664E3DC75A1A9AA19FEE6ED6B993AB1E50E2B9,
	EnvMapAnimator_Awake_mFFC04BC5320F83CA8C45FF36A11BF43AC17C6B93,
	EnvMapAnimator_Start_mC0D348CAB0F0DC920EF3D3A008688B533F66D1BE,
	EnvMapAnimator__ctor_mC151D673A394E2E7CEA8774C4004985028C5C3EC,
	U3CStartU3Ed__4__ctor_m4D65F4FC2207AE4B6BE963AF9B5EDC55C7E29B23,
	U3CStartU3Ed__4_System_IDisposable_Dispose_mFEE2ACED70A3D825988E28CC61FEF8DCD7660A5B,
	U3CStartU3Ed__4_MoveNext_m2F1A8053A32AD86DA80F86391EC32EDC1C396AEE,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03E2A57659B8F598AC450183F55D43F903C0A1E,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m268BA538CF6812C56EB281C0CE29D5AA2E9A2CAB,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mFE9CA9F52F9AFA1E4C9DBADA2064F854D6931CFF,
	TMP_DigitValidator_Validate_m5D303EB8CD6E9E7D526D633BA1021884051FE226,
	TMP_DigitValidator__ctor_m1E838567EF38170662F1BAF52A847CC2C258333E,
	TMP_PhoneNumberValidator_Validate_mF0E90A277E9E91BC213DD02DC60088D03C9436B1,
	TMP_PhoneNumberValidator__ctor_mB3C36CAAE3B52554C44A1D19194F0176B5A8EED3,
	TMP_TextEventHandler_get_onCharacterSelection_m90C39320C726E8E542D91F4BBD690697349F5385,
	TMP_TextEventHandler_set_onCharacterSelection_m5ED7658EB101C6740A921FA150DE18C443BDA0C4,
	TMP_TextEventHandler_get_onSpriteSelection_m0E645AE1DFE19B011A3319474D0CF0DA612C8B7B,
	TMP_TextEventHandler_set_onSpriteSelection_m4AFC6772C3357218956A5D33B3CD19F3AAF39788,
	TMP_TextEventHandler_get_onWordSelection_mA42B89A37810FB659FCFA8539339A3BB8037203A,
	TMP_TextEventHandler_set_onWordSelection_m4A839FAFFABAFECD073B82BA8826E1CD033C0076,
	TMP_TextEventHandler_get_onLineSelection_mB701B6C713AD4EFC61E1B30A564EE54ADE31F58D,
	TMP_TextEventHandler_set_onLineSelection_m0B2337598350E51D0A17B8FCB3AAA533F312F3DA,
	TMP_TextEventHandler_get_onLinkSelection_mF5C3875D661F5B1E3712566FE16D332EA37D15BB,
	TMP_TextEventHandler_set_onLinkSelection_m200566EDEB2C9299647F3EEAC588B51818D360A5,
	TMP_TextEventHandler_Awake_m43EB03A4A6776A624F79457EC49E78E7B5BA1C70,
	TMP_TextEventHandler_LateUpdate_mE1D989C40DA8E54E116E3C60217DFCAADD6FDE11,
	TMP_TextEventHandler_OnPointerEnter_m8FC88F25858B24CE68BE80C727A3F0227A8EE5AC,
	TMP_TextEventHandler_OnPointerExit_mBB2A74D55741F631A678A5D6997D40247FE75D44,
	TMP_TextEventHandler_SendOnCharacterSelection_m78983D3590F1B0C242BEAB0A11FDBDABDD1814EC,
	TMP_TextEventHandler_SendOnSpriteSelection_m10C257A74F121B95E7077F7E488FBC52380A6C53,
	TMP_TextEventHandler_SendOnWordSelection_m95A4E5A00E339E5B8BA9AA63B98DB3E81C8B8F66,
	TMP_TextEventHandler_SendOnLineSelection_mE85BA6ECE1188B666FD36839B8970C3E0130BC43,
	TMP_TextEventHandler_SendOnLinkSelection_m299E6620DFD835C8258A3F48B4EA076C304E9B77,
	TMP_TextEventHandler__ctor_m6345DC1CEA2E4209E928AD1E61C3ACA4227DD9B8,
	CharacterSelectionEvent__ctor_m06BC183AF31BA4A2055A44514BC3FF0539DD04C7,
	SpriteSelectionEvent__ctor_m0F760052E9A5AF44A7AF7AC006CB4B24809590F2,
	WordSelectionEvent__ctor_m106CDEB17C520C9D20CE7120DE6BBBDEDB48886C,
	LineSelectionEvent__ctor_m5D735FDA9B71B9147C6F791B331498F145D75018,
	LinkSelectionEvent__ctor_m7AB7977D0D0F8883C5A98DD6BB2D390BC3CAB8E0,
	Benchmark01_Start_mE7E5146B0D8D926CC410CAA48F3B617A0A7D955C,
	Benchmark01__ctor_mB92568AA7A9E13B92315B6270FCA23584A7D0F7F,
	U3CStartU3Ed__10__ctor_mBE5D8B4B98C372BD6DC3936437999DF3048DE3AB,
	U3CStartU3Ed__10_System_IDisposable_Dispose_m1F4180A7FDAE9AC5F11F30B67F813B3D7CD56D73,
	U3CStartU3Ed__10_MoveNext_mF3B1D38CD37FD5187C3192141DB380747C66AD3C,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22B2975D42326E3DE437174BBE4F0E8790CB6591,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m514DA545ECA10AA62BFC239BB582FF0017B91D2B,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m6904A738F04118BA24E473F1809B3D69E711627E,
	Benchmark01_UGUI_Start_m2F8F9E1798943DA8086A7C7E73BA9D7C67482BD4,
	Benchmark01_UGUI__ctor_mACFE8D997EAB50FDBD7F671C1A25A892D9F78376,
	U3CStartU3Ed__10__ctor_m653B757B49A674E239C804FFF4EDEF325B5DB651,
	U3CStartU3Ed__10_System_IDisposable_Dispose_mAD0BC985E1E01C8369A7A955A60D3E545B0B3561,
	U3CStartU3Ed__10_MoveNext_mF9B2C8D290035BC9A79C4347772B5B7B9D404DE1,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF438D42D7C5A811FF9A283F8780F015FF55826CD,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m65545EB5B6DDDDAB5ACF92600137C1C754B322BA,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD4523BE1D1014AAF1C0709EB767C0B0F47D739D2,
	Benchmark02_Start_m315C0DF3A9AC66B4F5802FDAED056E7E5F3D2046,
	Benchmark02__ctor_m526B38D3B7E75905208E4E57B168D8AC1900F4E5,
	Benchmark03_Awake_mD23429C1EE428EEF4ED9A753385BF194BAA41A77,
	Benchmark03_Start_m19E638DED29CB7F96362F3C346DC47217C4AF516,
	Benchmark03__ctor_m89F9102259BE1694EC418E9023DC563F3999B0BE,
	Benchmark04_Start_mEABD467C12547066E33359FDC433E8B5BAD43DB9,
	Benchmark04__ctor_m5015375E0C8D19060CB5DE14CBF4AC02DDD70E7C,
	CameraController_Awake_m420393892377B9703EC97764B34E32890FC5283E,
	CameraController_Start_m32D90EE7232BE6DE08D224F824F8EB9571655610,
	CameraController_LateUpdate_m6D81DEBA4E8B443CF2AD8288F0E76E3A6B3B5373,
	CameraController_GetPlayerInput_m6695FE20CFC691585A6AC279EDB338EC9DD13FE3,
	CameraController__ctor_m09187FB27B590118043D4DC7B89B93164124C124,
	ObjectSpin_Awake_mC1EB9630B6D3BAE645D3DD79C264F71F7B18A1AA,
	ObjectSpin_Update_mD39DCBA0789DC0116037C442F0BA1EE6752E36D3,
	ObjectSpin__ctor_m1827B9648659746252026432DFED907AEC6007FC,
	ShaderPropAnimator_Awake_mE04C66A41CA53AB73733E7D2CCD21B30A360C5A8,
	ShaderPropAnimator_Start_mC5AC59C59AF2F71E0CF65F11ACD787488140EDD2,
	ShaderPropAnimator_AnimateProperties_mAF49CD157AD41377CE00AA10F0C06C8BF5AA0469,
	ShaderPropAnimator__ctor_mAA626BC8AEEB00C5AE362FE8690D3F2200CE6E64,
	U3CAnimatePropertiesU3Ed__6__ctor_mFAC0F8A7368D9D35AD2780C118E13414DA79B56A,
	U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m628EECBFCBC49087298185F17BC2AE7A73FC8A7B,
	U3CAnimatePropertiesU3Ed__6_MoveNext_m8A58CCFDAE59F55AB1BB2C103800886E62C807CF,
	U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0742A0FA62C518EBD119ED5FEBF849F918E12390,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mC076F281DD17668D6CE42EB04EA974DE1FFB3F6B,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mA585E786DA86D2589EBCA029AD9E64E8B5135362,
	SimpleScript_Start_m75CD9CEDCAFA9A991753478D7C28407C7329FB8F,
	SimpleScript_Update_mE8C3930DCC1767C2DF59B622FABD188E6FB91BE5,
	SimpleScript__ctor_mEB370A69544227EF04C48C604A28502ADAA73697,
	SkewTextExample_Awake_m6FBA7E7DC9AFDD3099F0D0B9CDE91574551105DB,
	SkewTextExample_Start_m536F82F3A5D229332694688C59F396E07F88153F,
	SkewTextExample_CopyAnimationCurve_m555177255F5828DBC7E677ED06F7EFFC052886B3,
	SkewTextExample_WarpText_m0E0C46988600673F0E5DFA3133534DC6CA5950D3,
	SkewTextExample__ctor_m945059906734DD38CF860CD32B3E07635D0E1F86,
	U3CWarpTextU3Ed__7__ctor_mEAD3C39209B75514446A44B6C2FA76F8097EBD6F,
	U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m32AA7120BE15547799BDC515FA3486488952BDCF,
	U3CWarpTextU3Ed__7_MoveNext_mB832E4A3DFDDFECC460A510BBC664F218B3D7FCF,
	U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CF506B740C16FEDE58B6E1BE4556CAC2C6AEAD1,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mDDCDBB794F21DF70178A75747D8D8398F38A9C2F,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m6D89CF1910F76577E23C924B535E93FD1B01CF0A,
	TMP_ExampleScript_01_Awake_m381EF5B50E1D012B8CA1883DEFF604EEAE6D95F4,
	TMP_ExampleScript_01_Update_m31611F22A608784F164A24444195B33B714567FB,
	TMP_ExampleScript_01__ctor_m3641F2E0D25B6666CE77773A4A493C4C4D3D3A12,
	TMP_FrameRateCounter_Awake_m958B668086DB4A38D9C56E3F8C2DCCB6FF11FAA3,
	TMP_FrameRateCounter_Start_mC642CA714D0FB8482D2AC6192D976DA179EE3720,
	TMP_FrameRateCounter_Update_m5E41B55573D86C3D345BFE11C489B00229BCEE21,
	TMP_FrameRateCounter_Set_FrameCounter_Position_mA72ECDE464E3290E4F533491FC8113B8D4068BE1,
	TMP_FrameRateCounter__ctor_mC79D5BF3FAE2BB7D22D9955A75E3483725BA619C,
	TMP_TextEventCheck_OnEnable_mE6D5125EEE0720E6F414978A496066E7CF1592DF,
	TMP_TextEventCheck_OnDisable_m94F087C259890C48D4F5464ABA4CD1D0E5863A9A,
	TMP_TextEventCheck_OnCharacterSelection_mDA044F0808D61A99CDA374075943CEB1C92C253D,
	TMP_TextEventCheck_OnSpriteSelection_mE8FFA550F38F5447CA37205698A30A41ADE901E0,
	TMP_TextEventCheck_OnWordSelection_m6037166D18A938678A2B06F28A5DCB3E09FAC61B,
	TMP_TextEventCheck_OnLineSelection_mD2BAA6C8ABD61F0442A40C5448FA7774D782C363,
	TMP_TextEventCheck_OnLinkSelection_m184B11D5E35AC4EA7CDCE3340AB9FEE3C14BF41C,
	TMP_TextEventCheck__ctor_mA5E82EE7CE8F8836FC6CF3823CBC7356005B898B,
	TMP_TextInfoDebugTool__ctor_mF6AA30660FBD4CE708B6147833853498993CB9EE,
	TMP_TextSelector_A_Awake_mD7252A5075E30E3BF9BEF4353F7AA2A9203FC943,
	TMP_TextSelector_A_LateUpdate_mDBBC09726332EDDEAF7C30AB6C08FB33261F79FD,
	TMP_TextSelector_A_OnPointerEnter_mC19B85FB5B4E6EB47832C39F0115A513B85060D0,
	TMP_TextSelector_A_OnPointerExit_mE51C850F54B18A5C96E3BE015DFBB0F079E5AE66,
	TMP_TextSelector_A__ctor_mEB83D3952B32CEE9A871EC60E3AE9B79048302BF,
	TMP_TextSelector_B_Awake_m7E413A43C54DF9E0FE70C4E69FC682391B47205A,
	TMP_TextSelector_B_OnEnable_m0828D13E2D407B90038442228D54FB0D7B3D29FB,
	TMP_TextSelector_B_OnDisable_m2B559A85B52C8CAFC7350CC7B4F8E5BC773EF781,
	TMP_TextSelector_B_ON_TEXT_CHANGED_m1597DBE7C7EBE7CFA4DC395897A4779387B59910,
	TMP_TextSelector_B_LateUpdate_m8BA10E368C7F3483E9EC05589BBE45D7EFC75691,
	TMP_TextSelector_B_OnPointerEnter_m0A0064632E4C0E0ADCD4358AD5BC168BFB74AC4D,
	TMP_TextSelector_B_OnPointerExit_m667659102B5B17FBAF56593B7034E5EC1C48D43F,
	TMP_TextSelector_B_OnPointerClick_m8DB2D22EE2F4D3965115791C81F985D82021469F,
	TMP_TextSelector_B_OnPointerUp_m303500BE309B56F3ADCE8B461CEC50C8D5ED70BC,
	TMP_TextSelector_B_RestoreCachedVertexAttributes_m3D637CF2C6CA663922EBE56FFD672BE582E9F1F2,
	TMP_TextSelector_B__ctor_mE654F9F1570570C4BACDA79640B8DB3033D91C33,
	TMP_UiFrameRateCounter_Awake_m83DBE22B6CC551BE5E385048B92067679716179E,
	TMP_UiFrameRateCounter_Start_m59DA342A492C9EF8AD5C4512753211BF3796C944,
	TMP_UiFrameRateCounter_Update_m28FC233C475AA15A3BE399BF9345977089997749,
	TMP_UiFrameRateCounter_Set_FrameCounter_Position_m2025933902F312C0EC4B6A864196A8BA8D545647,
	TMP_UiFrameRateCounter__ctor_m5774BF4B9389770FC34991095557B24417600F84,
	TMPro_InstructionOverlay_Awake_m2444EA8749D72BCEA4DC30A328E3745AB19062EC,
	TMPro_InstructionOverlay_Set_FrameCounter_Position_m62B897E4DB2D6B1936833EC54D9C246D68D50EEB,
	TMPro_InstructionOverlay__ctor_m7AB5B851B4BFB07547E460D6B7B9D969DEB4A7CF,
	TeleType_Awake_m099FCB202F2A8299B133DC56ECB9D01A16C08DFE,
	TeleType_Start_m72DE9DE597F4FA0B1CA02115CBC1E74EB53F63F7,
	TeleType__ctor_m12A79B34F66CBFDCA8F582F0689D1731586B90AF,
	U3CStartU3Ed__4__ctor_mF3575DBEBF4F153F6A899AF3362940298C11B629,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m6DABFDBC2A313BF6DD90AA1C43B4EF9D6249CBE9,
	U3CStartU3Ed__4_MoveNext_mEF7A3215376BDFB52C3DA9D26FF0459074E89715,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DC06D52865D30DAA78DBD70B6554D13E2EA70B,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mE854A2CECA0E01F3C2D90DA4F720CF0F387994F8,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m77D3936B94684C817EE9F6D2C903238A457D5261,
	TextConsoleSimulator_Awake_mA51EE182A528CCA5CAEA8DBE8AD30E43FDFAE7B0,
	TextConsoleSimulator_Start_m429701BE4C9A52AA6B257172A4D85D58E091E7DA,
	TextConsoleSimulator_OnEnable_m3397FBCDA8D9DA264D2149288BE4DCF63368AB50,
	TextConsoleSimulator_OnDisable_m43DF869E864789E215873192ECFCDC51ABA79712,
	TextConsoleSimulator_ON_TEXT_CHANGED_m51F06EE5DD9B32FEFB529743F209C6E6C41BE3B8,
	TextConsoleSimulator_RevealCharacters_mE8E415644F7BD2056D0809F7BB0FA9A2F9FE8534,
	TextConsoleSimulator_RevealWords_mFDBC863D30BC63ADCF2860F303AF252E27D0F4F4,
	TextConsoleSimulator__ctor_m4719FB9D4D89F37234757D93876DF0193E8E2848,
	U3CRevealCharactersU3Ed__7__ctor_mD45A85F5F50909F70C80AC8CE460F4FD261CDE9D,
	U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m85F270FDC11A4D79E9CF47AADC9FA1FBF032F86C,
	U3CRevealCharactersU3Ed__7_MoveNext_mA9A6555E50889A7AA73F20749F25989165023DE3,
	U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BBAE6868EB7F1C11BE5DF001641E18C9D625F83,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mA9F4893381AB24E01EAEBFC38A88AF363A9A0691,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_mFD0B7538B1A650FB389FFE9296B0E51AEA5B6B6F,
	U3CRevealWordsU3Ed__8__ctor_m22FF9E770988107A928C5D1EA639F60239BFFEF0,
	U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m3EAF6EF4A8C99A71FEED251BF3F28A26BF6AD7F8,
	U3CRevealWordsU3Ed__8_MoveNext_m3811753E2384D4CBBAD6BD712EBA4FAF00D73210,
	U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3F93E4AA532F15D52D68B7121805C014AB2D7FB,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mCDC8982948ED5F7743567569CA1D4A354218714F,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mF39E492D576810F58DB9031556CFD6806FD32E27,
	TextMeshProFloatingText_Awake_m679E597FF18E192C1FBD263D0E5ECEA392412046,
	TextMeshProFloatingText_Start_m68E511DEEDA883FE0F0999B329451F3A8A7269B1,
	TextMeshProFloatingText_DisplayTextMeshProFloatingText_m3C381B8A53C58CF001D7A9212B8BDA6F368CC2C7,
	TextMeshProFloatingText_DisplayTextMeshFloatingText_m22691E6EA41B7FCF782B03954865D8E3B890E4DF,
	TextMeshProFloatingText__ctor_m968E2691E21A93C010CF8205BC3666ADF712457E,
	TextMeshProFloatingText__cctor_m5FF3949FBDB0FD13908FB7349F64DDB3012512B8,
	U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_m3F7F9561735FF8403090A93E708E781B3997B81B,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m6F24E1DC5777D4682375D8742BC9272EE6D4EA2C,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_MoveNext_m04B1BB9074CBA76BA7F63849721F4CAEA1045A28,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14A7C9DEBD0FF0C7A6979AFEC78B0BEE9E4C3963,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mC0D5548A92F57550F60EE76E17B58125B57B94A8,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mD3CBFD6F8558EF09F2B4A3D8D1B76BD7FDB3A98B,
	U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m0466586E44A01852547C6CECCA91C14673B98C20,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m8730A3A2EFBF759A51F0E5330C3BBF8EFC1736A3,
	U3CDisplayTextMeshFloatingTextU3Ed__16_MoveNext_m93E19CF96C4EBC57CC4E03E2D3D0B56B3E92A36A,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4076BE73343A473EFE46E73ABA5DE66CC797598,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_mC33B355BB392C5A52DCA0AAF2588E1E78AF6D3A5,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m28CCE2E51295AD5F368C504BADDDFF008C2CC0A6,
	TextMeshSpawner_Awake_mDF8CCC9C6B7380D6FA027263CDC3BC00EF75AEFB,
	TextMeshSpawner_Start_m7CC21883CA786A846A44921D2E37C201C25439BA,
	TextMeshSpawner__ctor_m1255777673BE591F62B4FC55EB999DBD7A7CB5A1,
	VertexColorCycler_Awake_m84B4548078500DA811F3ADFF66186373BE8EDABC,
	VertexColorCycler_Start_mC3FF90808EDD6A02F08375E909254778D5268B66,
	VertexColorCycler_AnimateVertexColors_m6960F777E4876DFCA726BCAE7A8163850D58FA42,
	VertexColorCycler__ctor_m09990A8066C8A957A96CDBEBDA980399283B45E9,
	U3CAnimateVertexColorsU3Ed__3__ctor_m0038B1054BCC928D35F8C0021ED7D2E1C533E35F,
	U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5BAD394A0B09B3E0FF19E91521E02C2B3ADD6007,
	U3CAnimateVertexColorsU3Ed__3_MoveNext_m2842AF5B12AFC17112D1AE75E46AB1B12776D2A6,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE810D7968957A09C88E61C29DAAEC68E4AF1E51,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m72ABAC50E9E4D972FB44CAFF387F3E23FEC5D932,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m8DE595A1D01F3A507A356F8BCE020D0851412B52,
	VertexJitter_Awake_m9C5586A35BD9C928455D6479C93C1CE095447D8F,
	VertexJitter_OnEnable_m97ED60DBD350C72D1436ADFB8009A6F33A78C825,
	VertexJitter_OnDisable_mF8D32D6E02E41A73C3E958FB6EE5D1D659D2A846,
	VertexJitter_Start_m8A0FED7ED16F90DBF287E09BFCBD7B26E07DBF97,
	VertexJitter_ON_TEXT_CHANGED_mC7AB0113F6823D4FF415A096314B55D9C1BE9549,
	VertexJitter_AnimateVertexColors_mECAE037FC0CBA52CAC71C0B61E88829FF18BCC16,
	VertexJitter__ctor_m550C9169D6FCD6F60D6AABCB8B4955DF58A12DCE,
	U3CAnimateVertexColorsU3Ed__11__ctor_m0222C3457F5ACA497FE3A8EC829DE4AD11A169F8,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m717E79A39A8161ADDA9E62F7CDFB67B8F2D65099,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_m169A75C7E2147372CB933520B670AF77907C1C6B,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920EBA43D59A1A88E27FED92CF0AC0DF90179479,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m6C045A6DD0B60F1512457448E615877EAB86D75D,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m017345FE58B497EAFC9D0CB1FB733F76EB3449AF,
	VertexShakeA_Awake_m005C6F9EE8A8FD816CD3A736D6AF199CD2B615A2,
	VertexShakeA_OnEnable_m9F60F3951A7D0DF4201A0409F0ADC05243D324EA,
	VertexShakeA_OnDisable_m59B86895C03B278B2E634A7C68CC942344A8D2D2,
	VertexShakeA_Start_m3E623C28F873F54F4AC7579433C7C50B2A3319DE,
	VertexShakeA_ON_TEXT_CHANGED_m7AD31F3239351E0916E4D02148F46A80DC148D7E,
	VertexShakeA_AnimateVertexColors_mC451E2732A4E3E90E2553811AD75903EEF974599,
	VertexShakeA__ctor_m7E6FD1700BD08616532AF22B3B489A18B88DFB62,
	U3CAnimateVertexColorsU3Ed__11__ctor_m92612416BEC0EBF5E9849FB603629C0F2F95FEF2,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m77D966994D4717EAFD8EFE169F3E8A4EE8B05B81,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_mCEEDE03667D329386BB4AE7B8252B7A9B54F443F,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCCD3645ACF9B18D760B341C863F853996FA9BCE,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mB9444B5E58B4E97105C447F547C0F74C51BCFBFA,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m5CC464185D5C0251C6206E20AFFA681BA0525A7E,
	VertexShakeB_Awake_m99C9A0474DBFE462DC88C898F958C1805376F9D5,
	VertexShakeB_OnEnable_m64299258797D745CDFE7F3CBEC4708BDBC7C3971,
	VertexShakeB_OnDisable_m07D520A8D7BCD8D188CE9F5CC7845F47D5AD6EF4,
	VertexShakeB_Start_m9642281210FA5F701A324B78850331E4638B2DD1,
	VertexShakeB_ON_TEXT_CHANGED_m5650D69D258D0EEF35AF390D6D5086B327B308A5,
	VertexShakeB_AnimateVertexColors_mDF7A7E7028361D288DF588D9301541E4FA1EFA87,
	VertexShakeB__ctor_m605A778C36B506B5763A1AE17B01F8DCCBCD51EC,
	U3CAnimateVertexColorsU3Ed__10__ctor_m3E3B1D286DEBED2BC028AD490308568B930C3760,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m0F9D4B2A6ED0500C2120DBA29932CC279E8908DC,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m0534E436514E663BF024364E524EE5716FF15C8E,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2A8BAFC0261ACBBE8EEA74DE4B498D30C68AE3D,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5F12D7C6F2523BEB6326FE49AE972116D6157CBB,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mB9C12786011A3B78517AEFAE8D5A78B95A4219AD,
	VertexZoom_Awake_m1B5D386B98CF2EB05A8155B238D6F6E8275D181C,
	VertexZoom_OnEnable_m7F980FC038FC2534C428A5FD33E3E13AEAEB4EEC,
	VertexZoom_OnDisable_m880C38B62B4BB05AF49F12F75B83FFA2F0519884,
	VertexZoom_Start_m10A182DCEF8D430DADAFBFFA3F04171F8E60C84C,
	VertexZoom_ON_TEXT_CHANGED_mB696E443C61D2212AC93A7615AA58106DD25250F,
	VertexZoom_AnimateVertexColors_mEFBA6C940DFECB485236C247358A32011A7963ED,
	VertexZoom__ctor_mE7B36F2D3EC8EF397AB0AD7A19E988C06927A3B2,
	U3CU3Ec__DisplayClass10_0__ctor_m4EC5F8042B5AC645EA7D0913E50FD22144DBD348,
	U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m959A7E1D6162B5AAF28B953AFB04D7943BCAB107,
	U3CAnimateVertexColorsU3Ed__10__ctor_m537CF0A5ADF5BBC2DF784BF526E3F32EB528E1B2,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mF0CD944C393771101D2718A660E1DFF22385819F,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m8340EFEB2B2CF6EA1545CFB6967968CA171FEE66,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093E922A5A046910B2A7EE826D804CA3064A7BD9,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDED63FD52741D58D8D5A3E53415F91B6560F680C,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m933EBF7C4300C7C1E3EE60E4741110025877CA5F,
	WarpTextExample_Awake_mEDB072A485F3F37270FC736E1A201624D696B4B0,
	WarpTextExample_Start_m36448AEA494658D7C129C3B71BF3A64CC4DFEDC4,
	WarpTextExample_CopyAnimationCurve_m744026E638662DA48AC81ED21E0589E3E4E05FAB,
	WarpTextExample_WarpText_mA29C98CF3B92F253C2DD2BADFC76618F8324BEF2,
	WarpTextExample__ctor_m62299DFDB704027267321007E16DB87D93D0F099,
	U3CWarpTextU3Ed__8__ctor_m9FADE04C27A0034C5A276232FCA187AECDC6BF49,
	U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m5AB0DCF4B3DE6C290487F3DBBC7BAE931130DE60,
	U3CWarpTextU3Ed__8_MoveNext_m9C83C8455FF8ADFBAA8D05958C3048612A96EB84,
	U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D5776AF80C4227401ADD7E13D98F2530CB9E7A1,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4F2FBBE7B375E6FC837736B4275A7B601A01F535,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m01FA935D93D737C9DDD88A051F7DFC393DD7BD25,
	All1ShaderDemoController_Start_mEEC3CAE48A5261CB2CD689EC5D84A2D0660F16AB,
	All1ShaderDemoController_Update_mBE75BD58B1BBC36FC1B3E75A8726E714724A8670,
	All1ShaderDemoController_GetInput_m6073AC5247202DE21350D05BA66835A8CB7E658E,
	All1ShaderDemoController_ChangeExpositor_mA47C171B722C938B020A66A4DFD595911762BC68,
	All1ShaderDemoController_SetExpositorText_m2449D154B641AB7FD4FFCDF4A20E80EE15028A3B,
	All1ShaderDemoController_GetCurrExpositor_m4183C3946ABF9B688E29C8C03B0C80B20DA65304,
	All1ShaderDemoController__ctor_m4AA052973914A05EB579354C2C30ED258D9965B3,
	All1TextureOffsetOverTime_Start_m077B271F3A70CBE1CFEB63B14BE1F839B4CFBCE0,
	All1TextureOffsetOverTime_Update_m006EF87314FBB31C5282570A8F8200DFAA654250,
	All1TextureOffsetOverTime_DestroyComponentAndLogError_m21D043B2F255B83C420B2D1865663F067B3F707F,
	All1TextureOffsetOverTime__ctor_m17D0106F116F9EDA4F1AEF77BADCD69EC6913110,
	AllIn1ScrollProperty_Start_m5941A1504E99412B1CE17548E002BD336132A5C1,
	AllIn1ScrollProperty_Update_mF2B333FA53FA2347D0463187B6B0FE5C0B85F661,
	AllIn1ScrollProperty_DestroyComponentAndLogError_mB50B67930AB289F8FDC6E5063C58DF52D095786A,
	AllIn1ScrollProperty__ctor_m5148B11659A91C216AE3AE3B85778E71C857429B,
	Demo2AutoScroll_Start_m4AEE65959275348130AE119246FF1C33FEB00F66,
	Demo2AutoScroll_ScrollElements_mF866F08A3586845E528E753205F2B65A8767EAF1,
	Demo2AutoScroll__ctor_mF6BEB75AB83B28B7CA50FB2BD6F7F8C9EAD8CC3E,
	U3CScrollElementsU3Ed__4__ctor_m1F1BD6DCB9CA445D10DE6B2AF051AF11BD9AC02A,
	U3CScrollElementsU3Ed__4_System_IDisposable_Dispose_m34188717DA14DF12B0E29B18A60F135381015DBD,
	U3CScrollElementsU3Ed__4_MoveNext_m1F69AE110D7DAD42C71B82AD9FE7355D770B7249,
	U3CScrollElementsU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m13230DA8EF32AA27571693E059C0AA976BCABAAA,
	U3CScrollElementsU3Ed__4_System_Collections_IEnumerator_Reset_m7BEF0DE9D77F41B1DB4C48C89D506FFC04E0B6BC,
	U3CScrollElementsU3Ed__4_System_Collections_IEnumerator_get_Current_m580B00C2C8C6CB363A03D46AF29E653CD6A69DE5,
	DemoCamera_Awake_m4ECA403039A6C68D7048F875C9846C2F2F845E7F,
	DemoCamera_Update_mEABD0D432561814631853E4CFD6127C05DA44231,
	DemoCamera_SetCamAfterStart_m96A3BE2CDF47F4D52320617D2FFAF0CA33AA8773,
	DemoCamera__ctor_m7298363D9C905F16A40F4B02225B2B3406574773,
	U3CSetCamAfterStartU3Ed__8__ctor_mA8337C68757B7D40FAFF6E81E64A03E89715DC6C,
	U3CSetCamAfterStartU3Ed__8_System_IDisposable_Dispose_m5993ADE0141B93FCBED8DCE77C4C8285A477C86B,
	U3CSetCamAfterStartU3Ed__8_MoveNext_m52B1967F30180CFE83B28C71F5BBC32612538076,
	U3CSetCamAfterStartU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0E94003660DA4E83C91F84E8055A6627CFB56B0A,
	U3CSetCamAfterStartU3Ed__8_System_Collections_IEnumerator_Reset_m7400F544CC8BE1E2C846322EDBD54765C8C3654B,
	U3CSetCamAfterStartU3Ed__8_System_Collections_IEnumerator_get_Current_m2753443439BEB359F0D4991B19ECB0479FFBBE0C,
	DemoCircleExpositor_Start_m0B710706757CA7D195C82649D39619394ACC86BE,
	DemoCircleExpositor_Update_m4699A9A58BDB0F195BA3090CCBADC35BC04CFF74,
	DemoCircleExpositor_ChangeTarget_m119354E8A96778A683F8C43043E37F4FDDED57B1,
	DemoCircleExpositor__ctor_m83F52F9F725417DBB3980F9AFDB8F962AF4C670F,
	DemoItem_Update_mB33B944D0C5E63E68B40BC4D67F573F51D332B50,
	DemoItem__ctor_m6C6224B09B25A50F9D2DE6553CADDC0A5E4F4140,
	DemoItem__cctor_m9A552D77A553FA30F3D0AA90A89DC964E7892F44,
	DemoRandomColorSwap_Start_m94684B7699A9DFCDC803280D5F0A88E52D39AEE7,
	DemoRandomColorSwap_NewColor_m32EC1ABB92C3863FF51E05B6F4A6ADD03E0DCB6B,
	DemoRandomColorSwap_GenerateColor_mD5ED1C41EA7B3F993ACE60E082EA3EF41D9B4E6E,
	DemoRandomColorSwap__ctor_mA82A8FA6AC1D7CDED3D578301E68013F1C31E0D6,
	DemoRepositionExpositor_RepositionExpositor_m79FF08534B92E056E53EA21EA6B66E5BC63D4084,
	DemoRepositionExpositor__ctor_mA818DA5BD14FD598F148E4FE5C69E901D4045520,
	All1CreateUnifiedOutline_Update_m5641EFED7018929F1D58C04A4F850DD272764CEC,
	All1CreateUnifiedOutline_CreateOutlineSpriteDuplicate_m480A6FD6DDB94F456A7CE3E2026053BE9023F6C3,
	All1CreateUnifiedOutline_MissingMaterial_m3A82377519175A2D96AFF31E5EDD3A328D63BACB,
	All1CreateUnifiedOutline_GetAllChildren_mAAD7C28A2A5A8F6BDEDF235DB026C7451BD82F57,
	All1CreateUnifiedOutline__ctor_m189CDB191BB8F2F101E3A4E9F930F2D84981F0B8,
	RandomSeed_Start_mFC6E494BAB8F987A2994660771B42B9D11BCE86F,
	RandomSeed__ctor_mB7DC90975D873C9CF6D8BECF875EA44D8D2AA302,
	SetAtlasUvs_Start_m4AF6C9B645238AE5FF6B10438A8DEA87719F157A,
	SetAtlasUvs_Reset_m6D9E04258D341C27F0ECB36D9F342CA3D3D5B3BC,
	SetAtlasUvs_Setup_mB74C08D364BA97A6F16F170433CA50636E737745,
	SetAtlasUvs_OnWillRenderObject_mFFB7A45341EE9FE7E9E2D742F6523AF4C67CD8C7,
	SetAtlasUvs_GetAndSetUVs_mE82DE67BC9FE2E00501E177AE72C14D6692ADCB0,
	SetAtlasUvs_ResetAtlasUvs_m30348C40877A06CCF73A3169306F6C5DF40406EB,
	SetAtlasUvs_UpdateEveryFrame_m507682AEAEB64356EBE7486739B702025ADE20D3,
	SetAtlasUvs_GetRendererReferencesIfNeeded_mFA52628BDE471B7EAB32096B3CBA4F448AA45DD7,
	SetAtlasUvs__ctor_m042D771698F0B8BB203C900E7119BB6519CAB283,
	SetGlobalTime_Start_m8F51B543660EF7280297678A1ECB14FD403EF007,
	SetGlobalTime_Update_mE9CE1843FC11FC2EAB533E50F0D650FF1C1BB0DC,
	SetGlobalTime__ctor_mB3B514DAD04B536F81F563259EE75654F6029513,
	All1DemoChangeScene_Update_m5A3884916B6641F07AE5250499F0BD943574B5F5,
	All1DemoChangeScene_NextScene_mFF3DFBB30601DA5AC8F6191D7E8D17C62A2F780E,
	All1DemoChangeScene_LoadScene_m894077928CA689EB5D62947A5E912761197E2AF9,
	All1DemoChangeScene__ctor_m971C27B3CB025D12132CC3EF1B3F84E4E325AC02,
	All1DemoClaw_Start_m7F8EABD2497BDEE79FCC09E56E0A1C71BF9656B9,
	All1DemoClaw_Update_m7AFB8117C3AA03EF88C68EEBE0A9548F473D00A2,
	All1DemoClaw__ctor_mBCEE8397E3E75BC8210D7BECBCD15EB00093AE42,
	All1DemoRandomZ_Start_m130E074ABE1614D4BC687B2EF9A340383398657E,
	All1DemoRandomZ__ctor_m22F81ED995ED7FFA0EC0F8859F52A54B2B4DA6F2,
	All1DemoUpAndDown_Start_m6BAED669B9C7C67FB53F331D94FEA7989FA540C2,
	All1DemoUpAndDown_Update_m7D574BDE0D181FE6C89CE4C2442DBA984D6F41EA,
	All1DemoUpAndDown__ctor_m2D65F826D83D2CC364F901C5A6445FB54F0DEFBE,
	All1DemoUrpCamMove_Start_m6F378D55541F497B7C055E5891FB3EEDDB086B46,
	All1DemoUrpCamMove_FixedUpdate_m5F27951E3A2DADEDA3E8733020C550866FFDFFAA,
	All1DemoUrpCamMove__ctor_m4ED75302A93663B3B19B01B8A8AD1396B3DB8379,
};
static const int32_t s_InvokerIndices[1268] = 
{
	1866,
	3890,
	1866,
	3812,
	1038,
	3123,
	3890,
	3890,
	1847,
	3890,
	3890,
	3890,
	1836,
	3142,
	3890,
	3890,
	3172,
	3890,
	3890,
	1826,
	3890,
	1842,
	1301,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	815,
	3890,
	3142,
	3142,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	1879,
	3890,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3890,
	3890,
	3142,
	3890,
	3801,
	3132,
	3801,
	3132,
	3853,
	3179,
	3853,
	3179,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3846,
	3846,
	3846,
	3846,
	3846,
	3846,
	3846,
	3890,
	3890,
	3179,
	1129,
	3890,
	3890,
	3890,
	3890,
	3890,
	3882,
	3209,
	3792,
	3123,
	3812,
	3142,
	3846,
	3172,
	3890,
	3890,
	3179,
	3179,
	1127,
	1880,
	3890,
	3890,
	3123,
	3890,
	3890,
	3890,
	3179,
	3172,
	3179,
	3179,
	3812,
	3890,
	3123,
	3890,
	3846,
	3812,
	3890,
	3812,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3123,
	3890,
	3142,
	3890,
	3890,
	3890,
	3890,
	3890,
	3179,
	3890,
	292,
	3890,
	3890,
	3890,
	3890,
	3890,
	292,
	3890,
	3890,
	3890,
	3890,
	3890,
	292,
	3890,
	292,
	3890,
	3890,
	3890,
	3890,
	3890,
	185,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	292,
	3890,
	3890,
	3890,
	3890,
	292,
	3890,
	3890,
	3890,
	3890,
	292,
	3890,
	3890,
	3890,
	3890,
	3890,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3890,
	3890,
	3890,
	3890,
	292,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	292,
	3890,
	3890,
	3890,
	3890,
	3890,
	292,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	292,
	3890,
	3890,
	3890,
	3890,
	3890,
	185,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	292,
	3890,
	3890,
	3890,
	3890,
	3890,
	292,
	3890,
	3890,
	3890,
	3890,
	3890,
	185,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	292,
	3890,
	3890,
	3890,
	3890,
	3890,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3890,
	3890,
	3890,
	3890,
	292,
	3890,
	292,
	3890,
	3890,
	292,
	3890,
	3890,
	292,
	3890,
	292,
	3890,
	185,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	292,
	3890,
	3890,
	3890,
	3890,
	292,
	3890,
	292,
	3890,
	3890,
	3890,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3890,
	3890,
	3890,
	3890,
	292,
	3890,
	3890,
	3890,
	3890,
	3890,
	292,
	3890,
	3890,
	3890,
	3890,
	3890,
	292,
	3890,
	3890,
	3890,
	3890,
	3890,
	292,
	3890,
	3890,
	3890,
	3890,
	3890,
	185,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	292,
	3890,
	3890,
	3890,
	3890,
	3890,
	292,
	3890,
	3890,
	3890,
	3890,
	3890,
	185,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3890,
	3890,
	3890,
	3890,
	3812,
	3142,
	3812,
	3142,
	3792,
	3123,
	3812,
	3142,
	3890,
	3890,
	3890,
	3846,
	3846,
	3846,
	3846,
	3890,
	3179,
	3890,
	3890,
	3812,
	3142,
	3142,
	3142,
	3890,
	3853,
	3179,
	1077,
	3890,
	3890,
	3890,
	3890,
	3890,
	661,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	661,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	661,
	3890,
	3890,
	3890,
	3890,
	3890,
	661,
	3890,
	3890,
	3890,
	3890,
	3890,
	661,
	3890,
	3890,
	3890,
	3890,
	3890,
	661,
	3890,
	3890,
	3890,
	3890,
	3172,
	3890,
	3890,
	661,
	3890,
	3890,
	3890,
	3890,
	3890,
	3172,
	292,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	661,
	3890,
	3890,
	3890,
	3890,
	3890,
	661,
	3890,
	3890,
	3890,
	3890,
	3890,
	292,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	661,
	3890,
	3890,
	3890,
	3890,
	3890,
	661,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3142,
	3890,
	3890,
	3890,
	3142,
	3890,
	3890,
	3882,
	3209,
	3882,
	3209,
	3883,
	3210,
	3792,
	3123,
	3792,
	3123,
	3846,
	3172,
	3846,
	3172,
	3846,
	3172,
	3846,
	3172,
	3846,
	3172,
	3846,
	3172,
	3812,
	3142,
	3890,
	3890,
	3254,
	3254,
	3254,
	3254,
	3254,
	3254,
	3254,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	6232,
	6138,
	3890,
	3890,
	3142,
	3812,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	839,
	3890,
	3890,
	3123,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3846,
	3890,
	3890,
	3792,
	3890,
	3890,
	3890,
	3890,
	3890,
	3123,
	3890,
	3846,
	3812,
	3890,
	3812,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3179,
	3890,
	3890,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3890,
	3890,
	3890,
	3890,
	3179,
	3890,
	3890,
	3890,
	3890,
	3890,
	660,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3812,
	3142,
	3142,
	3142,
	3890,
	660,
	3890,
	3890,
	3142,
	3179,
	3172,
	3890,
	3890,
	3890,
	3846,
	3172,
	660,
	3890,
	3890,
	3890,
	3890,
	3890,
	3846,
	3890,
	660,
	3890,
	3846,
	3890,
	3890,
	3123,
	660,
	3890,
	3890,
	3172,
	660,
	3890,
	3890,
	3890,
	660,
	3890,
	3890,
	3890,
	660,
	3890,
	3890,
	3890,
	3890,
	3890,
	660,
	3890,
	3890,
	3890,
	3890,
	3890,
	660,
	3890,
	3890,
	3890,
	3890,
	3890,
	660,
	3890,
	3890,
	3890,
	3890,
	660,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	660,
	3209,
	3882,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3846,
	3172,
	660,
	3890,
	3890,
	3890,
	3890,
	3890,
	3846,
	3890,
	3890,
	660,
	3890,
	3890,
	3890,
	660,
	3890,
	3890,
	3890,
	3890,
	660,
	3890,
	3890,
	3890,
	660,
	3890,
	3890,
	3890,
	3890,
	3890,
	660,
	3890,
	3890,
	3890,
	3890,
	3890,
	660,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3142,
	3142,
	3890,
	3812,
	3142,
	3890,
	3890,
	3792,
	3123,
	3812,
	3142,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	1842,
	3890,
	3890,
	3890,
	3890,
	1126,
	3890,
	3890,
	3142,
	3890,
	3142,
	3890,
	-1,
	3179,
	1129,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3142,
	3142,
	3142,
	3890,
	3890,
	3890,
	3142,
	3890,
	3890,
	3890,
	3890,
	3812,
	3890,
	3123,
	3890,
	3846,
	3812,
	3890,
	3812,
	725,
	3890,
	725,
	3890,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3812,
	3142,
	3890,
	3890,
	3142,
	3142,
	1551,
	1551,
	1064,
	1064,
	1074,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3812,
	3890,
	3123,
	3890,
	3846,
	3812,
	3890,
	3812,
	3812,
	3890,
	3123,
	3890,
	3846,
	3812,
	3890,
	3812,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3812,
	3890,
	3123,
	3890,
	3846,
	3812,
	3890,
	3812,
	3890,
	3890,
	3890,
	3890,
	3890,
	2405,
	3812,
	3890,
	3123,
	3890,
	3846,
	3812,
	3890,
	3812,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3123,
	3890,
	3890,
	3890,
	1551,
	1551,
	1064,
	1064,
	1074,
	3890,
	3890,
	3890,
	3890,
	3142,
	3142,
	3890,
	3890,
	3890,
	3890,
	3142,
	3890,
	3142,
	3142,
	3142,
	3142,
	3123,
	3890,
	3890,
	3890,
	3890,
	3123,
	3890,
	3890,
	3123,
	3890,
	3890,
	3812,
	3890,
	3123,
	3890,
	3846,
	3812,
	3890,
	3812,
	3890,
	3890,
	3890,
	3890,
	3142,
	2405,
	2405,
	3890,
	3123,
	3890,
	3846,
	3812,
	3890,
	3812,
	3123,
	3890,
	3846,
	3812,
	3890,
	3812,
	3890,
	3890,
	3812,
	3812,
	3890,
	6270,
	3123,
	3890,
	3846,
	3812,
	3890,
	3812,
	3123,
	3890,
	3846,
	3812,
	3890,
	3812,
	3890,
	3890,
	3890,
	3890,
	3890,
	3812,
	3890,
	3123,
	3890,
	3846,
	3812,
	3890,
	3812,
	3890,
	3890,
	3890,
	3890,
	3142,
	3812,
	3890,
	3123,
	3890,
	3846,
	3812,
	3890,
	3812,
	3890,
	3890,
	3890,
	3890,
	3142,
	3812,
	3890,
	3123,
	3890,
	3846,
	3812,
	3890,
	3812,
	3890,
	3890,
	3890,
	3890,
	3142,
	3812,
	3890,
	3123,
	3890,
	3846,
	3812,
	3890,
	3812,
	3890,
	3890,
	3890,
	3890,
	3142,
	3812,
	3890,
	3890,
	1195,
	3123,
	3890,
	3846,
	3812,
	3890,
	3812,
	3890,
	3890,
	2405,
	3812,
	3890,
	3123,
	3890,
	3846,
	3812,
	3890,
	3812,
	3890,
	3890,
	3890,
	3123,
	3890,
	3792,
	3890,
	3890,
	3890,
	3142,
	3890,
	3890,
	3890,
	3142,
	3890,
	3890,
	3812,
	3890,
	3123,
	3890,
	3846,
	3812,
	3890,
	3812,
	3890,
	3890,
	3812,
	3890,
	3123,
	3890,
	3846,
	3812,
	3890,
	3812,
	3890,
	3890,
	3123,
	3890,
	3890,
	3890,
	6270,
	3890,
	3890,
	3739,
	3890,
	3890,
	3890,
	3890,
	3142,
	3890,
	1826,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3172,
	3846,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3142,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
	3890,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x02000083, { 0, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[2] = 
{
	{ (Il2CppRGCTXDataType)2, 579 },
	{ (Il2CppRGCTXDataType)1, 579 },
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	1268,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	2,
	s_rgctxValues,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
