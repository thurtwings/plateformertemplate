using UnityEngine;

[CreateAssetMenu(fileName = "NewPlayerData" , menuName = "Data/Player Data/ Base Data")]
public class PlayerData : ScriptableObject
{
    #region Move
    [Header("Move State")]
    [Tooltip("The speed of the running player")]
    [Range(1, 15)]
    public float movementVelocity = 10f;
    [Space]
    #endregion

    #region Crouch
    [Header("Crouch State")]
    [Tooltip("The speed of the crouched player")]
    [Range(1, 15)]
    public float crouchMovementVelocity = 5f;
    [Tooltip("Must be under 1")]
    public float crouchColliderHeight = .9f;
    public float crouchColliderWidth = .9f;
    public float standColliderHeight = 1.6f;
    public float standColliderWidth = .6f;
    #endregion

    #region Jump
    [Header("Jump State")]
    [Tooltip("The force of the jump impulse")]
    [Range(0f, 50f)]
    public float jumpVelocity = 15f;
    [Tooltip("Double jump? Triple Jump? (max 5)")]
    [Range(0,5)]
    public int amountOfJumps = 2;
    
    [Space]
    #endregion

    #region In Air
    [Header("In Air State")]
    [Tooltip("how long before the player can no longer jump (0.2f fits well)")]
    [Range(0f, .5f)]
    public float coyoteTime = 0.2f;
    [HideInInspector]
    public float jumpHeightMultiplier = 0.5f;
    
    [Space]
    #endregion

    #region Wall Slide
    [Header("Wall Slide State")]
    [Tooltip("How fast the player slide against the walls")]
    [Range(0f, 3f)]
    public float wallSlideVelocity = 2f;
    [Space]
    #endregion

    #region Wall Jump
    [Header("Wall Jump State")]
    [Tooltip("The force of the wall jump")]
    [Range(0f, 50f)]
    public float wallJumpVelocity = 20f;
    [HideInInspector]
    public float wallJumpTime = .1f;
    [Tooltip("For a good jump angle, x = 1, y = 2")]
    public Vector2 wallJumpAngle = new Vector2(1, 2);
    [Space]
    #endregion

    #region Wall Climb
    [Header("Wall Climb State")]
    [Tooltip("How fast does the player climb walls")]
    [Range(0f, 5f)]
    public float wallClimbVelocity = 3f;
    [Space]
    #endregion

    #region Ledge Climb
    [Header("Ledge Climb State")]
    public Vector2 startOffset;
    public Vector2 stopOffset;
    [Space]
    #endregion 

    #region Dash
    [Header("Dash State")]
    [Tooltip("How long after pressing the button are we forced to dash?")]
    [Range(0f, 2f)]
    public float maxHoldTime = 1f;
    [Tooltip("How fast does the time goes while holding the dash button")]
    [Range(0, 0.99f)]
    public float holdTimeScale = .25f;
    [Tooltip("How fast does the dash is")]
    [Range(10, 100)]
    public float dashVelocity = 50;
    [Tooltip("How long does the dash last for")]
    [Range(0.01f, 0.5f)]
    public float dashTime = .15f;
    [Tooltip("0 = no cooldown")]
    [Range(0f, 5f)]
    public float dashCooldown = .3f;
    [Tooltip("How the surroundong air density affects the player's velocity")]
    [Range(0f, 20f)]
    public float drag = 10f;
    [Tooltip("Avoid to fly really high")]
    [Range(0f, 1f)]
    public float dashEndYMultiplier = .2f;
    [Tooltip("How far apart the after images are spawned (ghost trail) ")]
    [Range(0f, 5f)]
    public float distanceBetweenAfterImages = .5f;
    [Space]
    #endregion

    #region Roll
    [Header("Roll State")]
    public float rollVelocity = 10f;
    public float rollCooldown = 0.3f;
    public float rollTime = 1f;
    public float rollColliderHeight = .9f;
    public float rollColliderWidth = .9f;
    #endregion

    #region Ground Pound
    public float groundPoundVelocity = 50f;
    public float groundPoundDamages = 100f;

    #endregion

}
