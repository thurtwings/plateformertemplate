using UnityEngine;

    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(Animator))]
    [RequireComponent(typeof(BoxCollider2D))]
public class Player : MonoBehaviour
{
    #region States Variables
    [Tooltip(" A scriptable object containing all the differentes variables \n Right click in the asset folder and go to: \n Create > Data > Player Data > Base Data")]
    [SerializeField] private PlayerData playerData;
    public PlayerStateMachine StateMachine { get; private set; }

    public PlayerIdleState IdleState { get; private set; }
    public PlayerMoveState MoveState { get; private set; }
    public PlayerJumpState JumpState { get; private set; }
    public PlayerLandState LandState { get; private set; }
    public PlayerDashState DashState { get; private set; }
    public PlayerRollState RollState { get; private set; }
    public PlayerInAirState InAirState { get; private set; }
    public PlayerAttackState PrimaryAttackState { get; private set; }
    public PlayerAttackState SecondaryAttackState { get; private set; }
    public PlayerWallGrabState WallGrabState { get; private set; }
    public PlayerWallJumpState WallJumpState { get; private set; }

    public PlayerWallSlideState WallSlideState { get; private set; }
    public PlayerWallClimbState WallClimbState { get; private set; }

    public PlayerLedgeClimbState LedgeClimbState { get; private set; }
    public PlayerCrouchIdleState CrouchIdleState { get; private set; }
    public PlayerCrouchMoveState CrouchMoveState { get; private set; }
    public PlayerGroundPoundState GroundPoundState { get; private set; }
    #endregion



    #region Added GameObjects
    public Transform dashDirectionIndicator { get; private set; }
    public ParticleSystem landingDust;
    public ParticleSystem jumpUpDust;
    public ParticleSystem wallJumpDust;
    public ParticleSystem dashEffect_1;
    public ParticleSystem dashEffect_2;
    public ParticleSystem dashTrailEffect;
    public ParticleSystem wallSlideEffect;
    public ParticleSystem wallGrabSweatEffect;
    public ParticleSystem DeathEffect;
    public ParticleSystem rollEffect;
    public ParticleSystem groundPoundLandingEffect;

    #endregion

    #region Components
    public Animator Anim { get; private set; }
    public Rigidbody2D Rb { get; private set; }
    public PlayerInputHandler InputHandler { get; private set; }
    public BoxCollider2D MovementCollider { get; private set; }
    public PlayerInventory Inventory { get; private set; }
    public Core Core { get; private set; }
    #endregion

    #region Main Animator Controller Animation Parameters

    private int idleParam;
    private int moveParam;
    private int landParam;
    private int rollParam;
    private int inAirParam;
    private int attackParam;
    private int wallGrabParam;
    private int wallSlideParam;
    private int wallClimbParam;
    internal int xVelocityParam;
    internal int yVelocityParam;
    private int crouchIdleParam;
    private int crouchMoveParam;
    private int ledgeClimbParam;


    #endregion

    #region Other Variables
    public bool enableDashSnappedDirection = true;
    private Vector2 workspace;
    
    #endregion

    #region Unity Callback Methods
    private void Awake()
    {
        Core = GetComponentInChildren<Core>();
        Anim = GetComponent<Animator>();

        idleParam = Animator.StringToHash("idle");
        moveParam = Animator.StringToHash("move");
        landParam = Animator.StringToHash("land");
        rollParam = Animator.StringToHash("roll");
        inAirParam = Animator.StringToHash("inAir");
        attackParam = Animator.StringToHash("attack");
        wallGrabParam = Animator.StringToHash("wallGrab");
        wallSlideParam = Animator.StringToHash("wallSlide");
        wallClimbParam = Animator.StringToHash("wallClimb");
        xVelocityParam = Animator.StringToHash("xVelocity");
        yVelocityParam = Animator.StringToHash("yVelocity");
        crouchIdleParam = Animator.StringToHash("crouchIdle");
        crouchMoveParam = Animator.StringToHash("crouchMove");
        ledgeClimbParam = Animator.StringToHash("ledgeClimbState");

        StateMachine = new PlayerStateMachine();

        IdleState = new PlayerIdleState(this, StateMachine, playerData, idleParam);
        MoveState = new PlayerMoveState(this, StateMachine, playerData, moveParam);
        LandState = new PlayerLandState(this, StateMachine, playerData, landParam);
        RollState = new PlayerRollState(this, StateMachine, playerData, rollParam);

        JumpState = new PlayerJumpState(this, StateMachine, playerData, inAirParam);
        DashState = new PlayerDashState(this, StateMachine, playerData, inAirParam);

        InAirState = new PlayerInAirState(this, StateMachine, playerData, inAirParam);

        WallJumpState = new PlayerWallJumpState(this, StateMachine, playerData, inAirParam);

        WallGrabState = new PlayerWallGrabState(this, StateMachine, playerData, wallGrabParam);

        GroundPoundState = new PlayerGroundPoundState(this, StateMachine, playerData, inAirParam);
        WallSlideState = new PlayerWallSlideState(this, StateMachine, playerData, wallSlideParam);
        WallClimbState = new PlayerWallClimbState(this, StateMachine, playerData, wallClimbParam);
        CrouchIdleState = new PlayerCrouchIdleState(this, StateMachine, playerData, crouchIdleParam);
        CrouchMoveState = new PlayerCrouchMoveState(this, StateMachine, playerData, crouchMoveParam);
        LedgeClimbState = new PlayerLedgeClimbState(this, StateMachine, playerData, ledgeClimbParam);

        PrimaryAttackState = new PlayerAttackState(this, StateMachine, playerData, attackParam);
        SecondaryAttackState = new PlayerAttackState(this, StateMachine, playerData, attackParam);

    }

    private void Start()
    {
        Rb = GetComponent<Rigidbody2D>();
        InputHandler = GetComponent<PlayerInputHandler>();
        MovementCollider = GetComponent<BoxCollider2D>();
        Inventory = GetComponent<PlayerInventory>();


        PrimaryAttackState.SetWeapon(Inventory.weapons[(int)CombatInputs.primary]);
        //SecondaryAttackState.SetWeapon(Inventory.weapons[(int)CombatInputs.secondary]);
        StateMachine.Initialize(IdleState);
        dashDirectionIndicator = transform.Find("DashDirectionIndicator");
    }

    private void Update()
    {
        Core.LogicUpdate();
        StateMachine.CurrentState.LogicUpdate();
    }

    private void FixedUpdate()
    {
        StateMachine.CurrentState.PhysicUpdate();
    }

    
    #endregion

    #region Set Methods

    
    
    public void SetColliderHeight(float height)
    {
        Vector2 center = MovementCollider.offset;
        workspace.Set(MovementCollider.size.x, height);

        center.y += (height - MovementCollider.size.y) / 2;
        MovementCollider.size = workspace;
        MovementCollider.offset = center;
    }

    
    #endregion

    

    #region Other Methods
    
    private void AnimationTrigger() => StateMachine.CurrentState.AnimationTrigger();
    private void AnimationFinishTrigger() => StateMachine.CurrentState.AnimationFinishTrigger();

    public void ActiveRollParticle()
    {
        rollEffect.Play();
    }
    public void DeactivateRollParticles()
    {
        rollEffect.Stop();
    }


    #endregion


}
