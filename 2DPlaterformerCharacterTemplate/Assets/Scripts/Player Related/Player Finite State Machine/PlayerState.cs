using UnityEngine;

public class PlayerState 
{
    protected Player player;
    protected PlayerStateMachine stateMachine;
    protected PlayerData playerData;
    protected float startTime;
    private int animBoolName;
    protected bool isAnimationFinished;
    protected bool isExitingState;
    protected Core core;
    public PlayerState(Player player, PlayerStateMachine stateMachine, PlayerData playerData, int animBoolName)
    {
        this.player = player;
        this.stateMachine = stateMachine;
        this.playerData = playerData;
        this.animBoolName = animBoolName;
        core = player.Core;
    }

    public virtual void DoChecks() { }
    
    public virtual void Enter()
    {
        DoChecks();
        startTime = Time.time;
        player.Anim.SetBool(animBoolName, true);
        isAnimationFinished = false;
        isExitingState = false;
        //Debug.Log(stateMachine.CurrentState);
    }

    public virtual void Exit()
    {
        player.Anim.SetBool(animBoolName, false);
        isExitingState = true;
    }

    public virtual void LogicUpdate() { }
    public virtual void PhysicUpdate() => DoChecks();
    public virtual void AnimationTrigger() { }
    public virtual void AnimationFinishTrigger() => isAnimationFinished = true;
    
    
}
