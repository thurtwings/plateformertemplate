using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealthbar : MonoBehaviour
{
    Image hb;
    public Color green, orange, red, darkRed;
    public Player player;
    Core core;
    // Start is called before the first frame update
    void Start()
    {
        hb = GetComponent<Image>();
        
        core = player.GetComponentInChildren<Core>();
        UpdateHealthBar();
    }

    private void UpdateHealthBar()
    {
        hb.fillAmount = core.Stats.currentHealth / core.Stats.maxHealth;
        //if (core.Stats.currentHealth == core.Stats.maxHealth )
        //{
        //    hb.color = green;
        //}
        //else
        //    hb.color = red;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
