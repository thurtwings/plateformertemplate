
This package is provided by Leneveu Steeven aka Thurtwings Games

The Player Finite State Machine Folder:
	
	- Player.cs	
		The script that has to be on the player gameobject

	- PlayerState.cs	
		The base script for all states. 
		It contains all the base State logic functions as virtual.

	- PlayerStateMachine.cs 
		As the name suggest, it contains the main mechanic, nothing fancy

The Input Actions Folder:
	
	- PlayerControl.asset 
		This is the new input action map, that is used to create new buttons to control the player

	- PlayerInputHandler.cs
		This script communicate with the PlayerControl.asset to read the value that the player input in the game and act accordingly.

The Player States Folder:

	This folder contains all the different actions that the player can execute
	It is divided in 2 folders : 
		SubStates:

			This folder is also divided in 4 parts:

				Ability States : 
					Contains the states that derive from PlayerAbilityState.cs (SuperState)
					They require only one input to be activated

				Ground States:
					Contains the states that derive from PlayerGroundedState.cs (SuperState)
					they require to be on the ground to be activated

				Standalone States:
					Contains the states that derive from PlayerState.cs. (Player Finite State Machine)
					They can be activated by either none or at least 2 inputs
					
				Wall States:
					Contains the states that derive from PlayerTouchingWall.cs (SuperState)
					They can be activated if the player is touching the wall

		Super States:
			Contains the states that derive from PlayerState.cs and are used 
			to determine if we touch the ground, the walls, or if we need either one or 
			the other for the FSM to change the current state to another

	
	


	