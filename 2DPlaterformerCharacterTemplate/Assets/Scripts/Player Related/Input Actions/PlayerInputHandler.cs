using System;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerInputHandler : MonoBehaviour
{
    private PlayerInput playerInput;
    private Player player;
    private Camera cam;
    public Vector2 RawMovementInput { get; private set; }
    public Vector2 RawDashDirectionInput { get; private set; }
    public Vector2Int DashDirectionInput { get; private set; }
    public int NormalizedInputX { get; private set; }
    public int NormalizedInputY { get; private set; }

    public bool JumpInput { get; private set; }
    public bool GrabInput { get; private set; }
    public bool JumpInputStop { get; private set; }
    public bool DashInput { get; private set; }
    public bool DashInputStop { get; private set; }
    public bool RollInput { get; private set; }
    public bool[] AttackInputs { get; private set; }

    [SerializeField] private float inputHoldTime = .2f;
    private float jumpStartTime;
    private float dashStartTime;
    private float rollStartTime;
    private void Start()
    {
        playerInput = GetComponent<PlayerInput>();
        player = GetComponent<Player>();

        int count = Enum.GetValues(typeof(CombatInputs)).Length;
        AttackInputs = new bool[count];
        cam = Camera.main;
    }
    private void Update()
    {
        CheckJumpInputHoldTime();
        CheckDashInputHoldTime();
    }

    public void OnPrimaryAttackInput(InputAction.CallbackContext ctx)
    {
        if (ctx.started)
        {
            AttackInputs[(int)CombatInputs.primary] = true;

        }
        if (ctx.canceled)
        {
            AttackInputs[(int)CombatInputs.primary] = false;

        }
    }

    //public void OnSecondaryAttackInput(InputAction.CallbackContext ctx)
    //{
    //    if (ctx.started)
    //    {
    //        AttackInputs[(int)CombatInputs.secondary] = true;

    //    }
    //    if (ctx.canceled)
    //    {
    //        AttackInputs[(int)CombatInputs.secondary] = false;

    //    }
    //}
    public void OnMoveInput(InputAction.CallbackContext ctx)
    {
        RawMovementInput = ctx.ReadValue<Vector2>();

        NormalizedInputX = Mathf.RoundToInt(RawMovementInput.x);
        NormalizedInputY = Mathf.RoundToInt(RawMovementInput.y);

        //if (Mathf.Abs(RawMovementInput.x) > .5f)
        //{
        //    NormalizedInputX = (int)(RawMovementInput * Vector2.right).normalized.x;
        //}
        //else
        //{
        //    NormalizedInputX = 0;
        //}
        //if(Mathf.Abs(RawMovementInput.y) > .5f)
        //{
        //    NormalizedInputY = (int)(RawMovementInput * Vector2.up).normalized.y;

        //}
        //else
        //{
        //    NormalizedInputY = 0;
        //}
    }
    public void OnJumpInput(InputAction.CallbackContext ctx)
    {
        if (ctx.started)
        {
            JumpInput = true;
            JumpInputStop = false;
            jumpStartTime = Time.time;

        }
        if (ctx.canceled)
        {
            JumpInputStop = true;
        }
    }
    public void OnDashInput(InputAction.CallbackContext ctx)
    {
        if (ctx.started)
        {
            DashInput = true;
            DashInputStop = false;
            dashStartTime = Time.time;

        }
        else if (ctx.canceled)
        {
            DashInputStop = true;
        }
    }
    public void OnGrabInput(InputAction.CallbackContext ctx)
    {
        if (ctx.started)
        {
            GrabInput = true;
        }
        if (ctx.canceled)
        {
            GrabInput = false;
        }
    }
    public void OnDashDirectionInput(InputAction.CallbackContext ctx)
    {
        RawDashDirectionInput = ctx.ReadValue<Vector2>();

        if(playerInput.currentControlScheme == "Keyboard")
        {
            RawDashDirectionInput = cam.ScreenToWorldPoint((Vector3)RawDashDirectionInput) - transform.position;

        }
        if (player.enableDashSnappedDirection)
        {
            DashDirectionInput = Vector2Int.RoundToInt(RawDashDirectionInput.normalized); 

        }

    }
    public void OnRollInput(InputAction.CallbackContext ctx)
    {
        if (ctx.started)
        {
            RollInput = true;
            rollStartTime = Time.time;
        }
        if (ctx.canceled)
        {
            RollInput = false;
        }
    }
    public void UseJumpInput() => JumpInput = false;
    public void UseDashInput() => DashInput = false;
    public void UseRollInput() => RollInput = false;
    private void CheckJumpInputHoldTime()
    {
        if (Time.time >= jumpStartTime + inputHoldTime)
        {
            JumpInput = false;
        }
    }
    private void CheckDashInputHoldTime()
    {
        if (Time.time >= dashStartTime + inputHoldTime)
        {
            DashInput = false;
        }
    }
}


public enum CombatInputs { primary, secondary }
