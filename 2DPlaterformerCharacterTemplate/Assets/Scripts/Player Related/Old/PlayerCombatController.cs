using UnityEngine;

public class PlayerCombatController : MonoBehaviour
{
    //[Header(">>> Components")]
    //[SerializeField] private Transform attack1HitBoxPos;
    //private Animator animator;
    //PlayerController pc;
    //PlayerStats ps;
    //[Space]

    //[Header(">>> Combat Variables")]
    //[SerializeField] private bool combatEnabled;
    //[SerializeField] private float waitTimeBetweenAttack;
    //[SerializeField] private LayerMask damageableMask;
    //private bool gotInput;
    //private bool isAttacking;
    //private float lastInputTime = Mathf.NegativeInfinity;
    //[Space]

    //[Header(">>> Attack 1 Variables")]
    //[SerializeField] private float attack1Radius;
    //[SerializeField] private float attack1Damages;
    //[SerializeField] private float stunDamageAmount = 1f;
    //private AttackDetails attackDetails; 
    //private bool isFirstAttack;
    //[Space]

    //[Header(">>> Animations Parameters")]
    //private int isAttackingParam;
    //private int firstAttackParam;
    //private int attack1Param;
    //private int canAttackParam;
    //private void Start()
    //{
    //    pc = GetComponent<PlayerController>();
    //    ps = GetComponent<PlayerStats>();
    //    animator = GetComponent<Animator>();
    //    isAttackingParam = Animator.StringToHash("isAttacking");
    //    firstAttackParam = Animator.StringToHash("firstAttack");
    //    attack1Param = Animator.StringToHash("attack1");
    //    canAttackParam = Animator.StringToHash("canAttack");
    //    animator.SetBool(canAttackParam, combatEnabled);
        
    //}
    //private void Update()
    //{
    //    CheckCombatInput();
    //    CheckAttacks();
    //}



    //private void CheckCombatInput()
    //{
    //    if (Input.GetMouseButtonDown(0))
    //    {
    //        gotInput = true;
    //        lastInputTime = Time.time;
    //    }
    //}

    //private void CheckAttacks()
    //{
    //    if (gotInput)
    //    {
    //        if (!isAttacking)
    //        {
    //            gotInput = false;
    //            isAttacking = true;
    //            isFirstAttack = !isFirstAttack;
    //            animator.SetBool(attack1Param, true);
    //            animator.SetBool(firstAttackParam, isFirstAttack);
    //            animator.SetBool(isAttackingParam, isAttacking);
    //        }
    //    }

    //    if(Time.time >= lastInputTime + waitTimeBetweenAttack)
    //    {
    //        gotInput = false;
    //    }
    //}
    
    //public void CheckAttack1HitBox()
    //{
    //    Collider2D[] detectedObjects = Physics2D.OverlapCircleAll(attack1HitBoxPos.position, attack1Radius, damageableMask);
    //    attackDetails.damageAmount = attack1Damages;
    //    attackDetails.position = transform.position;
    //    attackDetails.stunDamageAmount = stunDamageAmount;
    //    foreach (Collider2D obj in detectedObjects)
    //    {
    //        obj.transform.parent.SendMessage("Damage", attackDetails);
    //    }
    //}
    //public void FinishAttack1()
    //{
    //    isAttacking = false;
    //    animator.SetBool(isAttackingParam, isAttacking);
    //    animator.SetBool(attack1Param, false);
    //}

    //private void Damage(AttackDetails attackDetails)
    //{
    //    if (!pc.GetDashStatus())
    //    {
    //        int direction;
    //        ps.TakeDamage(attackDetails.damageAmount);
    //        if (attackDetails.position.x < transform.position.x)
    //        {
    //            direction = 1;
    //        }
    //        else
    //        {
    //            direction = -1;
    //        }

    //        pc.Knockback(direction);
    //    }
        
         
    //}
    //private void OnDrawGizmos()
    //{
    //    Gizmos.color = Color.magenta;
    //    Gizmos.DrawWireSphere(attack1HitBoxPos.position, attack1Radius);
    //}

}
