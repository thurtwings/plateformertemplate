using UnityEngine;

public class PlayerParticulesManager : MonoBehaviour
{
    [SerializeField] GameObject jumpParticules;
    [SerializeField] GameObject wallSlideParticules;
    [SerializeField] GameObject runParticules;

    private void Start()
    {
        jumpParticules.SetActive(false);
        wallSlideParticules.SetActive(false);
        runParticules.SetActive(false);
    }
    private void ActivateJumpParticules()
    {
        jumpParticules.SetActive(true);
    }
    private void ActivateWallSlideParticules()
    {
        wallSlideParticules.SetActive(true);
    }
    private void ActivateRunParticules()
    {
        runParticules.SetActive(true);
    }
    private void DeactivateJumpParticules()
    {
        jumpParticules.SetActive(false);
    }
    private void DeactivateWallSlideParticules()
    {
        wallSlideParticules.SetActive(false);
    }
    private void DeactivateRunParticules()
    {
        runParticules.SetActive(false);
    }

}
