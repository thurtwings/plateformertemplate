using UnityEngine;

public class PlayerAfterImage : MonoBehaviour
{
    public float activeTime = .5f;
    float timeActivated;
    float alpha;
    float alphatSet = .8f;

    [SerializeField]
    float alphaMultiplier = .97f;
    private Transform player;

    private SpriteRenderer SR;
    private SpriteRenderer playerSR;

    private Color color;


    private void OnEnable()
    {
        SR = GetComponent<SpriteRenderer>();
        player = GameObject.FindGameObjectWithTag("Player").transform;
        playerSR = player.GetComponent<SpriteRenderer>();

        alpha = alphatSet;
        SR.sprite = playerSR.sprite;
        transform.position = player.position;
        transform.rotation = player.rotation;
        transform.localScale = player.localScale;
        timeActivated = Time.time;
    }

    private void Update()
    {
        
        alpha -= alphaMultiplier * Time.deltaTime;
        color = new Color(1, 1, 1, alpha);
        SR.color = color;
        if(Time.time >= (timeActivated + activeTime))
        {
            PlayerAfterImagePool.Instance.AddToPool(gameObject); 
        }
        
    }


}
