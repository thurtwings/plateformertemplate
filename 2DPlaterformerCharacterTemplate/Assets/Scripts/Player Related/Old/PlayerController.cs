using System.Collections;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    #region Variables
   
    #region Components
    [Header(">>> Components")]
    [SerializeField] private Transform groundCheckTransform;
    [SerializeField] private Transform wallCheckTransform;
    [SerializeField] private Transform ledgeCheckTransform;
    [SerializeField] private GameObject jumpParticuleEffect;
    [SerializeField] private GameObject wallSlideParticuleEffect;
    [SerializeField] private GameObject runParticuleEffect;
    private SpriteRenderer playerSR;
    private Rigidbody2D rb;
    private Animator animator;
    [Space]
    #endregion
    #region Movement
    [Header(">>> Movement Variables")]
    [SerializeField] private float moveSpeed = 10;
    //[SerializeField] private float moveSpeedInAir = 50;
    [SerializeField] private float airDragMultiplier = .95f;
    private float movementDirection;
    private int facingDirection = 1;
    private bool facingRight = true;
    private bool isWalking;
    [Space]
    #endregion
    #region Surroundings
    [Header(">>> Surroundings Check")]
    [SerializeField] private LayerMask groundMask;
    [Range(.1f, 1f)]
    [SerializeField] private float groundCheckRadius = .2f;
    [Range(.1f, 1f)]
    [SerializeField] private float wallCheckLenght = .3f;
    private bool isGrounded;
    private bool isTouchingWall;
    
    [Space]
    #endregion
    #region Jump
    [Header(">>> Jump Variables")]
    [SerializeField] KeyCode jumpInput = KeyCode.Space;
    [SerializeField] private float jumpForce = 12;
    [SerializeField] private float jumpHeightMultiplier= .5f;
    [SerializeField] private float jumpBufferTimerSet = .1f;
    [SerializeField] private int maxAmountOfJump = 2;
    private float jumpBufferTimer;
    private int amountOfJumpLeft;
    private bool canNormalJump;
    private bool canWallJump;
    private bool isAttemptingToJump;
    private bool checkJumpMultiplier;
    //private bool isJumping;
    #endregion
    #region Wall Slide
    [Header(">>> Wall sliding Variables")]
    [SerializeField] private float wallSlidingSpeed = 4f;
    private bool isWallSliding;
    [Space]
    #endregion
    #region Wall Jump
    [Header(">>> Wall Jump Variables")]
    [SerializeField] private Vector2 wallJumpDirection;
    [SerializeField] private float wallJumpForce;
    [SerializeField] private float wallJumpTimerSet = .5f;
    [SerializeField] private float turnTimerSet = .1f;
    private float wallJumpTimer;
    private float turnTimer;
    private int lastWallJumpDirection;
    private bool hasWallJumped;
    private bool canMove;
    private bool canFlip;
    [Space]
    #endregion
    //#region Wall Grab
    //[Header(">>> Wall Grab Variables")]
    //[SerializeField] private KeyCode wallGrabInput = KeyCode.LeftShift;
    //public bool hasGrabTheWall;
    //[Space]
    //#endregion
    #region Ledge Climb
    [Header(">>> Ledge Climbing Variables")]
    [SerializeField] private float ledgeClimbXOffset1 = 0f;
    [SerializeField] private float ledgeClimbXOffset2 = 0f;
    [SerializeField] private float ledgeClimbYOffset1 = 0f;
    [SerializeField] private float ledgeClimbYOffset2 = 0f;
    private bool isTouchingLedge;
    private bool canClimbLedge = false;
    private bool detectLedge;
    private Vector2 ledgePosBottom;
    private Vector2 ledgePos1;
    private Vector2 ledgePos2;
    [Space]
    #endregion
    #region Dash
    [Header(">>> Dash Variables")]
    [SerializeField] private KeyCode dashInput;
    [SerializeField] private float dashLenght = .05f;
    [SerializeField] private float dashSpeed = 50f;
    [SerializeField] private float distanceBetweenImages = .25f;
    [SerializeField] private float dashCooldown = 1f;
    private float dashTimeLeft;
    private float lastImageXPos;
    private float lastDash = -100f;
    private bool isDashing;

    #endregion

    #region Knockback
    [Header(">>> Player's Knockback Variables ")]
    [SerializeField] private float knockbackDuration;
    [SerializeField] private Vector2 knockbackSpeed;
    private float knockbackStartTime;
    private bool knockback;
    #endregion
    #region Animations
        
    [Header(">>> Animations Parameters")]
    private int isGroundedParam;
    private int yVelocityParam;
    private int isWalkingParam;
    private int isWallSlidingParam;
    private int canClimbLedgeParam;
    #endregion

    #endregion
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        isWalkingParam = Animator.StringToHash("isWalking");
        isGroundedParam = Animator.StringToHash("isGrounded");
        isWallSlidingParam = Animator.StringToHash("isWallsliding");
        yVelocityParam = Animator.StringToHash("yVelocity");
        canClimbLedgeParam = Animator.StringToHash("canClimbLedge");
        amountOfJumpLeft = maxAmountOfJump;
        wallJumpDirection.Normalize();
        playerSR = GetComponent<SpriteRenderer>();
        runParticuleEffect.SetActive(false);
        wallSlideParticuleEffect.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        GetInput();
        CheckMovementDirection();
        UpdateAnimations();
        CheckIfCanJump();
        CheckIfWallSliding();
        CheckJump();
        CheckLedgeClimb();
        CheckParticules();
        CheckKnockback();
        
    }

    private void FixedUpdate()
    {
        CheckDash();
        ApplyMovement();
        CheckSurroundings();
    }
    private void GetInput()
    {
        movementDirection = Input.GetAxisRaw("Horizontal");
        //hasGrabTheWall = Input.GetKey(wallGrabInput) && isTouchingWall;

        if(movementDirection == 0)
        {
            rb.velocity = new Vector2(0f, rb.velocity.y);
        }
        //if(hasGrabTheWall)
        //{
        //    isWallSliding = false;
        //    rb.velocity = new Vector2(0, 0) ;
        //    rb.gravityScale = 0f;
        //}
        //else
        //{

        //}
        if (Input.GetKeyDown(jumpInput))
        {
            if (isGrounded || amountOfJumpLeft > 0 && !isTouchingWall)
            {
                NormalJump();

            }
            else
            {
                jumpBufferTimer = jumpBufferTimerSet;
                isAttemptingToJump = true;
                //isJumping = false;
            }

        }
        
        if(Input.GetButtonDown("Horizontal") && isTouchingWall)
        {
            if(!isGrounded && movementDirection != facingDirection)
            {
                canMove = false;
                canFlip = false;

                turnTimer = turnTimerSet;
            }
        }

        if (turnTimer >= 0)
        {
            turnTimer -= Time.deltaTime;

            if(turnTimer <= 0f)
            {
                canMove = true;
                canFlip = true;
            }
        }

        if (checkJumpMultiplier && !Input.GetKey(jumpInput) && !isDashing)
        {
            checkJumpMultiplier = false;
            rb.velocity = new Vector2(rb.velocity.x, rb.velocity.y * jumpHeightMultiplier);
        }

        if (Input.GetKeyDown(dashInput))
        {
            
            if (Time.time >= (lastDash + dashCooldown))
            {
                AttemptToDash();
            }

        }
    }

    private void ApplyMovement()
    {
        //if (!hasGrabTheWall)
        //{
           
        //}
        if (!isGrounded && !isWallSliding && movementDirection == 0 && !knockback)
        {
            rb.velocity = new Vector2(rb.velocity.x * airDragMultiplier, rb.velocity.y);
        }
        else if (canMove && !knockback)
        {
            rb.velocity = new Vector2(moveSpeed * movementDirection, rb.velocity.y);
        }

        if (isWallSliding)
        {
            if (rb.velocity.y < -wallSlidingSpeed)
            {
                rb.velocity = new Vector2(rb.velocity.x, -wallSlidingSpeed);

            }
        }

    }

    private void CheckParticules()
    {
        if(isGrounded && rb.velocity.x != 0f)
            runParticuleEffect.SetActive(true);
        else
            runParticuleEffect.SetActive(false);

        if (isWallSliding)
            wallSlideParticuleEffect.SetActive(true);
        else
            wallSlideParticuleEffect.SetActive(false);

        
    }
    private void ActivateJumpParticles()
    {
        jumpParticuleEffect.GetComponent<ParticleSystem>().Play();
    }
    private void DeactivateJumpParticles()
    {
        jumpParticuleEffect.SetActive(false);
    }
    private void NormalJump()
    {
        if (canNormalJump && !isDashing) // normal Jump
        {
            //isJumping = true;
            ActivateJumpParticles();
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
            amountOfJumpLeft--;
            jumpBufferTimer = 0;
            isAttemptingToJump = false;
            checkJumpMultiplier = true;
            StartCoroutine(JumpSqueeze(1.2f, .8f, .1f));
        }
    }
    IEnumerator JumpSqueeze(float x, float y, float z)
    {
        Vector3 originalSize = Vector3.one;
        Vector3 newSize = new Vector3(x, y, originalSize.z);
        float t = 0f;
        while (t <= 1.0f)
        {
            t += Time.deltaTime /z;
            playerSR.transform.localScale = Vector3.Lerp(originalSize, newSize, t);
            yield return null;
        }
        t = 0f;
        while (t <= 1.0f)
        {
            t += Time.deltaTime/z;
            playerSR.transform.localScale = Vector3.Lerp(newSize, originalSize, t);
            yield return null;
        }
    }
    private void WallJump()
    {
        if (canWallJump)
        {
            
            rb.velocity = new Vector2(rb.velocity.x, 0f);
            isWallSliding = false;
            amountOfJumpLeft = maxAmountOfJump;
            amountOfJumpLeft--;
            Vector2 forceToAdd = new Vector2(wallJumpForce * wallJumpDirection.x * movementDirection, wallJumpForce * wallJumpDirection.y);
            rb.AddForce(forceToAdd, ForceMode2D.Impulse);
            jumpBufferTimer = 0f;
            isAttemptingToJump = false;
            checkJumpMultiplier = true;
            turnTimer = 0f;
            canMove = true;
            canFlip = true;
            hasWallJumped = true;
            wallJumpTimer = wallJumpTimerSet;
            lastWallJumpDirection = -facingDirection;
            
        }
    }
    private void CheckJump()
    {

        if (jumpBufferTimer > 0)
        {
            if (!isGrounded && isTouchingWall && movementDirection != 0 && movementDirection != facingDirection)
                WallJump();
            else if (isGrounded && !isDashing)
                NormalJump();

        }

        if (isAttemptingToJump)
            jumpBufferTimer -= Time.deltaTime;

        if (wallJumpTimer > 0)
        {
            if (hasWallJumped && movementDirection == -lastWallJumpDirection)
            {
                rb.velocity = new Vector2(rb.velocity.x, 0f);
                hasWallJumped = false;

            }
            else if (wallJumpTimer <= 0)
                hasWallJumped = false;
            else
                wallJumpTimer -= Time.deltaTime;
        }
    }

    public void Knockback(int direction)
    {
        knockback = true;
        knockbackStartTime = Time.time;
        rb.velocity = new Vector2(knockbackSpeed.x * direction, knockbackSpeed.y);

    }

    private void CheckKnockback()
    {
        if (Time.time >= knockbackStartTime + knockbackDuration && knockback)
        {
            knockback = false;
            rb.velocity = new Vector2(0.0f, rb.velocity.y);
        }
    }
    private void CheckLedgeClimb()
    {
        if(detectLedge && !canClimbLedge)
        {
            canClimbLedge = true;

            if (facingRight)
            {
                ledgePos1 = new Vector2(Mathf.Floor(ledgePosBottom.x + wallCheckLenght) - ledgeClimbXOffset1, Mathf.Floor(ledgePosBottom.y) + ledgeClimbYOffset1);
                ledgePos2 = new Vector2(Mathf.Floor(ledgePosBottom.x + wallCheckLenght) + ledgeClimbXOffset2, Mathf.Floor(ledgePosBottom.y) + ledgeClimbYOffset2);
            }
            else
            {
                ledgePos1 = new Vector2(Mathf.Ceil(ledgePosBottom.x - wallCheckLenght) + ledgeClimbXOffset1, Mathf.Floor(ledgePosBottom.y) + ledgeClimbYOffset1);
                ledgePos2 = new Vector2(Mathf.Ceil(ledgePosBottom.x - wallCheckLenght) - ledgeClimbXOffset2, Mathf.Floor(ledgePosBottom.y) + ledgeClimbYOffset2);
            }

            canMove = false;
            canFlip = false;
            animator.SetBool(canClimbLedgeParam, canClimbLedge);    
            
        }

        if (canClimbLedge)
        {
            transform.position = ledgePos1;
        }
    }
    public void FinishLedgeClimb()
    {
        canClimbLedge = false;
        transform.position = ledgePos2;
        canMove = true;
        canFlip = true;
        detectLedge = false;
        animator.SetBool(canClimbLedgeParam, canClimbLedge);
    }
    private void CheckIfCanJump()
    {
        if (isGrounded && rb.velocity.y <= 0.01f)
            amountOfJumpLeft = maxAmountOfJump;

        if (isTouchingWall)
            canWallJump = true;


        if (amountOfJumpLeft <= 0)
            canNormalJump = false;
        else
            canNormalJump = true;
                
    }
    private void CheckIfWallSliding()
    {
        if (isTouchingWall && movementDirection == facingDirection && rb.velocity.y < 0f && !canClimbLedge)
        {
            isWallSliding = true;
            //wallSlideParticuleEffect.SetActive(true);
        }
        else
        {
            isWallSliding = false;
            //wallSlideParticuleEffect.SetActive(false);


        }

    }
    private void CheckMovementDirection()
    {
        if(facingRight && movementDirection < 0f)
        {
            Flip();
        }
        else if(!facingRight && movementDirection > 0f)
        {
            Flip();
        }

        if(rb.velocity.x != 0f)
            //Mathf.Abs(rb.velocity.x) >= 0.01f 
        {
            isWalking = true;
        }
        else
        {
            isWalking = false; 
        }
    }
    private void AttemptToDash()
    {
        isDashing = true;
        dashTimeLeft = dashLenght;
        lastDash = Time.time;
        PlayerAfterImagePool.Instance.GetFromPool();
        lastImageXPos = transform.position.x;
    }
    private void CheckDash()
    {
        if (isDashing)
        {
            ActivateJumpParticles();
            if (dashTimeLeft > 0f)
            {
                canMove = false;
                canFlip = false;
                rb.velocity = new Vector2(dashSpeed * facingDirection, 0f);
                dashTimeLeft -= Time.deltaTime;

                if (Mathf.Abs(transform.position.x - lastImageXPos) > distanceBetweenImages)
                {
                    PlayerAfterImagePool.Instance.GetFromPool();
                    lastImageXPos = transform.position.x;
                }

                if(dashTimeLeft <= 0f || isTouchingWall)
                {
                    isDashing = false;
                    canMove = true;
                    canFlip = true;
                }
            }
            
            
        }
    }
    public bool GetDashStatus()
    {
        return isDashing;
    }
    private void CheckSurroundings()
    {
        isGrounded = Physics2D.OverlapCircle(groundCheckTransform.position, groundCheckRadius, groundMask);
        isTouchingWall = Physics2D.Raycast(wallCheckTransform.position, transform.right, wallCheckLenght, groundMask);
        isTouchingLedge = Physics2D.Raycast(ledgeCheckTransform.position, transform.right, wallCheckLenght, groundMask);

        if (isTouchingWall && !isTouchingLedge && !detectLedge)
        {
            detectLedge = true;
            ledgePosBottom = wallCheckTransform.position;

        }
    }
    private void Flip()
    {
        if (!isWallSliding && canFlip && !knockback)
        {
            ActivateJumpParticles();
            facingDirection *= -1;
            facingRight = !facingRight;
            transform.Rotate(0, 180f, 0f);
        }
        
    }
    public int GetFacingDirection()
    {
        return facingDirection;
    }

    public void DisableFlip()
    {
        canFlip = false;
    } 

    public void EnableFlip()
    {
        canFlip = true;
    }
    private void UpdateAnimations()
    {
        animator.SetBool(isWalkingParam, isWalking);
        animator.SetBool(isGroundedParam, isGrounded);
        animator.SetFloat(yVelocityParam, rb.velocity.y);
        animator.SetBool(isWallSlidingParam, isWallSliding);

    }
    private void OnDrawGizmos()
    {
        if (isGrounded || detectLedge)
            Gizmos.color = Color.green;
        else
            Gizmos.color = Color.red;
        //Ground check visual
        Gizmos.DrawWireSphere(groundCheckTransform.position, groundCheckRadius);

        // Ledge Check Visual
        if (facingRight)
            Gizmos.DrawLine(ledgeCheckTransform.position, new Vector3(ledgeCheckTransform.position.x + wallCheckLenght, ledgeCheckTransform.position.y, ledgeCheckTransform.position.z));
        else
            Gizmos.DrawLine(ledgeCheckTransform.position, new Vector3(ledgeCheckTransform.position.x - wallCheckLenght, ledgeCheckTransform.position.y, ledgeCheckTransform.position.z));

        // Wall Check Visual
        Gizmos.color = Color.blue;
        if(facingRight)
            Gizmos.DrawLine(wallCheckTransform.position, new Vector3(wallCheckTransform.position.x + wallCheckLenght, wallCheckTransform.position.y, wallCheckTransform.position.z));
        else
            Gizmos.DrawLine(wallCheckTransform.position, new Vector3(wallCheckTransform.position.x - wallCheckLenght, wallCheckTransform.position.y, wallCheckTransform.position.z));
    }
}
