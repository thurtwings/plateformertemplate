using UnityEngine;

public class PlayerStats : MonoBehaviour
{
    [Header(">>> Components")]
    [SerializeField]private GameObject deathChunkParticles, deathBloodParticles;
    private GameManager Gm;
    [Space]

    [Header(">>> Health Variables")]
    [SerializeField] private float maxHealth = 100f;
    private float currentHealth;


    private void Start()
    {
        currentHealth = maxHealth;
        Gm = GameObject.Find("GameManager").GetComponent<GameManager>();
    }

    public void TakeDamage(float amount)
    {
        currentHealth -= amount;

        if (currentHealth <= 0.0f)
            Die();
    }

    private void Die()
    {

        Instantiate(deathChunkParticles, transform.position, deathChunkParticles.transform.rotation);
        Instantiate(deathBloodParticles, transform.position, deathBloodParticles.transform.rotation);
        Gm.Respawn();
        Destroy(gameObject);
    }
}
