public class PlayerLandState : PlayerGroundedState
{
    public PlayerLandState(Player player, PlayerStateMachine stateMachine, PlayerData playerData, int animBoolName) : base(player, stateMachine, playerData, animBoolName)
    {
    }

    public override void DoChecks()
    {
        base.DoChecks();
    }

    public override void Enter()
    {
        base.Enter();

        player.landingDust.Play();
        player.DashState.ResetCanDash();

    }

    
    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if (!isExitingState)
        {
            if (xInput != 0)
            {
                stateMachine.ChangeStateTo(player.MoveState);
            }
            else if (isAnimationFinished)
            {
                stateMachine.ChangeStateTo(player.IdleState);
            }
        }
        
    }

    public override void PhysicUpdate()
    {
        base.PhysicUpdate();
    }
}
