public class PlayerIdleState : PlayerGroundedState
{
    
    public PlayerIdleState(Player player, PlayerStateMachine stateMachine, PlayerData playerData, int animBoolName) : base(player, stateMachine, playerData, animBoolName)
    {
    }

    public override void DoChecks()
    {
        base.DoChecks();
    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if (!isExitingState)
        {
            if (xInput != 0)
            {
                stateMachine.ChangeStateTo(player.MoveState);
            }
            else if (yInput == -1)
            {
                stateMachine.ChangeStateTo(player.CrouchIdleState);
            }
        }
       
    }

    public override void PhysicUpdate()
    {
        base.PhysicUpdate();
    }
}
