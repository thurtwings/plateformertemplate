public class PlayerMoveState : PlayerGroundedState
{
    public PlayerMoveState(Player player, PlayerStateMachine stateMachine, PlayerData playerData, int animBoolName) : base(player, stateMachine, playerData, animBoolName)
    {
    }

    public override void DoChecks()
    {
        base.DoChecks();
    }

    public override void Enter()
    {
        base.Enter();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        core.Movement.CheckIfPlayerShouldFlip(xInput);
        core.Movement.SetVelocityX(playerData.movementVelocity * xInput);

        if (!isExitingState)
        {
            if (xInput == 0)
            {
                stateMachine.ChangeStateTo(player.IdleState);
            }
            else if(yInput == -1)
            {
                stateMachine.ChangeStateTo(player.CrouchMoveState);
            }
        }
        
    }

    public override void PhysicUpdate()
    {
        base.PhysicUpdate();
    }
}
