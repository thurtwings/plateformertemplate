public class PlayerJumpState : PlayerAbilityState
{
    private int amountOfJumpLeft;
    public PlayerJumpState(Player player, PlayerStateMachine stateMachine, PlayerData playerData, int animBoolName) : base(player, stateMachine, playerData, animBoolName)
    {
        amountOfJumpLeft = playerData.amountOfJumps;
    }

    public override void Enter()
    {
        base.Enter();
        player.InputHandler.UseJumpInput();

        player.jumpUpDust.Play();
        core.Movement.SetVelocityY(playerData.jumpVelocity);

        isAbilityDone = true;
        amountOfJumpLeft--;
        player.DashState.ResetCanDash();
        player.InAirState.SetIsJumping();
    }

    public bool CanJump() => amountOfJumpLeft > 0;

    public void ResetAmountOfJumpsLeft() => amountOfJumpLeft = playerData.amountOfJumps;

    public void DecreaseAmountOfJumpsLeft() => amountOfJumpLeft--;
    public void DecreaseAmountOfJumpsLeft(int amount) => amountOfJumpLeft -= amount;


}
