using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerRollState : PlayerAbilityState
{
    public bool CanRoll { get; private set; }
    protected bool isRolling;
    private float lastRollTime;
    protected bool isTouchingCeiling;
    Vector2 rollDirection;
    public PlayerRollState(Player player, PlayerStateMachine stateMachine, PlayerData playerData, int animBoolName) : base(player, stateMachine, playerData, animBoolName)
    {
    }

    

    public override void Enter()
    {
        base.Enter();
        
        player.SetColliderHeight(playerData.rollColliderHeight);
        CanRoll = false;
        player.InputHandler.UseRollInput();
        core.Movement.SetVelocityX(playerData.rollVelocity);
        rollDirection = Vector2.right * core.Movement.FacingDirection;
        startTime = Time.time;
        isRolling = true;

    }

    public override void Exit()
    {
        base.Exit();
        core.Movement.SetVelocityZero();
        CanRoll = true;
        isRolling = false;
        player.SetColliderHeight(playerData.standColliderHeight);

    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();

        if (!isExitingState)
        {
            core.Movement.SetVelocityX(playerData.rollVelocity * rollDirection.x);
            if (Time.time >= startTime + playerData.rollTime)
            {

                isAbilityDone = true;
                lastRollTime = Time.time;

            }
        }

    }

    
    public override void AnimationFinishTrigger()
    {
        base.AnimationFinishTrigger();

        isAbilityDone = true;
    }
    public override void PhysicUpdate()
    {
        base.PhysicUpdate();
    }

    public bool CheckIfCanRoll()
    {
        return CanRoll && Time.time >= lastRollTime + playerData.rollCooldown;
    }
    public void ResetCanRoll() => CanRoll = true;

    public override void DoChecks()
    {
        base.DoChecks();
        
    }
}
