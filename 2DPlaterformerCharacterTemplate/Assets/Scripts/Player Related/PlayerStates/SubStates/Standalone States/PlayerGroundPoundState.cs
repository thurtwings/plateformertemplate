using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerGroundPoundState : PlayerInAirState
{
    
    public PlayerGroundPoundState(Player player, PlayerStateMachine stateMachine, PlayerData playerData, int animBoolName) : base(player, stateMachine, playerData, animBoolName)
    {
    }

    public override void DoChecks()
    {
        base.DoChecks();
    }

    public override void Enter()
    {
        base.Enter();
        isGroundPounding = true;
        player.dashTrailEffect.Play();
        core.Movement.SetVelocityY(-playerData.groundPoundVelocity);
    }

    public override void Exit()
    {
        base.Exit();
        CameraShakeCinemachine.Instance.CameraShake(10, .2f);
        core.Movement.SetVelocityZero();
        player.dashTrailEffect.Stop();
        player.groundPoundLandingEffect.Play();
        isGroundPounding = false;
        
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if (!isExitingState)
        {
            if (isGroundPounding)
            {
                core.Movement.SetVelocityY(-playerData.groundPoundVelocity);

                if (isGrounded)
                {
                    isExitingState = true;
                }
                
            }
            
        }
        
    }



    
}
