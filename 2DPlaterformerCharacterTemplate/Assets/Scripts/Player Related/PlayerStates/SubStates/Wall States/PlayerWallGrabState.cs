using UnityEngine;

public class PlayerWallGrabState : PlayerTouchingWallState
{
    private Vector2 holdPos;
    public PlayerWallGrabState(Player player, PlayerStateMachine stateMachine, PlayerData playerData, int animBoolName) : base(player, stateMachine, playerData, animBoolName)
    {
    }

    public override void Enter()
    {
        base.Enter();
        holdPos = player.transform.position;
        HoldPosition();
        player.wallGrabSweatEffect.Play();
    }

    public override void Exit()
    {
        base.Exit();
        player.wallGrabSweatEffect.Stop();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();


        if (!isExitingState)
        {
            HoldPosition();
            if (yInput > 0)
            {
                stateMachine.ChangeStateTo(player.WallClimbState);
            }
            else if (yInput < 0 || !grabInput)
            {
                stateMachine.ChangeStateTo(player.WallSlideState);
            }
            //else if (dashInput && player.DashState.CheckIfCanDash() && !isTouchingCeiling)
            //{
            //    stateMachine.ChangeStateTo(player.DashState);

            //}
        }
        
    }

    private void HoldPosition()
    {
        player.transform.position = holdPos;
        core.Movement.SetVelocityX(0);
        core.Movement.SetVelocityY(0);
    }

   
}
