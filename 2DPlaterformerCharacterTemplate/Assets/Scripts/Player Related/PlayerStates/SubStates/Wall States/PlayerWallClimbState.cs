public class PlayerWallClimbState : PlayerTouchingWallState
{
    public PlayerWallClimbState(Player player, PlayerStateMachine stateMachine, PlayerData playerData, int animBoolName) : base(player, stateMachine, playerData, animBoolName)
    {
    }

    public override void Enter()
    {
        base.Enter();
        player.wallGrabSweatEffect.Play();
    }

    public override void Exit()
    {
        base.Exit();
        player.wallGrabSweatEffect.Stop();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();

        
        if (!isExitingState)
        {
            core.Movement.SetVelocityY(playerData.wallClimbVelocity);
            
            if (yInput != 1)
            {
                stateMachine.ChangeStateTo(player.WallGrabState);
            }
        }
       
    }
}
