public class PlayerGroundedState : PlayerState
{
    protected int xInput;
    protected int yInput;

    private bool jumpInput;
    private bool grabInput;
    private bool dashInput;
    private bool rollInput;
    private bool isGrounded;
    private bool isTouchingWall;
    private bool isTouchingLedge;
    protected bool isTouchingCeiling;
    public PlayerGroundedState(Player player, PlayerStateMachine stateMachine, PlayerData playerData, int animBoolName) : base(player, stateMachine, playerData, animBoolName)
    {
    }

    public override void DoChecks()
    {
        base.DoChecks();
        isGrounded = core.CollisionSenses.Ground;
        isTouchingWall = core.CollisionSenses.WallFront;
        isTouchingLedge = core.CollisionSenses.LedgeHorizontal;
        isTouchingCeiling = core.CollisionSenses.Ceiling;
    }

    public override void Enter()
    {
        base.Enter();
        player.JumpState.ResetAmountOfJumpsLeft();
        player.DashState.ResetCanDash();
        
        player.RollState.ResetCanRoll();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        xInput = player.InputHandler.NormalizedInputX;
        yInput = player.InputHandler.NormalizedInputY;

        jumpInput = player.InputHandler.JumpInput;
        grabInput = player.InputHandler.GrabInput;
        dashInput = player.InputHandler.DashInput;
        rollInput = player.InputHandler.RollInput;


        if (player.InputHandler.AttackInputs[(int)CombatInputs.primary] && !isTouchingCeiling)
        {
            stateMachine.ChangeStateTo(player.PrimaryAttackState);
        }
        else if (player.InputHandler.AttackInputs[(int)CombatInputs.secondary] && !isTouchingCeiling)
        {
            stateMachine.ChangeStateTo(player.SecondaryAttackState);
        }
        else if (jumpInput && player.JumpState.CanJump() && !isTouchingCeiling)
        {
            
            stateMachine.ChangeStateTo(player.JumpState);
        }
        else if(rollInput && isGrounded && player.RollState.CheckIfCanRoll() && !isTouchingCeiling)
        {
            stateMachine.ChangeStateTo(player.RollState);
        }
        else if(isGrounded && isTouchingCeiling && xInput==0)
        {
            stateMachine.ChangeStateTo(player.CrouchIdleState);
        }
        else if (!isGrounded)
        {
            player.InAirState.StartCoyoteTime();
            stateMachine.ChangeStateTo(player.InAirState);
        }
        else if(isTouchingWall && grabInput && isTouchingLedge)
        {
            stateMachine.ChangeStateTo(player.WallGrabState);
        }

        //else if (dashInput && player.DashState.CheckIfCanDash() && !isTouchingCeiling)
        //{
        //    stateMachine.ChangeStateTo(player.DashState);

        //}
    }

    public override void PhysicUpdate()
    {
        base.PhysicUpdate();
    }
}
