public class PlayerAbilityState : PlayerState
{

    protected bool isAbilityDone;
    private bool isGrounded;
    

    public PlayerAbilityState(Player player, PlayerStateMachine stateMachine, PlayerData playerData, int animBoolName) : base(player, stateMachine, playerData, animBoolName)
    {
    }

    public override void DoChecks()
    {
        base.DoChecks();
        isGrounded = core.CollisionSenses.Ground;
    }

    public override void Enter()
    {
        base.Enter();
        isAbilityDone = false;
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if (isAbilityDone)
        {
            if (isGrounded && core.Movement.CurrentVelocity.y < 0.1f)
            {
                stateMachine.ChangeStateTo(player.IdleState);
            }
            
            else
            {
                stateMachine.ChangeStateTo(player.InAirState);
            }
        }
    }

    public override void PhysicUpdate()
    {
        base.PhysicUpdate();
    }
    public override void Exit()
    {
        base.Exit();
    }
}
