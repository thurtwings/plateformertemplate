using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Core : MonoBehaviour
{
    private Stats stats;
    private Combat combat;
    private Movement movement;
    private CollisionSenses collisionSenses;

    public Movement Movement
    {
             
        get => GenericNotImplementedError<Movement>.TryToGet(movement, transform.parent.name);

        private set => movement = value; 
    }
    public CollisionSenses CollisionSenses
    {
        get => GenericNotImplementedError<CollisionSenses>.TryToGet(collisionSenses, transform.parent.name);

        private set => collisionSenses = value; 
    }
    public Combat Combat
    {
        get => GenericNotImplementedError<Combat>.TryToGet(combat, transform.parent.name);

        private set => combat = value;
    }
    public Stats Stats
    {
        get => GenericNotImplementedError<Stats>.TryToGet(stats, transform.parent.name);
        private set => stats = value;
    }

    private List<ILogicUpdate> components = new List<ILogicUpdate>();
    private void Awake()
    {
        Movement = GetComponentInChildren<Movement>();
        CollisionSenses = GetComponentInChildren<CollisionSenses>();
        Combat = GetComponentInChildren<Combat>();
        Stats = GetComponentInChildren<Stats>();
    }

    public void LogicUpdate()
    {
        foreach (ILogicUpdate component in components)
        {
            component.LogicUpdate();
        }

    }

    public void AddComponent(ILogicUpdate component)
    {
        if (!components.Contains(component))
        {
            components.Add(component);
        }
    }











}
