using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : CoreComponent
{
    #region Getter Setter
    public Vector2 CurrentVelocity { get; private set; }
    public int FacingDirection { get; private set; }
    public Rigidbody2D Rb { get; private set; }
    public bool CanSetVelocity { get; set; }
    #endregion

    private Vector2 workspace;
    protected override void Awake()
    {
        base.Awake();

        Rb = GetComponentInParent<Rigidbody2D>();

        FacingDirection = 1;
        CanSetVelocity = true;
    }

    public override void LogicUpdate()
    {
        CurrentVelocity = Rb.velocity;
    }
    #region SetVelocity()
    public void SetVelocityX(float xVelocity)
    {
        workspace.Set(xVelocity, CurrentVelocity.y);
        SetFinalVelocity();
    }
    public void SetVelocityY(float yVelocity)
    {
        workspace.Set(CurrentVelocity.x, yVelocity);
        SetFinalVelocity();
    }
    public void SetVelocity(float velocity, Vector2 angle, int direction)
    {
        angle.Normalize();
        workspace.Set(angle.x * velocity * direction, angle.y * velocity);
        SetFinalVelocity();
    }
    public void SetVelocity(float velocity, Vector2 direction)
    {
        workspace = direction * velocity;
        SetFinalVelocity();

    }
    public void SetVelocityZero()
    {
        workspace = Vector2.zero;
        
        SetFinalVelocity();
    }

    private void SetFinalVelocity()
    {
        if (CanSetVelocity)
        {
            Rb.velocity = workspace;
            CurrentVelocity = workspace;
        }
        
    }
    #endregion

    #region Check Functions
    public void CheckIfPlayerShouldFlip(int xInput)
    {
        if (xInput != 0 && xInput != FacingDirection)
        {
            Flip();
        }
    }
    #endregion
    #region Other Functions
    public void Flip()
    {
        FacingDirection *= -1;
        Rb.transform.Rotate(0, 180, 0);
    }

    #endregion
}
