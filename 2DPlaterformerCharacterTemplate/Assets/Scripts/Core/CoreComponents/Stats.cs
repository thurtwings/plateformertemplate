using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AllIn1SpriteShader;

public class Stats : CoreComponent
{
    [SerializeField] internal float maxHealth = 50f;
    [SerializeField] private float maxStamina = 100f;
    private float currentStamina;
    internal float currentHealth;
    public bool isPlayer;
    [SerializeField] GameObject deathChunks;
    [SerializeField] GameObject deathBlood;
    protected override void Awake()
    {
        base.Awake();

        currentHealth = maxHealth;
        currentStamina = maxStamina;
    }

    public void DecreaseHealth(float amount)
    {
        currentHealth -= amount;
        StartCoroutine(HitEffect());
        if (currentHealth <= 0)
        {
            currentHealth = 0;
            
            Die(isPlayer);
           
        }
    }
    private void Die(bool isPlayer)
    {
        Instantiate(deathChunks, core.transform.parent.position, deathChunks.transform.rotation);
        Instantiate(deathBlood, core.transform.parent.position, deathBlood.transform.rotation);
        if (isPlayer)
        {
            
            GameManager.Instance.Respawn();
        }
        //Destroy(core.transform.parent.gameObject);
        core.transform.parent.gameObject.SetActive(false);


    }
    
    public void IncreaseHealth(float amount)
    {
        currentHealth += amount;
        currentHealth = Mathf.Clamp(currentHealth + amount, 0, maxHealth);
    }

    public void DecreaseStamina(float amount)
    {
        currentStamina -= amount;
    }

    IEnumerator HitEffect()
    {
        Material mat = GetComponentInParent<Renderer>().material;
        mat.EnableKeyword("HITEFFECT_ON");
        yield return new WaitForSeconds(.1f);
        mat.DisableKeyword("HITEFFECT_ON");
    }
}
