﻿using System.Collections;
using UnityEngine;

public class CoreComponent : MonoBehaviour, ILogicUpdate
{
    protected Core core;

    protected virtual void Awake()
    {
        core = transform.parent.GetComponent<Core>();

        if(!core)
        {
            Debug.LogWarning("Missing Core.cs on parent");
        }
        core.AddComponent(this);
    }

    public virtual void LogicUpdate() { }

}