using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionSenses : CoreComponent
{

    #region Check Transforms

    #region Getter Setter
    public LayerMask GroundMask { get => groundMask; private set => groundMask = value; }
    public LayerMask EnemyMask { get => enemyMask; private set => enemyMask = value; }
    public float GroundCheckRadius { get => groundCheckRadius; private set => groundCheckRadius = value; }
    public float WallCheckdistance { get => wallCheckdistance; private set => wallCheckdistance = value; }
    public Transform WallCheck 
    {
        get => GenericNotImplementedError<Transform>.TryToGet(wallCheck, core.transform.parent.name);

        private set => wallCheck = value; }
    public Transform GroundCheck 
    {
        get => GenericNotImplementedError<Transform>.TryToGet(groundCheck, core.transform.parent.name);

        private set => groundCheck = value; }
    public Transform CeilingCheck 
    {
        get => GenericNotImplementedError<Transform>.TryToGet(ceilingCheck, core.transform.parent.name);

        private set => ceilingCheck = value; }
    public Transform LedgeCheckVertical 
    {
        get => GenericNotImplementedError<Transform>.TryToGet(ledgeCheckVertical, core.transform.parent.name);

        private set => ledgeCheckVertical = value; }
    public Transform LedgeCheckHorizontal 
    {
        get => GenericNotImplementedError<Transform>.TryToGet(ledgeCheckHorizontal, core.transform.parent.name);

        private set => ledgeCheckHorizontal = value; }

    #endregion



    [Header("Surrounding checkers references")]
    [Space]
    [Tooltip("An empty GameObject child of this CollisionSenses GameObject \n\n Used by the player detect crouched zones \n\n Better placed if it's on the middle of the character sprite")]
    [SerializeField] private Transform ceilingCheck;

    [Tooltip("An empty GameObject child of this CollisionSenses GameObject \n\n Better placed at the bottom of the character sprite")]
    [SerializeField] private Transform groundCheck;

    [Tooltip("An empty GameObject child of this CollisionSenses GameObject \n\n Used by the player to climb up ledge \n\n Better placed at the top of the character sprite over the eyes")]
    [SerializeField] private Transform ledgeCheckHorizontal;

    [Tooltip("An empty GameObject child of this CollisionSenses GameObject \n\n Better placed at the bottom corner of the enemy sprite to detect if he's walking on a ledge")]
    [SerializeField] private Transform ledgeCheckVertical;

    [Tooltip("An empty GameObject child of this CollisionSenses GameObject \n\n Better placed if it's on the middle of the character sprite")]
    [SerializeField] private Transform wallCheck;

    [Header("Checker's size and layers")]
    [Space]
    [Tooltip("The radius of the detection circle at the player's feet")]
    [SerializeField] private float groundCheckRadius;

    [Tooltip("The distance between the player and a wall. Better keep it really close to the outside of the collider")]
    [SerializeField] private float wallCheckdistance;

    [Tooltip("The Layer Mask that represent anything the player can walk, jump or climb")]
    [SerializeField] private LayerMask groundMask;

    [Tooltip("The Layer Mask that all enemies should have")]
    [SerializeField] private LayerMask enemyMask;

    #endregion




    #region Check Methods

    public bool Ground 
    {
        get => Physics2D.OverlapCircle(GroundCheck.position, groundCheckRadius, groundMask);
    }
    public bool Ceiling
    {
        get => Physics2D.OverlapCircle(CeilingCheck.position, groundCheckRadius, groundMask);
    }
    public bool WallFront
    {
        get => Physics2D.Raycast(WallCheck.position, Vector2.right * core.Movement.FacingDirection, wallCheckdistance, groundMask);
    }
    public bool LedgeHorizontal
    {
        get => Physics2D.Raycast(LedgeCheckHorizontal.position, Vector2.right * core.Movement.FacingDirection, wallCheckdistance, groundMask);
    }
    public bool LedgeVertical
    {
        get => Physics2D.Raycast(LedgeCheckVertical.position, Vector2.down , wallCheckdistance, groundMask);
    }
    public bool WallBack
    {
        get => Physics2D.Raycast(WallCheck.position, Vector2.right * -core.Movement.FacingDirection, wallCheckdistance, groundMask);
    }

    public bool PoundEnemy
    {
        get => Physics2D.OverlapCircle(groundCheck.position, groundCheckRadius, enemyMask);
    }
    #endregion

    

}
