using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public Transform respawnPoint;
    [SerializeField] private GameObject player;
    [SerializeField] private float respawnTime = 3f;
    private float respawnTimeStart;
    private bool respawn;
    //[SerializeField] private CinemachineVirtualCamera cm;
    [SerializeField] private CinemachineVirtualCamera[] cms;
    //[SerializeField] private CinemachineVirtualCamera cm2;


    private void Awake()
    {
        Instance = this;
    }
    public void Respawn()
    {
        respawnTimeStart = Time.time;
        respawn = true;

    }

    private void CheckRespawn()
    {
        if(Time.time >= respawnTimeStart + respawnTime && respawn)
        {
            var playerTemp = Instantiate(player, respawnPoint);
            playerTemp.GetComponent<Player>().DeathEffect.Play();
            foreach (CinemachineVirtualCamera cam in cms)
            {
                cam.m_Follow = playerTemp.transform;
            }
            
            
            respawn = false;
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        //cm = GameObject.Find("PlayerCamera").GetComponent<CinemachineVirtualCamera>();
        //cm2 = GameObject.Find("PlayerCamera2").GetComponent<CinemachineVirtualCamera>();
    }

    // Update is called once per frame
    void Update()
    {
        CheckRespawn();
    }

    public void Quit()
    {
        Application.Quit();
    }
}
