using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spikes : MonoBehaviour
{
    public Vector2 knockbackAngle;
    public float strenght;
    public float damages;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            Core core = collision.GetComponentInChildren<Core>();
            core.Combat.Knockback(knockbackAngle, strenght, core.Movement.FacingDirection);
            core.Stats.DecreaseHealth(damages);
        }
        else
            return;

    }
    
}
