using System.Collections;
using System.Collections.Generic;
using UnityEngine;


/// <summary>
/// Base class for all enemies, it contains all basic variables and functions that they share
/// </summary>
public class Entity : MonoBehaviour
{
    public FiniteStateMachine stateMachine;
    public D_Entity entityData;
    public Animator Animator { get; private set; }
    public AnimationToFSM Atfsm { get; private set; }
    public int LastDamageDirection { get; private set; }
    public Core Core { get; private set; }


    private Vector2 velocityWorspace;
    private float currentHealth;
    private float currentStunResistance;
    private float lastDamageTime;
    [SerializeField] private Transform ledgeCheck;
    [SerializeField] private Transform wallCheck;
    [SerializeField] private Transform playerCheck;
    [SerializeField] private Transform groundCheck;

    protected bool isStunned;
    protected bool isDead;
    public virtual void Awake()
    {
        Core = GetComponentInChildren<Core>();
        currentHealth = entityData.maxHealth;
        currentStunResistance = entityData.stunResistance;

        Animator = GetComponent<Animator>();
        Atfsm = GetComponent<AnimationToFSM>();
        stateMachine = new FiniteStateMachine();
    }

    public virtual void Update()
    {
        Core.LogicUpdate();
        stateMachine.currentState.LogicUpdate();

        if(Time.time >= lastDamageTime + entityData.stunRecoveryTime)
        {
            ResetStunResistance();
        }
        Animator.SetFloat("yVelocity", Core.Movement.Rb.velocity.y);
    }

    public virtual void FixedUpdate()
    {
        stateMachine.currentState.PhysicsUpdate();
    }


    public virtual bool CheckPlayerInMinimumRange()
    {
        return Physics2D.Raycast(playerCheck.position, transform.right, entityData.minAgroDistance, entityData.playerMask) /*|| Physics2D.Raycast(playerCheck.position, -transform.right, entityData.minAgroDistance, entityData.playerMask)*/;
    }

    public virtual bool CheckPlayerInMaximumRange()
    {
        return Physics2D.Raycast(playerCheck.position, transform.right, entityData.maxAgroDistance, entityData.playerMask);
    }

    public virtual bool CheckPlayerInCloseRangeAction()
    {
        return Physics2D.Raycast(playerCheck.position, transform.right, entityData.closeRangeActionDistance, entityData.playerMask);
    }
   
    public virtual bool CheckForPlayerToEmbush()
    {
        return Physics2D.OverlapCircle(transform.position, entityData.embushAgroRadius, entityData.playerMask);
    }
    public virtual void ResetStunResistance()
    {
        isStunned = false;
        currentStunResistance = entityData.stunResistance;
    }

    

    //public virtual void Damage(AttackDetails attackDetails)
    //{
    //    lastDamageTime = Time.time;
    //    currentHealth -= attackDetails.damageAmount;
    //    currentStunResistance -= attackDetails.stunDamageAmount;
    //    DamageHop(entityData.damageHopSpeed);
    //    Instantiate(entityData.hitParticles, transform.position, Quaternion.Euler(0, 0, Random.Range(0f, 360f)));
    //    if (attackDetails.position.x > transform.position.x)
    //        LastDamageDirection = -1;
    //    else
    //        LastDamageDirection = 1;
    //    if (currentStunResistance <= 0)
    //    {
    //        isStunned = true;
    //    }
    //    if(currentHealth <= 0)
    //    {
    //        isDead = true;
    //    }

    //}
    public virtual void DamageHop(float velocity)
    {
        velocityWorspace.Set(Core.Movement.Rb.velocity.x, velocity);
        Core.Movement.Rb.velocity = velocityWorspace;

    }

    //public virtual void Flip()
    //{
    //    FacingDirection *= -1;
    //    transform.Rotate(0, 180, 0);
    //}
    public virtual void OnDrawGizmos()
    {
        if (Core)
        {
            Gizmos.color = Color.green;

            //LedgeCheck visual
            Gizmos.DrawLine(ledgeCheck.position, new Vector2(ledgeCheck.position.x, ledgeCheck.position.y - entityData.ledgeCheckDistance));
            //WallCheck Visual
            Gizmos.DrawLine(wallCheck.position, new Vector2(wallCheck.position.x + entityData.wallCheckDistance * Core.Movement.FacingDirection, wallCheck.position.y));

            Gizmos.color = Color.magenta;
            Gizmos.DrawWireSphere(playerCheck.position + (Vector3)(Vector2.right * Core.Movement.FacingDirection * entityData.closeRangeActionDistance), .2f);


            Gizmos.color = Color.blue;
            Gizmos.DrawWireSphere(playerCheck.position + (Vector3)(Vector2.right * Core.Movement.FacingDirection * entityData.minAgroDistance), .2f);
            Gizmos.color = Color.yellow;
            Gizmos.DrawWireSphere(playerCheck.position + (Vector3)(Vector2.right * Core.Movement.FacingDirection * entityData.maxAgroDistance), .2f);
        }
    
    }
}
