using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Keep track of witch state the enemy is currently in, and run the correct code from that state
/// </summary>
public class FiniteStateMachine
{
    public State currentState { get; private set; } // Means that this variable can publicly being seens and used as it's, but can only be changed withing this class

    /// <summary>
    /// It's purpose is to set the first state (startingState) and make sure everything is rolling
    /// </summary>
    /// <param name="startingState"></param>
    public void Initialize(State startingState) 
    { 
        currentState = startingState;
        currentState.Enter();
    }

    public void ChangeState(State newState)
    {
        currentState.Exit();
        currentState = newState;
        currentState.Enter();
    }
}
