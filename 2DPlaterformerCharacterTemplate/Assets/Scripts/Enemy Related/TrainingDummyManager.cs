
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrainingDummyManager : MonoBehaviour
{
    //[SerializeField] private float maxHealth = 50f;
    //[SerializeField] private float knockbackSpeedX;
    //[SerializeField] private float knockbackSpeedY;
    //[SerializeField] private float knockbackDuration;
    //[SerializeField] private float knockbackDeathSpeedX;
    //[SerializeField] private float knockbackDeathSpeedY;
    //[SerializeField] private float knockbackTorque;
    //private float currentHealth, knockbackStart;
    //[SerializeField] private bool applyKnockback;
    //private PlayerController pc;
    //private int playerFacingDirection;
    //private bool playerOnLeft;
    //private bool knockback;
    //private int playerOnLeftParam;
    //[SerializeField] private GameObject aliveGO, brokenTopGO, brokenBottomGo;
    //[SerializeField] private GameObject hitParticules;
    //[SerializeField] private Rigidbody2D aliveRb, brokenTopRb, brokenBottomRb;
    //private Animator animator;

    //// Start is called before the first frame update
    //void Start()
    //{
    //    currentHealth = maxHealth;

    //    pc = GameObject.Find("Player").GetComponent<PlayerController>();
    //    animator = aliveGO.GetComponent<Animator>();
    //    aliveGO.SetActive(true);
    //    brokenTopGO.SetActive(false);
    //    brokenBottomGo.SetActive(false);

    //    playerOnLeftParam = Animator.StringToHash("PlayerOnLeft");
    //}

    //// Update is called once per frame
    //void Update()
    //{
    //    CheckKnockback();
    //}

    //private void Damage(AttackDetails damagesAmount)
    //{
    //    currentHealth -= damagesAmount.damageAmount;



    //    playerFacingDirection = pc.GetFacingDirection();
    //    Instantiate(hitParticules, animator.transform.position, Quaternion.Euler(0, 0, Random.Range(0, 360)));
    //    if (playerFacingDirection == 1)
    //        playerOnLeft = true;
    //    else
    //        playerOnLeft = false;

    //    animator.SetBool(playerOnLeftParam, playerOnLeft);
    //    animator.SetTrigger("Damage");

    //    if(applyKnockback && currentHealth >= 0.0f)
    //    {
    //        Knockback();
    //    }

    //    if(currentHealth <= 0.0f)
    //    {
    //        Die();
    //    }
    //}

    //private void Knockback()
    //{
    //    knockback = true;
    //    knockbackStart = Time.time;
    //    aliveRb.velocity = new Vector2(knockbackSpeedX * playerFacingDirection, knockbackSpeedY);
    //}

    //private void CheckKnockback()
    //{
    //    if(Time.time>= knockbackStart+knockbackDuration && knockback)
    //    {
    //        knockback = false;
    //        aliveRb.velocity = new Vector2(0.0f, aliveRb.velocity.y);
    //    }
    //}
    //private void Die()
    //{
    //    aliveGO.SetActive(false);
    //    brokenTopGO.SetActive(true);
    //    brokenBottomGo.SetActive(true);

    //    brokenTopGO.transform.position = aliveGO.transform.position;
    //    brokenBottomGo.transform.position = aliveGO.transform.position;

    //    brokenBottomRb.velocity = new Vector2(knockbackSpeedX * playerFacingDirection, knockbackSpeedY);
    //    brokenTopRb.velocity = new Vector2(knockbackDeathSpeedX * playerFacingDirection, knockbackDeathSpeedY);
    //    brokenTopRb.AddTorque(knockbackTorque * -playerFacingDirection, ForceMode2D.Impulse);
    //}

}
