using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new D_RegenStateData", menuName = "Data/State Data/Regen State Data")]
public class D_RegenState : ScriptableObject
{
    public float regenTimer = 5f;
    public int RegenCount = 1;
}
