using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new E_LookForPlayerStateData", menuName = "Data/State Data/Look For Player State Data")]

public class D_LookForPlayerState : ScriptableObject
{
    public int amountOfTurn = 2;
    public float timeBetweenTurn = .75f;
}
