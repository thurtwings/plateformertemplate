using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new E_RangedAttackStateData", menuName = "Data/State Data/Ranged Attack State Data")]

public class D_RangedAttackState : ScriptableObject
{
    public GameObject projectilePrefab;
    public float projectileDamage = 20f;
    public float projectileSpeed = 10f;
    public float projectileTravelDistance = 10f;
}
