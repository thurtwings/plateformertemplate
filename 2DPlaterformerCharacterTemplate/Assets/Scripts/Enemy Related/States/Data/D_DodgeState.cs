using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "new E_DodgeStateData", menuName = "Data/State Data/Dodge State Data")]

public class D_DodgeState : ScriptableObject
{
    public float dodgeSpeed = 10f;
    public float dodgeTime = .2f;
    public float dodgeCooldown = 2f;
    public Vector2 dodgeAngle;
}
