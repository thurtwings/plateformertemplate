using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new E_MoveStateData", menuName = "Data/State Data/Move State Data")]
public class D_MoveState : ScriptableObject
{
    public float _movementSpeed = 2.5f;

}
