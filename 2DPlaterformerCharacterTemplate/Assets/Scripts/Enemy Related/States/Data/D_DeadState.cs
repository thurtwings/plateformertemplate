using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new E_DeadStateData", menuName = "Data/State Data/Dead State Data")]

public class D_DeadState : ScriptableObject
{
    public GameObject deathChunkParticles;
    public GameObject deathBloodParticles;
}
