using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new E_MeleeAttackStateData", menuName = "Data/State Data/Melee Attack State Data")]
public class D_MeleeAttackState : ScriptableObject
{
    public float attackRadius = .8f;
    public float attackDamages = 10f;
    public Vector2 knockbackAngle = Vector2.one;
    public float knockbackStrength = 10f;
    public LayerMask playerMask;
}
