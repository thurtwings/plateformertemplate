using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "new E_PlayerDetectedStateData", menuName = "Data/State Data/Player Detected State Data")]
public class D_PlayerDetected : ScriptableObject
{
    public float longRangeActionTime = 1.5f;
    
}
