using System.Collections;
using System.Collections.Generic;
using UnityEngine;



[CreateAssetMenu(fileName = "new E_EntityData", menuName = "Data/Entity Data/Base Data")]
public class D_Entity : ScriptableObject
{
    [Header("Health related")]
    public float maxHealth = 50f;
    public float recoverySpeed = 5f;
    [Space]
    [Header("Damages received related")]
    public float damageHopSpeed = 3f;
    public GameObject hitParticles;
    [Space]
    
  
    [Header("Ground and wall detection lenght")]
    public float wallCheckDistance = .2f;
    public float ledgeCheckDistance = .4f;
    public float groundCheckRadius = .3f;
    [Space]
    [Header("Player Detection aggro range")]
    public float minAgroDistance = 3f;
    public float maxAgroDistance = 4f;
    public float embushAgroRadius = 5f;
    [Space]
    [Header("Player Detection Action distance")]
    public float closeRangeActionDistance = 1f;
    [Space]
    [Header("Stun resistance")]
    public float stunResistance = 3f;
    public float stunRecoveryTime = 2f;
    [Space]
    [Header("Specific layer masks ")]
    public LayerMask groundMask;
    public LayerMask playerMask;
    
}
