using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "new E_IdleStateData", menuName = "Data/State Data/Idle State Data")]
public class D_IdleState : ScriptableObject
{
    public float minIdleTime = 0, maxIdleTime = 3;

    
}
