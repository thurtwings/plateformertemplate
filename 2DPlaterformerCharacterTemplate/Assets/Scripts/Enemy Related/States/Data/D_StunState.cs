using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "new E_StunStateData", menuName = "Data/State Data/Stun State Data")]
public class D_StunState : ScriptableObject
{
    public float stunDuration = 3f;
    public float stunKnockbackTime = .2f;
    public float stunKnockbackSpeed = 20f;

    public Vector2 stunKnockbackAngle;
    
}
