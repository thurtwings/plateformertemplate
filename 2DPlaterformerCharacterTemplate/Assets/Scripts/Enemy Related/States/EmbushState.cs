using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmbushState : State
{
    protected D_EmbushState stateData;
    protected bool playerInEmbushRange;
    public EmbushState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, D_EmbushState stateData) : base(entity, stateMachine, animBoolName)
    {
        this.stateData = stateData;
    }

    public override void Enter()
    {
        base.Enter();
        core.Movement.SetVelocityZero();
    }

    public override void Exit()
    {
        base.Exit();
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        core.Movement.SetVelocityZero();
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }

    public override void TickChecks()
    {
        base.TickChecks();
        playerInEmbushRange = entity.CheckForPlayerToEmbush();
    }
}
