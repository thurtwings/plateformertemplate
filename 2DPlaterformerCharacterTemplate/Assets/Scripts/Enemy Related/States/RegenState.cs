using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegenState : State
{
    internal bool regenTimeIsOver;
    D_RegenState stateData;
    Material mat;
    Enemy3 enemy;
    
    public RegenState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, D_RegenState stateData) : base(entity, stateMachine, animBoolName)
    {
        this.stateData = stateData;
    }

    public override void Enter()
    {
        base.Enter();
        enemy = entity.GetComponentInParent<Enemy3>();
        enemy.regenParticles.Play();
        mat = entity.GetComponentInParent<Renderer>().material;
        mat.EnableKeyword("SHAKEUV_ON");
        core.Movement.SetVelocityZero();
        
    }

    public override void Exit()
    {
        base.Exit();
        mat.DisableKeyword("SHAKEUV_ON");
        

    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        core.Movement.SetVelocityZero();
        if(enemy.regenCount > 0)    
        {
            if (Time.time >= startTime + stateData.regenTimer)
            {
                enemy.regenCount--;
                regenTimeIsOver = true;
            }
        }

        
        
    }

    public override void PhysicsUpdate()
    {
        base.PhysicsUpdate();
    }

    public override void TickChecks()
    {
        base.TickChecks();
    }
}
