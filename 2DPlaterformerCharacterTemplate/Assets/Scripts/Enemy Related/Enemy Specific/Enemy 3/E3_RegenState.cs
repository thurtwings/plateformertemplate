using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E3_RegenState : RegenState
{
    private Enemy3 enemy;
    public E3_RegenState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, D_RegenState stateData, Enemy3 enemy) : base(entity, stateMachine, animBoolName, stateData)
    {
        this.enemy = enemy;
    }

    public override void Enter()
    {
        base.Enter();
        
    }

    public override void Exit()
    {
        base.Exit();

    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if (regenTimeIsOver)
        {
            
            stateMachine.ChangeState(enemy.lookForPlayerState);
        }
    }
}
