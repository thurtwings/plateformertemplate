using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy3 : Entity
{
    public E3_IdleState idleState { get; private set; }
    public E3_MoveState moveState { get; private set; }
    public E3_PlayerDetectedState playerDetectedState { get; private set; }
    public E3_ChargeState chargeState { get; private set; }
    public E3_LookForPlayerState lookForPlayerState { get; private set; }
    public E3_MeleeAttackState meleeAttackState { get; private set; }
    public E3_EmbushState embushState { get; private set; }
    public E3_DeadState deadState { get; private set; }
    public E3_RegenState regenState { get; private set; }

    [Header("Scriptable objects for States")]
    [SerializeField] private D_IdleState idleStateData;
    [SerializeField] private D_MoveState moveStateData;
    [SerializeField] private D_PlayerDetected playerDetectedStateData;
    [SerializeField] private D_ChargeState chargeStateData;
    [SerializeField] private D_LookForPlayerState lookForPlayerStateData;
    [SerializeField] private D_MeleeAttackState meleeAttackStateData;
    [SerializeField] private D_EmbushState embushStateData;
    [SerializeField] private D_DeadState deadStateData;
    [SerializeField] private D_RegenState regenStateData;
    [SerializeField] private Transform melleeAttackPosition;
    public ParticleSystem regenParticles;
    public int regenCount = 1;
    public GameObject hb;
    public override void Awake()
    {
        base.Awake();
        hb.SetActive(false);
        moveState = new E3_MoveState(this, stateMachine, "move", moveStateData, this);
        idleState = new E3_IdleState(this, stateMachine, "idle", idleStateData, this);
        playerDetectedState = new E3_PlayerDetectedState(this, stateMachine, "playerDetected", playerDetectedStateData, this);
        chargeState = new E3_ChargeState(this, stateMachine, "charge", chargeStateData, this);
        lookForPlayerState = new E3_LookForPlayerState(this, stateMachine, "lookForPlayer", lookForPlayerStateData, this);
        meleeAttackState = new E3_MeleeAttackState(this, stateMachine, "meleeAttack", melleeAttackPosition, meleeAttackStateData, this);
        embushState = new E3_EmbushState(this, stateMachine, "embush", embushStateData, this);
        deadState = new E3_DeadState(this, stateMachine, "dead", deadStateData, this);
        regenState = new E3_RegenState(this, stateMachine, "regen", regenStateData, this);
    }

    private void Start()
    {
        stateMachine.Initialize(embushState);

    }

    public override void OnDrawGizmos()
    {
        base.OnDrawGizmos();
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(melleeAttackPosition.position, meleeAttackStateData.attackRadius);
        Gizmos.color = Color.blue;
        Gizmos.DrawWireSphere(transform.position, entityData.embushAgroRadius);


    }
}
