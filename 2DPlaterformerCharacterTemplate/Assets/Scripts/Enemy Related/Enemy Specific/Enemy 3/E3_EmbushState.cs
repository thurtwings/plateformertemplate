using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class E3_EmbushState : EmbushState
{
    Enemy3 enemy;

    public E3_EmbushState(Entity entity, FiniteStateMachine stateMachine, string animBoolName, D_EmbushState stateData, Enemy3 enemy) : base(entity, stateMachine, animBoolName, stateData)
    {
        this.enemy = enemy;
    }

    public override void Exit()
    {
        base.Exit();
        enemy.Animator.SetBool("embush", false);
        enemy.hb.SetActive(true);
    }

    public override void LogicUpdate()
    {
        base.LogicUpdate();
        if (playerInEmbushRange)
        {
            stateMachine.ChangeState(enemy.moveState);
        }
        //else if (core.Stats.currentHealth >= core.Stats.maxHealth / 2 && enemy.regenCount > 0)
        //{
        //    stateMachine.ChangeState(enemy.regenState);
        //}
    }
}
