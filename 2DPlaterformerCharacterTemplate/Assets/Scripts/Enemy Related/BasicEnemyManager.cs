using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicEnemyManager : MonoBehaviour
{
    private enum State { Moving, Knockback, Dead}
    private State currentState;
    [Header(">>> Components")]
    [SerializeField] private GameObject alive;
    [SerializeField] private GameObject hitParticles, deathChunkParticles, deathBloodParticles;
    [SerializeField] private Rigidbody2D rbAlive;
    [SerializeField] private Animator animator;
    [SerializeField] private Transform groundCheckTransform;
    [SerializeField] private Transform wallCheckTransform;
    [SerializeField] private Transform touchDamagesCheckTransform;

    [Header(">>> Detection Variables")]
    [Range(.1f, 1f)]
    [SerializeField] private float groundCheckLenght;
    [Range(.1f, 1f)]
    [SerializeField] private float wallCheckLenght;
    [SerializeField] private LayerMask groundMask;
    private bool groundDetected;
    private bool wallDetected;

    [Header(">>> Movement Variables")]
    [SerializeField] private float moveSpeed;
    private int facingDirection;
    private Vector2 movement;

    [Header(">>> Health Variable")]
    [SerializeField] private float maxHealth;
    private float currentHealth;
    private int damageDirection;

    [Header(">>> Knockback Variables")]
    [SerializeField] private float knockbackSpeedX;
    [SerializeField] private float knockbackSpeedY;
    [SerializeField] private float knockbackDuration;
    private float knockbackStart;

    [Header(">>> Damages inflicted Variables")]
    [SerializeField] private float TouchDamageCooldown;
    [SerializeField] private float TouchDamage;
    [SerializeField] private float TouchDamageWidth;
    [SerializeField] private float TouchDamageHeight;
    [SerializeField] private LayerMask playerMask;
    private Vector2 touchDamageBottomLeft;
    private Vector2 touchDamageTopRight;
    private float lastTouchDamageTime;
    private float[] attackDetails = new float[2];
                                        
    [Header(">>> Animation parameters")]
    private int knockbackParam;
    private void Start()
    {
        facingDirection = 1;
        knockbackParam = Animator.StringToHash("knockback");
        currentHealth = maxHealth;
    }
    private void Update()
    {
        switch (currentState)
        {
            case State.Moving:
                UpdateMovingState();
                break;
            case State.Knockback:
                UpdateKnockbackState();
                break;
            case State.Dead:
                UpdateDeadState();
                break;
            
        }
    }

    #region Moving State
    //---------------------------------- Walking State --------------------------------------

    private void EnterMovingState()
    {

    }

    private void UpdateMovingState()
    {
        groundDetected = Physics2D.Raycast(groundCheckTransform.position, Vector2.down, groundCheckLenght, groundMask);
        wallDetected = Physics2D.Raycast(wallCheckTransform.position, transform.right, wallCheckLenght, groundMask);

        CheckTouchDamages();

        if(!groundDetected || wallDetected)
        {
            Flip();
        }
        else
        {
            movement.Set(moveSpeed * facingDirection, rbAlive.velocity.y);
            rbAlive.velocity = movement;
        }
    }


    private void ExitMovingState()
    {

    }
    #endregion
    #region Knockback State
    //---------------------------------- Knockback State --------------------------------------
    private void EnterKnockbackState()
    {
        knockbackStart = Time.time;
        movement.Set(knockbackSpeedX * damageDirection, knockbackSpeedY);
        rbAlive.velocity = movement;
        animator.SetBool(knockbackParam, true);
    }

    private void UpdateKnockbackState()
    {
        if(Time.time >= knockbackStart + knockbackDuration)
        {
            SwitchState(State.Moving);
        }
    }

    private void ExitKnockbackState()
    {
        animator.SetBool(knockbackParam, false);
    }

    #endregion
    #region Dead State
    //---------------------------------- Dead State --------------------------------------

    private void EnterDeadState()
    {
        Instantiate(deathChunkParticles, alive.transform.position, deathChunkParticles.transform.rotation);
        Instantiate(deathBloodParticles, alive.transform.position, deathBloodParticles.transform.rotation);
        Destroy(gameObject);
    }

    private void UpdateDeadState()
    {

    }

    private void ExitDeadState()
    {

    }

    #endregion

    //---------------------------------- Other functions --------------------------------------

    private void SwitchState(State state)
    {
        switch (currentState)   
        {
            case State.Moving:
                ExitMovingState();
                break;
            case State.Knockback:
                ExitKnockbackState();
                break;
            case State.Dead:
                ExitDeadState();
                break;
            
        }
        switch (state)
        {
            case State.Moving:
                EnterMovingState();
                break;
            case State.Knockback:
                EnterKnockbackState();
                break;
            case State.Dead:
                EnterDeadState();
                break;

        }

        currentState = state;
    }
    private void Flip()
    {
        facingDirection *= -1;
        alive.transform.Rotate(0, 180f, 0);
    }

    private void Damage(float[] attackDetails)
    {
        currentHealth -= attackDetails[0];
        Instantiate(hitParticles, alive.transform.position, Quaternion.Euler(0, 0, Random.Range(0, 360)));

        if(attackDetails[1] > alive.transform.position.x)
        {
            damageDirection = -1;
        }
        else { damageDirection = 1; }

        //hit particules
        if (currentHealth > 0.0f)
            SwitchState(State.Knockback);
        else if (currentHealth <= 0.0f)
            SwitchState(State.Dead);
    }

    private void CheckTouchDamages()
    {
        if(Time.time >= lastTouchDamageTime + TouchDamageCooldown)
        {
            touchDamageBottomLeft.Set(touchDamagesCheckTransform.position.x - (TouchDamageWidth / 2), touchDamagesCheckTransform.position.y - (TouchDamageHeight / 2));
            touchDamageTopRight.Set(touchDamagesCheckTransform.position.x + (TouchDamageWidth / 2), touchDamagesCheckTransform.position.y + (TouchDamageHeight / 2));

            Collider2D hit = Physics2D.OverlapArea(touchDamageBottomLeft, touchDamageTopRight, playerMask);

            if(hit != null)
            {
                lastTouchDamageTime = Time.time;
                attackDetails[0] = TouchDamage;
                attackDetails[1] = alive.transform.position.x;
                hit.SendMessage("Damage", attackDetails);
            }
        }
    }










    private void OnDrawGizmos()
    {
        Gizmos.color = Color.green;

        //GroundCheck visual
        Gizmos.DrawLine(groundCheckTransform.position, new Vector2(groundCheckTransform.position.x, groundCheckTransform.position.y - groundCheckLenght));
        //WallCheck Visual
        Gizmos.DrawLine(wallCheckTransform.position, new Vector2(wallCheckTransform.position.x + wallCheckLenght, wallCheckTransform.position.y));

        //touch box visual
        Vector2 botLeft = new Vector2(touchDamagesCheckTransform.position.x - (TouchDamageWidth / 2), touchDamagesCheckTransform.position.y - (TouchDamageHeight / 2));
        Vector2 botRight = new Vector2(touchDamagesCheckTransform.position.x + (TouchDamageWidth / 2), touchDamagesCheckTransform.position.y - (TouchDamageHeight / 2));
        Vector2 topRight = new Vector2(touchDamagesCheckTransform.position.x + (TouchDamageWidth / 2), touchDamagesCheckTransform.position.y + (TouchDamageHeight / 2));
        Vector2 topLeft = new Vector2(touchDamagesCheckTransform.position.x - (TouchDamageWidth / 2), touchDamagesCheckTransform.position.y + (TouchDamageHeight / 2)); 

        Gizmos.color = Color.red;
        Gizmos.DrawLine(botLeft, botRight);
        Gizmos.DrawLine(botRight, topRight);
        Gizmos.DrawLine(topRight, topLeft);
        Gizmos.DrawLine(topLeft, botLeft);



    }
}
