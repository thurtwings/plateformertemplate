using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatTestDummy : MonoBehaviour, IDamageable
{
    private Animator animator;
    [SerializeField] private GameObject hitParticles;

    // Start is called before the first frame update
    void Awake()
    {
        animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Damage(float amount)
    {
        //Debug.Log(amount + " dégats pris");
        GameObject hitParticle = Instantiate(hitParticles, transform.position, Quaternion.Euler(0, 0, Random.Range(0.0f, 360f)));
        animator.SetTrigger("Damage");
        Destroy(hitParticle, .1f);
    }
}
