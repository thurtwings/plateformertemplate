using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticulesManager : MonoBehaviour
{
    
    private ParticleSystem ps;

    private void Start()
    {
        ps = GetComponent<ParticleSystem>();
    }
    private void OnParticleCollision(GameObject other)
    {
        //if (other.CompareTag("Player"))
        //{
            
        //    Core core = other.GetComponentInChildren<Core>();
        //    core.Stats.DecreaseHealth(1);
            
        //}

        if (other.CompareTag("Enemy"))
        {
            Core core = other.GetComponentInChildren<Core>();
            core.Stats.IncreaseHealth(10);
        }
    }
}
