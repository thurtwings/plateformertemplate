using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour
{

    [SerializeField] WeaponAttackDetails attackDetails;
    private float speed;
    private float travelDistance;
    private float xStartPosition;
    private Rigidbody2D rb;
    private bool isGravityOn;
    [SerializeField] private float gravity = 8f;
    [SerializeField] private float damageRadius = .8f;
    [SerializeField] private LayerMask groundMask;
    [SerializeField] private LayerMask playerMask;
    [SerializeField] private Transform damagePosition;
    private bool hasHitGround;

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        rb.gravityScale = 0.0f;
        rb.velocity = transform.right * speed;
        xStartPosition = transform.position.x;
        isGravityOn = false;
    }

    private void Update()
    {
        if (!hasHitGround)
        {
            //attackDetails.position = transform.position;
            if (isGravityOn)
            {
                float angle = Mathf.Atan2(rb.velocity.y, rb.velocity.x) * Mathf.Rad2Deg;
                transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
            }
        }
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if (!hasHitGround)
        {
            Collider2D damageHit = Physics2D.OverlapCircle(damagePosition.position, damageRadius, playerMask);
            Collider2D groundHit = Physics2D.OverlapCircle(damagePosition.position, damageRadius, groundMask);
            if (damageHit)
            {
                Core core = damageHit.GetComponentInChildren<Core>();
                core.Stats.DecreaseHealth(attackDetails.damageAmount);
                Destroy(gameObject);

            }
            if (groundHit)
            {
                hasHitGround = true;
                rb.gravityScale = 0.0f;
                rb.velocity = Vector2.zero;
                Destroy(this.gameObject,5f);
            }
            if (Mathf.Abs(xStartPosition - transform.position.x) >= travelDistance && !isGravityOn)
            {
                isGravityOn = true;
                rb.gravityScale = gravity;

            }
        }
        
    }

    public void FireProjectile(float speed, float travelDistance, float damage)
    {
        this.speed = speed;
        this.travelDistance = travelDistance;
        //this.attackDetails.damageAmount = damage;
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(damagePosition.position, damageRadius);
    }
}
