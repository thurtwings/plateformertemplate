using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
public class ConfinerSwitcher : MonoBehaviour
{
    public GameObject cam;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player") && !collision.isTrigger)
        {
            cam.SetActive(true);
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.CompareTag("Player") && !collision.isTrigger)
        {
            Debug.Log("player sort de mon champ de vision " + cam.name);
            cam.SetActive(false);
        }
    }
}
