using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHealthBar : MonoBehaviour
{
    Image hb;
    Entity enemy;
    Core core;
    // Start is called before the first frame update
    void Start()
    {
        enemy = GetComponentInParent<Entity>();
        core = enemy.GetComponentInChildren<Core>();
        hb = GetComponent<Image>();
        UpdateHealthBar();
    }

   public void UpdateHealthBar()
    {
        hb.fillAmount = core.Stats.currentHealth / core.Stats.maxHealth;
    }
    private void Update()
    {
        UpdateHealthBar();
    }
}
